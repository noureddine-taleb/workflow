import { HttpInterceptor, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { LoaderService } from "./layout/services/loader.service";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
})
export class AppComponent {
	/**
	 * Constructor
	 */
	constructor(
		public loaderService: LoaderService,
		private cdRef: ChangeDetectorRef,
	) {}

	ngOnInit(): void {
	}

	ngAfterViewChecked() {
		this.cdRef.detectChanges();
	}
}
