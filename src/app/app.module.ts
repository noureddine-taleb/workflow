import { NgModule } from "@angular/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ExtraOptions, PreloadAllModules, RouterModule } from "@angular/router";
import { AppComponent } from "app/app.component";
import { appRoutes } from "app/app.routing";
import { LayoutModule } from "app/layout/layout.module";
import { CurrencyMaskInputMode, NgxCurrencyModule } from "ngx-currency";
import { CoreModule } from "./core/core.module";

const routerConfig: ExtraOptions = {
	scrollPositionRestoration: "enabled",
	preloadingStrategy: PreloadAllModules,
};

const customCurrencyMaskConfig = {
	align: "right",
	allowNegative: false,
	prefix: "",
	thousands: ".",
	decimal: ",",
	suffix: "",
	inputMode: CurrencyMaskInputMode.NATURAL,
	allowZero: true,
	precision: 2,
	nullable: false,
	min: null,
	max: null,
};

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule.forRoot(appRoutes, routerConfig),

		// core
		CoreModule,

		// Layout module
		LayoutModule,

		// 3rd party modules that require global configuration via forRoot
		NgxCurrencyModule.forRoot(customCurrencyMaskConfig),

		// services depandencies
		MatSnackBarModule,
		MatDialogModule,
	],
	providers: [
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
