import { Route } from "@angular/router";
import { InitialDataResolver } from "app/app.resolvers";
import { LayoutComponent } from "app/layout/layout.component";
import { AuthGuard } from "./modules/auth/guards/auth.guard";
import { NoAuthGuard } from "./modules/auth/guards/noAuth.guard";

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [
	// Redirect empty path to '/example'
	{ path: "", pathMatch: "full", redirectTo: "welcome" },
	// Redirect signed in user to the '/example'
	//
	// After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
	// path. Below is another redirection for that path to redirect the user to the desired
	// location. This is a small convenience to keep all main routes together here on this file.
	{ path: "signed-in-redirect", pathMatch: "full", redirectTo: "demande/taches" },

	{
		path: "",
		canActivate: [NoAuthGuard],
		component: LayoutComponent,
		data: {
			layout: "empty",
		},
		loadChildren: () =>
			import("app/modules/auth/auth.module").then(
				(m) => m.AuthModule,
			),
	},
	
	{
		path: "",
		canActivate: [AuthGuard],
		canActivateChild: [AuthGuard],
		component: LayoutComponent,
		data: {
			layout: "main",
		},
		resolve: {
			initialData: InitialDataResolver,
		},
		children: [
			{
				path: "welcome",
				loadChildren: () =>
					import("app/modules/welcome/welcome.module").then(
						(m) => m.WelcomeModule,
					),
			},
			{
				path: "affectation",
				loadChildren: () =>
					import("app/modules/affectation/affectation.module").then(
						(m) => m.AffectationModule,
					),
			},
			{
				path: "comite",
				loadChildren: () =>
					import("app/modules/comite/comite.module").then((m) => m.ComiteModule),
			},

			{
				path: "simulation",
				loadChildren: () =>
					import("app/modules/simulation/simulations.module").then(
						(m) => m.SimulationsModule,
					),
			},
			{
				path: "demande",
				loadChildren: () =>
					import("app/modules/demandes/demande.module").then((m) => m.DemandeModule),
			},

			{
				path: "sort-demande-cc",
				loadChildren: () =>
					import("app/modules/sort-demande-cc/sort-demande-cc.module").then(
						(m) => m.SortDemandeCCModule,
					),
			},
		]
	},	
];
