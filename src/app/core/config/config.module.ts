import { APP_INITIALIZER, ModuleWithProviders, NgModule, Optional, SkipSelf } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { LayoutConfigService } from "./layout-config.service";
import { APP_LAYOUT_CONFIG } from "./layout.config";

export function initializeApp(configService: ConfigService) {
	return () => configService.load();
}

@NgModule()
export class ConfigModule {
	constructor(@SkipSelf() @Optional() existingConfigModule?: ConfigModule) {
		if (existingConfigModule) {
			throw new Error("ConfigModule should be loaded only once (in AppModule)!");
		}
	}

	static forRoot(config: any): ModuleWithProviders<ConfigModule> {
		return {
			ngModule: ConfigModule,
			providers: [
				ConfigService,
				{ provide: APP_INITIALIZER, useFactory: initializeApp, deps: [ConfigService], multi: true },		
				{
					provide: APP_LAYOUT_CONFIG,
					useValue: config,
				},
				LayoutConfigService,
			]
		}
	}
}
