import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

export interface AppConfig {
	apiServer: {
		restGTUrl: string;
		restWKFUrl: string;
		restREFUrl: string;
		restReportUrl: string;
	};
}

// you can't use this, without importing config-loader-module (preferably) in app.module
@Injectable()
export class ConfigService {
	static settings: AppConfig;
	constructor() {}
	// tslint:disable-next-line:typedef
	load() {
		const jsonFile = `assets/config/config.${environment.name}.json`;
		return new Promise((resolve, reject) => {
			const xhr = new XMLHttpRequest();
			xhr.open("GET", jsonFile);

			xhr.addEventListener("readystatechange", () => {
				if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
					ConfigService.settings = JSON.parse(xhr.responseText);
					resolve(ConfigService.settings);
				} else if (xhr.readyState === XMLHttpRequest.DONE) {
					reject(`Could not load file '${jsonFile}`);
				}
			});

			xhr.send(null);
		});
	}
}