import { InjectionToken } from "@angular/core";
import { AppLayoutConfig } from "./layout.config.types";

export const APP_LAYOUT_CONFIG = new InjectionToken<any>("APP_LAYOUT_CONFIG");

export const appConfig: AppLayoutConfig = {
	layout: "main",
	scheme: "light",
	theme: "default",
};
