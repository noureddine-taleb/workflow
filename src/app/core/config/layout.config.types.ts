import { Layout } from "app/layout/layout.types";

export type Scheme = "auto" | "dark" | "light";

export type Theme = "default" | string;

export interface AppLayoutConfig {
	layout: Layout;
	scheme: Scheme;
	theme: Theme;
}