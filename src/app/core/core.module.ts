import { CommonModule } from '@angular/common';
import { LOCALE_ID, NgModule, Optional, SkipSelf } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { mockApiServices } from 'app/mock-api';
import { ConfigModule } from './config/config.module';
import { appConfig } from './config/layout.config';
import { IconsModule } from './icons/icons.module';
import { InterceptorsModule } from './interceptors/interceptors.module';
import { MockApiModule } from './lib/mock-api';

const CUSTOM_DATE_FORMAT = {
	parse: {
		dateInput: "DD/MM/YYYY",
	},
	display: {
		dateInput: "DD/MM/YYYY",
		monthYearLabel: "MMM YYYY",
		dateA11yLabel: "LL",
		monthYearA11yLabel: "MMMM YYYY",
	},
}

function getMatPaginatorIntl() {
	const int = new MatPaginatorIntl();
	int.itemsPerPageLabel = "element par page";
	int.nextPageLabel = "page prochaine";
	int.previousPageLabel = "page précédente";
	int.lastPageLabel = "dernière page";
	int.firstPageLabel = "premiere page";
	int.getRangeLabel = (page: number, pageSize: number, length: number) => {
		return `page ${page + Number(!!length)} sur ${Math.ceil(length / pageSize)} | ${length} element total`;
	}

	return int;
}

@NgModule({
  imports: [
    CommonModule,
    ConfigModule.forRoot(appConfig),
    InterceptorsModule,
    MockApiModule.forRoot(mockApiServices),
    IconsModule,
  ],
  providers: [
	{ provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
	{
		provide: MAT_DATE_FORMATS,
		useValue: CUSTOM_DATE_FORMAT,
	},
	{
		provide: LOCALE_ID,
		useValue: "fr-FR"
	},
	{
		provide: MatPaginatorIntl,
		useValue: getMatPaginatorIntl()
	}
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentCoreModule?: CoreModule) {
    if (parentCoreModule) {
      throw new Error("CoreModule already loaded!");
    }
  }
}
