import { NgModule } from "@angular/core";
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@NgModule()
export class IconsModule {
	/**
	 * Constructor
	 */
	constructor(private _domSanitizer: DomSanitizer, private _matIconRegistry: MatIconRegistry) {
		// Register icon sets
		this._matIconRegistry.addSvgIconSet(
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/material-twotone.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"mat_outline",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/material-outline.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"mat_solid",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/material-solid.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"iconsmind",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/iconsmind.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"feather",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/feather.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"heroicons_outline",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/heroicons-outline.svg"),
		);
		this._matIconRegistry.addSvgIconSetInNamespace(
			"heroicons_solid",
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/heroicons-solid.svg"),
		);

		this._matIconRegistry.addSvgIcon(
			'presentation-chart-line',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/dashboard.svg"),
		)

		this._matIconRegistry.addSvgIcon(
			'assignement',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/affectation.svg"),
		)

		this._matIconRegistry.addSvgIcon(
			'clipboard-list',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/mes-taches.svg"),
		)

		this._matIconRegistry.addSvgIcon(
			'user-group',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/comite.svg"),
		)

		this._matIconRegistry.addSvgIcon(
			'office-building',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/habitat.svg"),
		)

		this._matIconRegistry.addSvgIcon(
			'briefcase',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/credit-entreprise.svg"),
		)
		
		this._matIconRegistry.addSvgIcon(
			'credit-card',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/credit-conso.svg"),
		)
		
		this._matIconRegistry.addSvgIcon(
			'template',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/autre-demandes.svg"),
		)
		
		this._matIconRegistry.addSvgIcon(
			'adjustments',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/parametrage.svg"),
		)
		
		this._matIconRegistry.addSvgIcon(
			'logout',
			this._domSanitizer.bypassSecurityTrustResourceUrl("assets/icons/logout.svg"),
		)
	}
}
