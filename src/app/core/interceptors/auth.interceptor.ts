import { HttpEvent, HttpHandler, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { AuthService, TokenType } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";

const TOKEN_HEADER_KEY = "Authorization";

@Injectable() /*implements HttpInterceptor*/
export class AuthInterceptor {
	/**
	 * Constructor
	 */
	constructor(
		private _authService: AuthService,
	) {}

	/**
	 * Intercept
	 *
	 * @param req
	 * @param next
	 */
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		if (this._authService.isAuth()) {
			return this.getToken(req.url).pipe(
				switchMap(
					token => {
						let authReq = req.clone({
							headers: req.headers.set(
								TOKEN_HEADER_KEY,
								`Bearer ${token}`,
							),
						});
						return next.handle(authReq);
					}
				)
			)
		} else {
			return next.handle(req);
		}
	}

	/**
	 * map endpoint to its required access token
	 * @param url 
	 * @returns required token to access the gateway
	 */
	getToken(url: string) {
		let tokenId: TokenType = "";
		if (url.indexOf(ConfigService.settings.apiServer.restREFUrl) !== -1) {
			tokenId = "ref";
		} else if (url.indexOf(ConfigService.settings.apiServer.restGTUrl) !== -1) {
			tokenId = "gt";
		}

		return this._authService.getToken(tokenId);
	}
}
