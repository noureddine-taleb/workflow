import {
	HttpErrorResponse,
	HttpHandler,
	HttpInterceptor,
	HttpRequest,
	HttpResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from 'app/modules/auth/auth.service';
import { throwError } from "rxjs";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
	constructor(
		private snackbarService: SnackBarService,
		private authService: AuthService,
		private router: Router,
	) {}

	intercept(req: HttpRequest<any>, next: HttpHandler) {
		return next.handle(req).pipe(
			map((result) => {
				// work arround server weird data structure (concedering 500 a valid response)
				if (
					result instanceof HttpResponse &&
					result.body.status &&
					result.body.status !== 200
				)
					throw new HttpErrorResponse({ error: result.body });
				return result;
			}),
			catchError((errorHttp: HttpErrorResponse) => {
				let errorMessage = "Un problème de source inconnue est survenu!";
				if (errorHttp?.error?.message)
					errorMessage = errorHttp.error.message;
				if (errorHttp.status === 401) {
					let oldRoute = this.router.url;
					errorMessage = "La session a expiré!";
					this.authService.signOut(false);
					this.router.navigate(["sign-in"], { queryParams: { redirectURL: oldRoute } });
				}
				
				this.snackbarService.openErrorSnackBar({ message: errorMessage });

				return throwError(errorHttp);
			}),
		);
	}
}
