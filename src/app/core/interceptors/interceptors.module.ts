import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, Optional, SkipSelf } from "@angular/core";
import { ErrorInterceptor } from "app/core/interceptors/error.interceptor";
import { LoaderInterceptor } from "app/core/interceptors/loader.interceptor";
import { AuthInterceptor } from "./auth.interceptor";

@NgModule({
	imports: [HttpClientModule],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: LoaderInterceptor,
			multi: true,
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ErrorInterceptor,
			multi: true,
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true,
		},	
	],
})
export class InterceptorsModule {
	constructor(@SkipSelf() @Optional() existingInterceptorModule?: InterceptorsModule) {
		if (existingInterceptorModule) {
			throw new Error("InterceptorsModule should be loaded only once (in AppModule)!");
		}
	}
}
