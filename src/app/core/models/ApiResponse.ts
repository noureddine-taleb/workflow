export class ApiResponse<T> {
	constructor(
		public result: T,
		public status: number = 200,
		public message: string = null,
	) {}
}
