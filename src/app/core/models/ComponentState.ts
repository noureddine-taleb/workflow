// this is used when you have a component that is used for both the add and update

export enum ComponentState {
	Add,
	Update,
	ReadOnly,
}