import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  constructor() { }

  store(key: string, obj: any) {
    let value = JSON.stringify(obj);
    localStorage.setItem(key, value);
  }

  isAvailable(key: string): boolean {
    let value = localStorage.getItem(key);
    return !!value;
  }

  fetch<T>(key: string): T | null {
    if (!this.isAvailable(key))
      return null;

    let value = localStorage.getItem(key);
    return JSON.parse(value);
  }

}
