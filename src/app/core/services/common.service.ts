import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { GoodType } from "app/modules/investitssement/investissement.types";
import { SelectModel } from "app/modules/simulation/simulation-form/simulation-form.component";
import { Tiers } from "app/shared/demande-qui/demande-qui.component";
import { Account } from "app/shared/models/Account";
import { Action, ActionModule } from "app/shared/models/Action";
import { Client } from "app/shared/models/Client";
import { CreationType } from "app/shared/models/CreationType";
import { RequestType } from "app/shared/models/RequestType";
import { ThousandSuffixModule } from "app/shared/pipes/thousand-suffix/thousand-suffix.module";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class CommonService {
	getRateType(critere) {
		return this.http.post(`${this.restWKFUrl}` + "rateType", critere);
	}

	private restWKFUrl = ConfigService.settings.apiServer.restWKFUrl + "/ref/";
	private restREFUrl = ConfigService.settings.apiServer.restREFUrl + "/ref/";

	private restWKFBaseUrl = ConfigService.settings.apiServer.restWKFUrl + "/";

	constructor(
			private http: HttpClient,
			private authService: AuthService,
			private demandeService: DemandeService
		) {}

	getAgences(): Observable<any> {
		return this.http.post(`${this.restREFUrl}` + "entities", {
			userEntityId: this.authService.user?.entityId,
		});
	}
	getCategories(parentId: number) {
		console.log({parentId})
		return this.http.post(`${this.restWKFUrl}` + "category", {parentId});
	}

	getTypeProductInversissement() {
		return this.http.get(`${this.restWKFUrl}` + "typeProduct");
	}

	getFinancialObjectEntreprise(productId: string): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "FinancialObjectEntreprise", {productId});
	}

	getproductList(creditType: RequestType) {
		let payload = {
			creditType,
			userId: this.authService.user.userId,
			userEntityId: this.authService.user.userEntityId,
			entityId: this.authService.user.entityId,
		}
		return this.http.post(`${this.restWKFUrl}` + "productList", payload);
	}

	getTypeBien(paramCriteria) {
		/*let selectModels = {
        status:200,
        message:'',
        result:[

      {id : 1, code : null, designation : 'Terrain', parentId	: null, parentCode	: null},
      {id : 2, code : null, designation : 'Logement', parentId	: null, parentCode	: null},
    ]
    };


         return of(selectModels);

     */

		return this.http.post<ApiResponse<GoodType[]>>(`${this.restWKFUrl}` + "goodsTypes", paramCriteria);
	}

	getBudgetAllocation(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "budgetAllocation");
	}
	getPatrimony(paramCriteria): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "garantiesType", { operator: "" });
	}

	getTypeGarantie(creditType): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "garantiesType", { parentId: creditType });
	}

	getGoodLocation(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "goodLocations");
	}

	getFinancielNatures(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "financielNatures");
	}

	getOrganisms(paramCriteria: SelectModel): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "organisms", paramCriteria);
	}

	getConventions(paramCriteria: SelectModel): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "conventions", paramCriteria);
	}

	getSegments(paramCriteria: SelectModel): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "segements", paramCriteria);
	}

	getFinancielObjects(paramCriteria: SelectModel): Observable<any> {
		return this.http.post(`${this.restWKFUrl}` + "financielObjects", paramCriteria);
	}

	getActivityStatus(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "statusActivity");
	}

	getPrelevementTypes(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "debtTypes");
	}

	getPrelevementTypesByCode(code: string): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "debtTypes");
	}

	getSegmentTypes(): Observable<any> {
		return this.http.get(`${this.restWKFUrl}` + "segmentsType");
	}

	getStatus() {
		return this.http.get(`${this.restWKFUrl}` + "statusList");
	}
	getPacks(element) {
		element["entityId"] = this.authService.user.entityId;
		element["userEntityId"] = this.authService.user.userEntityId;
		element["userId"] = this.authService.user.userId;

		let critere = {};
		return this.http.post(`${this.restWKFUrl}` + "productList", element);
	}
	getPerson(clientCriteria: Client): Observable<any> {
		return this.http.post(`${this.restREFUrl}` + "person", clientCriteria);
	}

	getPersonBonus(clientCriteria: Client): Observable<any> {
		return this.http.post<ApiResponse<any>>(`${this.restREFUrl}` + "infoPerson", clientCriteria);
	}

	getListPerson(clientCriteria: Client): Observable<any> {
		return this.http.post(`${this.restREFUrl}` + "listPerson", clientCriteria);
	}
	getPerson2(clientCriteria: any): Observable<any> {
		return this.http.post(`${this.restREFUrl}` + "person", clientCriteria);
	}

	getAccounts(compteCriteria: Account): Observable<any> {
		return this.http.post(`${this.restREFUrl}` + "accounts", compteCriteria);
	}
	// : <ApiResponse<Account>>
	getAccountByNumCompte(compteCriteria: any): Observable<ApiResponse<Account | null>> {
		compteCriteria["userEntityId"] = this.authService.user.userEntityId;
		compteCriteria["userId"] = this.authService.user.userId;
		compteCriteria["entityId"] = this.authService.user.entityId;
		return this.http.post<ApiResponse<Account>>(`${this.restREFUrl}` + "account", compteCriteria)
		.pipe(
			map(
				res => {
					// if accountId == 0 means account doesn't exist
					if (!res.result.accountId)
						return new ApiResponse<null>(null)
					return res;
				}
			)
		)
	}

	getCreationTypes() {
		return this.http.get<ApiResponse<CreationType[]>>(`${this.restWKFUrl}creationType`);
	}

	getItemByCode(list: any, code: string): any {
		let selected = "";
		list.forEach((item) => {
			// tslint:disable-next-line:triple-equals
			item.code == code ? (selected = item) : null;
		});
		return selected;
	}

	getItemById(list: any, id: number): any {
		let selected = "";
		list.forEach((item) => {
			item.id == id ? (selected = item) : null;
		});
		return selected;
	}

	getMensualites() {
		return this.http.get(`${this.restWKFUrl}` + "periodicity");
		/*return of(
            {
                result:[{code: 'M', libelle: 'Mensuel'}, {   code:'TR', libelle : 'Trimestriel'  } ],
                message:null,
                status:200
            }
        )*/
	}

	_getActionPrivelege(statusCode: string, module: string, override: any = {}) {
		let payload = {
			statusCode,
			module,
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
			...override
		}

		return this.http.post<ApiResponse<any>>(`${this.restWKFBaseUrl}action/getPrivilege`, payload);
	}

	getGuarantyPrivelege(statusCode: string) {
		return this._getActionPrivelege(statusCode, ActionModule.gauranty);
	}

	getBordereauPrivelege(statusCode: string) {
		return this._getActionPrivelege(statusCode, ActionModule.bordereau);
	}

	getAttestationConfPrivelege(statusCode: string) {
		return this._getActionPrivelege(statusCode, ActionModule.attestation_conf);
	}

	getOpcPrivelege(statusCode: string, override: any = {}) {
		return this._getActionPrivelege(statusCode, ActionModule.opc, override);
	}

	getRequestPrivelege() {
		return this._getActionPrivelege(this.demandeService.request.statusCode, ActionModule.request);
	}

	getCntPrivelege(statusCode: string) {
		return this._getActionPrivelege(statusCode, ActionModule.cnt);
	}

	getCcgProjectPrivelege(statusCode: string) {
		return this._getActionPrivelege(statusCode, ActionModule.ccg);
	}

	_getActions(statusCode: string, module: string, override: any = {}) {
		const critere: any = {};
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		critere["entityId"] = this.authService.user.entityId;
		critere["requestId"] = this.demandeService.request.requestId;
		critere["lineId"] = critere["requestId"];
		critere["module"] = module;
		critere["creationEntityId"] = this.demandeService.request.creationEntityId;
		critere["statusCode"] = statusCode;

		return this.http.post<ApiResponse<Action[]>>(this.restWKFBaseUrl + "action/get", {...critere, ...override});
	}


	getGuarantyActions(statusCode: string) {
		return this._getActions(statusCode, ActionModule.gauranty);
	}

	getBordereauActions(statusCode: string) {
		return this._getActions(statusCode, ActionModule.bordereau);
	}

	getAttestationConfActions(statusCode: string) {
		return this._getActions(statusCode, ActionModule.attestation_conf);
	}

	getAutorisationLivActions(statusCode: string) {
		return this._getActions(statusCode, ActionModule.autorisation_liv);
	}

	getAttestationDblActions(statusCode: string) {
		return this._getActions(statusCode, ActionModule.attestation_dbl);
	}

	getCntActions(statusCode: string, override: any) {
		return this._getActions(statusCode, ActionModule.cnt, override);
	}

	getCcProjectActions(statusCode: string, lineId: number) {
		return this._getActions(statusCode, ActionModule.ccg, {lineId});
	}

	getOpcActions(statusCode: string, lineId: number) {
		return this._getActions(statusCode, ActionModule.opc, {lineId, creditType: this.demandeService.request.creditTypeId });
	}

	getParams() {
		return this.http.get(this.restWKFBaseUrl + "ref/params");
	}

	dispatchAction(obj: any, action: Action) {
		obj["entityId"] = this.authService.user.entityId;
		obj["userEntityId"] = this.authService.user.userEntityId;
		obj["userId"] = this.authService.user.userId;
		obj["requestId"] = this.demandeService.request.requestId;

		return this.http.post(`${this.restWKFBaseUrl}${action.urlAction}`, obj);
	}

	hasPrivilege(priv: string, privileges: any[]) {
		return !!privileges?.find((p) => p.privilegeCode == priv);
	}

	executeActionByUrl(critere) {
		return this.http.post(`${this.restWKFBaseUrl}${critere["url"]}`, critere);
	}

	getAdviceCodes() {
		return this.http.get<ApiResponse<any[]>>(`${this.restWKFUrl}adviceCode`);
	}

	getAgreements() {
		return this.http.get(`${this.restWKFUrl}` + "agreements");
	}

	getProviders() {
		return this.http.get(`${this.restWKFUrl}` + "providers");
	}

	getRequestTitle(): string {
		let tier = new Tiers();
		this.demandeService.persons?.forEach((item) => {
			tier.nomComplet += item.cin + " " + item.clientName + " , ";
			tier.rcNumber += (item.rcNumber || "") + ", ";
			tier.entityName += item.entityName + ", ";
		});

		if (this.demandeService.request?.creditTypeId != RequestType.HORS_AGRI)
			return `${tier.nomComplet} ${ this.demandeService.request?.activityStatusName }`
		else
			return `RC N° ${ tier?.rcNumber } ${ tier.entityName }`;
	}

    getRequestTitleSimple(): string {
        let tier = new Tiers();
        this.demandeService.persons?.forEach((item) => {
            tier.nomComplet +=  item.clientName + "  ";
            tier.rcNumber += (item.rcNumber || " ") + "  ";
            tier.entityName += item.entityName + "  ";
        });

        if (this.demandeService.request?.creditTypeId != RequestType.HORS_AGRI)
            return `${tier.nomComplet} `
        else
            return `RC N° ${ tier?.rcNumber } ${ tier.entityName }`;
    }
}
