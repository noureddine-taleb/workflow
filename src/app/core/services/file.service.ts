import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class FileService {
	constructor(private _httpClient: HttpClient) {}

	getFileByUrl(url) {
		return this._httpClient.get(url, {
			observe: "response",
			responseType: "blob",
		});
	}

	convertBase64ToBlobData(
		base64Data: string,
		contentType: string = "image/png",
		sliceSize = 512,
	) {
		const byteCharacters = atob(base64Data);
		const byteArrays = [];
		for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			const slice = byteCharacters.slice(offset, offset + sliceSize);
			const byteNumbers = new Array(slice.length);
			for (let i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			const byteArray = new Uint8Array(byteNumbers);
			byteArrays.push(byteArray);
		}

		const blob = new Blob(byteArrays, { type: contentType });
		return blob;
	}
}
