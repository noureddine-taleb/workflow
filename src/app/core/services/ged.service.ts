import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { FileSaverService } from "ngx-filesaver";
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';
import { FileService } from "./file.service";
import { RequestDoc } from "app/shared/demande-docs/demande-docs.types";
import { RequestDocumentType } from "app/shared/demande-docs-attch/demande-docs-attch.types";

@Injectable({ providedIn: "root" })
export class GedService {
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;
	constructor(
		private httpClient: HttpClient,
		private authService: AuthService,
		private fileSaverService: FileSaverService,
		private fileService: FileService,
	) {}

	uploadFile(gedDto) {
		gedDto.userEntityId = this.authService.user.userEntityId;
		gedDto.entityId = this.authService.user.entityId;
		gedDto.userId = this.authService.user.userId;
		gedDto.profil = this.authService.user.userId;

		//this.toJsonServ.jsonToFile(gedDto);
		return this.httpClient.post(this.wkfPath + "/ged/upload", gedDto);
	}
	getDocType(requestId) {
		let critere = {};
		critere["requestId"] = requestId;

		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;
		critere["first"] = 0;
		critere["step"] = 999;

		// return of<ApiResponse<RequestDocumentType[]>>(
		// 	{ "status": 200, "message": null, "result": 
		// 	[{ "docTypeId": 12, "docTypeName": "Documents spécifiques", 
		// 	"documents": [{ "docId": 135, "docName": "Attestation d'architecte (constructions ou aménagement)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 130, "docName": "Compromis de vente (acquisition d'un logement ou local commercial)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 132, "docName": "Devis et factures", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 137, "docName": "Autres", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 131, "docName": "Autorisation de construire, Plans approuvés et Plans BET", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 136, "docName": "Etat d'avancement des travaux avec la valeur des travaux engagés estimés par l'architecte (constructions ou aménagement)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 133, "docName": "Bulletin de paie des 3 derniers mois", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 134, "docName": "Attestation de travail", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 176, "docName": "Attestation de salaire", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 20, "docTypeName": "Pièces d'identité", "documents": [{ "docId": 79, "docName": "carte professionnelle", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 69, "docName": "CIN", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 85, "docName": "Autres", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 5, "docName": "Titre de séjour Valide ou pièce d'identité étrangère", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "documents": [{ "docId": 8, "docName": "Attestation de travail", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 16, "docName": "Copie de la Taxe Professionnelle", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 10, "docName": "Attestation de salaire", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 13, "docName": "Relevés des 6 derniers mois de l'ancienne banque pour les nouveaux clients", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 18, "docName": "Contrat de bail en cas de revenus locatifs", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 11, "docName": "Attestation de pension", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 15, "docName": "Justificatifs de l'exercice pour son propore compte", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 17, "docName": "Avis d'imposition ou quittance de réglement ou récepissé d'imposition IR", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 9, "docName": "Etat d'engagement recent", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 113, "docName": "Tout autre document justifiant les autres revenus (contrat de location de terrains agricoles/biens immobiliers¿)", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 12, "docName": "Attestation CNSS précisnant l'ancienté de la déclaration", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 3, "docTypeName": "Dérogations", "documents": [{ "docId": 61, "docName": "Dérogation sur le taux", "docTypeId": 3, "docTypeName": "Dérogations", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 65, "docTypeName": "Documents OPC", "documents": [{ "docId": 156, "docName": "Autorisation de consultation CNT", "docTypeId": 65, "docTypeName": "Documents OPC", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 37, "docTypeName": "Garanties", "documents": [{ "docId": 127, "docName": "Certificats de propriété récents (Maximum 3 mois)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 129, "docName": "Situation patrimoniale détaillée dûment signée et actualisée par le client (Biens mobiliers et immobiliers)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 89, "docName": "Engagement Notaire", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 128, "docName": "Evaluation récente des sûretés proposées (source de l'évaluation, date)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 49, "docTypeName": "Réalisation des garanties", "documents": [{ "docId": 142, "docName": "Autres", "docTypeId": 49, "docTypeName": "Réalisation des garanties", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 13, "docTypeName": "Demande client", "documents": [{ "docId": 39, "docName": "Demande de basculement signé par le client", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 31, "docName": "EVCC Basculement", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 155, "docName": "Autorisation de consultation CNT", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 80, "docName": "Demande du client signée", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }] }] }as any)
		return this.httpClient.post<ApiResponse<RequestDocumentType[]>>(this.wkfPath + "/ged/doctype", critere);
	}

	getUploadedFiles(requestId) {
		let critere = {};
		critere["requestId"] = requestId;

		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;
		return this.httpClient.post<ApiResponse<RequestDoc[]>>(this.wkfPath + "/ged/list", critere);
	}

	dowloadFile(critere) {
		return this.httpClient.post(this.wkfPath + "/ged/getDoc", critere);
	}

	downloadSaveFile(folder: any) {
		this.dowloadFile(folder).subscribe((data) => {
			const blob = this.fileService.convertBase64ToBlobData(data["result"]["data"]);
			this.fileSaverService.save(blob, data["result"]["fileName"]);
		});
	}

	delete(critere: any) {
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.wkfPath + "/ged/deleteDoc", critere);
	}
}
