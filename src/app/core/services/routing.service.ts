import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestType } from '../../shared/models/RequestType';

export const requestTypeRoute = {
	[RequestType.MAINLEVEE]: "mainlevee",
	[RequestType.SOLDE_DE_TOUT_COMPTE]: "solde_de_tout_compte",
	[RequestType.REPORT]: "report",
	[RequestType.CONSOLIDATION]: "consolidation",
	[RequestType.BASCULEMENT_S_B]: "basculement_s_b",
	[RequestType.RESTRUCTURATION]: "restructuration",
}

/**
 * routes are vulnerable to change,
 * and for sure need to be refactored at some point,
 * for now it's better to put them in a service for ease of change
 */
@Injectable({
	providedIn: 'root'
})
export class RoutingService {

	// used in demande list to distinguish between myTasks view and global view
	// private __needToSetUserId = false;

	constructor(
		private router: Router,
		private route: ActivatedRoute
	) { }

	/**
	 * misc routes
	 */
	gotoAffectation() {
		this.router.navigate(["/", "affectation"])
	}

	reloadCurrentRoute() {
		let currentRoute = this.router.url;
		return this.router.navigate(["../"]).then(_ => this.router.navigateByUrl(currentRoute));
	}

	/**
	 * goto simulation
	 */
	gotoSimulations() {
		this.router.navigate(["/", "simulation"])
	}

	gotoSimulationEdit(id: string) {
		this.router.navigate(["/", "simulation", "edit", id])
	}

	gotoSimulationAdd(cin: string, milieuBien: string, typeAchat: string) {
		this.router.navigate(["/", "simulation", "add"], {queryParams: {cin, milieuBien: milieuBien, typeAchat: typeAchat}})
	}

	/**
	 * comite routes
	 */
	gotoComites() {
		this.router.navigate(["/", "comite"])
	}

	gotoComite(comiteId: number) {
		this.router.navigate(["/", "comite", "traiter", comiteId])
	}

	gotoComiteAdd() {
		this.router.navigate(["/", "comite", "add"])
	}

	gotoComiteEdit(comiteId: number) {
		this.router.navigate(["/", "comite", "edit", comiteId])
	}

	/**
	 * requests list for specific type
	 */
	gotoTasks() {
		this.router.navigate(["/", "demande", "taches"])
	}

	gotoHabitatRequests() {
		this.router.navigate(["/", "demande", "habitat"])
	}

	gotoConsoRequests() {
		this.router.navigate(["/", "demande", "conso"])
	}

	gotoMazayaRequests() {
		this.router.navigate(["/", "demande","mazaya"])
	}
    gotoIstidamaRequests() {
        this.router.navigate(["/", "demande","istitadam"])
    }
    gotoTasbiqFdaRequests() {
        this.router.navigate(["/", "demande","tasbiqfda"])
    }
    gotoQuatroRequests() {
		this.router.navigate(["/", "demande","quatro"])
    }

	gotoHorsAgriRequests() {
		this.router.navigate(["/", "demande", "investi", "hors-agri"])
	}

	gotoAgriRequests() {
		this.router.navigate(["/", "demande", "investi", "agri"])
	}

	gotoAmaqRequests() {
		this.router.navigate(["/", "demande", "investi", "amaq"])
	}

	gotoOtherRequests(creditTypeId: RequestType) {
		this.router.navigate(["/", "demande", "other", requestTypeRoute[creditTypeId]])
	}

	gotoRequestsList(creditTypeId: RequestType, checkRWFlag: boolean = false) {
		if (checkRWFlag && this.isDemandeRW()) {
			this.gotoTasks();
			return;
		}

		switch(creditTypeId) {
			case RequestType.HABITAT:
				this.gotoHabitatRequests();
				break;

			case RequestType.CONSO:
				this.gotoConsoRequests();
				break;

			case RequestType.AGRI:
				this.gotoAgriRequests();
				break;

			case RequestType.HORS_AGRI:
				this.gotoHorsAgriRequests();
				break;

			case RequestType.AMAQ:
				this.gotoAmaqRequests();
				break;

			case RequestType.MAINLEVEE:
			case RequestType.SOLDE_DE_TOUT_COMPTE:
			case RequestType.REPORT:
			case RequestType.CONSOLIDATION:
			case RequestType.BASCULEMENT_S_B:
			case RequestType.RESTRUCTURATION:
				this.gotoOtherRequests(creditTypeId);
				break;

			case RequestType.QUATRO:
				this.gotoQuatroRequests();
				break;

			case RequestType.MAZAYA:
				this.gotoMazayaRequests();
				break;

			default:
				console.warn("[goback] undefined behaviour for creditTypeId", creditTypeId)
		}
	}

	/**
	 * goto single request details
	 * @param requestId
	 */
	gotoTask(requestId: number) {
		// TODO: better set this in route resolver service
		this.setDemandeRW()
		this.router.navigate(["/", "demande", requestId], {queryParams: {rw: true}})
	}

	gotoHabitatRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

	gotoConsoRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}
	gotoMazayaRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

    gotoTasbiqFdaRequest(requestId: number) {
        this.setDemandeRO()
        this.router.navigate(["/", "demande", requestId])
    }

    gotoIstidamaRequest(requestId: number) {
        this.setDemandeRO()
        this.router.navigate(["/", "demande", requestId])
    }

    gotoQuatroRequest(requestId: number) {
        this.setDemandeRO()
        this.router.navigate(["/", "demande", requestId])
    }
	gotoHorsAgriRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

	gotoAgriRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

	gotoAmaqRequest(requestId: number) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

	gotoOtherRequest(requestId: RequestType) {
		this.setDemandeRO()
		this.router.navigate(["/", "demande", requestId])
	}

	gotoRequest(request: { requestId: number, creditTypeId: number }) {
		if (this.isInMyTasks()) {
			this.gotoTask(request.requestId);
			return;
		}
		switch (request.creditTypeId) {
			case RequestType.HABITAT:
				this.gotoHabitatRequest(request.requestId)
				break;
			case RequestType.CONSO:
				this.gotoConsoRequest(request.requestId)
				break
			case RequestType.MAZAYA:
				this.gotoMazayaRequest(request.requestId)
				break
            case RequestType.TASBIQFDA:
                this.gotoTasbiqFdaRequest(request.requestId)
                break
            case RequestType.ISTIDAMA:
                this.gotoIstidamaRequest(request.requestId)
                break
            case RequestType.QUATRO:
                this.gotoQuatroRequest(request.requestId)
                break
			case RequestType.AGRI:
				this.gotoAgriRequest(request.requestId)
				break
			case RequestType.HORS_AGRI:
				this.gotoHorsAgriRequest(request.requestId)
				break
			case RequestType.AMAQ:
				this.gotoAmaqRequest(request.requestId)
				break

			case RequestType.MAINLEVEE:
			case RequestType.SOLDE_DE_TOUT_COMPTE:
			case RequestType.REPORT:
			case RequestType.CONSOLIDATION:
			case RequestType.BASCULEMENT_S_B:
			case RequestType.RESTRUCTURATION:
					this.gotoOtherRequest(request.requestId);
					break;
			default:
				throw new Error(`Unknowen creditTypeId ${request.creditTypeId}`)
		}
	}

	/**
	 * add request
	 */
	gotoHorsAgriAdd() {
		this.router.navigate(["/", "demande", "investi", "add", RequestType.HORS_AGRI])
	}

	gotoAgriAdd() {
		this.router.navigate(["/", "demande", "investi", "add", RequestType.AGRI])
	}

	gotoConsoAdd() {
		this.router.navigate(["/", "demande", "conso", "add"])
	}

	gotoMazayaAdd() {
		this.router.navigate(["/", "demande", "mazaya", "add"])
	}

    gotoIstidamaAdd() {
        this.router.navigate(["/", "demande", "istidama", "add"]);
    }

    gotoTasbiqFdaAdd() {
        this.router.navigate(["/", "demande", "tasbiqfda", "add"]);
    }

	gotoOtherAdd(creditTypeId: RequestType) {
		this.router.navigate(["/", "demande", "other", "add", creditTypeId])
	}

	gotoQuatroAdd() {
		this.router.navigate(["/", "demande", "quatro", "add"])
	}

	gotoAdd(creditTypeId: RequestType) {
		switch (creditTypeId) {
			case RequestType.CONSO:
				this.gotoConsoAdd()
				break
			case RequestType.QUATRO:
				this.gotoQuatroAdd()
				break
			case RequestType.MAZAYA:
				this.gotoMazayaAdd()
				break
            case RequestType.ISTIDAMA:
                this.gotoIstidamaAdd();
                break;
            case RequestType.TASBIQFDA:
                this.gotoTasbiqFdaAdd();
                break;
			case RequestType.AGRI:
				this.gotoAgriAdd()
				break
			case RequestType.HORS_AGRI:
				this.gotoHorsAgriAdd()
				break

			case RequestType.MAINLEVEE:
			case RequestType.SOLDE_DE_TOUT_COMPTE:
			case RequestType.REPORT:
			case RequestType.CONSOLIDATION:
			case RequestType.BASCULEMENT_S_B:
			case RequestType.RESTRUCTURATION:
				this.gotoOtherAdd(creditTypeId);
				break;

			case RequestType.QUATRO:
				this.gotoQuatroAdd();
				break;
			default:
				throw new Error(`Operation not supported for creditTypeId = ${creditTypeId}`)
		}
	}

	isAddSupported(creditTypeId: number) {
		return (
			creditTypeId == RequestType.HABITAT ||
			creditTypeId == RequestType.CONSO ||
			creditTypeId == RequestType.AGRI ||
			creditTypeId == RequestType.HORS_AGRI ||

			creditTypeId == RequestType.MAINLEVEE ||
			creditTypeId == RequestType.SOLDE_DE_TOUT_COMPTE ||
			creditTypeId == RequestType.REPORT ||
			creditTypeId == RequestType.CONSOLIDATION ||
			creditTypeId == RequestType.BASCULEMENT_S_B ||
			creditTypeId == RequestType.RESTRUCTURATION||
			creditTypeId == RequestType.QUATRO ||
			creditTypeId == RequestType.MAZAYA ||
            creditTypeId == RequestType.ISTIDAMA ||
            creditTypeId == RequestType.TASBIQFDA
		)
	}

	/**
	 * external route params
	 * TODO: maybe we should implement this using query params
	 */
	private __setDemandeState(state: boolean) {
		this.router.navigate(["."], {queryParams: {rw: state}})
	}

	// the actual logic is here: (rw, rw=true) = true, (rw=false, rw=undefined) = false
	private __getDemandeState(): boolean {
		let rwFlag = this.route.snapshot.queryParams.rw;
		return !(rwFlag === undefined || rwFlag === 'false');
	}

	private setDemandeRO() {
		this.__setDemandeState(false);
	}

	private setDemandeRW() {
		this.__setDemandeState(true);
	}

	isDemandeRO() {
		return !this.__getDemandeState();
	}

	isDemandeRW() {
		return !this.isDemandeRO();
	}

	/**
	 * general helpers
	 */
	isInMyTasks(): boolean {
		let route = this.router.routerState.root;
		while (route.firstChild)
			route = route.firstChild;
		return !!route.snapshot.data.myTasks;
		// return this.router.isActive("/demande/taches", { paths: 'exact', queryParams: 'ignored', fragment: 'ignored', matrixParams: 'ignored' });
	}

	requestDetailGoBack(request: LoanRequest, route: ActivatedRoute) {
		this.gotoRequestsList(request.creditTypeId, true);
	}
}
