import { Injectable } from "@angular/core";
import { PrivilegeService } from "app/modules/demandes/services/privilege.service";
import { CommonService } from "app/core/services/common.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { EventCode } from "app/modules/demandes/models/EventCode";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";
import { RequestEvent } from "app/modules/demandes/models/RequestEvent";

@Injectable({ providedIn: "root" })
export class UtilsService {
	privilegeCodes = PrivilegeCode;

	constructor(
		public globasprivilege: PrivilegeService,
		private demandeService: DemandeService,
		private commonService: CommonService
	) {}

	_afficheable(events: RequestEvent[], code: EventCode): boolean {
		return events.filter((item) => item.eventTypeCode == code).length > 0;
	}

	afficheable(code: EventCode): boolean {
		return this._afficheable(this.demandeService.request.events, code);
	}

	_hasPrivilege(code: string, listPrivs: any[]): boolean {
		return !!listPrivs?.filter((item) => item.privilegeCode == code)
			?.length;
	}

	hasPrivilege(code: string): boolean {
		return this._hasPrivilege(code, this.globasprivilege.listPrivilege)
	}

	loadRequestPrivs() {
		this.commonService.getRequestPrivelege().subscribe((data) => {
			this.globasprivilege.listPrivilege = data["result"];
		});
	}

	hasPrivilegeCnt(code: string): boolean {
		return this._hasPrivilege(code, this.globasprivilege.listPrivilegeCnt);
	}

	hasPrivilegeOpc(code: string): boolean {
		return this._hasPrivilege(code, this.globasprivilege.listPrivilegeOpc);
	}

	hasPrivilegeGar(code: string): boolean {
		return this._hasPrivilege(code, this.globasprivilege.listPrivilegeGar);
	}
}
