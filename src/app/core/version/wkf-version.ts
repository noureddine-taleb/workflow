import { Version } from "app/core/version/version";

export const WKF_VERSION = new Version("2.0.0").full;
