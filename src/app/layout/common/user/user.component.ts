import { BooleanInput } from "@angular/cdk/coercion";
import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	OnDestroy,
	OnInit,
	ViewEncapsulation
} from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from 'app/modules/auth/auth.service';
import { User } from "app/shared/models/User";
import { Subject } from "rxjs";

@Component({
	selector: "user",
	templateUrl: "./user.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush,
	exportAs: "user",
})
export class UserComponent implements OnInit, OnDestroy {
	/* eslint-disable @typescript-eslint/naming-convention */
	static ngAcceptInputType_showAvatar: BooleanInput;
	/* eslint-enable @typescript-eslint/naming-convention */
	user: User;

	private _unsubscribeAll: Subject<any> = new Subject<any>();

	/**
	 * Constructor
	 */
	constructor(
		private _changeDetectorRef: ChangeDetectorRef,
		private _router: Router,
		private authService: AuthService,
	) {}

	// -----------------------------------------------------------------------------------------------------
	// @ Lifecycle hooks
	// -----------------------------------------------------------------------------------------------------

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.user = this.authService.user;
		this._changeDetectorRef.markForCheck();
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// Unsubscribe from all subscriptions
		this._unsubscribeAll.next();
		this._unsubscribeAll.complete();
	}

	/**
	 * Sign out
	 */
	signOut(): void {
		this.authService.signOut();
	}
}
