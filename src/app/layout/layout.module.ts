import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { LayoutComponent } from "app/layout/layout.component";
import { EmptyLayoutModule } from "app/layout/layouts/empty/empty.module";
import { MainLayoutModule } from "app/layout/layouts/main/main.module";

const layoutModules = [
	EmptyLayoutModule,
	MainLayoutModule,
];

@NgModule({
	declarations: [LayoutComponent],
	imports: [
		CommonModule,
		MatIconModule, 
		MatTooltipModule, 
		...layoutModules
	],
	exports: [LayoutComponent, ...layoutModules],
	providers: [
		{
			// Use the 'fill' appearance on Angular Material form fields by default
			provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
			useValue: {
				appearance: "fill",
			},
		},
	]
})
export class LayoutModule {}
