import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { EmptyLayoutComponent } from "app/layout/layouts/empty/empty.component";

@NgModule({
	declarations: [EmptyLayoutComponent],
	imports: [
		CommonModule,
		RouterModule,
	],
	exports: [EmptyLayoutComponent],
})
export class EmptyLayoutModule {}
