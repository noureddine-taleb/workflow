import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { NavigationEnd, NavigationStart, Router } from "@angular/router";
import { LayoutService } from "app/layout/services/layout.service";
import { NavigationComponent, NavigationList, NavigationService } from "app/shared/navigation";
import { Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";

@Component({
	selector: "main-layout",
	templateUrl: "./main.component.html",
})
export class MainLayoutComponent implements OnInit, OnDestroy {
	isScreenSmall: boolean;
	navigation: NavigationList;
	navigationAppearance: "default" | "dense" = "dense";
	private _unsubscribeAll: Subject<any> = new Subject<any>();
	@ViewChild("body") body: ElementRef;

	/**
	 * Constructor
	 */
	constructor(
		private _navigationService: NavigationService,
		public layoutService: LayoutService,
		public router: Router,
	) {}

	// -----------------------------------------------------------------------------------------------------
	// @ Accessors
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Getter for current year
	 */
	get currentYear(): number {
		return new Date().getFullYear();
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Lifecycle hooks
	// -----------------------------------------------------------------------------------------------------

	/**
	 * On init
	 */
	ngOnInit(): void {
		// Subscribe to navigation data
		this._navigationService.navigation$
			.pipe(takeUntil(this._unsubscribeAll))
			.subscribe((navigation: NavigationList) => {
				this.navigation = navigation;
			});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// Unsubscribe from all subscriptions
		this._unsubscribeAll.next();
		this._unsubscribeAll.complete();
	}

	ngAfterViewInit(): void {
		// reset scroll on route change
		let oldUrl: string;

		this.router.events
		.pipe(filter(e => e instanceof NavigationStart))
		.subscribe((e: NavigationStart) => {
			oldUrl = e.url.split('?')[0];
		})

		this.router.events
		.pipe(filter(e => e instanceof NavigationEnd))
		.subscribe((e: NavigationEnd) => {
			// ignore query params
			if (oldUrl != e.url.split("?")[0])
				this.body.nativeElement.scrollTo(0, 0);
		})
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Public methods
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Toggle navigation
	 *
	 * @param name
	 */
	toggleNavigation(name: string): void {
		// Get the navigation
		const navigation =
			this._navigationService.getComponent<NavigationComponent>(name);

		if (navigation) {
			// Toggle the opened status
			navigation.toggle();
		}
	}

	/**
	 * Toggle the navigation appearance
	 */
	toggleNavigationAppearance(): void {
		this.navigationAppearance = this.navigationAppearance === "default" ? "dense" : "default";
	}
}
