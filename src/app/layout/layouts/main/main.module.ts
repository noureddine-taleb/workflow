import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { RouterModule } from "@angular/router";
import { NotificationsModule } from "app/layout/common/notifications/notifications.module";
import { UserModule } from "app/layout/common/user/user.module";
import { NavigationModule } from "app/shared/navigation";
import { MainLayoutComponent } from "./main.component";

@NgModule({
	declarations: [MainLayoutComponent],
	imports: [
		CommonModule,
		HttpClientModule,
		RouterModule,
		MatButtonModule,
		MatDividerModule,
		MatIconModule,
		MatMenuModule,
		NotificationsModule,
		UserModule,
		NavigationModule
	],
	exports: [MainLayoutComponent],
})
export class MainLayoutModule {}
