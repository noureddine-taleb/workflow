import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  public title = new BehaviorSubject("");
  public title$ = this.title.asObservable();

  setTitle(title: string) {
    this.title.next(title);
  }
}
