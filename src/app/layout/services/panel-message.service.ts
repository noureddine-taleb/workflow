import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class PanelMessageService {
	public messages: { code: string; message: string; btnOk: string; btnNon: string }[] = [
		{
			code: "enregistrer",
			message: "Voulez-vous confirmer cette opération ? ",
			btnOk: "Oui",
			btnNon: "Non",
		},
		{
			code: "supprimer",
			message: "Voulez-vous confirmer la suppression ? ",
			btnOk: "Oui",
			btnNon: "Non",
		},
		{
			code: "all",
			message: "Voulez-vous confirmer cette opération ? ",
			btnOk: "Oui",
			btnNon: "Non",
		},
	];

	getItem(code) {
		let mess = this.messages.filter((x) => x.code === code.toLowerCase());
		if (mess && mess.length > 0) return mess[0];
		return { code: code, message: code, btnOk: "Oui", btnNon: "Non" };
	}
}
