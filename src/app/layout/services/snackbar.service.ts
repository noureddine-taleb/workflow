import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import Swal from "sweetalert2";
import { PanelMessageService } from "./panel-message.service";

@Injectable({ providedIn: "root" })
export class SnackBarService {
	data: { message: string };

	constructor(private snackBar: MatSnackBar, private panelMessageService: PanelMessageService) {}

	/* openErrorSnackBar(data: { message: string }){
    this.snackBar.open(data.message,'X',{duration:200000,horizontalPosition:'center',verticalPosition:'top',panelClass:['error-snackbar']})
  }

  openSuccesSnackBar(data: { message: string }){
    this.snackBar.open(data.message,'X',{duration:200000,horizontalPosition:'center',verticalPosition:'top',panelClass:['success-snackbar']})
  }*/
	openErrorSnackBar(data: { message: string }) {
		Swal.fire({
			title: "Erreur !",
			text: data.message,
			icon: "error",
			confirmButtonText: "Fermer",
			confirmButtonColor: "#E30A17",
		});
	}

	openSuccesSnackBar(data: { message: string }) {
		Swal.fire({
			title: "",
			html: data.message,
			icon: "success",
			confirmButtonText: "Fermer",
			confirmButtonColor: "#085F27",
		});
	}

    openSuccesSnackBarWithReturn(data: { message: string }) {
        return Swal.fire({
            title: "",
            text: data.message,
            icon: "success",
            confirmButtonText: "Fermer",
            confirmButtonColor: "#085F27",
        });
    }

	openConfirmSnackBar(data: { message: string }) {
		const constMessage = this.panelMessageService.getItem(data.message);
		return Swal.fire({
			title: "",
			text: constMessage.message,
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",

			cancelButtonText: constMessage.btnNon,
			confirmButtonText: constMessage.btnOk,
		});
	}

	openInfoSnackBar(data: { message: string }) {
		return Swal.fire({
			title: "",
			text: data.message,
			icon: "info",
			confirmButtonColor: "#3085d6",
			confirmButtonText: "OK",
		});
	}

	reloadTaches(item: number) {
		alert("rrrrr");
	}
}
