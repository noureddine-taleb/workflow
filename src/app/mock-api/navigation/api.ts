import { Injectable } from "@angular/core";
import { MockApiService } from "app/core/lib/mock-api";
import {
	defaultNavigation
} from "app/mock-api/navigation/data";
import { NavigationItem } from "app/shared/navigation";
import { cloneDeep } from "lodash-es";

@Injectable({
	providedIn: "root",
})
export class NavigationMockApi {
	private readonly _defaultNavigation: NavigationItem[] = defaultNavigation;

	/**
	 * Constructor
	 */
	constructor(private _mockApiService: MockApiService) {
		// Register Mock API handlers
		this.registerHandlers();
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Public methods
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Register Mock API handlers
	 */
	registerHandlers(): void {
		// -----------------------------------------------------------------------------------------------------
		// @ Navigation - GET
		// -----------------------------------------------------------------------------------------------------
		this._mockApiService.onGet("api/common/navigation").reply(() => {
			// Return the response
			return [
				200,
				cloneDeep(this._defaultNavigation)
			];
		});
	}
}
