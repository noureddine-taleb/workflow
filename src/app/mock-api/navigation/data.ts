/* tslint:disable:max-line-length */

import { NavigationItem } from "app/shared/navigation";

// TODO: turn this into an injectable
export const defaultNavigation: NavigationItem[] = [
	{
		id: "user-interface.page-layouts",
		title: "Tableau de bord",
		type: "basic",
		icon: "presentation-chart-line",
	},
	{
		id: "user-interface.page-layouts",
		title: "Affectation",
		type: "basic",
		icon: "assignement",
		link: "/affectation",
	},
	{
		id: "user-interface.page-layouts",
		title: "Mes tâches",
		type: "basic",
		icon: "clipboard-list",
		link: "/demande/taches",
	},
	{
		id: "user-interface.page-layouts",
		title: "Comités",
		type: "basic",
		icon: "user-group",
		link: "/comite",
	},
	{
		id: "simulation",
		title: "Credit habitat",
		type: "collapsable",
		icon: "office-building",
		children: [
			{
				id: "simulation",
				title: "Simulation",
				type: "basic",
				link: "/simulation",
			},
			{
				id: "habitat",
				title: "Demandes",
				type: "basic",
				link: "/demande/habitat",
			},
		],
	},
	{
		id: "user-interface.page-layouts",
		title: "Credit entreprise",
		type: "collapsable",
		icon: "briefcase",
		children: [
			{
				id: "user-interface.page-layouts.empty",
				title: "Hors Agri",
				type: "basic",
				link: "/demande/investi/hors-agri",
			},
			{
				id: "user-interface.page-layouts.overview",
				title: "Agri",
				type: "basic",
				link: "/demande/investi/agri",
			},
			{
				id: "user-interface.page-layouts.overview",
				title: "AMAQ",
				type: "basic",
				link: "/demande/investi/amaq",
			},
		],
	},

	{
		id: "conso",
		title: "Credit conso",
		type: "basic",
		icon: "shopping_bag",
		link: "/demande/conso",
	},
    {
        id: "quatro",
        title: "Demande Quatro",
        type: "basic",
        icon: "credit-card",
        link: "/demande/quatro",
    },
	{
        id: "mazaya",
        title: "Demande Mazaya",
        type: "basic",
        icon: "credit_score",
        link: "/demande/mazaya",
    },
    /*{
        id: "istidama",
        title: "Demande Istidama",
        type: "basic",
        icon: "crop_rotate",
        link: "/demande/istidama",
    },*/
    {
        id: "tasbiqfda",
        title: "Demande Tasbiq Fda",
        type: "basic",
        icon: "crop_rotate",
        link: "/demande/tasbiqfda",
    },
	{
		id: "user-interface.page-layouts",
		title: "Sort des avals",
		type: "basic",
		icon: "template",
		// icon: "clipboard-check",
		link: "/sort-demande-cc",
	},
	{
		id: "user-interface.page-layouts",
		title: "Autres demandes",
		type: "collapsable",
		icon: "template",
		children: [
			{
				id: "user-interface.page-layouts.overview",
				title: "Mainlevée",
				type: "basic",
				link: "/demande/other/mainlevee",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Solde de tout compte",
				type: "basic",
				link: "/demande/other/solde_de_tout_compte",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Report",
				type: "basic",
				link: "/demande/other/report",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Consolidation",
				type: "basic",
				link: "/demande/other/consolidation",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Basculement   S -> B",
				type: "basic",
				link: "/demande/other/basculement_s_b",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Restructuration",
				type: "basic",
				link: "/demande/other/restructuration",
			},
		],
	},
	{
		id: "user-interface.page-layouts",
		title: "Paramètrage",
		type: "collapsable",
		icon: "adjustments",
		children: [
			{
				id: "user-interface.page-layouts.overview",
				title: "Profils",
				type: "basic",
				link: "/ui/page-layouts/overview",
			},
			{
				id: "user-interface.page-layouts.empty",
				title: "Gestion des utilisateurs",
				type: "basic",
				link: "/ui/page-layouts/empty",
			},
		],
	},
];

