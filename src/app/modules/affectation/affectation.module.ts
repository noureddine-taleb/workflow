import { DragDropModule } from "@angular/cdk/drag-drop";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { CardModule } from "app/shared/card";
import { affectationRoutes } from "./affectation.routing";
import { AffectationComponent } from "./affectation/affectation.component";
import { SuperTableComponent } from './super-table/super-table.component';

@NgModule({
	declarations: [AffectationComponent, SuperTableComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(affectationRoutes),
		CardModule,
		DragDropModule,
		MatExpansionModule,
		MatTableModule,
		MatExpansionModule,
		MatPaginatorModule,
		MatIconModule,
		MatCheckboxModule,
		MatButtonModule,
		FormsModule,
	],
})
export class AffectationModule {}
