import { Route } from "@angular/router";
import { AffectationComponent } from "./affectation/affectation.component";

export const affectationRoutes: Route[] = [
	{
		path: "",
		component: AffectationComponent,
	},
];
