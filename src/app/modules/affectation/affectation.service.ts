import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class AffectationService {
	private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient, private authService: AuthService) {}

	getAssignment(param: {
		entityId: number;
		first: number;
		pas: number;
		userEntityId: number;
		userId: number;
	}): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/creditRequest/getAssignment", param);
	}

	assignment(
		dstUser: number,
		requestId: number,
		urlAssignment: string,
	): Observable<any> {
		return this.httpClient.post(this.demandesPath + urlAssignment, {
			actionUserId: this.authService.user.userId,
			userId: dstUser,
			requestId,
		});
	}
}
