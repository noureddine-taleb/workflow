import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { ThrowStmt } from "@angular/compiler";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { LayoutService } from "app/layout/services/layout.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { ApexOptions } from "ng-apexcharts";
import { forkJoin } from "rxjs";
import { AffectationService } from "../affectation.service";
import { AssignmentGroup } from "../assignment.types";

@Component({
	selector: "app-affectation",
	templateUrl: "./affectation.component.html",
	styleUrls: ["./affectation.component.scss"],
})
export class AffectationComponent implements OnInit {
	@ViewChild(MatPaginator)
	paginator: MatPaginator;
	assignements: AssignmentGroup[] = [];
	myAssignements: AssignmentGroup;
	// chart statut
	chartGender: ApexOptions;
	chartStatutData = {
		gender: {
			uniqueVisitors: 46085,
			series: [20, 5, 24, 36],
			labels: ["En cours", "Rejet�es", "En attente", "Deblocage"],
		},
	};

	listeDemande = [];
	connectedTo = [0];
	dropHandler = this.drop.bind(this);

	constructor(
		private affectationService: AffectationService,
		private userInfo: AuthService,
		private snackBar: SnackBarService,
		private layoutService: LayoutService
	) {
		this._prepareChartData();
	}

	ngOnInit(): void {
		this.layoutService.setTitle("Affectation")
	}

	// sorry I don't know any better :)
	bruteForceChangeDetection(requests: LoanRequest[]) {
		for (let assg of this.assignements) {
			for (let user of assg.users) {
				if (user.requests === requests) {
					user.requests = user.requests.slice();
					return true;
				}
			}
		}
		return false;
	}

	dragDropChangeDetection(event: CdkDragDrop<LoanRequest[]>) {
		let src = this.bruteForceChangeDetection(event.previousContainer.data)
		let dst = this.bruteForceChangeDetection(event.container.data)
		if (!src || !dst)
			this.myAssignements.users[0].requests = this.myAssignements.users[0].requests.slice();
	}

	drop(event: CdkDragDrop<LoanRequest[]>) {
		let src = event.previousContainer;
		let dst = event.container;
		let srcIndex = event.previousIndex;
		let dstIndex = event.currentIndex;

		if (src === dst) {
			moveItemInArray(dst.data, srcIndex, dstIndex);
			this.dragDropChangeDetection(event)
			return;
		}

		this.snackBar.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.affectationService
					.assignment(
						Number(dst.id),
						src.data[srcIndex].requestId,
						(src.data[srcIndex] as any).urlAssignment,
					)
					.subscribe(
						data => {
							transferArrayItem(
								src.data,
								dst.data,
								srcIndex,
								dstIndex,
							);
							this.dragDropChangeDetection(event)
							this.snackBar.openSuccesSnackBar({message: data.message})
						}
					);
			}
		});
	}

	getUserAssignements(userId: number): LoanRequest[] {
		if (userId == this.myAssignements.users[0].userId)
			return this.myAssignements.users[0].requests

		for (let group of this.assignements) {
			for (let user of group.users) {
				if (user.userId == userId)
					return user.requests;
			}
		}

		return null;
	}

	// assignRequest(src: CdkDropList, dst: CdkDropList, srci: number, dsti: number) {
	async assignBucketRequests(userId: number) {
		this.bucket = this.bucket.filter(req => {
			if (req.userId == userId) {
				(req as any).checked = false;
				return false;
			}
			return true;
		})

		if (this.bucket.length && await this.snackBar.openConfirmSnackBar({ message: 'all' })) {
			forkJoin(this.bucket.map(req => {
				return this.affectationService.assignment(userId, req.requestId, (req as any).urlAssignment)
			})).subscribe(data => {
				// let dst = this.getUserAssignements(userId);
				// for (let req of this.bucket) {
				// 	let src = this.getUserAssignements(req.userId);
				// 	transferArrayItem(
				// 		src,
				// 		dst,
				// 		src.indexOf(req),
				// 		dst.length
				// 	)
				// }
				this.loadListAssignment();
				this.bucket = []
				this.snackBar.openSuccesSnackBar({ message: data.map(d => d.message).join("<br/>")})
			})
		}
	}

	bucket: LoanRequest[] = [];
	bucketAddRequest(req: LoanRequest) {
		this.bucket.push(req);
	}

	bucketDeleteRequest(req: LoanRequest) {
		this.bucket = this.bucket.filter(r => {
			return r.requestId != req.requestId;
		})
	}

	// chart fuctions
	private _prepareChartData(): void {
		// Statut
		this.chartGender = {
			chart: {
				animations: {
					enabled: true,
					easing: "easeinout",
					speed: 800,
					animateGradually: {
						enabled: true,
						delay: 150,
					},
					dynamicAnimation: {
						enabled: true,
						speed: 350,
					},
				},
				fontFamily: "inherit",
				foreColor: "inherit",
				height: "100%",
				type: "donut",
				sparkline: {
					enabled: true,
				},

				events: {
					/*click: (event: any, chartContext: any, config: any) => {
                        this.clickEvent(config.dataPointIndex);
 
 
                        var test: number = config.dataPointIndex;
 
                    },*/
					dataPointSelection: (e: any, chart?: any, options?: any) => {
						this.clickEvent(
							options.dataPointIndex,
							options.w.config.labels[options.dataPointIndex],
						);
					},
				},
			},
			colors: ["#319795", "#cf442b", "#b9f507", "#a8ede9"],
			labels: this.chartStatutData.gender.labels,
			plotOptions: {
				pie: {
					customScale: 0.9,
					expandOnClick: true,
					donut: {
						size: "30%",
					},
				},
			},
			legend: {
				show: true,
				showForSingleSeries: false,
				showForNullSeries: true,
				showForZeroSeries: true,
				position: "bottom",
				horizontalAlign: "center",
				floating: false,
				fontSize: "14px",
				fontFamily: "Helvetica, Arial",
				fontWeight: 400,
				formatter: undefined,
				inverseOrder: false,
				width: undefined,
				height: undefined,
				tooltipHoverFormatter: undefined,
				customLegendItems: [],
				offsetX: 0,
				offsetY: 0,
				labels: {
					colors: undefined,
					useSeriesColors: false,
				},
				markers: {
					width: 12,
					height: 12,
					strokeWidth: 0,
					strokeColor: "#fff",
					fillColors: undefined,
					radius: 12,
					customHTML: undefined,
					onClick: undefined,
					offsetX: 0,
					offsetY: 0,
				},
				itemMargin: {
					horizontal: 5,
					vertical: 0,
				},
				onItemClick: {
					toggleDataSeries: true,
				},
				onItemHover: {
					highlightDataSeries: true,
				},
			},
			series: this.chartStatutData.gender.series,
			states: {
				hover: {
					filter: {
						type: "none",
					},
				},
				active: {
					filter: {
						type: "none",
					},
				},
			},
			tooltip: {
				enabled: true,
				fillSeriesColor: false,
				theme: "dark",
				custom: ({
					seriesIndex,
					w,
				}): string => `<div class="flex items-center h-8 min-h-8 max-h-8 px-3">
                                                     <div class="w-3 h-3 rounded-full" style="background-color: ${w.config.colors[seriesIndex]};"></div>
                                                     <div class="ml-2 text-md leading-none">${w.config.labels[seriesIndex]}:</div>
                                                     <div class="ml-2 text-md font-bold leading-none">${w.config.series[seriesIndex]}%</div>
                                                 </div>`,
			},
		};
	}

	clickEvent(idx: number, label: string) {}

	ngAfterViewInit() {
		this.loadListAssignment();

		this.paginator?.page.subscribe((event) => {
			//this.critere.pas = event.pageSize;
			//this.critere.first = event.pageIndex * event.pageSize;
			this.loadTaches(event.pageSize, event.pageIndex * event.pageSize);
		});
	}

	//load liste des tache correspodant au critere page index et page size
	loadTaches(pageSize, firstItemIndex) {
		return (this.listeDemande = this.listeDemande);
	}

	loadListAssignment() {
		this.listeDemande = [];
		this.connectedTo = [0];
		this.affectationService
			.getAssignment({
				entityId: this.userInfo.user.entityId,
				first: 0,
				pas: 10,
				userEntityId: this.userInfo.user.entityId,
				userId: this.userInfo.user.userId,
			})
			.subscribe((data) => {
				console.log(data);
				let assignementGroup: AssignmentGroup[] = data.result;
				for (let assg of assignementGroup) {
					for (let [i, user] of assg.users.entries()) {
						if (user.userId === this.userInfo.user.userId) {
							assg.users.splice(i, 1)
							this.myAssignements = {
								profilId: -1,
								profilName: "Mes Demandes",
								profilOrder: -1,
								users: [user]
							}
							break
						}
					}
					if (this.myAssignements)
						break;
				}

				for(let assg of assignementGroup) {
					assg.users = assg.users.sort((a, b) => a.requests.length - b.requests.length)
				}

				this.assignements = assignementGroup;
			});
	}
}
