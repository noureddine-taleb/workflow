import { LoanRequest } from "app/modules/demandes/models/LoanRequest";

// export interface Assignment {
// 	id?: number;
// 	userId: number;
// 	userName: string;
// 	entityId: number;
// 	entityName?: any;
// 	profilId: number;
// 	profilCode: string;
// 	profilName: string;
// 	actionCode?: any;
// 	rowsNumber?: number;
// 	requests?: DemandesAssignment[];
// }

export interface AssignmentGroup {
	profilId: number,
	profilName: string,
	profilOrder: number,
	users: AssignmentUser[]
}

export interface AssignmentUser {
	userId: number;
	userName: string;
	entityId: number;
	entityName: string;
	profilId: number;
	profilCode: string;
	profilName: string;
	actionCode: string;
	requests: LoanRequest[];
}

// export interface DemandesAssignment {
// 	id?: any;
// 	statusColor?: string;
// 	statusName?: string;
// 	statusCode?: any;
// 	requestId?: number;
// 	requestNumber?: string;
// 	simulationId?: any;
// 	creditTypeId?: number;
// 	creditTypeName?: string;
// 	goodLocationCode?: string;
// 	goodLocationName?: string;
// 	financialNatureCode?: any;
// 	financialNatureName?: any;
// 	activityStatusCode?: any;
// 	activityStatusName?: any;
// 	segmentId?: any;
// 	segmentName?: any;
// 	organismId?: any;
// 	organismName?: any;
// 	conventionId?: any;
// 	conventionName?: any;
// 	accountNumber?: string;
// 	accountProduct?: any;
// 	seniority?: any;
// 	requestAmount?: any;
// 	productsNumber?: any;
// 	monthlyPay?: number;
// 	monthlyPaymentImmo?: number;
// 	monthlyPaymentImmoCam?: number;
// 	monthlyPaymentImmoConf?: number;
// 	othersMonthlyIncome?: number;
// 	monthlyPaymentConso?: number;
// 	monthlyPaymentConsoCam?: number;
// 	monthlyPaymentConsoConf?: number;
// 	entityName?: string;
// 	currentEntityId?: any;
// 	requestObject?: string;
// 	userId?: number;
// 	userName?: string;
// 	profilId?: number;
// 	profilName?: string;
// 	creationDate?: Date;
// 	assignmentDate?: Date;
// 	globalDuration?: number;
// 	stayDuration?: number;
// 	personId?: any;
// 	monthlyPaySim?: any;
// 	monthlyPaymentImmoSim?: any;
// 	monthlyPaymentImmoCamSim?: any;
// 	monthlyPaymentImmoConfSim?: any;
// 	othersMonthlyIncomeSim?: any;
// 	monthlyPaymentConsoSim?: any;
// 	monthlyPaymentConsoCamSim?: any;
// 	monthlyPaymentConsoConfSim?: any;
// 	fullName?: any;
// 	adress?: any;
// 	phoneNumber?: any;
// 	birthDate?: any;
// 	identityClient?: any;
// 	clientCategoryName?: string;
// 	clientCategoryId?: number;
// 	urlAssignment?: string;
// 	products?: any;
// 	events?: any;
// }

// export interface RequestObject {
// 	entityId: number;
// 	first: number;
// 	pas: number;
// 	requestId: number;
// 	userEntityId: number;
// 	userId: number;
// }