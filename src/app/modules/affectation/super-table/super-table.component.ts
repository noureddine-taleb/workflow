import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { LoanRequest } from 'app/modules/demandes/models/LoanRequest';

@Component({
  selector: 'app-super-table',
  templateUrl: './super-table.component.html',
  styleUrls: ['./super-table.component.scss']
})
export class SuperTableComponent implements OnInit, OnChanges, AfterViewInit {

	@Input() dropList: LoanRequest[];
	@Input() drop: any;
	@Input() id: any;

	@Output() check = new EventEmitter<LoanRequest>();
	@Output() uncheck = new EventEmitter<LoanRequest>();
	
	@ViewChild(MatPaginator)
	paginator: MatPaginator;
	
	dataSource: MatTableDataSource<LoanRequest>;

	constructor() { }

	ngOnInit(): void {
		this.initDataSource()
	}

	checkboxClicked(e: boolean, req: LoanRequest) {
		if (e)
			this._check(req)
		else
			this._uncheck(req)
	}

	toggleAll(checked: boolean) {
		for (let req of this.dropList) {
			(req as any).checked = checked
			this.checkboxClicked(checked, req);
		}
		this.initDataSource()
	}


	_check(req: LoanRequest) {
		this.check.emit(req);
	}

	_uncheck(req: LoanRequest) {
		this.uncheck.emit(req);
	}

	checkedCount() {
		return this.dropList.filter(r => (r as any).checked).length;
	}

	ngOnChanges(changes: SimpleChanges): void {
		for (let k of Object.keys(changes)) {
			this[k] = changes[k].currentValue
		}

		if ("dropList" in changes)
			this.initDataSource();
	}

	initDataSource() {
		this.dataSource = new MatTableDataSource(this.dropList);
		this.dataSource.paginator = this.paginator;
	}

	ngAfterViewInit(): void {
		this.initDataSource()
	}
}
