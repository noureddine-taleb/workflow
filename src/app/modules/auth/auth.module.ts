import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { AlertModule } from 'app/shared/alert';
import { CardModule } from 'app/shared/card';
import { AuthInterceptor } from '../../core/interceptors/auth.interceptor';
import { authRoutes } from './auth.routing';
import { AuthSignInComponent } from './sign-in/sign-in.component';
import { AuthSignOutComponent } from './sign-out/sign-out.component';



@NgModule({
  declarations: [AuthSignInComponent, AuthSignOutComponent],
  imports: [
	CommonModule,
	RouterModule.forChild(authRoutes),
	MatButtonModule,
	MatCheckboxModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatProgressSpinnerModule,
	ReactiveFormsModule,
	AlertModule,
	CardModule,
  ],
})
export class AuthModule { }
