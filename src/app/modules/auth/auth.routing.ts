import { Route } from "@angular/router";
import { AuthSignInComponent } from "app/modules/auth/sign-in/sign-in.component";
import { AuthGuard } from "./guards/auth.guard";
import { NoAuthGuard } from "./guards/noAuth.guard";
import { AuthSignOutComponent } from "./sign-out/sign-out.component";

export const authRoutes: Route[] = [
	{
		path: "sign-in",
		component: AuthSignInComponent,
	},
	{
		path: "sign-out",
		component: AuthSignOutComponent,
	},
];
