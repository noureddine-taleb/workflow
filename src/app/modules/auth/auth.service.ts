import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';
import { User } from "app/shared/models/User";
import { Observable, of } from "rxjs";
import { switchMap, tap } from "rxjs/operators";
import { AuthUtils } from "./auth.utils";

const TOKEN_KEY = "AuthToken";
const REF_KEY = "RefToken";
const GT_KEY = "GtToken";
const USER_KEY = "AuthUser";
export type TokenType = "gt" | "ref" | "";

@Injectable({
	providedIn: "root",
})
export class AuthService {

	private baseWKFUrl = ConfigService.settings.apiServer.restWKFUrl + `/auth/`;
	private refTokenUrl = ConfigService.settings.apiServer.restWKFUrl + "/token/ref";
	private gtTokenUrl = ConfigService.settings.apiServer.restWKFUrl + "/token/gt";

	/**
	 * Constructor
	 */
	constructor(
		private _httpClient: HttpClient,
		private _router: Router,
	) {}

	/**
	 * Setter & getter for access token
	 */
	set accessToken(token: string) {
		sessionStorage.setItem(TOKEN_KEY, token);
	}

	get accessToken(): string {
		return sessionStorage.getItem(TOKEN_KEY);
	}

	/**
	 * Setter & getter for gt token
	 */
	set gtToken(token: string) {
		sessionStorage.setItem(GT_KEY, token);
	}

	get gtToken(): string {
		return sessionStorage.getItem(GT_KEY);
	}

	/**
	 * Setter & getter for ref token
	 */
	set refToken(token: string) {
		sessionStorage.setItem(REF_KEY, token);
	}

	get refToken(): string {
		return sessionStorage.getItem(REF_KEY);
	}

	set user(user: User) {
		if (user) {
			user.userId = user.identifiant;
			user.userEntityId = user.entityId;
		}

		window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
	}

	get user(): User {
		return JSON.parse(sessionStorage.getItem(USER_KEY));
	}

	/**
	 * Check the authentication status
	 */
	check(): Observable<boolean> {
		return of(this.isAuth());
	}

	isAuth() {
		return this.accessToken && !AuthUtils.isTokenExpired(this.accessToken);
	}

	signIn(credential: { ussername: string; password: string }): Observable<any> {
		const credentials = credential;
		return this._httpClient.post(this.baseWKFUrl + "signin", credentials);
	}

	signOut(redirect = true) {
		this.user = null;
		window.sessionStorage.clear();
		if (redirect)
			this._router.navigate(["/sign-out"]);
	}

	saveCreds(result: { token: string; user: User }) {
		this.accessToken = result.token;
		this.user = result.user;
	}

	getToken(type: TokenType) {
		switch (type) {
			case "gt":
				if (!this.gtToken) {
					return this.getGtToken().pipe(
						switchMap(_ => of(this.gtToken))
					)
				}
				return of(this.gtToken);

			case "ref":
				if (!this.refToken) {
					return this.getRefToken().pipe(
						switchMap(_ => of(this.refToken))
					)
				}
				return of(this.refToken);
			
			default:
				return of(this.accessToken);
		}
	}
	
	/**
	 * you should never need to use those methods directly,
	 * because all is handled through an interceptor.
	 *
	 * @param token
	 */
	private saveTokenRef(token: string) {
		this.refToken = token;
	}
	
	private saveTokenGt(token: string) {
		this.gtToken = token;
	}

	private getRefToken() {
		return this._httpClient.get<ApiResponse<string>>(this.refTokenUrl).pipe(
			tap(data => {
				this.saveTokenRef(data.result);
			})
		)
	}

	private getGtToken() {
		return this._httpClient.get<ApiResponse<string>>(this.gtTokenUrl).pipe(
			tap(data => {
				this.saveTokenGt(data.result);
			})
		)
	}
}
