import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, NgForm, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertType } from "app/shared/alert";
import { animations } from "app/shared/animations";
import { AuthService } from 'app/modules/auth/auth.service';

@Component({
	selector: "auth-sign-in",
	templateUrl: "./sign-in.component.html",
	styleUrls: ["./sign-in.component.scss"],
	encapsulation: ViewEncapsulation.None,
	animations: animations,
})
export class AuthSignInComponent implements OnInit {
	@ViewChild("signInNgForm") signInNgForm: NgForm;

	alert: { type: AlertType; message: string } = {
		type: "success",
		message: "",
	};
	signInForm: FormGroup;
	showAlert: boolean = false;

	constructor(
		private _activatedRoute: ActivatedRoute,
		private _authService: AuthService,
		private _formBuilder: FormBuilder,
		private _router: Router,
		private token: AuthService,
	) {}

	ngOnInit(): void {
		// Create the form
		this.signInForm = this._formBuilder.group({
			username: ["", Validators.required],
			password: ["", Validators.required],
		});

		this._authService.signOut(false);
	}

	signIn(): void {
		// Return if the form is invalid
		if (this.signInForm.invalid) {
			return;
		}

		// Disable the form
		this.signInForm.disable();

		// Hide the alert
		this.showAlert = false;

		// Sign in
		this._authService.signIn(this.signInForm.value).subscribe(
			(data) => {
				if (data.result != null) {
					this.token.saveCreds(data.result);
					const redirectURL =
						this._activatedRoute.snapshot.queryParamMap.get("redirectURL") ||
						"/signed-in-redirect";
					this._router.navigateByUrl(redirectURL);
				}
			},
			err => {
				// Re-enable the form
				this.signInForm.enable();

				// Reset the form
				this.signInNgForm.resetForm();

				// Set the alert
				this.alert = {
					type: "error",
					message: "Le nom d'utilisateur ou le mot de passe est incorrect",
				};

				// Show the alert
				this.showAlert = true;
			},
		);
	}
}
