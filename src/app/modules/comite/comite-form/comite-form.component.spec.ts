import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ComiteFormComponent } from "./comite-form.component";

describe("ComiteAddComponent", () => {
	let component: ComiteFormComponent;
	let fixture: ComponentFixture<ComiteFormComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ComiteFormComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ComiteFormComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
