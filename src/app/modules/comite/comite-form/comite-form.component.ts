import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { ComponentState } from "app/core/models/ComponentState";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DemandeDetailComponent } from "app/shared/demande-detail/demande-detail.component";
import { forkJoin, Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { SimulationGtService } from "../../simulation/services/simulation.gt.service";
import { CommiteServices } from "../services/commite.service";

@Component({
	selector: "app-comite-form",
	templateUrl: "./comite-form.component.html",
	styleUrls: ["./comite-form.component.scss"],
})
export class ComiteFormComponent implements OnInit {
	comiteEntet = new CommiteeDto();
	objects = [];
	isReady: boolean = false;
	options = [];
	filteredOptions: Observable<User[]>[] = [];
	myForm: FormGroup;
	state: ComponentState;

	constructor(
		private fb: FormBuilder,
		private routingService: RoutingService,
		private route: ActivatedRoute,
		private comiteService: CommiteServices,
		private snackService: SnackBarService,
		private demandeService: DemandeService,
		private simGTServ: SimulationGtService,
		private dialog: MatDialog,
	) { }

	usersList = []
	ngOnInit(): void {
		this.getUsers();
		this.state = this.route.snapshot.data.state;
		this.createForm();
		this.getResolvedParam();
	}

	qualitys = [];
	extractQuality(qualites) {
		this.qualitys = qualites["result"];
	}

	listDmd = [];
	getDemandes() {
		this.simGTServ.getListDemandeByStatut("DC_COM_IN").subscribe((data) => {
			this.todo = data["result"];
		});
	}

	getUsers() {
		this.comiteService.getUsers().subscribe((data) => {
			this.usersList = data["result"]
			//this.options = data["result"].map((res) => ({
			//	participantId: res.identifiant,
			//	name: res.fullName,
			//	quality: "all",
			//}));
		});
	}

	bucket: LoanRequest[] = [];
	checkboxClicked(e: boolean, req: LoanRequest) {
		if (e)
			this.bucket.push(req)
		else
			this.bucket = this.bucket.filter(r => r.requestId != req.requestId);
	}
	
	toggleAll(checked: boolean, list: LoanRequest[]) {
		for (let req of list) {
			(req as any).checked = checked
			this.checkboxClicked(checked, req);
		}
	}

	checkedCount(list: LoanRequest[]) {
		return list.filter(r => (r as any).checked).length;
	}

	deleteRequestFromTheCommittee(req: LoanRequest) {
		return this.comiteService.deleteCommitRequest(
				this.comiteEntet.committeeId,
				req.requestId,
				req["committeeRequestId"],
			)
	}

	addRequestToTheCommittee(req: LoanRequest) {
		return this.comiteService.updateCommitRequest(
			this.comiteEntet.committeeId,
			req.requestId,
		)
	}

	async moveRequests(dstList: LoanRequest[]) {
		this.bucket = this.bucket.filter(r => {
			if (dstList.includes(r)) {
				(r as any).checked = false;
				return false;
			}
			return true;
		})

		if (!this.bucket.length || !this.comiteEntet.committeeId || !await this.snackService.openConfirmSnackBar({ message: "all" }))
			return;

		if (dstList == this.todo) {
			//suppression de la demande
			forkJoin(this.bucket.map(req => this.deleteRequestFromTheCommittee(req)))
			.subscribe((data) => {
				for (let req of this.bucket) {
					req["committeeRequestId"] = null;
					transferArrayItem(this.done, this.todo, this.done.indexOf(req), this.todo.length)
				}
				this.snackService.openSuccesSnackBar({
					message: data.map(d => d.message).join("<br/>"),
				});
			})
		} else if (dstList == this.done) {
			forkJoin(this.bucket.map(req => this.addRequestToTheCommittee(req)))
			.subscribe((data) => {
				
				for (let [i, req] of this.bucket.entries()) {
					req["committeeRequestId"] = data[i].result;
					transferArrayItem(this.todo, this.done, this.todo.indexOf(req), this.done.length)
				}

				this.snackService.openSuccesSnackBar({
					message: data.map(d => d.message).join("<br/>"),
				});
			})
		}
	}

	getResolvedParam() {
		let sub = this.route.data.subscribe((result) => {
			this.extractUsers(result.resolvedParam["users"]);
			this.extractComite(result.resolvedParam["commite"]);
			this.extractDemande(result.resolvedParam["demandes"]);
			this.extractTypeCommite(result.resolvedParam["typecomites"]);
			this.extractQuality(result.resolvedParam["qualites"]);
		});
	}
	extractUsers(users) {
		this.options = users["result"].map((res) => ({
			participantId: res.identifiant,
			name: res.fullName,
			quality: "all",
		}));
	}

	extractTypeCommite(data) {
		this.objects = data["result"];
	}

	extractComite(comite) {
		if (comite["result"] == -1) {
			//this.comiteEntet=new CommiteeDto();
		} else {
			this.comiteEntet = comite["result"];
			this.comiteEntet["committeeType"] = this.comiteEntet["committeTypeCode"];
			this.done = this.comiteEntet.requests;
			//let participants = this.myForm.get("items") as FormArray;
			//let participantsValue = this.comiteEntet["participants"].map((x) => ({
			//	participantId: x["participantId"],
			//	quality: x["qualityCode"],
			//}));

			//if (participantsValue.length > 1) {
			//	for (let i = 0; i < participantsValue.length - 1; i++) {
			//		let formGroup = this.fb.group({
			//			participantId: ["", [Validators.required]],
			//			quality: ["", [Validators.required]],
			//		});
			//
			//		participants.push(formGroup);
			//	}
			//}

			//this.myForm.get("items").setValue(participantsValue);
			if (this.comiteEntet.participants.length == 0) {
				this.comiteEntet.participants.push(new Intervenant())
			}
		}
	}

	extractDemande(data) {
		this.todo = data["result"];
	}

	createForm() {
		this.myForm = this.fb.group({
			date: [{ value: "", disabled: true }, [Validators.required]],
			notes: [""],
			items: this.initItems(),
		});
		this.ManageNameControl(0);
		this.isReady = true;
	}

	initItems() {
		var formArray = this.fb.array([]);

		//for (let i = 0; i < 2; i++) {
		formArray.push(
			this.fb.group({
				participantId: ["", [Validators.required]],
				quality: ["", [Validators.required]],
			}),
		);
		//}
		return formArray;
	}

	ManageNameControl(index: number) {
		var arrayControl = this.myForm.get("items") as FormArray;
		this.filteredOptions[index] = arrayControl
			.at(index)
			.get("participantId")
			.valueChanges.pipe(
				startWith<string | User>(""),
				map((value) => (typeof value === "string" ? value : value.name)),
				map((name) => (name ? this._filter(name) : this.options.slice())),
			);
	}
	addNewItem() {
		//const controls = <FormArray>this.myForm.controls["items"];
		//let formGroup = this.fb.group({
		//	participantId: ["", [Validators.required]],
		//	quality: ["", [Validators.required]],
		//});
		let participant = new Intervenant();
		this.comiteEntet.participants.push(participant)
		//controls.push(formGroup);
		// Build the account Auto Complete values
		//this.ManageNameControl(controls.length - 1);
	}
	removeItem(item, index) {
		if (this.comiteEntet.committeeId) {
			//const participant = (<FormArray>this.myForm.get("items")).at(index).value;
			//const part = this.comiteEntet.participants.find(
			//	(x) => x["participantId"] == participant["participantId"],
			//);
			if (item.committeeParticipantId) {
				let critere = {};
				critere = {
					committeeId: this.comiteEntet.committeeId,
					committeeParticipantId: item.committeeParticipantId,
					participantId: item.participantId,
				};
				this.comiteService.deleteParticipant(critere).subscribe((data) => {
					this.snackService.openSuccesSnackBar({ message: data["message"] });
					//const controls = <FormArray>this.myForm.controls["items"];
					//controls.removeAt(index);
					// remove from filteredOptions too.
					//this.filteredOptions.splice(i, 1);
					this.comiteEntet.participants.splice(index, 1);
				});
			} else {
				//const controls = <FormArray>this.myForm.controls["items"];
				//controls.removeAt(index);
				// remove from filteredOptions too.
				//this.filteredOptions.splice(i, 1);
				this.comiteEntet.participants.splice(index, 1);
			}
		} else {
			//const controls = <FormArray>this.myForm.controls["items"];
			//controls.removeAt(index);
			// remove from filteredOptions too.
			this.comiteEntet.participants.splice(index, 1);
			this.filteredOptions.splice(index, 1);
		}
	}

	addEdit = 1;

	date = new FormControl(null, [Validators.required]);


	displayFn(user): string | undefined {
		return user ? user : undefined;
	}

	private _filter(name: string): User[] {
		const filterValue = name.toLowerCase();
		return this.options.filter(
			(option) => option.name.toLowerCase().indexOf(filterValue) === 0,
		);
	}

	compareObjects(o1: any, o2: any) {
		if (o1 == o2) return true;
		else return false;
	}

	// drag and drop////////////////////////////////////////////////////////////////////
	todo = [];

	done = [];

	getTitle(userId) {
		return this.options.find((user) => user.participantId === userId)?.name;
	}
	//TODO
	//apres suppression de le demande du comite ,la demande ne s'affiche plus dans les demandes a programmer

	drop(event: CdkDragDrop<LoanRequest[]>) {
		this.todo = this.todo ? this.todo : [];
		this.done = this.done ? this.done : [];

		if (!this.comiteEntet.committeeId)
			return;
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			this.snackService.openConfirmSnackBar({ message: "all" }).then((result) => {
				if (result.value) {

					if (event.previousContainer.id == "cdk-drop-list-1") {
						//suppression de la demande
						let request = event.previousContainer.data[event.previousIndex];

						this.deleteRequestFromTheCommittee(request)
						.subscribe((data) => {
							request["committeeRequestId"] = null;
							transferArrayItem(
								event.previousContainer.data,
								event.container.data,
								event.previousIndex,
								event.currentIndex,
							);
							this.snackService.openSuccesSnackBar({
								message: data["message"],
							});
						});
					} else if (event.previousContainer.id == "cdk-drop-list-0") {
						let request = event.previousContainer.data[event.previousIndex];

						this.addRequestToTheCommittee(request)
						.subscribe((data) => {
								request["committeeRequestId"] = data.result;
								transferArrayItem(
									event.previousContainer.data,
									event.container.data,
									event.previousIndex,
									event.currentIndex,
								);
								this.snackService.openSuccesSnackBar({
									message: data["message"],
								});
							});
					}
				}
			});
		}
	}

	afficher() {
		console.warn(this.myForm.value);
	}

	onchangeEvent($event: any) {
		console.warn($event);
	}
	onRetour() {
		this.snackService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.routingService.gotoComites();
			}
		});
	}

	onSaveCommite() {
		console.log(this.comiteEntet["participants"])
		this.snackService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				//this.comiteEntet["participants"] = this.myForm.get("items").value;
				this.comiteEntet["participants"].forEach(element => {
					element.quality = element['qualityCode']

				});
				let requestId: Request[] = [];

				this.done.forEach((request) => {
					requestId.push({
						requestId: request["requestId"],
						committeeRequestId: request.committeeRequestId
							? request.committeeRequestId
							: null,
					});
				});
				this.comiteEntet["requests"] = requestId;
				var filterArray = this.comiteEntet.participants.reduce((accumalator, current) => {
					if (!accumalator.some(
						(item) => item.participantId === current.participantId
					)
					) {
						accumalator.push(current);

					}
					return accumalator;
				}, []);
				this.comiteEntet.participants = filterArray
				console.log(this.comiteEntet)

				this.comiteService.saveComite(this.comiteEntet).subscribe((data) => {
					this.snackService.openSuccesSnackBar({ message: data["message"] });
					this.routingService.gotoComites()
				});
			}
		});
	}

	demandeDetail;
	ondemandDetail(demande: { requestId: number }) {
		this.demandeService.getDemande(demande.requestId).subscribe((data) => {
			this.demandeDetail = data.result;
			this.demandeService.getTiers(demande.requestId).subscribe((tier) => {
				this.dialog.open(DemandeDetailComponent, {
					data: { demande: this.demandeDetail, tiers: tier.result?.results },
					width: "90vw",
				});
			});
		});
	}
}

interface User {
	name: string;
}

export class ComiteEntete {
	objet: string;
	date: Date;
	description: string;
}

export class Intervenant {
	committeeId: number = null;
	committeeParticipantId: number = null;
	description: string = null;
	participantId: number = null;
	quality: string = null;
	signatureDate: Date = null;
	signing: string = null;
	userEntityId: number = null;
	userId: number = null;
}

export class CommiteeDto {
	committeeDate?: Date;
	committeeId?: number;
	committeeType?: string;
	description?: string;
	participants?: Intervenant[] = [];
	requests?: Request[];
	userEntityId?: number;
	userId?: number;
}
export class Request {
	requestId: number;
	committeeRequestId: number;
}
