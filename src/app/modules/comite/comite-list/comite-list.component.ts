import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { ActivatedRoute } from "@angular/router";
import { RoutingService } from "app/core/services/routing.service";
import { LayoutService } from "app/layout/services/layout.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CommiteeCDK } from "../commiteeCDK";
import { CommiteServices } from "../services/commite.service";
import {ReportGeneratorService} from "../../../shared/reportGenerator/report-generator.service";

@Component({
	selector: "app-comite-list",
	templateUrl: "./comite-list.component.html",
	styleUrls: ["./comite-list.component.scss"],
})
export class ComiteListComponent implements OnInit {
	critere = { first: 0, pas: 10 };
	constructor(
		private routingService: RoutingService,
		private _activatedRoute: ActivatedRoute,
		private commiteeServ: CommiteServices,
		private snackBarService: SnackBarService,
		private layoutService: LayoutService,
        private reportGeneratorService: ReportGeneratorService
	) {}
	displayedColumns = [];

	ngOnInit(): void {
		this.layoutService.setTitle("Liste des Comités")
		this.loadListComite();
	}

	listCommitees;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	//integration cdk datasource
	ngAfterViewInit() {
		this.paginator.page.subscribe((event) => {
			this.critere.pas = event.pageSize;
			this.critere.first = event.pageIndex * event.pageSize;
			this.loadListComite();
		});
	}

	//////web service call
	dataSource: CommiteeCDK;
	loadListComite() {
		this.dataSource = new CommiteeCDK(this.commiteeServ, this.snackBarService);
		this.dataSource.loadCommite(this.critere);
		this.displayedColumns = [
			"committeTypeName",
            "NPV",
			"committeeDate",
			"NBRDEMANDE",
			"TRAITE",
			"NONTRAITE",
			"description",
			"statusName",
			"ACTION",
		];
	}

	onreset() {
		this.critere = { pas: 10, first: 0 };
	}

	onTraiter(element) {
		this.routingService.gotoComite(element["committeeId"])
	}

    onEditePvComite(element): void {
        console.log(element);
            this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
                if (result.value) {
                    const criteria = {};
                    criteria['report'] = 'PVCOMITE';
                    criteria['committeeId'] = element['committeeId'];

                    this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                        this.reportGeneratorService.convertToPdf(data.result,('PV_COMITE_'+element['committeeId'])) ;
                    });
                }
            });
    }

	onEdit(element) {
		this.routingService.gotoComiteEdit(element["committeeId"]);
	}

	onAdd() {
		this.routingService.gotoComiteAdd();
	}
}
