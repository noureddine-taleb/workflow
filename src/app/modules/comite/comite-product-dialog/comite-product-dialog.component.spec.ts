import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ComiteProductDialogComponent } from "./comite-product-dialog.component";

describe("ComiteProductDialogComponent", () => {
	let component: ComiteProductDialogComponent;
	let fixture: ComponentFixture<ComiteProductDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ComiteProductDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ComiteProductDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
