import { Component, Input, OnInit } from "@angular/core";
import { MatCheckboxChange } from "@angular/material/checkbox";
import { MatDialog } from "@angular/material/dialog";
import { CommiteServices } from "app/modules/comite/services/commite.service";
import { EtudeAddComponent } from "app/modules/demandes/etude-add/etude-add.component";
import { CautionAddComponent } from "app/shared/caution-add/caution-add.component";
import { RequestProduct } from "app/shared/models/RequestProduct";
import { take } from "rxjs/operators";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Guaranty } from "app/modules/demandes/constitution/constitution.types";

@Component({
	selector: "app-comite-product-dialog",
	templateUrl: "./comite-product-dialog.component.html",
	styleUrls: ["./comite-product-dialog.component.scss"],
})
export class ComiteProductDialogComponent implements OnInit {
	fiancialObjectDto: Guaranty;
	displayedColumns: string[] = [
		"check",
		"reference",
		"natureName",
		"value",
		"annule",
		"annulationDate",
		"rank",
		"action",
	];
	cautionDisplayedColumns: string[] = [
		" ",
		"firstName",
		"lastName",
		"reference",
		"birthDate",
		"adress",
		"annule",
		"annulationDate",
		"action",
	];

	garantyType;
	constructor(
		private snakBarService: SnackBarService,
		private communService: CommonService,
		private comiteService: CommiteServices,
		private dialog: MatDialog,
	) { }

	listPack: RequestProduct[] = [];

	@Input() product;
	@Input() demande;
	@Input() typeCredit;
	@Input() comite;
	ngOnInit(): void {
		console.log(this.demande.committeeStatus)
		this.getListPack();
		for (let g of this.product.guaranties) {
			if (g.productId) g.checked = true;
		}
		this.getTypeGarantie();
		this.getUnlockType()
		this.getReleaseMode()
	}

	getTypeGarantie(): void {
		this.communService
			.getTypeGarantie(this.demande["creditTypeId"])
			.pipe(take(1))
			.subscribe((data) => (this.garantyType = data["result"]));
	}

	__setProductId(element) {
		element.productId = this.product.productId;
	}

	setProductId(element) {
		if (element.checked) this.__setProductId(element);
		else element.productId = null;
	}


	noSimulation() {
		let product = this.listPack.find(p => p.financialProductId === this.product.simulationFlag);
		return product?.simulationFlag === 'N';
	}

	onSave() {
		let comite = {
			committeeId: this.comite.committeeId,
			committeeRequestId: this.comite.committeeRequestId,
			mode: "",
			product: this.product,
			requestId: this.demande.requestId,
		}
		//for (let p of this.demande.products) {
		//	if (p.productId == this.product.productId) {
		//		p.guaranties = this.product.guaranties
		//	}
		//}
		//this.product.guarantie
		//comite.advice['products'] = this.demande.products

		console.log(comite)

		this.snakBarService.openConfirmSnackBar({ message: "all" })
			.then((result) => {
				if (result.value) {
					this.comiteService.traiterProduit(comite).subscribe(
						data => {
							console.log(data)
							this.snakBarService.openSuccesSnackBar({ message: data['message'] })
							//this.dialogRef.close();
						}
					)

				}
			})
	}

	showOptions($event: MatCheckboxChange) {
		this.product["contracted"] = $event.checked ? "O" : "N";
	}

	getListPack() {
		let critere = { creditType: this.typeCredit };

		this.communService.getPacks(critere).subscribe((data) => {
			this.listPack = data["result"];
		});
	}

	addGuaranty(requestId: number) {
		this.fiancialObjectDto = new Guaranty();
		this.fiancialObjectDto.requestId = requestId;

		const dialogRef = this.dialog.open(EtudeAddComponent, {
			data: {
				objet: this.fiancialObjectDto,
				casTraite: 2,
				listType: this.garantyType,
				save: false,
			},
			width: "90vw",
		});

		dialogRef.afterClosed().subscribe((data) => {
			if (!data) return;
			data["checked"] = true;
			this.setProductId(data);
			this.product.guaranties = [...this.product.guaranties, data];
		});
	}

	addCaution() {
		const dialogRef = this.dialog.open(CautionAddComponent, {
			data: {
				save: false,
			},
			width: "90vw",
		});

		dialogRef.afterClosed().subscribe((data) => {

			if (!data) return;
			data["checked"] = true;
			this.setProductId(data);
			this.product.deposits = [...this.product.deposits, data];
		});
	}

	deleteGuaranty(e) {
		this.product.guaranties = this.product.guaranties.filter((g) => g != e);
	}

	deleteCaution(e) {
		this.product.deposits = this.product.deposits.filter((c) => c != e);
	}

	unlockTypeList = []
	getUnlockType() {
		let params = {

		}
		this.comiteService.getMethod('/ref/unlockType').subscribe(
			(response) => {
				this.unlockTypeList = response.result
				console.log(response)
			}, (error) => {
			}, () => {
			})
	}
	releaseModeList = []
	getReleaseMode() {
		let params = {
		}
		this.comiteService.getMethod('/ref/releaseMode ').subscribe(
			(response) => {
				this.releaseModeList = response.result
				console.log(response)
			}, (error) => {
			}, () => {
			})
	}

}
