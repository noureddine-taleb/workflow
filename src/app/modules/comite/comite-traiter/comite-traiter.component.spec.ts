import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ComiteTraiterComponent } from "./comite-traiter.component";

describe("ComiteTraiterComponent", () => {
	let component: ComiteTraiterComponent;
	let fixture: ComponentFixture<ComiteTraiterComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ComiteTraiterComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ComiteTraiterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
