import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from 'app/modules/auth/auth.service';
import { CreationType } from "app/shared/models/CreationType";
import { RequestType } from "app/shared/models/RequestType";
import { Subscription } from "rxjs";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Comite } from "../comite.types";
import { ProductHistoryComponent } from "../product-history/product-history.component";
import { CommiteServices } from "../services/commite.service";
import { CommonService } from "app/core/services/common.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import {ReportGeneratorService} from "../../../shared/reportGenerator/report-generator.service";

@Component({
	selector: "app-comite-traiter",
	templateUrl: "./comite-traiter.component.html",
	styleUrls: ["./comite-traiter.component.scss"],
})
export class ComiteTraiterComponent implements OnInit {
	commiteDto = new Comite();
	subsription = new Subscription();
	editMode = 0;
	adviceCodes = []
	dirty = false;
	requestTypes = RequestType;
	creationTypes: CreationType[] = []

	constructor(
		private dialog: MatDialog,
		private route: ActivatedRoute,
		private _router: Router,
		private snackBarService: SnackBarService,
		private demandeService: DemandeService,
		private authService: AuthService,
		private comiteService: CommiteServices,
		public commonService: CommonService,
        private reportGeneratorService: ReportGeneratorService,
	) {
		this.getResolvedParam();
	}

	userConnected: any = {}
	ngOnInit(): void {
		this.commonService.getAdviceCodes().subscribe(data => {
			this.adviceCodes = data.result;
		});
		this.getCreationTypes()
		this.userConnected = this.authService.user;
	}


	getResolvedParam() {
		let sub = this.route.data.subscribe((result) => {
			this.commiteDto = result.resolvedParam["result"];
			this.comite["committeeId"] = this.commiteDto["committeeId"];
			this.comite["userEntityId"] = this.authService.user.userEntityId;
			this.comite["userId"] = this.authService.user.userId;
			this.comite["entityId"] = this.authService.user.entityId;
		});
		this.subsription.add(sub);
	}

	displayedCol = ["numdmd", "date", "compteName", "type", "requestObject", "statusCode", "action"];
	displayedColLibelle = ["N°Demande", "Date création", "Intitulé Compte", "Type crédit", "Objet", "Statut", ""];

	comite = {};
	demande: any;
	tiers: any;
	typeCredit = 0;
	selectedReqeust: any = {}
	onEdit(demande) {
		this.selectedReqeust = { ...demande }
		this.comite["committeeRequestId"] = demande["committeeRequestId"];
		this.comite["requestId"] = demande["requestId"];
		this.demande = demande;
		let critere = {
			requestId: demande.requestId,
			committeeId: this.commiteDto["committeeId"],
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
			entityId: this.authService.user.entityId,
		};
		let sub = this.comiteService.getDemande(critere).subscribe((data) => {
			this.demande = data["result"];
			this.advice["opinion"] = this.demande["opinion"];
			this.advice["guarantees"] = this.demande["guarantees"];
			this.advice["conditionsReserves"] = this.demande["conditionsReserves"];
			this.showDecision()
		});
		let sub2 = this.demandeService._getTiers(critere).subscribe((data) => {
			this.tiers = data.result.results;
			this.editMode = 1;

		});

		this.subsription.add(sub);
		this.subsription.add(sub2);
	}

	//produit
	displayedColProd = [
		"pack",
		"montantDmd",
		"montantAccorde",
		"apport",
		"duree",
		"taux",
		"mensualite",
		"committeeStatus",
		"Action",
	];
	displayedColLibelleProd = [
		"Pack",
		"Montant demandé",
		"Montant Accordé",
		"Apport",
		"Durée",
		"Taux",
		"Mensualité",
		"Statut",
		"Action",
	];

	selectedProduct;
	onComiteProduit(element) {
		//this.dirty = true;
		this.selectedProduct = { ...element };
		this.editMode = 2
		return;

	}

	advice = {};
	onRegistrateDecision() {
		this.advice["products"] = this.demande.products;
		this.advice["requestId"] = this.demande["requestId"];
		this.advice["userId"] = this.authService.user.userId;
		this.advice["userEntityId"] = this.authService.user.userEntityId;
		this.advice["entityId"] = this.authService.user.entityId;
		this.comite["advice"] = this.advice;

		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.comiteService
					.traiter(this.comite, this.demande.requestId)
					.subscribe((data) => {
						this.onEdit(this.selectedReqeust);
						this.getCommittee()
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
						this.dirty = false;
					});
			}
		});
	}

	onValidate() {

		let obj: any = {}
		obj.advice = this.advice

		obj.advice.adviceCode = null
		obj.advice.amaqCreation = null
		obj.advice.committee = null
		obj.advice.dateCreation = null
		obj.advice.entityTypeName = null
		obj.advice.identifiant = null
		obj.advice.listConditionsReserves = []

		obj.advice.products = this.demande.products;
		obj.advice["entityId"] = this.authService.user.entityId;
		obj.advice["userEntityId"] = this.authService.user.userEntityId;
		obj.advice["userId"] = this.authService.user.userId;

		obj.committeeId = this.comite["committeeId"]
		obj.committeeRequestId = this.comite["committeeRequestId"]
		obj.mode = ""
		obj.requestId = this.demande.requestId

		//return
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.comiteService.postMethode("/committee/validateRequest", obj)
					.subscribe((data) => {
						this.onEdit(this.selectedReqeust);
						this.getCommittee()
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });

					});
			}
		});
	}


    onCancel(): void {

        let obj: any = {}
        obj.advice = this.advice

        obj.advice.adviceCode = null
        obj.advice.amaqCreation = null
        obj.advice.committee = null
        obj.advice.dateCreation = null
        obj.advice.entityTypeName = null
        obj.advice.identifiant = null
        obj.advice.listConditionsReserves = []

        obj.advice.products = this.demande.products;
        obj.advice["entityId"] = this.authService.user.entityId;
        obj.advice["userEntityId"] = this.authService.user.userEntityId;
        obj.advice["userId"] = this.authService.user.userId;

        obj.committeeId = this.comite["committeeId"]
        obj.committeeRequestId = this.comite["committeeRequestId"]
        obj.mode = ""
        obj.requestId = this.demande.requestId

        //return
        this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
            if (result.value) {
                this.comiteService.postMethode("/committee/cancelRequest", obj)
                    .subscribe((data) => {
                        this.onEdit(this.selectedReqeust);
                        this.getCommittee()
                        this.snackBarService.openSuccesSnackBar({ message: data["message"] });

                    });
            }
        });
    }

	/////signature

	displayedColSignature = ["dateVal", "signataire", "profil", "etat", "action"];
	displayedColLibelleSignature = ["Date Validation", "Signataire", "profil", "Etat", "Action"];

	onAnnuler() {
		if (this.dirty) {
			this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
				if (result.value)
					this.__onAnnuler();
			});
		} else
			this.__onAnnuler();
	}

    onPvEdite(): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['report'] = 'PV';
                criteria['creditRequest'] = this.demande['requestId'];
                criteria['demande'] = this.demande['requestId'];

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,('PV_'+this.demande['requestId'])) ;
                });
            }
        });
    }

    onDecisionEdite(): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['report'] = 'DECISION';
                criteria['creditRequest'] = this.demande['requestId'];

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,('DECISION_'+this.demande['requestId'])) ;
                });
            }
        });
    }

	__onAnnuler() {
		this.editMode = 0;
		this.demande = null;
		this.dirty = false;
	}

	onRetour() {
		if (this.editMode == 0) {
			this._router.navigate(["/comite"]);
		}
		this.editMode--
		if (this.editMode == 1) {
			this.onEdit(this.selectedReqeust);
		}
	}

	onSigner(element) {
		let obj: any = {}
		obj.advice = this.advice
		obj.advice.adviceCode = null
		obj.advice.amaqCreation = null
		obj.advice.committee = null
		obj.advice.dateCreation = null
		obj.advice.entityTypeName = null
		obj.advice.identifiant = null
		//obj.advice.listConditionsReserves = []

		//obj.advice.products = this.demande.products;
		obj.advice["entityId"] = this.authService.user.entityId;
		obj.advice["userEntityId"] = this.authService.user.userEntityId;
		obj.advice["userId"] = this.authService.user.userId;

		obj.committeeId = this.comite["committeeId"]
		obj.committeeRequestId = this.comite["committeeRequestId"]
		obj.mode = ""
		obj.requestId = this.demande.requestId
		obj.committeeRequestIntervenantId = element.committeeRequestIntervenantId;
		obj.committeeParticipantId = element.committeeParticipantId;
		obj.participantId = element.participantId;
		//obj.signing = "O";

		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.comiteService.signe(obj).subscribe(
					(response) => {
						this.snackBarService.openSuccesSnackBar({message: response.message});
						this.getCommittee();
					});
			}
		});
	}

	getCreationTypes() {
		this.commonService.getCreationTypes().subscribe(data => {
			this.creationTypes = data.result;
		})
	}

	getCreationTypeName(id: number) {
		return this.creationTypes.find(v => v.id == id)?.designation;
	}


	onShowHistory(element) {

		let params = {
			entityId: 0,
			requestId: this.demande.requestId, //element.requestId,
			userEntityId: 0,
			userId: 0
		}
		this.comiteService.getAdviceService(params).subscribe(
			(response: any) => {
				const dialogRef = this.dialog.open(ProductHistoryComponent, {
					data: {
						element: element,
						products: response.result
					},
					width: "90vw",
					maxHeight: "90vw",
				});
				dialogRef.afterClosed().subscribe((data) => { });
			}, (error) => {
			}, () => {
			})


	}

	decision;
	showDecision() {
		this.decision = true;
		for (let index = 0; index < this.demande.products.length; index++) {
			const element = this.demande.products[index];
			if (!element.committeeStatus || element.committeeStatus == "") {
				this.decision = false
				break;
			}
		};
	}

	getCommittee() {
		let params = {
			committeeId: this.commiteDto.committeeId,
		}
		this.comiteService.postMethode("/committee/getCommittee", params)
			.subscribe((data) => {
				this.commiteDto = data.result
			});
	}

}
export class Dmd {
	numdmd: string;
	date: string;
	compteName: string;
	type: string;
	statut: string;
	statutName: string;
	produits: Prod[];
	client: Client;
}
export class Prod {
	pack: string;
	montantDmd: number;
	montantAccorde: number;
	apport: number;
	duree: number;
	taux: number;
	derog: number;
	differe: number;
	mensualite: number;
}
export class Client {
	name: string;
	birthday: string;
	cin: string;
	salaire: number;
}
