import { DragDropModule } from "@angular/cdk/drag-drop";
import { CommonModule, DatePipe, registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { LOCALE_ID, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatTooltipModule } from "@angular/material/tooltip";
import { RouterModule } from "@angular/router";
import { CardModule } from "app/shared/card";
import { CautionAddModule } from "app/shared/caution-add/caution-add.module";
import { DemandeInfoModule } from "app/shared/demande-info/demande-info.module";
import { DemandeQuiModule } from "app/shared/demande-qui/demande-qui.module";
import { NgxCurrencyModule } from "ngx-currency";
import { ComiteFormComponent } from "./comite-form/comite-form.component";
import { ComiteListComponent } from "./comite-list/comite-list.component";
import { ComiteProductDialogComponent } from "./comite-product-dialog/comite-product-dialog.component";
import { ComiteTraiterComponent } from "./comite-traiter/comite-traiter.component";
import { comiteRoutes } from "./comite.routing";
import { GuarantiesComponent } from "./guaranties/guaranties.component";
import { ProductHistoryComponent } from "./product-history/product-history.component";

registerLocaleData(localeFr);
registerLocaleData(localeEs);
@NgModule({
	declarations: [
		ComiteListComponent,
		ComiteFormComponent,
		ComiteTraiterComponent,
		ComiteProductDialogComponent,
		GuarantiesComponent,
		ProductHistoryComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(comiteRoutes),
		DragDropModule,
		CardModule,
		NgxCurrencyModule,
		MatExpansionModule,
		MatFormFieldModule,
		MatSelectModule,
		FormsModule,
		MatDatepickerModule,
		MatButtonModule,
		MatTableModule,
		MatPaginatorModule,
		MatRadioModule,
		MatDividerModule,
		MatCheckboxModule,
		MatMenuModule,
		MatListModule,
		MatDialogModule,
		CautionAddModule,
		MatIconModule,
		MatInputModule,
		DemandeQuiModule,
		DemandeInfoModule,
		MatTooltipModule
	],
	providers: [
		{ provide: LOCALE_ID, useValue: "fr-FR" },
		{ provide: DatePipe, useClass: DatePipe }
	],
})
export class ComiteModule { }
