import { Routes } from "@angular/router";
import { ComponentState } from "app/core/models/ComponentState";
import { ComiteFormComponent } from "./comite-form/comite-form.component";
import { ComiteListComponent } from "./comite-list/comite-list.component";
import { ComiteTraiterComponent } from "./comite-traiter/comite-traiter.component";
import { CommiteResolverService } from "./services/commite-resolver.service";
import { CommiteTraiterResolverService } from "./services/commite-traiter-resolver.service";

export const comiteRoutes: Routes = [
	{
		path: "",
		component: ComiteListComponent,
	},
	{
		path: "add",
		resolve: { resolvedParam: CommiteResolverService },
		component: ComiteFormComponent,
		data: {
			state: ComponentState.Add
		}
	},
	{
		path: "edit/:id",
		resolve: { resolvedParam: CommiteResolverService },
		component: ComiteFormComponent,
		data: {
			state: ComponentState.Update
		}
	},
	{
		path: "traiter/:id",
		resolve: { resolvedParam: CommiteTraiterResolverService },
		component: ComiteTraiterComponent,
	},
];
