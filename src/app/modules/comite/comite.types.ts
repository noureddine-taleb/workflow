export class Comite {
	committeNumber: number
	committeeDate: string;
	committeeId: number;
	committeeType: string;
	committeTypeName: string;
	description: string;
	entityId: number;
	participants: any;
	requests: Request[];
	userEntityId: number;
	userId: number;
	totalRequest: any;
	statusName: string

	totalValidatedRequest: number;
	totalRejectedRequest: number;
	totalAdjournedRequest: number;
	totalWaitingforRequest: number;
}
