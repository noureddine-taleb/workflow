import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { BehaviorSubject, Observable, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CommiteServices } from "./services/commite.service";

// TODO: to be removed
export class CommiteeCDK implements DataSource<any> {
	private anySubject = new BehaviorSubject<any[]>([]);
	private loadingSubject = new BehaviorSubject<boolean>(false);

	private totalElements = new BehaviorSubject<number>(0);
	private pageElements = new BehaviorSubject<number>(0);

	public loading$ = this.loadingSubject.asObservable();
	public totalElements$ = this.totalElements.asObservable();
	public pageElements$ = this.pageElements.asObservable();
	public data$ = this.anySubject.asObservable();
	constructor(private commiteSrv: CommiteServices, private snackBarService: SnackBarService) {}

	connect(collectionViewer: CollectionViewer): Observable<any[]> {
		return this.anySubject.asObservable();
	}

	disconnect(collectionViewer: CollectionViewer): void {
		this.anySubject.complete();
		this.loadingSubject.complete();
	}

	loadCommite(critere) {
		this.loadingSubject.next(true);
		this.commiteSrv
			.getCommitees(critere["first"], critere["pas"])
			.pipe(
				catchError(() => of([])),
				finalize(() => this.loadingSubject.next(false)),
			)
			.subscribe((data) => {
				this.anySubject.next(data["result"]["results"]);
				this.totalElements.next(data["result"]["rowsNumber"]);
				this.pageElements.next(data["result"]["results"].length);
			});
	}
}
