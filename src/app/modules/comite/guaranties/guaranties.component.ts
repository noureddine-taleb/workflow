import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Guaranty } from "app/modules/demandes/constitution/constitution.types";
import { EtudeAddComponent } from "../../demandes/etude-add/etude-add.component";

@Component({
	selector: "app-guaranties",
	templateUrl: "./guaranties.component.html",
	styleUrls: ["./guaranties.component.css"],
})
export class GuarantiesComponent implements OnInit {
	@Input() requestId: number;
	@Input() personId: number;

	displayedColumns: string[] = [
		"nature",
		"reference",
		"natureName",
		"value",
		"annule",
		"annulationDate",
		"action",
	];
	dataSource: Guaranty[];

	constructor(
		private demandeService: DemandeService,
		private snackBar: SnackBarService,
		private communService: CommonService,
		private dialog: MatDialog,
	) {}

	ngOnInit(): void {
		this.getListTypeBien();
	}
	listBien;
	getListTypeBien(): void {
		this.communService.getPatrimony("params").subscribe((data) => {
			this.listBien = data.result;
		});
	}
	getList(): void {
		if (this && this.personId && this.requestId) {
			// console.warn(this.personId , this.requestId);
			this.demandeService
				.getGaranties({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource = data.result;
				});
		}
	}

	onDelete(element): void {
		this.snackBar
			.openConfirmSnackBar({ message: "Êtes-vous sûr de vouloir supprimer cette ligne" })
			.then((result) => {
				if (result.value) {
					this.demandeService
						.deleteGarantie({
							personId: this.personId,
							requestId: this.requestId,
							identifiant: element.identifiant,
						})
						.subscribe((data) => {
							this.getList();
							this.snackBar.openSuccesSnackBar({ message: data.message });
						});
				}
			});
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.requestId) {
			this.getList();
		}
	}
	editStat = 0;
	fiancialObjectDto: Guaranty;
	onPrepareAdd(): void {
		this.editStat = 1;
		this.fiancialObjectDto = new Guaranty();
		this.fiancialObjectDto.requestId = this.requestId;
	}

	onAdd(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		this.demandeService.addGarantie(this.fiancialObjectDto).subscribe((_) => {
			this.getList();
		});
		this.editStat = 0;
	}

	onAdd2() {
		this.fiancialObjectDto = new Guaranty();
		this.fiancialObjectDto.requestId = this.requestId;
		const dialogRef = this.dialog.open(EtudeAddComponent, {
			data: {
				objet: this.fiancialObjectDto,
				listBien: this.listBien,
				casTraite: 2,
			},
			width: "90vw",
		});

		dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
	}

	displayedColumnsFooter(): any {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}
}
// // Type bien , N° Titre, Propiétaire , Superficie (m²), Adresse , Prix Achat ,Justif transaction

export interface FinacialObject {
	adress: string;
	annulationDate: string;
	canceledWith: string;
	createdWith: string;
	creationDate: string;
	entityId: number;
	goodTypeId: number;
	goodTypeName: string;
	identifiant: number;
	landTitle: string;
	observation: string;
	owner: string;
	proofTransaction: string;
	quota: number;
	requestId: number;
	size: number;
	status: string;
	userEntityId: number;
	userId: number;
	value: number;
}
