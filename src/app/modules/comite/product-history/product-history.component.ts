import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommiteServices } from "app/modules/comite/services/commite.service";

@Component({
    selector: "app-product-history",
    templateUrl: "./product-history.component.html",
    styleUrls: ["./product-history.component.scss"],
})
export class ProductHistoryComponent implements OnInit {
    products: any[]
    displayedColumns = [
        "financialProductName",
        "financialObjectCode",
        "duration",
        "deferredPeriod",
        "rate",
        "bonus",
        "derogation",
        "goodAmount",
        "startDate",
        // this field is added only if simulationFlag==N
        "endDate",
    ];
    constructor(
        public dialogRef: MatDialogRef<ProductHistoryComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private comiteService: CommiteServices,
    ) { }
    ngOnInit(): void {
        console.log(this.data)
        this.products = this.data.products[0].products
    }

}