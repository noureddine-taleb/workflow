import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { ComponentState } from "app/core/models/ComponentState";
import { CommiteServices } from "app/modules/comite/services/commite.service";
import { forkJoin, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { SimulationGtService } from "../../simulation/services/simulation.gt.service";

@Injectable({ providedIn: "root" })
export class CommiteResolverService implements Resolve<any> {
	commitId = 0;
	constructor(private commiteSrv: CommiteServices, private simGTServ: SimulationGtService) {}
	//resolve get route param

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
		let cmpState = route.data.state;
		let commiteId = route.params["id"];

		return forkJoin({
			commite:
			cmpState == ComponentState.Add
					? of({ status: 200, message: null, result: -1 })
					: this.commiteSrv
							.getCommiteeById(commiteId)
							.pipe(catchError((error) => of(error))),
			users: this.commiteSrv.getUsers().pipe(catchError((error) => of(error))),
			demandes: this.commiteSrv
				.getDemandeComiteeByStatut("DC_COM_IN")
				.pipe(catchError((error) => of(error))),
			typecomites: this.commiteSrv.getComiteType().pipe(catchError((error) => of(error))),
			qualites: this.commiteSrv.getQuality().pipe(catchError((error) => of(error))),
		});
	}
}
