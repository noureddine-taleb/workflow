import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { CommiteServices } from "app/modules/comite/services/commite.service";
import { SimulationGtService } from "app/modules/simulation/services/simulation.gt.service";

@Injectable({ providedIn: "root" })
export class CommiteTraiterResolverService implements Resolve<any> {
	commitId = 0;
	constructor(private commiteSrv: CommiteServices, private simGTServ: SimulationGtService) {}
	//resolve get route param

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
		let commiteId = route.params["id"];
		return this.commiteSrv.getCommiteeById(commiteId);
	}
}
