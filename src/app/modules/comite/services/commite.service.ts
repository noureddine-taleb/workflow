import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from "app/core/models/ApiResponse";

@Injectable({ providedIn: "root" })
export class CommiteServices {
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;
	constructor(private httpClient: HttpClient, private authService: AuthService) { }

	getComiteType() {
		return this.httpClient.get(`${this.wkfPath}/ref/committeeType`);
	}
	getQuality() {
		return this.httpClient.get(`${this.wkfPath}/ref/qualityParticipant`);
	}
	getUsers() {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
		};
		return this.httpClient.post(`${this.wkfPath}/ref/userList`, critere);
	}
	saveComite(comiteEntet) {
		comiteEntet["userEntityId"] = this.authService.user.userEntityId;
		comiteEntet["userId"] = this.authService.user.userId;
		return this.httpClient.post(`${this.wkfPath}/committee/create`, comiteEntet);
	}

	getCommitees(first, pas) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
			first: first,
			pas: pas,
		};

		return this.httpClient.post(`${this.wkfPath}/committee/list`, critere);
	}

	getCommiteeById(commiteId) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
			committeeId: commiteId,
		};

		return this.httpClient.post(`${this.wkfPath}/committee/getCommittee`, critere);
	}

	deleteCommitRequest(commiteId, requestId, committeeRequestId) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
			committeeId: commiteId,
			requestId: requestId,
			committeeRequestId: committeeRequestId,
		};

		return this.httpClient.post<ApiResponse<any>>(`${this.wkfPath}/committee/deleteRequest`, critere);
	}

	updateCommitRequest(commiteId, requestId) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
			committeeId: commiteId,
			requestId: requestId,
		};

		return this.httpClient.post<ApiResponse<number>>(`${this.wkfPath}/committee/updateRequest`, critere);
	}

	getDemandeComiteeByStatut(statut): Observable<any> {
		let critere = {};
		critere["userEntityId"] = this.authService.user.entityId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;
		critere["userEntityId"] = this.authService.user.entityId;
		critere["first"] = 0;
		critere["pas"] = 100;
		critere["statusCode"] = statut;
		return this.httpClient.post(`${this.wkfPath}/committee/getRequests`, critere);
	}

	traiter(comite: any, requestId: number) {
		comite["userEntityId"] = this.authService.user.entityId;
		comite["entityId"] = this.authService.user.entityId;
		comite["userId"] = this.authService.user.userId;
		comite["requestId"] = requestId;

		return this.httpClient.post(`${this.wkfPath}/committee/treatCommitteRequest`, comite);
	}

	getDemande(critere) {
		return this.httpClient.post(`${this.wkfPath}/committee/getRequestsCommittee`, critere);
	}

	signe(object): Observable<any> {
		//element.signing = "O";
		object["userId"] = this.authService.user.userId;
		object["userEntityId"] = this.authService.user.userEntityId;
		object["entityId"] = this.authService.user.entityId;
		console.log(object)
		return this.httpClient.post(`${this.wkfPath}/committee/signing`, object);
	}

	deleteParticipant(element) {
		element["entityId"] = this.authService.user.entityId;
		element["userEntityId"] = this.authService.user.userEntityId;
		element["userId"] = this.authService.user.userId;

		return this.httpClient.post(`${this.wkfPath}/committee/deleteParticipant`, element);
	}

	getAdviceService(obj) {
		obj["userEntityId"] = this.authService.user.entityId;
		obj["entityId"] = this.authService.user.entityId;
		obj["userId"] = this.authService.user.userId;

		return this.httpClient.post(`${this.wkfPath}/advice/getAdvices`, obj);
	}

	traiterProduit(obj) {
		obj["userEntityId"] = this.authService.user.entityId;
		obj["entityId"] = this.authService.user.entityId;
		obj["userId"] = this.authService.user.userId;
		console.log(obj)
		return this.httpClient.post(`${this.wkfPath}/committee/treatProduct`, obj);
	}

	postMethode(url, object): Observable<any> {
		//url doit commencer par '/'
		object["userId"] = this.authService.user.userId;
		object["userEntityId"] = this.authService.user.userEntityId;
		object["entityId"] = this.authService.user.entityId;
		console.log(object)
		return this.httpClient.post(this.wkfPath + url, object);
	}

	getMethod(url): Observable<any> {
		//url doit commencer par '/'
		return this.httpClient.get(this.wkfPath + url);
	}

	listStatus = [];
	findStatus(statusModule, statusCode) {
		let cr = this.listStatus.filter((x) => x.moduleCode == statusModule);
		if (cr.length == 0)
			return { statusColor: "#FFF", statusName: "", statusCode: "", statusModule: "" };
		let status = cr[0].status.filter((x) => x["statusCode"] == statusCode);
		if (status.length > 0) return status[0];

		return { statusColor: "#FFF", statusName: "", statusCode: "", statusModule: "" };
	}
}
