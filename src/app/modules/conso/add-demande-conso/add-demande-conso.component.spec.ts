import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AddDemandeConsoComponent } from "./add-demande-conso.component";

describe("AddDemandeComponent", () => {
	let component: AddDemandeConsoComponent;
	let fixture: ComponentFixture<AddDemandeConsoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AddDemandeConsoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddDemandeConsoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
