import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSelectChange } from "@angular/material/select";
import { ActivatedRoute } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { SimulationGtService } from "app/modules/simulation/services/simulation.gt.service";
import { SelectModel } from "app/modules/simulation/simulation-form/simulation-form.component";
import { Account } from "app/shared/models/Account";
import { Subscription } from "rxjs";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AddProductConsoComponent } from "../add-product-conso/add-product-conso.component";
import { DemandeConso } from "../models/DemandeConso";
import { PersonConso } from "../models/PersonConso";
import { ProductConso } from "../models/ProductConso";

@Component({
	selector: "app-add-demande-conso",
	templateUrl: "./add-demande-conso.component.html",
	styleUrls: ["./add-demande-conso.component.scss"],
})
export class AddDemandeConsoComponent implements OnInit {
	consoDemande: DemandeConso = new DemandeConso();
	subscriptions = new Subscription();
	tiers: PersonConso[] = [];
	familleSegments = [];
	segments = [];
	filteredSegments = [];

	constructor(
		private _activatedRoute: ActivatedRoute,
		private dialog: MatDialog,
		private snackeBarService: SnackBarService,
		private tokenService: AuthService,
		private commonService: CommonService,
		private demandeService: DemandeService,
		private refGtToken: AuthService,
		private simulationService: SimulationGtService,
		private routingService: RoutingService,
	) {}

	ngOnInit(): void {}

	retour() {
		this.routingService.gotoConsoRequests();
	}

	listProducts: ProductConso[] = [];

	displayedColumns = [
		"produit",
		"montant",
		"duree",
		"taux",
		"differe",
		"periodicite",
		"mensualite",
		"assurance",
		"action",
	];
	displayedColumnsLib = [
		"Produit",
		"Montant",
		"Durée",
		"Taux",
		"Différé",
		"Périodicité",
		"Mensualité",
		"Assurance",
	];

	setSegmentName(person, $event) {
		person.segmentName = this.filteredSegments.find((s) => s.id == $event.value)?.designation;
	}

	onAddProduct() {
		const dialogRef = this.dialog.open(AddProductConsoComponent, {
			data: { persons: this.tiers },
		});
		dialogRef.afterClosed().subscribe((data) => {
			if (data?.product) this.listProducts = this.listProducts.concat(data?.product);
		});
	}

	onDelete(product) {
		this.snackeBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.listProducts = this.listProducts.filter((p) => p != product);
			}
		});
	}

	onUpdate(product) {
		this.dialog.open(AddProductConsoComponent, {
			data: { persons: this.tiers, product },
			width: "90vw",
		});
	}

	compte: Account;
	getAccount(account: Account) {
		this.compte = account;
		this.getTiers();
		this.getSegFamilly();
		this.getSegments();
	}

	getSegFamilly() {
		this.simulationService.getList("segmentsType").subscribe((data) => {
			this.familleSegments = data["result"];
		});
	}

	getSegments() {
		this.simulationService.getListByPost("segements", new SelectModel()).subscribe((data) => {
			this.segments = data["result"];
			this.filteredSegments = this.segments;
		});
	}

	filtrerSegment($event: MatSelectChange, person: any): void {
		person.segment = null;
		this.filteredSegments = this.segments.filter((x) => x.parentCode === $event.value);
	}

	initPersons() {
		for (let person of this.tiers) {
			person.monthlyPaymentConsoCam || (person.monthlyPaymentConsoCam = 0);
			person.monthlyPaymentConsoConf || (person.monthlyPaymentConsoConf = 0);
			person.monthlyPaymentImmoCam || (person.monthlyPaymentImmoCam = 0);
			person.monthlyPaymentImmoConf || (person.monthlyPaymentImmoConf = 0);
			person.monthlyPay || (person.monthlyPay = 0);
			person.othersMonthlyIncome || (person.othersMonthlyIncome = 0);
		}
	}

	getTiers() {
		this.commonService
			.getListPerson({
				clientId: (this.compte as any).clientId,
			})
			.subscribe((data) => {
				(this.tiers = data["result"]), this.initPersons();
			});
	}

	listCateories = [];
	listTypeProduct = [];

	onCreateConsoDemande() {
		if (!this.isReadyToBeSaved()) {
			this.snackeBarService.openErrorSnackBar({ message: "certaines informations manquent" });
			return;
		}
		this.consoDemande.accountId = this.compte.accountId;
		this.consoDemande.products = this.listProducts;
		this.consoDemande.creditType = 2;
		this.consoDemande.persons = this.tiers;
		for (let person of this.tiers) {
			person.monthlyPaymentConso =
				person.monthlyPaymentConsoCam + person.monthlyPaymentConsoConf;
			person.monthlyPaymentImmo =
				person.monthlyPaymentImmoCam + person.monthlyPaymentImmoConf;
		}

		this.demandeService.createDemandeConso(this.consoDemande).subscribe((data) => {
			this.routingService.gotoTask(data["result"])
		});
	}

	isTierInfoSaved(): boolean {
		let ret = true;
		for (let person of this.tiers) {
			if (
				person.segment &&
				person.segmentType &&
				person.monthlyPay &&
				(person.othersMonthlyIncome || person.othersMonthlyIncome === 0) &&
				(person.monthlyPaymentImmoCam || person.monthlyPaymentImmoCam === 0) &&
				(person.monthlyPaymentImmoConf || person.monthlyPaymentImmoConf === 0) &&
				(person.monthlyPaymentConsoCam || person.monthlyPaymentConsoCam === 0) &&
				(person.monthlyPaymentConsoConf || person.monthlyPaymentConsoConf === 0)
			)
				continue;
			ret = false;
			break;
		}
		return ret;
	}

	isReadyToBeSaved(): boolean {
		return !!(
			this.compte &&
			this.isTierInfoSaved() &&
			this.consoDemande.objectCredit &&
			this.listProducts?.length
		);
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}
}
