import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AddProductConsoComponent } from "./add-product-conso.component";

describe("AddProductComponent", () => {
	let component: AddProductConsoComponent;
	let fixture: ComponentFixture<AddProductConsoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AddProductConsoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddProductConsoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
