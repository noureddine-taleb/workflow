import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { RouterModule } from "@angular/router";
import { CardModule } from "app/shared/card";
import { AddDemandeConsoComponent } from "./add-demande-conso/add-demande-conso.component";
import { MatTreeSelectInputModule } from "mat-tree-select-input";
import { AddProductConsoComponent } from "./add-product-conso/add-product-conso.component";
import { ConsRoutes } from "./conso.routing";
import { NgxCurrencyModule } from "ngx-currency";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatButtonModule } from "@angular/material/button";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatTableModule } from "@angular/material/table";
import { MatSelectModule } from "@angular/material/select";
import { MatRadioModule } from "@angular/material/radio";
import { MatIconModule } from "@angular/material/icon";
import { AccountModule } from "app/shared/account/account.module";
import { MatDialogModule } from "@angular/material/dialog";
import { SimulationResultModule } from "app/shared/simulation-result/simulation-result.module";

@NgModule({
	declarations: [AddDemandeConsoComponent, AddProductConsoComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(ConsRoutes),
		CardModule,
		ReactiveFormsModule,
		MatInputModule,
		MatFormFieldModule,
		MatTreeSelectInputModule,
		NgxCurrencyModule,
		MatDatepickerModule,
		MatButtonModule,
		MatExpansionModule,
		FormsModule,
		MatTableModule,
		MatSelectModule,
		MatRadioModule,
		MatIconModule,
		AccountModule,
		MatDialogModule,
		SimulationResultModule,
	],
})
export class ConsoModule {}
