import { Route } from "@angular/router";
import { ListDemandeComponent } from "app/shared/list-demande/list-demande.component";
import { RequestType } from "app/shared/models/RequestType";
import { AddDemandeConsoComponent } from "./add-demande-conso/add-demande-conso.component";

export const ConsRoutes: Route[] = [
	{
		path: "",
		component: ListDemandeComponent,
		data: { title: "Liste des demandes de crédit conso", demandeType: RequestType.CONSO, addAllowed: true },
	},
	{
		path: "add",
		component: AddDemandeConsoComponent,
	},
];
