import { PersonConso } from "./PersonConso";
import { ProductConso } from "./ProductConso";

export class DemandeConso {
	accountId: number;
	clientCategory: number;
	creditType: number;
	entityId: number;
	newConstitution: string;
	objectCredit: string;
	products: ProductConso[];
	persons: PersonConso[];
	requestId: number;
	userEntityId: number;
	userId: number;
}
