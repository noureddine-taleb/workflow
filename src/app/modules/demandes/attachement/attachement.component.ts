import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { Subscription } from "rxjs";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";

@Component({
	selector: "app-attachement",
	templateUrl: "./attachement.component.html",
	styleUrls: ["./attachement.component.scss"],
})
export class AttachementComponent implements OnInit {
	produit: any;
	requestId;
	dossier = new Dossier();
	mensualites = [];
	subscription = new Subscription();
	simulationFlag = false;

	constructor(
		public dialogRef: MatDialogRef<AttachementComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private demandeService: DemandeService,
		private snackBarService: SnackBarService,
		private communService: CommonService,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {
		this.produit = data.produit;
		this.requestId = data.requestId;
		this.getReport(data.produit);
	}

	ngOnInit(): void {
		this.getMensualites();
	}

	disableInput() {
		return (this.dossier as any)?.contractId || this.routingService.isDemandeRO();
	}


	getReport(report) {
		//TODO
		//WS get credit report by product ID
		let critere = {};
		/*
            "entityId": 962,
            "productId": 47,
            "userEntityId": 962,
            "userId": 900957,
            "requestId":126
            */

		critere["productId"] = report["productId"];
		critere["requestId"] = this.requestId;

		let sub = this.demandeService.getReportById(critere).subscribe((data) => {
			this.dossier = <Dossier>(<unknown>data["result"]);
			this.simulationFlag = this.dossier.simulationFlag === 'N'
		});
		this.subscription.add(sub);
	}

	onValide(): void {
		this.dossier.productId = this.produit.productId;

		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService.createCreditReport(this.dossier).subscribe((data) => {
					this.snackBarService.openSuccesSnackBarWithReturn({ message: data['message'] }).then((result1) => {
                        this.dialogRef.close();
                    });
				});
				this.subscription.add(sub);
			}
		});
	}

	// eslint-disable-next-line @typescript-eslint/member-ordering
	dossierAttache = false;

	onAttache(): void {
		this.dossierAttache = true;
	}

    onClose(): void{
        this.dialogRef.close();
    }
	detacherDossier(): void {
		this.dossierAttache = false;
	}

	getMensualites() {
		let sub = this.communService.getMensualites().subscribe((data) => {
			this.mensualites = data["result"];
		});
		this.subscription.add(sub);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}

export class Dossier {
	accessoryFres: number;
	amortizedCapital: number;
	creditAmount: number;
	//deadlineDate: Date;
	deadlineNumber: number;
	description: string;
	//differed: number;
	//dueAmount: number;
	//duration: number;
	entityId: number;
	firstDeadlineDate: Date;
	fres: number;
	insuranceAmount: number;
	interstAmount: number;
	lastDeadlineDate: Date;
	nodoss: string;
	//nominalAmount: number;
	//periodicity: string;
	productId: number;
	//rate: number;
	reportId: number;
	teg: number;
	userEntityId: number;
	userId: number;
	variablePartRate: number;
	variableRate: number;
	versementInitial: number;
	simulationFlag: string;
}
