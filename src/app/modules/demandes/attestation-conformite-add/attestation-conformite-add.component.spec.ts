import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AttestationConformiteAddComponent } from "./attestation-conformite-add.component";

describe("AttestationConfirmiteAddComponent", () => {
	let component: AttestationConformiteAddComponent;
	let fixture: ComponentFixture<AttestationConformiteAddComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AttestationConformiteAddComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AttestationConformiteAddComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
