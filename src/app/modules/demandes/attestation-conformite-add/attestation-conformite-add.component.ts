import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { Subscription } from "rxjs";
import { take } from "rxjs/operators";
import { AttestationConformiteService } from "../services/attestation-conformite.service";

@Component({
	selector: "app-attestation-conformite-add",
	templateUrl: "./attestation-conformite-add.component.html",
	styleUrls: ["./attestation-conformite-add.component.scss"],
})
export class AttestationConformiteAddComponent implements OnInit {
	listDossierCredit = [];
	reportIds = [];
    selectedcertificate: any;

	subscription: Subscription = new Subscription();

	dialogMode = 0;
	certificateForm: FormGroup;
	showDetach = true;
	requestId;
	displayedColumns = [
		"nodoss",
		"productName",
		"creditAmount",
		"nominalAmount",
		"rate",
		"duration",
		"periodicity",
		"dueAmount",
		"input",
	];

	constructor(
		private attstationConfirmiteService: AttestationConformiteService,
		private snackBarService: SnackBarService,
		private dialogRef: MatDialogRef<AttestationConformiteAddComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private userservice: AuthService,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		public routingService: RoutingService,
	) {}

	ngOnInit(): void {
		this.dialogMode = this.data["add"];
		this.requestId = this.data["requestId"];
        this.selectedcertificate = this.data["certificat"];
		if (this.data["showDetach"] != undefined) {
			this.showDetach = this.data["showDetach"];
		}
		this.getListDossierCredit();
		if (this.dialogMode != 0) {
			this.certificateForm = this.fb.group({
				certificateId: [""],
				certificateObject: ["", [Validators.required]],
                certificateDossiers: ["", [Validators.required]],
				//emanatingFrom: ["", [Validators.required]],
				observation: ["", [Validators.required]],
				pj: ["", [Validators.required]],
			});
		}

		if (this.dialogMode == 0) {
			this.listDossierCredit = this.data["dossiers"];
		}
		// update
		if (this.dialogMode == 2) {
			this.certificateForm.patchValue(this.data["certificat"]);
		}

        if (this.dialogMode == 1) {
            console.log("test default value");
            const certificate = this.certificateForm.value;
            this.attstationConfirmiteService.attestationConformiteGetDefaultValue(certificate,this.requestId).subscribe((data)=>{
                this.certificateForm = this.fb.group({
                    certificateId: [""],
                    certificateObject: [data.result['V_OBJET'] , [Validators.required]],
                    certificateDossiers: ["", [Validators.required]],
                    //emanatingFrom: ["", [Validators.required]],
                    observation: [data.result['V_OBSERVATION'], [Validators.required]],
                    pj: [data.result['V_PJ'], [Validators.required]],
                });
            });
        }
	}

    checkDossier(event,element): void{
        console.log(element)
        let ligneChecked: string;
        this.listDossierCredit.forEach((ligne)=>{
            if(ligne.checked){
                ligneChecked= ((ligneChecked !== undefined)?(ligneChecked+'\n') : '' )  +'- Mise en place d\'un ' + ligne.productName
                    +' d\'un montant total de '+ ligne.nominalAmount +' DH';
            }
        });

        this.certificateForm.get('certificateDossiers').setValue(ligneChecked);
    }

	getListDossierCredit() {
		if (this.dialogMode == 0) return;
		let sub = this.attstationConfirmiteService
			.getDossierCredit(this.requestId)
			.subscribe((data) => {
                console.log(data)
				if (data["status"] == "200") {
					this.listDossierCredit = data["result"];
					this.listDossierCredit.forEach((item) => {
						item["checked"] = item["certificateId"] != null;
					});

                    this.listDossierCredit = this.listDossierCredit.filter(o=> (o.certificateId == this.selectedcertificate?.certificateId || o.certificateId == null ));

				} else this.snackBarService.openErrorSnackBar({ message: data["message"] });
			});
		this.subscription.add(sub);
	}

	onDetacher(element) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let param = new ContractReportDto();
				param["entityId"] = this.userservice.user.entityId;
				param["userEntityId"] = this.userservice.user.userEntityId;
				param["userId"] = this.userservice.user.userId;

				param["reportId"] = element["reportId"];
				param["certificateId"] = element["certificateId"];

				this.attstationConfirmiteService
					.attestationConformiteDetacherDossier(param)
					.pipe(take(1))
					.subscribe((data) => {
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
						this.data.reload();
						this.listDossierCredit = this.listDossierCredit.filter(
							(d) => d["reportId"] != element["reportId"],
						);
					});
			}
		});
	}

	getSelectedReports() {
        return this.listDossierCredit
            .filter(
                (x) =>
                    !x.certificateId ||
                    (x.certificateId &&
                        this.certificateForm.get("certificateId").value == x.certificateId),
            )
            .filter((x) => x["checked"]);
	}

	onValiderAdd() {
		const certificate = this.certificateForm.value;
		certificate.reports = this.getSelectedReports();
		this.snackBarService.openConfirmSnackBar({ message: "enregistrer" }).then((result) => {
			if (result.value) {
				let sub = this.attstationConfirmiteService
					.attestationConformiteCreate(certificate, this.requestId)
					.subscribe((data) => {
						if (data["status"] == "200") {
							this.snackBarService.openSuccesSnackBarWithReturn({ message: data["message"] }).then((result) => {
                                this.dialogRef.close(true);
                            });
						} else {
                            this.snackBarService.openErrorSnackBar({message: data["message"]});
                        };
					});
				this.subscription.add(sub);
			}
		});
	}

	isFormValid() {
		return this.certificateForm.valid && (this.listDossierCredit.filter(o=>o.checked === true).length>0);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}

export class ContractReportDto {
	idCertificate: number;
	idContract?: number;
	idReport: number;
	userEntityId: number;
	userId: number;
}
