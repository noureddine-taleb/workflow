import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AttestationConformiteTableComponent } from "./attestation-conformite-table.component";

describe("AttestationConfirmiteTableComponent", () => {
	let component: AttestationConformiteTableComponent;
	let fixture: ComponentFixture<AttestationConformiteTableComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AttestationConformiteTableComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AttestationConformiteTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
