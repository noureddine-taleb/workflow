import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DialogObjectActionsComponent } from "app/shared/dialog-object-actions/dialog-object-actions.component";
import { Subscription } from "rxjs";
import { take } from "rxjs/operators";
import { AttestationConformiteAddComponent } from "../attestation-conformite-add/attestation-conformite-add.component";
import { AttestationConformiteService } from "../services/attestation-conformite.service";
import {Opc} from "../realisation/opc/opc.component";
import {ReportGeneratorService} from "../../../shared/reportGenerator/report-generator.service";

@Component({
	selector: "app-attestation-conformite-table",
	templateUrl: "./attestation-conformite-table.component.html",
	styleUrls: ["./attestation-conformite-table.component.scss"],
})
export class AttestationConformiteTableComponent implements OnInit {
	subscription: Subscription = new Subscription();
	displayedColumns: string[] = [
		"certificateNumber",
		"certificateObject",
		"nbrDossier",
		"statusName",
		"action",
	];
	displayedColumnsLielle: string[] = ["N°Att", "Dossier", "Nbr.Dossier", "Statut", "Action"];
	dataSource: any[] = [];
	privileges = PrivilegeCode;
	demandeDTO: LoanRequest;
	demandePrivileges: any;

	constructor(
		private dialog: MatDialog,
		private attestationConfirmiteSrv: AttestationConformiteService,
		private snackBarService: SnackBarService,
		private commonService: CommonService,
		public routingService: RoutingService,
		private utilsService: UtilsService,
		private route: ActivatedRoute,
		private router: Router,
		private demandeService: DemandeService,
        private reportGeneratorService: ReportGeneratorService
	) {}

	ngOnInit(): void {
		this.getResolvedParam();
		this.getList();
	}

	displayedColumnsFooter() {
		return this.displayedColumns;
	}

	onPrepareAdd() {
		const dialogRef = this.dialog.open(AttestationConformiteAddComponent, {
			width: "80vw",
			data: { add: 1, dossier: null, requestId: this.demandeDTO.requestId },
		});
		dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
	}

	onPrepareEdit(element) {
		const dialogRef = this.dialog.open(AttestationConformiteAddComponent, {
			width: "80vw",
			data: { add: 2, certificat: element, requestId: this.demandeDTO.requestId },
		});
		dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
	}

	getDemandePrivileges() {
		this.utilsService.loadRequestPrivs();
	}

	getResolvedParam(): void {
		this.demandeDTO = this.demandeService.request;
		this.getDemandePrivileges();
	}

	showAddButton() {
		return this.utilsService.hasPrivilege(this.privileges.maj_cert_m);
	}

	showUpdateButton(privs: any[]) {
		return this.commonService.hasPrivilege(this.privileges.maj_cert_m, privs);
	}

	showDetachButton(privs: any[]) {
		return this.commonService.hasPrivilege(this.privileges.maj_cert_m, privs);
	}

	getList() {
		this.attestationConfirmiteSrv
			.attestationConformiteList(this.demandeDTO.requestId)
			.pipe(take(1))
			.subscribe((data) => {
                console.log(data)
				this.dataSource = data["result"];
				this.dataSource.forEach((element) => {
					element["nbrDossier"] = element["reports"].length;
				});
			});
	}

	onListDossiers(element) {
		const dialogRef = this.dialog.open(AttestationConformiteAddComponent, {
			width: "80vw",
			data: {
				add: 0,
				dossiers: element.reports,
				reload: this.getList.bind(this),
				showDetach: this.showDetachButton(element["privs"]),
				requestId: this.demandeDTO.requestId,
			},
		});
	}

    onEditAttestation(element): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['identifiant'] = element.certificateId;
                criteria['report'] = 'AUTORISATION_CONFORMITE';
                criteria['creditRequest'] = this.demandeDTO.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,'AUTORISATION_CONFORMITE_'+element.certificateId) ;
                });
            }
        });
    }

	loadCertificateActionsPrivs(certificate: any) {
		if (
			this.routingService.isDemandeRO() ||
			(Array.isArray(certificate.actions) && Array.isArray(certificate.privs))
		)
			return;
		this.commonService
			.getAttestationConfActions(
				certificate["statusCode"],
			)
			.pipe(take(1))
			.subscribe((data: any) => {
				certificate.actions = data.result;
				this.commonService
					.getAttestationConfPrivelege(certificate.statusCode)
					.pipe(take(1))
					.subscribe((data) => {
						certificate.privs = data["result"];
					});
			});
	}

	certificateDispatchAction(action, certificate) {
		let postAction = () => {
			this.getList();
			if (action["actionCode"] == "SN") {
				this.reloadDemande();
				this.getDemandePrivileges()
				// this will force actions reload
				delete certificate.actions
				delete certificate.privs
			}
		}

		if (action.users.length == 1) {
			this.snackBarService
			.openConfirmSnackBar({ message: "confirmer l'action ?" })
			.then((result) => {
				if (result.value) {
					this.demandeService
						._dispatchAction(certificate, action, action.users[0])
						.subscribe((data) => {
							this.snackBarService.openSuccesSnackBar({ message: data["message"] });
							postAction();
						});
				}
			});
		} else {
			const dialogRef = this.dialog.open(DialogObjectActionsComponent, { data: {
					action: action,
					obj: certificate,
				}
			});
			dialogRef.afterClosed().subscribe(success => {
				if (success)
					postAction()
			});
		}
	}

	reloadDemande() {
		this.routingService.reloadCurrentRoute()
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("requestId" in changes) {
			this.getList();
		}
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
