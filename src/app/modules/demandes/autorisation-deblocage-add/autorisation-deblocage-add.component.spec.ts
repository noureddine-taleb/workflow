import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AutorisationDeblocageAddComponent } from "./autorisation-deblocage-add.component";

describe("AutorisationDeblocageAddComponent", () => {
	let component: AutorisationDeblocageAddComponent;
	let fixture: ComponentFixture<AutorisationDeblocageAddComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AutorisationDeblocageAddComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AutorisationDeblocageAddComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
