import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { SectionService } from "app/modules/demandes/services/section.service";
import { AttestationConformiteService } from "../services/attestation-conformite.service";
import { DemandeService } from "../services/demande.service";

@Component({
	selector: "app-autorisation-deblocage-add",
	templateUrl: "./autorisation-deblocage-add.component.html",
	styleUrls: ["./autorisation-deblocage-add.component.scss"],
})
export class AutorisationDeblocageAddComponent implements OnInit {
	listDossierCredit = [];
	reportIds = [];
	attestationConformite = {};
	dialogMode = 0;
	requestId: number;
	sections = [];
	displayedColumns = [
		"nodoss",
		"productName",
		"creditAmount",
		"nominalAmount",
		"rate",
		"duration",
		"periodicity",
		"dueAmount",
		"unblockingAmount",
		"proofAmount",
		"proofLabel",
		"sectionId",
		"checked",
	];

	constructor(
		private attstationConfirmiteService: AttestationConformiteService,
		private snackBarService: SnackBarService,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private sectionService: SectionService,
		private demandeService: DemandeService,
	) {
		this.dialogMode = data["add"];
		this.requestId = data["requestId"];
		if (data["add"] == 0) {
			this.listDossierCredit = data["dossiers"];
		}
		if (data["add"] == 2) {
			this.attestationConformite = data["certificat"];
		}
	}

	ngOnInit(): void {
		this.getListDossierCredit();
		this.fetchSections();
	}

	getListDossierCredit() {
		if (this.dialogMode == 0) {
		} else {
			this.attstationConfirmiteService.getDossierCredit(this.requestId).subscribe((data) => {
				if (data["status"] == "200") {
					this.listDossierCredit = data["result"];
					this.listDossierCredit.forEach((x) => {
						x["unblockingAmount"] = x["unblockingAmount"] || 0;
						x["proofAmount"] = x["proofAmount"] || 0;
						x["proofLabel"] = x["proofLabel"] || "";
					});
				} else this.snackBarService.openErrorSnackBar({ message: data["message"] });
			});
		}
	}

	fetchSections() {
		this.sectionService.getAll(this.requestId).subscribe((data: any) => {
			this.sections = data.result;
		});
	}

	onValiderAdd() {
		let unblockingAmount = 0;
		let proofAmount = 0;
		this.listDossierCredit
			.filter((x) => x["checked"])
			.forEach((item) => {
				this.reportIds.push(item);
				unblockingAmount += item["unblockingAmount"];
				proofAmount += item["proofAmount"];
			});

		this.attestationConformite["reports"] = this.reportIds;
		this.attestationConformite["requestId"] = this.demandeService.request.requestId;

		this.attestationConformite["unblockingAmount"] = unblockingAmount;
		this.attestationConformite["proofAmount"] = proofAmount;

		this.attstationConfirmiteService
			.autorisationDeblockageAdd(this.attestationConformite)
			.subscribe((data) => {
				this.reportIds = [];
				if (data["status"] == "200") {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				} else this.snackBarService.openErrorSnackBar({ message: data["message"] });
			});
	}
}
