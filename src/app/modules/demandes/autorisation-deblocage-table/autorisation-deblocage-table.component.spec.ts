import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AutorisationDeblocageTableComponent } from "./autorisation-deblocage-table.component";

describe("AutorisationDeblocageTableComponent", () => {
	let component: AutorisationDeblocageTableComponent;
	let fixture: ComponentFixture<AutorisationDeblocageTableComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AutorisationDeblocageTableComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AutorisationDeblocageTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
