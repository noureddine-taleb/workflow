import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Subscription } from "rxjs";
import { take } from "rxjs/operators";
import { AutorisationDeblocageAddComponent } from "../autorisation-deblocage-add/autorisation-deblocage-add.component";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { AttestationConformiteService } from "../services/attestation-conformite.service";
import { DialogObjectActionsComponent } from "app/shared/dialog-object-actions/dialog-object-actions.component";

@Component({
	selector: "app-autorisation-deblocage-table",
	templateUrl: "./autorisation-deblocage-table.component.html",
	styleUrls: ["./autorisation-deblocage-table.component.scss"],
})
export class AutorisationDeblocageTableComponent implements OnInit {
	displayedColumns: string[] = [
		"unblokingNumber",
		"object",
		"unblockingAmount",
		"nbrDossier",
		"statusName",
		"action",
	];
	displayedColumnsLielle: string[] = [
		"N°Debl",
		"Objet",
		"Mnt.Débl",
		"Nbr.Doss",
		"Statut",
		"Action",
	];
	dataSource: any[] = [];
	request: LoanRequest;
	subscription: Subscription = new Subscription();

	constructor(
		private attestationConfirmiteSrv: AttestationConformiteService,
		private userService: AuthService,
		private dialog: MatDialog,
		private snackBarService: SnackBarService,
		public routingService: RoutingService,
		private utilsService: UtilsService,
		private commonService: CommonService,
		private route: ActivatedRoute,
		private router: Router,
		private demandeService: DemandeService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getList();
	}

	onPrepareAdd() {
		const dialogRef = this.dialog.open(AutorisationDeblocageAddComponent, {
			width: "90vw",
			data: { add: 1, dossier: null, requestId: this.request.requestId },
		});
		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
	}

	onPrepareEdit(element) {
		const dialogRef = this.dialog.open(AutorisationDeblocageAddComponent, {
			width: "90vw",
			data: { add: 2, certificat: element, dossier: null, requestId: this.request.requestId },
		});
		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});

		this.subscription.add(sub);
	}

	onDelete(element) {
		let critere = {};
		critere["entityId"] = this.userService.user.entityId;
		critere["userEntityId"] = this.userService.user.userEntityId;
		critere["userId"] = this.userService.user.userId;
		critere["requestId"] = this.request.requestId;
		critere["certificateId"] = element["certificateId"];

		this.snackBarService.openConfirmSnackBar({ message: "enregistrer" }).then((result) => {
			if (result.value) {
				let sub = this.attestationConfirmiteSrv
					.autorisationDeblockageDelete(critere)
					.subscribe((data) => {
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					});
				this.subscription.add(sub);
			}
		});
	}
	onListDossiers(element) {
		const dialogRef = this.dialog.open(AutorisationDeblocageAddComponent, {
			width: "90vw",
			data: { add: 0, dossiers: element.reports, requestId: this.request.requestId },
		});
		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
	}

	getList() {
		let criter: any = this.userService.user;
		criter.pas = 100;
		criter.first = 0;
		criter.requestId = this.request.requestId;

		let sub = this.attestationConfirmiteSrv.autorisationDeblockage(criter).subscribe((data) => {
			this.dataSource = data["result"];
			data["result"].forEach((element) => {
				element["nbrDossier"] = element["reports"].length;
			});
		});
		this.subscription.add(sub);
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("requestId" in changes) this.getList();
	}

	hasAddPriv() {
		return this.utilsService.hasPrivilege(this.utilsService.privilegeCodes.maj_dbl_m);
	}

	loadDblActions(dbl: any) {
		if (this.routingService.isDemandeRO() || Array.isArray(dbl.actions)) return;
		this.commonService
			.getAttestationDblActions(
				dbl["statusCode"],
			)
			.pipe(take(1))
			.subscribe((data: any) => {
				dbl.actions = data.result;
				// this.commonService.getActionPrivlege({module: "ATDEB", statusCode: dbl.statusCode}).pipe(take(1)).subscribe(
				//     data => {
				//             dbl.privs = data['result']
				//     }
				// )
			});
	}

	dblDispatchAction(action, dbl) {
		let postAction = () => {
			dbl.actions = null;
			this.reloadDemande();
			this.getList();
		}

		if (action.users.length == 1) {
			this.snackBarService
			.openConfirmSnackBar({ message: "confirmer l'action ?" })
			.then((result) => {
				if (result.value) {
					this.demandeService
						._dispatchAction(dbl, action, action.users[0])
						.subscribe((data) => {
							this.snackBarService.openSuccesSnackBar({ message: data["message"] });
							postAction();
						});
				}
			});
		} else {
			const dialogRef = this.dialog.open(DialogObjectActionsComponent, { data: {
					action: action,
					obj: dbl,
				}
			});
			dialogRef.afterClosed().subscribe(success => {
				if (success)
					postAction()
			});	
		}
	}

	reloadDemande() {
		this.routingService.reloadCurrentRoute()
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
