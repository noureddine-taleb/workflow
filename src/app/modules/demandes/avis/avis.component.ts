import { Component, OnInit, SimpleChanges } from "@angular/core";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { CreationType } from "app/shared/models/CreationType";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestType } from "app/shared/models/RequestType";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Advice, CreditRequestProduct } from "./avis.types";

@Component({
	selector: "app-avis",
	templateUrl: "./avis.component.html",
	styleUrls: ["./avis.component.scss"],
})
export class AvisComponent implements OnInit {
	productsdataSource;
	avisDatasource;
	CreditRequestProductsDtoList: CreditRequestProduct[];
	displayedColumns = [
		"financialProductName",
		"financialObjectCode",
		"duration",
		"deferredPeriod",
		"rate",
		"bonus",
		"derogation",
		"goodAmount",
		"startDate",
		// this field is added only if simulationFlag==N
		"endDate",
	];
	request: LoanRequest;
	requestTypes = RequestType;
	adviceDto = new Advice();
	products = []
	adviceCodes = []
	creationTypes: CreationType[] = []

	constructor(
		private demandeService: DemandeService,
		public authService: AuthService,
		private snackBarService: SnackBarService,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		public commonService: CommonService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.products = this.request.products;
		this.getAvis();
		this.commonService.getAdviceCodes().subscribe(data => {
			this.adviceCodes = data.result;
		});
		if (this.request.creditTypeId == RequestType.AMAQ)
			this.getCreationTypes()
	}

	getAdviceCodeName(code: string) {
		return this.adviceCodes.find(c => c.code === code)?.name;
	}

	advices = [];
	getAvis(): void {
		if (this.request.requestId) {
			this.demandeService.getAvis({ requestId: this.request.requestId,typewindow: 'AVIS'  }).subscribe((data) => {
				this.advices = data["result"];
				if (this.advices.filter((x) => !x.identifiant).length > 0)
					this.advices.filter((x) => !x.identifiant)[0].products = this.request.products;

				//this.productsdataSource = data['result']['products'];
				//this.avisDatasource = data['result']['advices'];
				this.adviceDto.products = data["result"]["products"];
				//this.CreditRequestProductsDtoList=[];
				//data['result']['products'].forEach(element => {
				//  this.CreditRequestProductsDtoList.push(this.toCreditRequestProductsDto(element))
				//});
				if (this.advices && this.advices.length > 0) {
					this.advices.forEach((x) => {
						if (x.products && x.products.length > 0) {
							x.products.forEach((pr) => {
								//lastDuration lastDeferredPeriod lastRate lastBonus
								pr["lastDuration_"] = pr["lastDuration"];
								pr["lastDeferredPeriod_"] = pr["lastDeferredPeriod"];
								pr["lastRate_"] = pr["lastRate"];
								pr["lastBonus_"] = pr["lastBonus"];
							});
						}
					});
				}
			});
		}
	}

	noSimulation(product) {
		return product.simulationFlag === 'N';
	}

	toCreditRequestProductsDto(product): CreditRequestProduct {
		let creditRequestProductsDto = new CreditRequestProduct();
		creditRequestProductsDto = product;
		creditRequestProductsDto.lastAmountGranted = creditRequestProductsDto.amountGranted;
		creditRequestProductsDto.lastBonus = creditRequestProductsDto.bonus;
		creditRequestProductsDto.lastContributionAmount =
			creditRequestProductsDto.contributionAmount;
		creditRequestProductsDto.lastDeferredPeriod = creditRequestProductsDto.deferredPeriod;
		creditRequestProductsDto.lastDuration = creditRequestProductsDto.duration;
		creditRequestProductsDto.lastFinancialProduct = creditRequestProductsDto.financialProduct;
		creditRequestProductsDto.lastGoodAmount = creditRequestProductsDto.goodAmount;
		creditRequestProductsDto.lastInsurance = creditRequestProductsDto.insurance;
		creditRequestProductsDto.lastMonthlyPayment = creditRequestProductsDto.monthlyPayment;
		creditRequestProductsDto.lastOthersFees = creditRequestProductsDto.othersFees;
		creditRequestProductsDto.lastPrelevementType = product.prelevementTypeCode;
		creditRequestProductsDto.lastRate = creditRequestProductsDto.rate;

		return creditRequestProductsDto;
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["requestId"]) {
			this.getAvis();
		}
	}

	onRegistrate(advice) {
		advice.requestId = this.request.requestId;
		//advice.products= advice.identifiant? advice.products:this.products;

		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeService.saveAvis(advice).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					this.getAvis();
				});
			}
		});
	}

	checkShowEcheance(element) {
		//lastDuration lastDeferredPeriod lastRate lastBonus
		return (
			element["lastDuration_"] == element["lastDuration"] &&
			element["lastDeferredPeriod_"] == element["lastDeferredPeriod"] &&
			element["lastRate_"] == element["lastRate"] &&
			element["lastBonus_"] == element["lastBonus"]
		);
	}

	getCreationTypes() {
		this.commonService.getCreationTypes().subscribe(data => {
			this.creationTypes = data.result;
		})
	}

	getCreationTypeName(id: number) {
		return this.creationTypes.find(v => v.id == id)?.designation;
	}
}
