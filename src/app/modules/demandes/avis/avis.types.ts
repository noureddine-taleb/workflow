export class CreditRequestProduct {
	amountGranted: number;
	bonus: number;
	contributionAmount: number;
	deferredPeriod: number;
	duration: number;
	financialProduct: number;
	financielObject: string;
	goodAmount: number;
	identifiant: number;
	insurance: number;
	lastAmountGranted: number;
	lastBonus: number;
	lastContributionAmount: number;
	lastDeferredPeriod: number;
	lastDuration: number;
	lastFinancialProduct: number;
	lastGoodAmount: number;
	lastInsurance: number;
	lastMonthlyPayment: number;
	lastOthersFees: number;
	lastPrelevementType: string;
	lastRate: number;
	monthlyPayment: number;
	othersFees: number;
	prelevementType: string;
	rate: number;
	refRequest: string;
	requestId: number;
	status: string;
	userEntityId: number;
	userId: number;
}

export class Advice {
	committee: number;
	conditionsReserves: string;
	guarantees: string;
	identifiant: number;
	opinion: string;
	products: CreditRequestProduct[];
	requestId: number;
	userEntityId: number;
	userId: number;
}
