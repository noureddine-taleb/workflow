import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FileService } from "app/core/services/file.service";
import { FileSaverService } from "ngx-filesaver";

@Component({
	selector: "app-bordereau",
	templateUrl: "./bordereau.component.html",
	styleUrls: ["./bordereau.component.scss"],
})
export class BordereauComponent implements OnInit {
	borderau = {};
	constructor(
		private _FileSaverService: FileSaverService,
		private fileService: FileService,
		private dialogRef: MatDialogRef<BordereauComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		this.borderau["garanties"] = data;
		this.borderau["observation"] = "";
	}

	ngOnInit(): void {}

	fileRegistrated = false;
	uploadFile() {
		this.fileRegistrated = true;
	}

	imageToShow: any;
	selectedFile: File = null;
	docPath: string = "";

	onFileSelected(event) {
		this.fileRegistrated = true;
		this.selectedFile = <File>event.target.files[0];
		this.docPath = this.selectedFile.name;
		let reader = new FileReader();
		reader.addEventListener(
			"load",
			() => {
				this.imageToShow = reader.result;
			},
			false,
		);
		if (this.selectedFile) {
			reader.readAsDataURL(this.selectedFile);
		}
	}

	bordereauCree = false;
	onCreateBordereau() {
		this.dialogRef.close(this.borderau);
	}

	onDeleteSignedBordereau() {
		this.fileRegistrated = false;
	}
}
