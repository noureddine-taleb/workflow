import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CheckRubriquesDialogComponent } from "./check-rubriques-dialog.component";

describe("CheckRubriquesDialogComponent", () => {
	let component: CheckRubriquesDialogComponent;
	let fixture: ComponentFixture<CheckRubriquesDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CheckRubriquesDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CheckRubriquesDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
