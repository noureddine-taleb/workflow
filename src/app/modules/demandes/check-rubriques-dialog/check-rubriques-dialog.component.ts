import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-check-rubriques-dialog",
	templateUrl: "./check-rubriques-dialog.component.html",
	styleUrls: ["./check-rubriques-dialog.component.scss"],
})
export class CheckRubriquesDialogComponent implements OnInit {
	listRubrique = [];
	constructor(
		public dialogRef: MatDialogRef<CheckRubriquesDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		this.listRubrique = data;
	}

	ngOnInit(): void {}

	onSelectRubrique(): void {
		this.dialogRef.close(this.listRubrique);
	}
}
