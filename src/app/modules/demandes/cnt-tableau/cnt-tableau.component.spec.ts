import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CntTableauComponent } from "./cnt-tableau.component";

describe("CntTableauComponent", () => {
	let component: CntTableauComponent;
	let fixture: ComponentFixture<CntTableauComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CntTableauComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CntTableauComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
