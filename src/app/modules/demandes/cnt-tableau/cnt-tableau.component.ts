import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DialogUsersComponent } from "app/shared/dialog-users/dialog-users.component";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CntHistoriqueComponent } from "../../../shared/cnt-historique/cnt-historique.component";
import { EditCntDialogComponent } from "../edit-cnt-dialog/edit-cnt-dialog.component";
import { Cnt } from "./cnt.types";
import { EventCode } from "app/modules/demandes/models/EventCode";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { PrivilegeService } from "app/modules/demandes/services/privilege.service";
import { CntService, CntAction } from "../services/cnt.service";
import {ReportGeneratorService} from "../../../shared/reportGenerator/report-generator.service";

@Component({
	selector: "app-cnt-tableau",
	templateUrl: "./cnt-tableau.component.html",
	styleUrls: ["./cnt-tableau.component.scss"],
})
export class CntTableauComponent implements OnInit {
	//displayedColumns=["typebien","produits.pack", "produits.prelev", "produits.duree", "differe", "taux", "bonif", "derog", "mensualite", "montantoctroye", "assurance", "statutcnt" ];
	displayedColumns = [
		"nomcp",
		"ppr",
		"mensualite",
		"pack",
		"duree",
		"taux",
		"impbudg",
		//"teg",
		"goodAmount",
		"loanAmount",
		"confirmedAmount",
		"startDate",
		"endDate",
		"statutCnt",
		"action",
	];

	dataSource2 = ELEMENT_DATA;
	dataSource = [];
	list: Cnt = new Cnt();
	tempRowId = null;
	tempRowCount = null;
	listImp = [];
	eventCodes = EventCode;
	request: LoanRequest;

	constructor(
		private cntService: CntService,
		private snackBarService: SnackBarService,
		private communService: CommonService,
		private dialog: MatDialog,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		private globasprivilege: PrivilegeService,
		private demandeService: DemandeService,
        private reportGeneratorService: ReportGeneratorService
	) {
		//this.cacheSpan('cin', d => d.cin);
	}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getReservation();
		this.getListImputation();
	}

	listAction = [];
	getCntAction() {
		let payload = new CntAction();
		payload.creditType = this.request.creditTypeId;
		payload.clientType = this.request.clientCategoryId;
		payload["lineId"] = this.list["cntId"];
		payload["creationEntityId"] = this.list["creationEntityId"];

		this.communService.getCntActions(this.list.statusCode, payload)
		.subscribe((data) => {
			this.listAction = data["result"];
		});
	}
	//statusCode
	getPrivilegeCnt() {
		if (this.list.cntId) {
			this.communService.getCntPrivelege(this.list.statusCode).subscribe((data) => {
				this.globasprivilege["listPrivilegeCnt"] = data["result"];
			});
		} else {
			this.globasprivilege["listPrivilegeCnt"] = this.globasprivilege["listPrivilege"];
		}
	}

    onEvccEdite(): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['report'] = 'EVCC';
                alert(this.list.cntId);
                criteria['demandecnt'] = this.list.cntId;
                criteria['requestId'] = this.request.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,('EVCEE_'+this.request.requestId)) ;
                });
            }
        });
    }

    onAutorisationCntEdite(): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['report'] = 'AUTORISATION_CNT';
                criteria['demandecnt'] = this.list.cntId;
                criteria['requestId '] = this.request.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,('AUTORISATION_CONSULTATION_CNT_'+this.request.requestId)) ;
                });
            }
        });
    }

	getReservation() {
		let sub = this.cntService.getReservations(this.request.requestId).subscribe((data) => {
			this.list = data["result"];
			this.transformData();

			if (data["result"]["cntId"]) {
				this.getCntAction();
			}
			this.getPrivilegeCnt();
		});
	}

	transformData() {
		if (!this.list?.persons) this.dataSource = [];
		this.dataSource = this.list?.persons?.reduce((current, next) => {
			next.products.forEach((b) => {
				current.push({
					registrationNumber: next.registrationNumber,
					cin: next.cin,
					fullname: next.fullname,
					budgetAllocation: next.budgetAllocation,
					products: b,
				});
			});
			return current;
		}, []);
		this.cashSpan();
	}

	getRowSpan(i) {
		const dt = this.spans.filter((x) => x.index == i);
		if (dt.length > 0) return dt[0]["span"];
		return 0;
	}
	spans = [];

	cashSpan() {
		let l = 0;

		this.list.persons.forEach((element) => {
			this.spans.push({ index: l, span: element.products.length });
			l += element.products.length;
		});
	}
	onRegister() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let cntDto = new Cnt();
				cntDto.cntId = this.list.cntId;
				cntDto.requestId = this.request.requestId;
				cntDto.statusCode = this.list.statusCode;
				const result = [];

				for (const tier of this.dataSource) {
					let item = result.find((x) => x.cin == tier.cin);
					if (!item) {
						result.push({
							cin: tier.cin,
							fullnam: tier.fullnam,
							registrationNumber: tier.registrationNumber,
							budgetAllocation: tier.budgetAllocation,
							products: [],
						});
						item = result.find((x) => x.cin == tier.cin);
					}
					item.products.push(tier.products);
				}

				cntDto.persons = result;

				this.cntService.crateRequest(cntDto).subscribe((data) => {
					this.getReservation();
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
			}
		});
	}

	datasourceToObject() {}

	getListImputation() {
		this.communService.getBudgetAllocation().subscribe((data) => {
			this.listImp = data["result"];
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("requestId" in changes) {
			//this.getReservation();
		}
	}

	onHistoric(element) {
		let critere = {};

		critere["reservationId"] = element["products"]["reservationId"];

		this.cntService.getHistory(critere).subscribe((data) => {
			this.dialog.open(CntHistoriqueComponent, { width: "90vw", data: data["result"] });
		});
	}

	onEditer(element) {
		let critere = {};
		this.dialog.open(EditCntDialogComponent, { width: "90vw", data: element });
	}
	onEditer2(element) {
		let critere = {};
		const dialogRef = this.dialog.open(EditCntDialogComponent, {
			width: "90vw",
			data: {
				reservations: element,
				cntId: this.list.cntId,
				requestId: this.request.requestId,
				statusCode: this.list.statusCode,
			},
		});
		dialogRef.afterClosed().subscribe((data) => {
			if (data.validation == true) this.getReservation();
		});
	}

	_onExecute(action: any) {
		let critere = {};
		critere["action"] = action.actionCode;
		critere["userActionId"] = action.userId;
		critere["cntId"] = action.cntId;

		this.cntService.executeAction(critere).subscribe((data) => {
			this.postExecuteAction();
			this.snackBarService.openSuccesSnackBar({ message: data["message"] });
		});
	}

	onExecuteAction(itemElement: any) {
		if (itemElement?.users?.length == 1) {
			this._onExecute({...itemElement, cntId: this.list.cntId, userId: itemElement.users[0].userId});
			return;
		}

		let critere = {};
		critere["action"] = itemElement;
		critere["cntId"] = this.list.cntId;
		critere["requestId"] = this.request.requestId;
		const dialogRef = this.dialog.open(DialogUsersComponent, { data: critere });
		dialogRef.afterClosed().subscribe(success => {
			if (success)
				this.postExecuteAction()
		});
	}

	postExecuteAction() {
		this.getReservation()
	}
}

export interface DemandeElement {
	ppr: string;
	cin: string;
	nomcp: string;
	produits: {
		pack: string;
		prelev: string;
		duree: number;
		differe: number;
		taux: number;
		bonif: number;
		derog: number;
		mensualite: number;
		montantoctroye: number;
		assurance: number;
		statutcnt: string;
		impbudg: string;
	}[];
}

const originalData = [
	{
		ppr: "15248",
		cin: "AB15247",
		nomcp: "Hicham Refouch",
		produits: [
			{
				pack: "sakan ataalim",
				prelev: "source",
				duree: 84,
				differe: 7,
				taux: 4.2,
				bonif: 2,
				derog: 4.0,
				mensualite: 2750,
				montantoctroye: 200000,
				assurance: 5432.15,
				statutcnt: "Réservé",
			},
			{
				pack: "sakan ataalim 2",
				prelev: "source",
				duree: 85,
				differe: 8,
				taux: 4.3,
				bonif: 2.1,
				derog: 3.0,
				mensualite: 2800,
				montantoctroye: 250000,
				assurance: 5632.15,
				statutcnt: "Rejeté",
			},
		],
	},
];

const DATA = originalData.reduce((current, next) => {
	next.produits.forEach((b) => {
		current.push({
			ppr: next.ppr,
			cin: next.cin,
			nomcp: next.nomcp,
			produits: {
				pack: b.pack,
				prelev: b.prelev,
				duree: b.duree,
				differe: b.differe,
				taux: b.taux,
				bonif: b.bonif,
				derog: b.derog,
				mensualite: b.mensualite,
				montantoctroye: b.montantoctroye,
				assurance: b.assurance,
				statutcnt: b.statutcnt,
			},
		});
	});
	return current;
}, []);

const ELEMENT_DATA: DemandeElement[] = DATA;
