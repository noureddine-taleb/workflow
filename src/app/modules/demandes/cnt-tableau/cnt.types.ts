export class CntPerson {
	budgetAllocation: number;
	cin: string;
	fullname: string;
	products: CntCreditProduct[];
	registrationNumber: string;
	requestPersonId: number;
}

export class Cnt {
	action: string;
	cntId: number;
	entityId: number;
	persons: CntPerson[];
	requestId: number;
	statusCode: string;
	userEntityId: number;
	userId: number;
}

export class CntCreditProduct {
	amountCnt: number;
	basePoint: number;
	bonus: number;
	budgetAllocation: number;
	cin: string;
	cntId: number;
	cntStatusCode: string;
	cntStatusName: string;
	contracted: string;
	contributionAmount: number;
	deferredPeriod: number;
	derogation: number;
	duration: number;
	financialObject: number;
	financialObjectCode: string;
	financialObjectName: string;
	financialProductId: number;
	financialProductName: string;
	fullname: string;
	goodAmount: number;
	insurance: number;
	lastAmountGranted: number;
	lastBonus: number;
	lastContributionAmount: number;
	lastDeferredPeriod: number;
	lastDuration: number;
	lastFinancialProduct: number;
	lastInsurance: number;
	lastMonthlyPayment: number;
	lastOthersFees: number;
	lastPrelevementType: string;
	lastProduct: number;
	lastRate: number;
	monthlyPayment: number;
	prelevementTypeCode: string;
	prelevementTypeName: string;
	productId: number;
	productType: string;
	rate: number;
	rateType: string;
	registrationNumber: string;
	requestAdviceId: number;
	requestId: number;
	requestPersonId: number;
	status: string;
}
