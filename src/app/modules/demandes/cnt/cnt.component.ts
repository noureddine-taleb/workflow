import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { EditCntDialogComponent } from "../edit-cnt-dialog/edit-cnt-dialog.component";

@Component({
	selector: "app-cnt",
	templateUrl: "./cnt.component.html",
	styleUrls: ["./cnt.component.css"],
})
export class CntComponent implements OnInit {
	@Input() idDemande;
	displayedColumns;
	dataSource;
	constructor(private demandeService: DemandeService, public dialog: MatDialog) {}

	ngOnInit(): void {
		this.loadData();
	}
	loadData() {
		this.displayedColumns = [
			"nomprenom",
			"numppr",
			"cin",
			"mensualite",
			"pack",
			"duree",
			"taux",
			"impbudg",
			"statutCnt",
			"statutDate",
			"10",
		]; //,'11','12'];
		this.dataSource = [
			{
				pack: "1245879",
				duree: 84,
				differe: 3,
				taux: 4.3,

				bonif: 2,
				derog: 4.0,
				mensualite: 2800.55,
				cin: "L487655",
				nompprenom: "ZAIMOUN ALI",
				envoireservation: "01/01/2021",
				receptionreponse: "02/02/2021",
				statutCnt: "Réservé",
				statutDate: "02/02/2021",
				ppr: 153215,
			},
			{
				pack: "1245879",
				duree: 84,
				differe: 3,
				taux: 4.3,
				bonif: 2,
				derog: 4.0,
				mensualite: 2800.55,
				cin: "AB201340",
				nompprenom: "KARIMA ALAHMADI",
				envoireservation: "01/01/2021",
				receptionreponse: "02/02/2021",
				statutCnt: "Réservé",
				statutDate: "02/02/2021",
				ppr: 325521,
			},
		];
	}

	onEdit() {
		const dialogRef = this.dialog.open(EditCntDialogComponent, {
			data: {
				demandeId: this.idDemande,
			},
			width: "600px",
		});

		dialogRef.afterClosed().subscribe((data) => {});
	}
}
