export class RequestComment {
	workflowId: number;
	userName: string;
	entityName: string;
	userRemark: string;
	actionDate: Date;
}