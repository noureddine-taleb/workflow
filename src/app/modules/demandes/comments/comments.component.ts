import { Component, OnInit } from '@angular/core';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import moment from 'moment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  /**
   * Constructor
   */
  constructor(public demandeService: DemandeService)
  {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
      // Get the activities
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Returns whether the given dates are different days
   *
   * @param current
   * @param compare
   */
  isSameDay(current: string, compare: string): boolean
  {
      return moment(current, moment.ISO_8601).isSame(moment(compare, moment.ISO_8601), 'day');
  }

  /**
   * Get the relative format of the given date
   *
   * @param date
   */
  getRelativeFormat(date: string): string
  {
      const today = moment().startOf('day');
      const yesterday = moment().subtract(1, 'day').startOf('day');

      // Is today?
      if ( moment(date, moment.ISO_8601).isSame(today, 'day') )
      {
          return "Aujourd'hui";
      }

      // Is yesterday?
      if ( moment(date, moment.ISO_8601).isSame(yesterday, 'day') )
      {
          return 'Hier';
      }

      return moment(date, moment.ISO_8601).locale('fr').fromNow();
  }

  /**
   * Track by function for ngFor loops
   *
   * @param index
   * @param item
   */
  trackByFn(index: number, item: any): any
  {
      return item.id || index;
  }
}
