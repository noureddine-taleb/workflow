import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { FileService } from "app/core/services/file.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DialogTableDetailComponent } from "app/shared/dialog-table-detail/dialog-table-detail.component";
import { GuarantyListComponent } from "app/shared/guaranty-list/guaranty-list.component";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";
import { PrivilegeService } from "app/modules/demandes/services/privilege.service";
import { OpcAddProductDialogComponent } from "app/shared/opc-add-product-dialog/opc-add-product-dialog.component";
import { FileSaverService } from "ngx-filesaver";
import { Subscription } from "rxjs";
import { AttestationConformiteAddComponent } from "../attestation-conformite-add/attestation-conformite-add.component";
import { BordereauComponent } from "../bordereau/bordereau.component";
import { ConstitutionGuaranty } from "./constitution.types";
import { DialogObjectActionsComponent } from "app/shared/dialog-object-actions/dialog-object-actions.component";
import {ReportGeneratorService} from "../../../shared/reportGenerator/report-generator.service";

@Component({
	selector: "app-constitution",
	templateUrl: "./constitution.component.html",
	styleUrls: ["./constitution.component.scss"],
})
export class ConstitutionComponent implements OnInit {
	displayedColumns: string[] = [
		"caucher",
		"couverage",
		"noGuaranty",
		"amount",
		"guarantyName",
		"reference",
		"guarantor",
		"consistency",
		"valinitGood",
		"amountLastEvaluation",
		"description",
		"status",
		"action",
	];

	displayedColumnsLielle: string[] = [
		"",
		"Couverture",
		"ID Sûreté",
		"Montant Sûreté",
		"Libellé Sûreté",
		"Référence Sûrete",
		"Garant",
		"Consistance",
		"Valeur Initiale",
		"Montant Derniere Evolution",
		"Description",
	];

	constututionGuarantieObject: ConstitutionGuaranty = new ConstitutionGuaranty();
	list;
	actions = [];
	dataSource = new MatTableDataSource();
	// demandePrivileges: any;
	privileges = PrivilegeCode;

	constructor(
		private demandeService: DemandeService,
		private dialog: MatDialog,
		private fileService: FileService,
		private _FileSaverService: FileSaverService,
		private snackBarService: SnackBarService,
		private route: ActivatedRoute,
		private router: Router,
		public communService: CommonService,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		public globasprivilege: PrivilegeService,
        public reportGeneratorService: ReportGeneratorService
	) {}
	request: LoanRequest;
	demandeDTO: LoanRequest;
	editStat = 0;
	ngOnInit(): void {
		//TODO
		//get lit from workflow
		this.request = this.demandeService.request;
		this.getResolvedParam();
		this.getList();
		this.getListReport();
		this.getListBordereau();
		this.constututionGuarantieObject.requestId = this.request.requestId;
		this.loadCreateBordereauPrivileges();
	}

	onAttacheReport(garantie) {
		this.listReport.forEach((item) => {
			let temp = garantie.reports?.find((x) => x.productId == item.productId);
			if (temp) {
				item["checked"] = true;
			} else item["checked"] = false;
		});
		const dialogRef = this.dialog.open(OpcAddProductDialogComponent, {
			data: { typeDemande: this.request.creditTypeId, products: this.listReport },
			width: "90vw",
		});
		let sub = dialogRef.afterClosed().subscribe((data) => {
			if (data) {
				garantie["reports"] = data;
				let sub2 = this.demandeService.garantyAttacheReport(garantie).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
				this.subscription.add(sub2);
			}
		});
		this.subscription.add(sub);
	}

	detacherGuaranty(guaranty) {
		this.constututionGuarantieObject.guaranties =
			this.constututionGuarantieObject.guaranties.filter(
				(g) => g.noGuaranty != guaranty.noGuaranty,
			);
		this.creerGarantiesDatasource();
	}

	displayedColumnsFooter(): any {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}

	// getDemandePrivileges() {
	//     let critere={};
	//     critere['module']='CRED';
	//     critere['statusCode']=this.demandeDTO.statusCode;
	//     this.communService.getActionPrivlege(critere).subscribe(
	//         data => {
	//                 this.demandePrivileges = data['result'];
	//         }
	//     )
	// }

	getResolvedParam(): void {
		this.demandeDTO = this.demandeService.request;
	}

	// ajouter fait le get d'un webservice externe
	//get list fait le get des garantie deja enregistré
	//
	onAddGarantieFromExterne(idSurety: string) {
		if (!idSurety) {
			this.snackBarService.openErrorSnackBar({ message: "ID Sûreté est vide" });
			return;
		}
		this.demandeService
			.getConstitutionGaranties(idSurety, this.request.requestId)
			.subscribe((data: any) => {
				this.constututionGuarantieObject.guaranties =
					this.constututionGuarantieObject.guaranties.concat(data.result);
				this.creerGarantiesDatasource();
			});
	}

	getList() {
		this.demandeService.getConstitutionGarantie(this.request.requestId).subscribe((data) => {
			this.constututionGuarantieObject.guaranties = data["result"];
			this.creerGarantiesDatasource();
		});
	}

	afficherButtonEnregistrer() {
		return (
			this.constututionGuarantieObject?.guaranties?.filter((garantie) => !garantie.guarantyId)
				.length > 0 &&
			this.utilsService.hasPrivilege(
				this.privileges.maj_garantie_realisation_m /*, this.demandePrivileges*/,
			)
		);
	}

	creerGarantiesDatasource() {
		this.dataSource = new MatTableDataSource(this.constututionGuarantieObject.guaranties);
	}

	loadGaurantyActionsPrivs(guaranty: any) {
		if (this.routingService.isDemandeRO() || Array.isArray(guaranty.actions))
			return;
		let sub = this.communService
			.getGuarantyActions(
				guaranty["statusCode"],
			)
			.subscribe((data: any) => {
				guaranty.actions = data.result;
				let sub = this.communService
					.getGuarantyPrivelege(guaranty.statusCode)
					.subscribe((data) => {
						guaranty.privs = data["result"];
						/*if (!this.communService.hasPrivilege(this.privileges.maj_garantie_m, guaranty.privs)) {
                            guaranty.actions = guaranty.actions.filter(a => a.actionCode != 'SU');
                        }*/
					});
				this.subscription.add(sub);
			});
		this.subscription.add(sub);
	}

	loadCreateBordereauPrivileges() {
		let sub = this.communService
			.getGuarantyPrivelege("GAR_VA")
			.subscribe((data) => {
				this.globasprivilege.listPrivilegeGar = data["result"];
			});
		this.subscription.add(sub);
	}

	loadBordereauActionsPrivs(bordereau: any) {
		if (this.routingService.isDemandeRO() || Array.isArray(bordereau.actions))
			return;
		let sub = this.communService
			.getBordereauActions(
				bordereau["statusCode"],
			)
			.subscribe((data: any) => {
				bordereau.actions = data.result;
				let sub = this.communService
					.getBordereauPrivelege(bordereau.statusCode)
					.subscribe((data) => {
						bordereau.privs = data["result"];
						if (
							!this.communService.hasPrivilege(
								this.privileges.maj_garantie_m,
								bordereau.privs,
							)
						) {
							bordereau.actions = bordereau.actions.filter(
								(a) => a.actionCode != "SU",
							);
						}
					});
				this.subscription.add(sub);
			});
		this.subscription.add(sub);
	}

	guarantyDispatchAction(action, guaranty) {
		let postAction = () => {
			this.getList();
		};

		if (action.users.length == 1) {
			this.snackBarService
				.openConfirmSnackBar({ message: "confirmer l'action ?" })
				.then((result) => {
					if (result.value) {
						this._dispatchAction(guaranty, action, postAction)
					}
				});
		} else {
			const dialogRef = this.dialog.open(DialogObjectActionsComponent, { data: {
				action: action,
				obj: guaranty,
			} });
			dialogRef.afterClosed().subscribe(success => {
				if (success)
					postAction()
			});
		}
	}

	onListDossiers(element) {
		this.dialog.open(AttestationConformiteAddComponent, {
			width: "80vw",
			data: {
				add: 0,
				dossiers: element.reports,
				showDetach: false,
				requestId: this.request.requestId,
			},
		});
	}

	onListGuaranties(element) {
		this.dialog.open(GuarantyListComponent, { width: "80vw", data: element.guaranties });
	}

	_dispatchAction(obj: any, action: any, postAction: () => void) {
		this.demandeService._dispatchAction(obj, action, action.users[0]).subscribe((data) => {
			this.snackBarService.openSuccesSnackBar({ message: data["message"] });
			obj.actions = null;
			postAction();
		});
	}

    onEditBordereau(element): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['bordereau'] = element.boId;
                criteria['report'] = 'BORDEREAU';
                criteria['creditRequest'] = this.demandeDTO.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,'AUTORISATION_CONFORMITE_'+element.boId) ;
                });
            }
        });
    }

	bordereauDispatchAction(action, bordereau) {
		let postAction = () => {
			this.reloadDemande();
			this.getListBordereau();
			this.utilsService.loadRequestPrivs();
		};

		if (action.users.length == 1) {
			this.snackBarService
			.openConfirmSnackBar({ message: "confirmer l'action ?" })
			.then((result) => {
				if (result.value) {
					this._dispatchAction(bordereau, action, postAction)
				}
			});
		} else {
			const dialogRef = this.dialog.open(DialogObjectActionsComponent, { data: {
				action: action,
				obj: bordereau,
			} });
			dialogRef.afterClosed().subscribe(success => {
				if (success)
					postAction()
			});
		}
	}

	reloadDemande() {
		this.routingService.reloadCurrentRoute()
	}

	saveGaranties() {
		//Envoi la liste des garanties pour etre enregistrer dans le workflow
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService
					.saveGuarantees(this.constututionGuarantieObject)
					.subscribe((data) => {
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
						this.getList();
					});
				this.subscription.add(sub);
			}
		});
	}

	onDeleteGaranty(element) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				if (element["guarantyId"]) {
					let sub = this.demandeService.deleteGuarante(element).subscribe((data) => {
						this.constututionGuarantieObject.guaranties.splice(
							this.list.indexOf(element),
							1,
						);
						this.creerGarantiesDatasource();
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					});
					this.subscription.add(sub);
				} else {
					this.constututionGuarantieObject.guaranties.splice(
						this.constututionGuarantieObject.guaranties.indexOf(element),
						1,
					);
					this.creerGarantiesDatasource();
				}
			}
		});
	}

	onValidateGaranty(element) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				if (element["guarantyId"]) {
					let sub = this.demandeService.validerGaranty(element).subscribe((data) => {
						this.getList();
						this.snackBarService.openSuccesSnackBar({ message: data["status"] });
					});
					this.subscription.add(sub);
				}
			}
		});
	}

	onVerifGaranty(element) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				if (element["guarantyId"]) {
					let sub = this.demandeService.verifGaranty(element).subscribe((data) => {
						this.getList();

						this.snackBarService.openSuccesSnackBar({ message: data["status"] });
					});
					this.subscription.add(sub);
				}
			}
		});
	}

	listReport = [];

	getListReport() {
		let sub = this.demandeService.listReports(this.request.requestId).subscribe((data) => {
			this.listReport = data["result"];
		});
		this.subscription.add(sub);
	}

	bordereauDasource;

	creerBordereau() {
		const dialogRef = this.dialog.open(BordereauComponent);
		let sub = dialogRef.afterClosed().subscribe((data) => {
			let bordereau = {};
			bordereau["guaranties"] = this.constututionGuarantieObject.guaranties.filter(
				(x) => x["checked"],
			);
			bordereau["observation"] = data["observation"];
			bordereau["requestId"] = this.request.requestId;
			let sub2 = this.demandeService.createBo(bordereau).subscribe((data) => {
				this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				this.getList();
				this.getListBordereau();
			});
			//this.listBordereau.push(data);
			//this.bordereauDasource=new MatTableDataSource(this.listBordereau);
			console.warn(bordereau);
		});
		this.subscription.add(sub);
	}

	private getListBordereau() {
		let sub = this.demandeService.listBordereau(this.request.requestId).subscribe((data) => {
			this.listBordereau = data["result"];
			this.bordereauDasource = new MatTableDataSource(this.listBordereau);
		});

		this.subscription.add(sub);
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("requestId" in changes) {
			//this.getList();
			//TODO
			//get lit from workflow
		}
	}

	listBordereau = [];
	imageToShow: any;
	selectedFile: File = null;
	docPath: string = "";

	onFileSelected(event) {
		this.selectedFile = <File>event.target.files[0];
		this.docPath = this.selectedFile.name;
		let reader = new FileReader();
		reader.addEventListener(
			"load",
			() => {
				this.imageToShow = reader.result;
			},
			false,
		);
		if (this.selectedFile) {
			reader.readAsDataURL(this.selectedFile);
		}
	}

	onTelechrgerBordereau() {
		this.fileService.getFileByUrl(`assets/files/test.pdf`).subscribe((res) => {
			this._FileSaverService.save(res.body, "save.pdf");
		});
		return;
	}

	onDeleteSignedBordereau(elemet) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService.deleteBordereau(elemet).subscribe((data) => {
					this.listBordereau.splice(this.listBordereau.indexOf(elemet), 1);
					this.bordereauDasource = new MatTableDataSource(this.listBordereau);
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
				this.subscription.add(sub);
			}
		});
	}

	signer(elemet) {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService.signerBordereau(elemet).subscribe((data) => {
					this.getListBordereau();
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
				this.subscription.add(sub);
			}
		});
	}

	subscription = new Subscription();
	idGuarantie: string;

	onBordereauGarantie(elemnt) {
		let dispco = [
			"couverage",
			"guarantyId",
			"amount",
			"guarantyName",
			"reference",
			"guarantor",
			"consistency",
			"valinitGood",
			"amountLastEvaluation",
			"description",
			"status",
		];
		let libCol = [
			"Couverture",
			"ID Sûreté",
			"Montant Sûreté",
			"Libellé Sûreté",
			"Référence Sûrete",
			"Garant",
			"Consistance",
			"Valeur Initiale",
			"Montant Derniere Evolution",
			"Description",
		];
		this.dialog.open(DialogTableDetailComponent, {
			width: "70vw",
			data: {
				dataSource: elemnt.guaranties,
				displayedColumns: dispco,
				displayedColumnsLielle: libCol,
			},
		});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	garantiesChecked() {
		if (!this.constututionGuarantieObject || !this.constututionGuarantieObject?.guaranties)
			return false;
		let len = this.constututionGuarantieObject?.guaranties
			?.filter((x) => x["checked"])
			.filter((x) => x["statusCode"] != "GAR_VA")?.length;
		if (!this.constututionGuarantieObject?.guaranties) return false;

		return (
			this.utilsService.hasPrivilegeGar(this.privileges.maj_bo_garantie_m) &&
			this.constututionGuarantieObject?.guaranties.filter((x) => x["checked"]).length > 0 &&
			len == 0
		);
	}
}
