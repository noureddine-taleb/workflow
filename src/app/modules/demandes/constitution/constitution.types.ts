export class ConstitutionGuaranty {
	guaranties: GuarantyDto[];
	requestId: number;
	userEntityId: number;
	userId: number;
}

export class GuarantyDto {
	amount: number;
	amountLastEvaluation: number;
	consistency: string;
	couverage: string;
	description: string;
	guarantor: string;
	guarantyCode: string;
	guarantyId: number;
	guarantyName: string;
	guarantyType: string;
	noGuaranty: string;
	rank: number;
	reference: string;
	reports: CreditReportResult[];

	requestId: number;
	userEntityId: number;
	userId: number;
	valinitGood: number;
}

export class CreditReportResult {
	accessoryFres: number;
	amortizedCapital: number;
	certificateId: number;
	contractId: number;
	creditAmount: number;
	deadlineDate: string;
	deadlineNumber: number;
	description: string;
	differed: number;
	dueAmount: number;
	duration: number;
	firstDeadlineDate: string;
	fres: number;
	guarantyId: number;
	insuranceAmount: number;
	interstAmount: number;
	lastDeadlineDate: string;
	nodoss: string;
	nominalAmount: number;
	periodicity: string;
	productId: number;
	productName: string;
	rate: number;
	reportId: number;
	statusCode: string;
	statusName: string;
	teg: number;
	userEntityId: number;
	userId: number;
	variablePartRate: number;
	variableRate: number;
	versementInitial: number;
}

export class Guaranty {
	description: string;
	entityId: number;
	identifiant: number;
	nature: number;
	natureName: string;
	observation: string;
	rank: number;
    totalcharges: number;
	reference: string;
	requestId: number;
	userEntityId: number;
	userId: number;
	value: number;
}
