import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CreditBureauDetailComponent } from "./credit-bureau-detail.component";

describe("CreditBureauDetailComponent", () => {
	let component: CreditBureauDetailComponent;
	let fixture: ComponentFixture<CreditBureauDetailComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CreditBureauDetailComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CreditBureauDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
