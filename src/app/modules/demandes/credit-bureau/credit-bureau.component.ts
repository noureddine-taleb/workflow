import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { RequestPerson } from "app/shared/models/RequestPerson";
import { CreditBureauDetailComponent } from "../credit-bureau-detail/credit-bureau-detail.component";
import { ScoringService } from "app/modules/demandes/services/scoring.service";
import { UtilsService } from "app/core/services/utils.service";

@Component({
	selector: "app-credit-bureau",
	templateUrl: "./credit-bureau.component.html",
	styleUrls: ["./credit-bureau.component.css"],
})
export class CreditBureauComponent implements OnInit {
	constructor(
		private dialog: MatDialog,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		private snackBarService: SnackBarService,
		private demandeService: DemandeService,
		private scoringService: ScoringService
	) {}

	displayedColumns;
	dataSource;

	requestId: number;
	tiers: RequestPerson[];

	ngOnInit(): void {
		this.dataSource = this.tiers;
		this.tiers = this.demandeService.persons;
		this.requestId = this.demandeService.request.requestId;
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("tiers" in changes) {
			this.dataSource = this.tiers;
		}
	}

	onDetail(element) {
		const dialogRef = this.dialog.open(CreditBureauDetailComponent, {
			width: "90vw",
			data: element,
		});
		dialogRef.afterClosed().subscribe((data) => {});
	}

	demanderCB() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.scoringService.getScoring(this.requestId).subscribe((data) => {
					this.demandeService.getTiers(this.requestId).subscribe((data) => {
						this.tiers = data.result.results;
					});
				});
			}
		});
	}
}
