import { Component, OnInit } from "@angular/core";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { CreationType } from "app/shared/models/CreationType";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestType } from "app/shared/models/RequestType";

@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.scss']
})
export class DecisionComponent implements OnInit {
	productsdataSource;
	avisDatasource;
	displayedColumns = [
		"financialProductName",
		"financialObjectCode",
		"duration",
		"deferredPeriod",
		"rate",
		"bonus",
		"derogation",
		"goodAmount",
		"startDate",
		// this field is added only if simulationFlag==N
		"endDate",
	];
	request: LoanRequest;
	requestTypes = RequestType;
	creationTypes: CreationType[] = []

	constructor(
		private demandeService: DemandeService,
		public authService: AuthService,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		public commonService: CommonService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getAvis();
	}

	advice: any = {};
	getAvis(): void {
		if (this.request.requestId) {
			this.demandeService.getAvis({ requestId: this.request.requestId, origin: 'C' }).subscribe((data) => {
				this.advice = data["result"][0];
			});
		}
	}

	checkShowEcheance(element) {
		//lastDuration lastDeferredPeriod lastRate lastBonus
		return (
			element["lastDuration_"] == element["lastDuration"] &&
			element["lastDeferredPeriod_"] == element["lastDeferredPeriod"] &&
			element["lastRate_"] == element["lastRate"] &&
			element["lastBonus_"] == element["lastBonus"]
		);
	}

	getCreationTypeName(id: number) {
		return this.creationTypes.find(v => v.id == id)?.designation;
	}
}
