import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DemandeActionDialogComponent } from "./demande-action-dialog.component";

describe("DemandeActionDialogComponent", () => {
	let component: DemandeActionDialogComponent;
	let fixture: ComponentFixture<DemandeActionDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DemandeActionDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DemandeActionDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
