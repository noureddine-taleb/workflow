import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Action } from "app/shared/models/Action";
import { User } from "app/shared/models/User";
import { Observable } from "rxjs";

export interface DemandeActionInput {
	actions: Action[],
	handler: (user: User, action: Action, note: string, ...args: any) => Observable<any>,
	args: any[],
};

@Component({
	selector: "app-demande-action-dialog",
	templateUrl: "./demande-action-dialog.component.html",
	styleUrls: ["./demande-action-dialog.component.scss"],
})
export class DemandeActionDialogComponent implements OnInit {
	note = "";
	action: Action;
	entite: any;
	user: User;
	requestId = 0;
	showNote = false;
	// showComment: boolean;

	constructor(
		private dialogRef: MatDialogRef<DemandeActionDialogComponent>,
		private demandeService: DemandeService,
		private snackBarService: SnackBarService,
		private authService: AuthService,
		@Inject(MAT_DIALOG_DATA) private data: DemandeActionInput,
	) {}

	ngOnInit(): void {
		this.getlistAction();
	}
	
	onValidate() {}

	listAction = [];

	getlistAction() {
		this.listAction = this.data.actions;
	}
	
	actionChanged($event) {
		this.user = null;
		if ($event?.users?.length == 1) {
			this.user = $event?.users[0];
			this.updateNoteVisibility()
		}
	}

	updateNoteVisibility() {
		this.showNote = this.user.userId != this.authService.user.userId; //&& !this.data.noComment;
	}

	onExecuter() {
		this.data.handler(this.user, this.action, this.note, ...this.data.args).subscribe(
			(data) => {
				this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				this.dialogRef.close({ validation: true });
			},
			(_) => {
				this.dialogRef.close({ validation: false });
			},
		);
	}
}
