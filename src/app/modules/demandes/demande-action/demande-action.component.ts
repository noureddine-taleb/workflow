import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DemandeActionDialogComponent } from "../demande-action-dialog/demande-action-dialog.component";

@Component({
	selector: "app-demande-action",
	templateUrl: "./demande-action.component.html",
	styleUrls: ["./demande-action.component.scss"],
})
export class DemandeActionComponent implements OnInit {

	show: boolean = false;

	constructor(
		private dialog: MatDialog,
		public demandeService: DemandeService,
		private authService: AuthService,
		private snackBarService: SnackBarService,
		private routingService: RoutingService,
		private router: Router,
	) {}

	ngOnInit(): void {
		this.show = this.demandeService.request.userId == this.authService.user.userId;
		console.log({show: this.show, userId: this.authService.user.userId, demandeUserId: this.demandeService.request.userId})
	}


	dispatchAction(direction: "next" | "prev") {
		if (this.demandeService.requestActions[direction].length == 1 && this.demandeService.requestActions[direction][0]?.users?.length == 1 && this.demandeService.requestActions[direction][0]?.users?.[0]?.userId == this.authService.user.userId) {
			let action = this.demandeService.requestActions[direction][0];
			let user = action.users[0]
			return this.demandeService.executeDemandeAction(user, action, null).subscribe(
				(data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					this.refresh();
				}
			)
		}

		const dialogRef = this.dialog.open(DemandeActionDialogComponent, {
			maxWidth: "575px",
			width: "575px",
			data: {
				actions: this.demandeService.requestActions[direction],
				handler: this.demandeService.executeDemandeAction.bind(this.demandeService),
				args: []
			},
		});

		dialogRef.afterClosed().subscribe((data) => {
			if (data && data["validation"] == true)
				this.refresh()
		});
	}

	async refresh() {
		await this.router.navigate([], {
			queryParams: {
				index: undefined
			},
			queryParamsHandling: 'merge'
		})
		this.routingService.reloadCurrentRoute().then(success => {
			if (success) {
				if (this.demandeService.request.reconsidredRequestId)
					this.demandeService.request.requestId = this.demandeService.request.reconsidredRequestId;
			}
		})
	}
}
