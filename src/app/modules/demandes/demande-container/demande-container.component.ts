import { animate, style, transition, trigger } from "@angular/animations";
import { AfterViewInit, Component, InjectionToken, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTabGroup } from "@angular/material/tabs";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { LayoutService } from "app/layout/services/layout.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { EventCode } from "app/modules/demandes/models/EventCode";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { PrivilegeService } from "app/modules/demandes/services/privilege.service";
import { RequestPerson } from "app/shared/models/RequestPerson";
import { SejourHistoriqueDialogComponent } from "app/shared/sejour-historique-dialog/sejour-historique-dialog.component";
import { Subscription } from "rxjs";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";

@Component({
	selector: "app-demande-container",
	templateUrl: "./demande-container.component.html",
	styleUrls: ["./demande-container.component.scss"],
	animations: [
		trigger("inOutAnimation", [
			transition(":enter", [
				style({ right: -600 }),
				animate("500ms ease-out", style({ right: 20 })),
			]),
			transition(":leave", [
				style({ right: 20 }),
				animate("500ms ease-in", style({ right: -600 })),
			]),
		]),
	],
})
export class DemandeContainerComponent implements OnInit, AfterViewInit {
	@ViewChild(MatTabGroup, {static: false}) tabGroup: MatTabGroup;

	eventCodes = EventCode;
	privilegeCode = PrivilegeCode;
	constructor(
		private route: ActivatedRoute,
		private snackBarService: SnackBarService,
		public demandeService: DemandeService,
		private commonService: CommonService,
		private dialog: MatDialog,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		public globasprivilege: PrivilegeService,
		private layoutService: LayoutService,
		private router: Router
	) {}
	listPrivilege = [];

	getPrivilege() {
		this.commonService.getRequestPrivelege()
		.subscribe((data) => {
			this.globasprivilege["listPrivilege"] = data["result"];
		});
	}

	getRequestActions() {
		this.demandeService.fetchDemandeActions().subscribe();
	}

	getRequestComments() {
		this.demandeService.getRequestComments().subscribe()
	}

	ngOnInit(): void {
		this.persons = this.demandeService.persons;
		this.request = this.demandeService.request;
		this.layoutService.title.next("Demande details")
		this.getPrivilege();
		this.getRequestActions();
		this.getListTypeBien();
		this.getRequestComments()
	}

	ngAfterViewInit(): void {
		this.updateTabGroupIndex();
	}

	updateTabGroupIndex() {
		setTimeout(() => this.tabGroup?.realignInkBar(), 500);
		this.tabGroup && (this.tabGroup.selectedIndex = this.getTabIndex());
	}

	getTabIndex() {
		let index = this.route.snapshot.queryParamMap.get("index")
		if (index?.length) {
			return Number(index);
		}

		// fallback to the last one
		return (this.getTopLevelTabsCount() - 1);
	}

	getTopLevelTabsCount() {
		let topLevelTabs = [this.eventCodes.vision_360, this.eventCodes.detail_dem, this.eventCodes.fiche_sign, this.eventCodes.present_entrep, this.eventCodes.projet_inv, this.eventCodes.revenu_charge, this.eventCodes.credit_bureau, this.eventCodes.etude_cnt, this.eventCodes.etude_demande, this.eventCodes.scoring, this.eventCodes.scoring_agr, this.eventCodes.avis, this.eventCodes.comite, this.eventCodes.realisation]
		return topLevelTabs.filter(tab => this.utilsService.afficheable(tab)).length;
	}

	onRetour() {
		this.routingService.requestDetailGoBack(this.request, this.route);
	}

	listBien;
	getListTypeBien(): void {
		let sub = this.commonService
			.getTypeBien({ parentId: this.request.creditTypeId })
			.subscribe((data) => (this.listBien = data["result"]));
		this.subsription.add(sub);
	}

	// partie dynamique /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	request: LoanRequest;
	persons: RequestPerson[];
	person;
	loadPerson(): void {
		this.commonService.getPerson({ personId: this.request.personId }).subscribe((data) => {
			this.person = data.result;
		});
	}

	editObject = false;

	onEditObjet(): void {
		this.editObject = true;
	}

	onValidEditObjet(): void {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeService.updateDemande(this.request).subscribe((_) => {
					this.snackBarService.openInfoSnackBar({
						message: "Modification effectuée avec succès",
					});
				});
			}
		});

		this.editObject = false;
	}

	toggledDoc = false;
	toggleDocCaontainer() {
		this.toggledDoc = !this.toggledDoc;
	}

	dateSituation: Date;

	onNewSituation($event: any): void {
		this.dateSituation = $event;
	}

	dateSituationDom: Date;
	onNewSituationDom($event: any): void {
		this.dateSituationDom = $event;
	}

	dateSituationCompt: Date;
	isVisible = false;
	onNewSituationCompt($event: any): void {
		this.dateSituationCompt = $event;
	}
	realisationPanl = 1;
	realisationPanel(number: number) {
		this.realisationPanl = number;
	}

	onclickSejour() {
		const dialogRef = this.dialog.open(SejourHistoriqueDialogComponent, {
			data: { demande: this.request.requestId },
			width: "90vw",
		});
		dialogRef.afterClosed().subscribe((data) => {});
	}
	
	subsription: Subscription = new Subscription();

	tabChange({index}: {index: number}) {
		this.router.navigate([], {
			relativeTo: this.route,
			queryParams: {
				index,
			},
			queryParamsHandling: 'merge',
		})
	}

	ngOnDestroy() {
		this.subsription.unsubscribe();
	}
}
