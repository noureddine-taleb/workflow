import { CommonModule, DatePipe, registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { MatTooltipModule } from "@angular/material/tooltip";
import { RouterModule } from "@angular/router";
import { AddDemandeHabitatModule } from "app/shared/add-demande-habitat/add-demande-habitat.module";
import { CardModule } from "app/shared/card";
import { CautionsModule } from "app/shared/cautions/cautions.module";
import { CntHistoriqueModule } from "app/shared/cnt-historique/cnt-historique.module";
import { DemandeDetailModule } from "app/shared/demande-detail/demande-detail.module";
import { DemandeDocsAttchModule } from "app/shared/demande-docs-attch/demande-docs-attch.module";
import { DemandeDocsModule } from "app/shared/demande-docs/demande-docs.module";
import { DemandeInfoModule } from "app/shared/demande-info/demande-info.module";
import { DemandeQuiModule } from "app/shared/demande-qui/demande-qui.module";
import { DialogObjectActionsModule } from "app/shared/dialog-object-actions/dialog-object-actions.module";
import { DialogTableDetailModule } from "app/shared/dialog-table-detail/dialog-table-detail.module";
import { DialogUsersModule } from "app/shared/dialog-users/dialog-users.module";
import { EnteteDemandeModule } from "app/shared/entete-demande/entete-demande.module";
import { GuarantyListModule } from "app/shared/guaranty-list/guaranty-list.module";
import { ListDemandeModule } from "app/shared/list-demande/list-demande.module";
import { OpcAddProductDialogModule } from "app/shared/opc-add-product-dialog/opc-add-product-dialog.module";
import { RubriqueModule } from "app/shared/rubrique/rubrique.module";
import { SejourHistoriqueDialogModule } from "app/shared/sejour-historique-dialog/sejour-historique-dialog.module";
import { NgxCurrencyModule } from "ngx-currency";
import { InvestitssementModule } from "../investitssement/investitssement.module";
import { AttachementComponent } from "./attachement/attachement.component";
import { AttestationConformiteAddComponent } from "./attestation-conformite-add/attestation-conformite-add.component";
import { AttestationConformiteTableComponent } from "./attestation-conformite-table/attestation-conformite-table.component";
import { AutorisationDeblocageAddComponent } from "./autorisation-deblocage-add/autorisation-deblocage-add.component";
import { AutorisationDeblocageTableComponent } from "./autorisation-deblocage-table/autorisation-deblocage-table.component";
import { AvisComponent } from "./avis/avis.component";
import { BordereauComponent } from "./bordereau/bordereau.component";
import { CheckRubriquesDialogComponent } from "./check-rubriques-dialog/check-rubriques-dialog.component";
import { CntTableauComponent } from "./cnt-tableau/cnt-tableau.component";
import { CntComponent } from "./cnt/cnt.component";
import { CommentsComponent } from './comments/comments.component';
import { ConstitutionComponent } from "./constitution/constitution.component";
import { CreditBureauDetailComponent } from "./credit-bureau-detail/credit-bureau-detail.component";
import { CreditBureauComponent } from "./credit-bureau/credit-bureau.component";
import { DecisionComponent } from './decision/decision.component';
import { DemandeActionDialogComponent } from "./demande-action-dialog/demande-action-dialog.component";
import { DemandeActionComponent } from "./demande-action/demande-action.component";
import { DemandeContainerComponent } from "./demande-container/demande-container.component";
import { demandeRoutes } from "./demande.routing";
import { DetailDemandeComponent } from './detail-demande/detail-demande.component';
import { DomiciliationComponent } from "./domiciliation/domiciliation.component";
import { EditCntDialogComponent } from "./edit-cnt-dialog/edit-cnt-dialog.component";
import { EndettementCamComponent } from "./endettement-cam/endettement-cam.component";
import { EtudeAddComponent } from "./etude-add/etude-add.component";
import { GuarantiesComponent } from "./guaranties/guaranties.component";
import { IncidentPaiementComponent } from "./incident-paiement/incident-paiement.component";
import { ObjetFinancementComponent } from "./objet-financement/objet-financement.component";
import { PatrimonyComponent } from "./patrimony/patrimony.component";
import { ProductsSimpleComponent } from "./products-simple/products-simple.component";
import { RealisationModule } from "./realisation/realisation.module";
import { RevenuChargeComponent } from "./revenu-charge/revenu-charge.component";
import { ScoringAgriComponent } from './scoring-agri/scoring-agri.component';
import { ScoringComponent } from "./scoring/scoring.component";
import { SituationComptesComponent } from "./situation-comptes/situation-comptes.component";
import {TasbikFdaModule} from "../tasbikFda/tasbik-fda.module";
import {IstidamaModule} from "../istidama/istidama.module";

registerLocaleData(localeFr);
registerLocaleData(localeEs);
@NgModule({
	declarations: [
		DemandeContainerComponent,
		EndettementCamComponent,
		CreditBureauComponent,
		CntComponent,
		RevenuChargeComponent,
		ProductsSimpleComponent,
		ObjetFinancementComponent,
		GuarantiesComponent,
		DomiciliationComponent,
		IncidentPaiementComponent,
		SituationComptesComponent,
		PatrimonyComponent,
		AvisComponent,
		EditCntDialogComponent,
		ScoringComponent,
		EtudeAddComponent,
		AttachementComponent,
		CntTableauComponent,
        CommentsComponent,
		CheckRubriquesDialogComponent,
		ScoringAgriComponent,
		DetailDemandeComponent,
  		DecisionComponent,
		AttestationConformiteAddComponent,
		AttestationConformiteTableComponent,
		AutorisationDeblocageAddComponent,
		AutorisationDeblocageTableComponent,
		BordereauComponent,
		ConstitutionComponent,
		CreditBureauDetailComponent,
		DemandeActionComponent,
		DemandeActionDialogComponent,
	],
	exports: [GuarantiesComponent],
    imports: [
        CommonModule,
        CardModule,
        RouterModule.forChild(demandeRoutes),
        RealisationModule,
        FormsModule,
        ReactiveFormsModule,
        AddDemandeHabitatModule,
        MatTableModule,
        MatDialogModule,
        MatTabsModule,
        MatFormFieldModule,
        MatButtonModule,
        MatExpansionModule,
        MatIconModule,
        EnteteDemandeModule,
        DemandeQuiModule,
        DemandeInfoModule,
        MatMenuModule,
        MatButtonToggleModule,
        NgxCurrencyModule,
        ListDemandeModule,
        MatDividerModule,
        InvestitssementModule,
        DemandeDetailModule,
        CntHistoriqueModule,
        DialogUsersModule,
        DialogTableDetailModule,
        GuarantyListModule,
        OpcAddProductDialogModule,
        SejourHistoriqueDialogModule,
        DemandeDocsModule,
        DemandeDocsAttchModule,
        DialogObjectActionsModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        MatCheckboxModule,
        MatDatepickerModule,
        RubriqueModule,
        CautionsModule,
        MatTooltipModule,
        TasbikFdaModule,
        IstidamaModule,
    ],
	providers: [
		{ provide: DatePipe, useClass: DatePipe }
	]
})
export class DemandeModule {}
