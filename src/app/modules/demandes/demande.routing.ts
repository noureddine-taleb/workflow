import { Route } from "@angular/router";
import { DemandeContainerComponent } from "./demande-container/demande-container.component";
import { DemandeResolverService } from "./services/demande-resolver.service";

export const demandeRoutes: Route[] = [
	{
		path: "habitat",
		loadChildren: () =>
			import("app/modules/habitat/habitat.module").then(m => m.HabitatModule)
	},
	{
		path: "conso",
		loadChildren: () =>
			import("app/modules/conso/conso.module").then((m) => m.ConsoModule),
	},
	{
		path: "investi",
		loadChildren: () =>
			import("app/modules/investitssement/investitssement.module").then(
				(m) => m.InvestitssementModule,
			),
	},
	{
		path: "other",
		loadChildren: () =>
			import("app/modules/other-demandes/other-demandes.module").then(
				(m) => m.OtherDemandesModule,
			),
	},
	{
		path: "quatro",
		loadChildren: () =>
			import("app/modules/quatro/quatro.module").then((m) => m.QuatroModule),
	},
	{
		path: "mazaya",
		loadChildren: () =>
			import("app/modules/mazaya/mazaya.module").then((m) => m.MazayaModule),
	},
    {
        path: "istidama",
        loadChildren: () =>
            import('app/modules/istidama/istidama.module').then((m) => m.IstidamaModule),
    },
    {
        path: "tasbiqfda",
        loadChildren: () =>
            import('app/modules/tasbikFda/tasbik-fda.module').then((m) => m.TasbikFdaModule),
    },
	{
		path: "taches",
		loadChildren: () =>
			import("app/modules/tasks/tasks.module").then(m => m.TasksModule)
	},
	{
		path: ":requestId",
		component: DemandeContainerComponent,
		resolve: { resolvedParam: DemandeResolverService },
	}
];
