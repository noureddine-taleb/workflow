import { Component, OnInit } from '@angular/core';
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";

@Component({
  selector: 'app-detail-demande',
  templateUrl: './detail-demande.component.html',
  styleUrls: ['./detail-demande.component.scss']
})
export class DetailDemandeComponent implements OnInit {

  details: string;

  constructor(
    public utilsService: UtilsService,
    private demandeService: DemandeService,
    private snackBarService: SnackBarService
  ) {}

  ngOnInit(): void {
    this.details = this.demandeService.request.details;
  }

  updateDetail() {
    this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeService.updateDemande({details: this.details, requestId: this.demandeService.request.requestId}).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
          this.demandeService.request.details = this.details;
				});
			}
		});
  }

  ro() {
    return !this.utilsService.hasPrivilege(this.utilsService.privilegeCodes.maj_demande_m)
  }
}
