import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DomiciliationComponent } from "./domiciliation.component";

describe("DomiciliationComponent", () => {
	let component: DomiciliationComponent;
	let fixture: ComponentFixture<DomiciliationComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DomiciliationComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DomiciliationComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
