import { Component, EventEmitter, OnInit, Output, SimpleChanges } from "@angular/core";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { UtilsService } from "app/core/services/utils.service";
import { RoutingService } from "app/core/services/routing.service";

@Component({
	selector: "app-domiciliation",
	templateUrl: "./domiciliation.component.html",
	styleUrls: ["./domiciliation.component.scss"],
})
export class DomiciliationComponent implements OnInit {
	personId: number;
	requestId: number;

	displayedColumns1 = [
		"agencyName",
		"productName",
		"accountNumber",
		"accountName",
		"balance",
		"action",
	];
	displayedColumns2 = ["accountNumber", "accountName", "yearDom", "domiciliation", "action"];
	displayedColumns3 = [
		"valueAmount",
		"montant",
		"injunctionEffectiveDate",
		"injunctionBroadcastDate",
		"regularisationDate",
		"annulationDate",
		"status",
	];

	dataSource1;
	dataSource2;
	dataSource3;

	constructor(
		public routingService: RoutingService,
		private demandeService: DemandeService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.requestId = this.demandeService.request.requestId;
		this.personId = this.demandeService.request.personId;
		this.chargerDataSourche();
	}

	onglet = 1;

	getDomiciliation(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.getDomiciliation({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource2 = data["result"];
					if (data["result"].length > 0) {
						this.onSituationChanged.emit(data["result"][0]["domSituation"]);
					}
				});
		}
	}
	refreshDomiciliation(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.refreshDomiciliation({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource2 = data["result"];
					if (data["result"].length > 0) {
						this.onSituationChanged.emit(data["result"][0]["domSituation"]);
					}
				});
		}
	}
	chargerDataSourche(): void {
		this.getDomiciliation();
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["requestId"]) {
			this.getDomiciliation();
		}
	}

	@Output() onSituationChanged: EventEmitter<any> = new EventEmitter<any>();
	rafraichir(): void {
		this.refreshDomiciliation();
	}
}
