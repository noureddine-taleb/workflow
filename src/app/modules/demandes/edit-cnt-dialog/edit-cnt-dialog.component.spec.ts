import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EditCntDialogComponent } from "./edit-cnt-dialog.component";

describe("EditCntDialogComponent", () => {
	let component: EditCntDialogComponent;
	let fixture: ComponentFixture<EditCntDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EditCntDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditCntDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
