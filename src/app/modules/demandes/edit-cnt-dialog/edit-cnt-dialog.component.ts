import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CntService } from "app/modules/demandes/services/cnt.service";
import { Cnt } from "../cnt-tableau/cnt.types";

@Component({
	selector: "app-edit-cnt-dialog",
	templateUrl: "./edit-cnt-dialog.component.html",
	styleUrls: ["./edit-cnt-dialog.component.scss"],
})
export class EditCntDialogComponent implements OnInit {
	i = 0;
	element;
	cntId = -1;
	requestId = -1;
	statusCode;
	constructor(
		public dialogRef: MatDialogRef<EditCntDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private snackBarService: SnackBarService,
		private cntService: CntService,
	) {
		this.element = data["reservations"];
		this.cntId = data["cntId"];
		this.requestId = data["requestId"];
		this.statusCode = data["statusCode"];
	}

	ngOnInit(): void {
		this.i = 0;
	}

	onSuivant() {
		this.i < this.element.length - 1 ? this.i++ : (this.i = 0);
	}
	onPrec() {
		this.i > 0 ? this.i-- : (this.i = this.i);
	}

	onValide() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let cntDto = new Cnt();
				cntDto.cntId = this.cntId;
				cntDto.requestId = this.requestId;
				cntDto.statusCode = this.statusCode;
				const result = [];

				for (const tier of this.element) {
					let item = result.find((x) => x.cin == tier.cin);
					if (!item) {
						result.push({
							cin: tier.cin,
							fullnam: tier.fullnam,
							registrationNumber: tier.registrationNumber,
							budgetAllocation: tier.budgetAllocation,
							products: [],
						});
						item = result.find((x) => x.cin == tier.cin);
					}
					item.products.push(tier.products);
				}

				cntDto.persons = result;

				this.cntService.crateRequest(cntDto).subscribe(
					(data) => {
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
						this.dialogRef.close({ validation: true });
					},
					(_) => {
						this.dialogRef.close({ validation: false });
					},
				);
			}
		});
	}
}
