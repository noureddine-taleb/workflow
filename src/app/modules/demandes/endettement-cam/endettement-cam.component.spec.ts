import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EndettementCamComponent } from "./endettement-cam.component";

describe("EndettementCamComponent", () => {
	let component: EndettementCamComponent;
	let fixture: ComponentFixture<EndettementCamComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EndettementCamComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EndettementCamComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
