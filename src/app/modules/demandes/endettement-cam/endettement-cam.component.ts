import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { UtilsService } from "app/core/services/utils.service";
import { RoutingService } from "app/core/services/routing.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { Endettement } from "./endettement.types";

@Component({
	selector: "app-endettement-cam",
	templateUrl: "./endettement-cam.component.html",
	styleUrls: ["./endettement-cam.component.css"],
})
export class EndettementCamComponent implements OnInit {
	request: LoanRequest;
	displayedColumns: string[] = [
		"productNumber",
		"productType",		
		"outstandingAmount",
		"amount",
		"crdu",
		"unpaidAmount",
		"unpaidNumber",
		"label",
		"action",
	];
	dataSource: Endettement[];

	constructor(
		private demandeService: DemandeService,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
			this.getEndettementCam();
	}

	getEndettementCam(): void {
		this.demandeService
			.getEndettementCam({
				personId: this.request.personId,
				requestId: this.request.requestId,
			})
			.subscribe((data) => {
				this.dataSource = data["result"];
				if (data["result"].length > 0) {
					this.onSituationChanged.emit(data["result"][0]["endenSituation"]);
				}
			});
	}

	refreshEndettementCam(): void {
		this.demandeService
			.refreshEndettementCam({
				personId: this.request.personId,
				requestId: this.request.requestId,
			})
			.subscribe((data) => {
				this.dataSource = data["result"];
				if (data["result"].length > 0) {
					this.onSituationChanged.emit(data["result"][0]["endenSituation"]);
				}
			});
	}

	@Output() onSituationChanged: EventEmitter<any> = new EventEmitter<any>();
	rafraichir(): void {
		this.refreshEndettementCam();
	}
}


