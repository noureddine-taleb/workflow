export interface Endettement {
	requestId: number;
	productNumber: string;
	productType: string;
	amount: number;
	outstandingAmount: number;
	unpaidAmount: number;
	unpaidNumber: string;
	creationDate: Date;
	crduAmount: number;
}