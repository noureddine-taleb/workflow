import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EtudeAddComponent } from "./etude-add.component";

describe("EtudeAddComponent", () => {
	let component: EtudeAddComponent;
	let fixture: ComponentFixture<EtudeAddComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EtudeAddComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EtudeAddComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
