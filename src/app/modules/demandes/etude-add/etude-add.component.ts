import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Guaranty } from "app/modules/demandes/constitution/constitution.types";
import { Subscription } from "rxjs";
import { FinancialObject } from "app/modules/demandes/objet-financement/financial-object.types";
import { Patrimony } from "app/modules/demandes/patrimony/patrimony.types";

@Component({
	selector: "app-etude-add",
	templateUrl: "./etude-add.component.html",
	styleUrls: ["./etude-add.component.scss"],
})
export class EtudeAddComponent implements OnInit {
	displayedColumns: string[] = [
		"goodTypeName",
		"landTitle",
		"owner",
		"size",
		"adress",
		"value",
		"proofTransaction",
		"annule",
		"annulationDate",
		"action",
	];

	constructor(
		public dialogRef: MatDialogRef<EtudeAddComponent>,
		@Inject(MAT_DIALOG_DATA) private data: any,
		private communService: CommonService,
		private demandeService: DemandeService,
		private snackBar: SnackBarService,
	) {}
	fiancialObjectDto: FinancialObject;
	guartiesDTO: Guaranty;
	patrimonyDto: Patrimony;
	casTraite;
	listBien;
	ngOnInit(): void {
		this.fiancialObjectDto = this.data.objet;
		this.casTraite = this.data.casTraite;
		this.listBien = this.data.listType;
		switch (this.data.casTraite) {
			case 1:
				this.fiancialObjectDto = this.data.objet;
				break;
			case 2:
				this.guartiesDTO = this.data.objet;
				break;
			case 3:
				this.patrimonyDto = this.data.objet;
				break;
			default:
				break;
		}
		//this.getListTypeBien();
	}

    keyPressNumbers(event): boolean {
        var charCode = (event.which) ? event.which : event.keyCode;
        // Only Numbers 0-9
        if ((charCode < 48 || charCode > 57)) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }

	shouldSave() {
		if (this.data.save == true || this.data.save == undefined) return true;
		else return false;
	}

	onValideObjetFinancement(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		let sub = this.demandeService
			.addFinancialObject(this.fiancialObjectDto)
			.subscribe((data) => {
				this.dialogRef.close(true);
			});
		this.subscription.add(sub);
	}

	onValideGarantie(): void {
		let sub = this.demandeService.addGarantie(this.guartiesDTO).subscribe((data) => {
			this.dialogRef.close(true);
		});
		this.subscription.add(sub);
	}
	onValidePatrimony(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		let sub = this.demandeService.addPatrimoine(this.patrimonyDto).subscribe((data) => {
			this.dialogRef.close(true);
		});
		this.subscription.add(sub);
	}

	onValide() {
		if (!this.shouldSave()) {
			switch (this.casTraite) {
				case 1:
					this.dialogRef.close(this.fiancialObjectDto);
					break;
				case 2:
					this.dialogRef.close(this.guartiesDTO);
					break;
				case 3:
					this.dialogRef.close(this.patrimonyDto);
					break;
				default:
					this.dialogRef.close(true);
					break;
			}
			return;
		}
		switch (this.casTraite) {
			case 1:
				this.onValideObjetFinancement();
				break;
			case 2:
				this.onValideGarantie();
				break;
			case 3:
				this.onValidePatrimony();
				break;
			default:
				break;
		}
	}

	guarantySetNatureName(natureId) {
		this.guartiesDTO.natureName = this.listBien.find(
			(nature) => nature.id == natureId,
		).designation;
	}

	subscription = new Subscription();
	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
