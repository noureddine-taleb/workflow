import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Guaranty } from "app/modules/demandes/constitution/constitution.types";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { Subscription } from "rxjs";
import { EtudeAddComponent } from "../etude-add/etude-add.component";

@Component({
	selector: "app-guaranties",
	templateUrl: "./guaranties.component.html",
	styleUrls: ["./guaranties.component.css"],
})
export class GuarantiesComponent implements OnInit {
	garantyType;
	displayedColumns: string[] = [
		"reference",
		"natureName",
		"value",
		"rank",
        "totalcharges",
		"description",
		"action",

	];
	dataSource: Guaranty[];
	subscription = new Subscription();
	request: LoanRequest;

	constructor(
		private demandeService: DemandeService,
		private snackBar: SnackBarService,
		private communService: CommonService,
		private dialog: MatDialog,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getTypeGarantie();
		this.getList();
	}

	getList(): void {
		if (this.request.requestId) {
			let sub = this.demandeService
				.getGaranties({
					personId: this.request.personId,
					requestId: this.request.requestId,
				})
				.subscribe((data) => {
					this.dataSource = data.result;
				});
			this.subscription.add(sub);
		}
	}

	getTypeGarantie(): void {
		let sub = this.communService
			.getTypeGarantie(this.request.creditTypeId)
			.subscribe((data) => (this.garantyType = data["result"]));
	}

	onDelete(element): void {
		this.snackBar.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService
					.deleteGarantie({
						personId: this.request.personId,
						requestId: this.request.requestId,
						identifiant: element.identifiant,
					})
					.subscribe((data) => {
						this.getList();
						this.snackBar.openSuccesSnackBar({ message: data.message });
					});
				this.subscription.add(sub);
			}
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.requestId) {
			this.getList();
		}
	}
	editStat = 0;
	fiancialObjectDto: Guaranty;
	onPrepareAdd(): void {
		this.editStat = 1;
		this.fiancialObjectDto = new Guaranty();
		this.fiancialObjectDto.requestId = this.request.requestId;
	}

	onAdd(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		let sub = this.demandeService.addGarantie(this.fiancialObjectDto).subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
		this.editStat = 0;
	}

	onAdd2() {
		this.fiancialObjectDto = new Guaranty();
		this.fiancialObjectDto.requestId = this.request.requestId;

		const dialogRef = this.dialog.open(EtudeAddComponent, {
			data: {
				objet: this.fiancialObjectDto,
				casTraite: 2,
				listType: this.garantyType,
			},
			width: "90vw",
		});

		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
	}

    onUpdate(element): void {
        this.fiancialObjectDto = element;
        this.fiancialObjectDto.requestId = this.request.requestId;

        const dialogRef = this.dialog.open(EtudeAddComponent, {
            data: {
                objet: this.fiancialObjectDto,
                casTraite: 2,
                listType: this.garantyType,
            },
            width: "90vw",
        });

        let sub = dialogRef.afterClosed().subscribe((data) => {
            this.getList();
        });
        this.subscription.add(sub);
    }

	displayedColumnsFooter(): any {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
// // Type bien , N° Titre, Propiétaire , Superficie (m²), Adresse , Prix Achat ,Justif transaction

export interface FinacialObject {
	adress: string;
	annulationDate: string;
	canceledWith: string;
	createdWith: string;
	creationDate: string;
	entityId: number;
	goodTypeId: number;
	goodTypeName: string;
	identifiant: number;
	landTitle: string;
	observation: string;
	owner: string;
	proofTransaction: string;
	quota: number;
	requestId: number;
	size: number;
	status: string;
	userEntityId: number;
	userId: number;
	value: number;
}
