import { ComponentFixture, TestBed } from "@angular/core/testing";
import { IncidentPaiementComponent } from "./incident-paiement.component";

describe("IncidentPaiementComponent", () => {
	let component: IncidentPaiementComponent;
	let fixture: ComponentFixture<IncidentPaiementComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [IncidentPaiementComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(IncidentPaiementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
