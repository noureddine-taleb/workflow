import { Component, OnInit, SimpleChanges } from "@angular/core";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { UtilsService } from "app/core/services/utils.service";
import { RoutingService } from "app/core/services/routing.service";

@Component({
	selector: "app-incident-paiement",
	templateUrl: "./incident-paiement.component.html",
	styleUrls: ["./incident-paiement.component.scss"],
})
export class IncidentPaiementComponent implements OnInit {
	personId: number;
	requestId: number;
	dateSituation: Date;
	displayedColumns3 = [
		"valueNumber",
		"valueAmount",
		"injunctionEffectiveDate",
		"injunctionBroadcastDate",
		"regularisationDate",
		"annulationDate",
		"status",
		"action",
	];
	dataSource3;
	onglet = 1;

	constructor(
		private demandeService: DemandeService,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.personId = this.demandeService.request.personId;
		this.requestId = this.demandeService.request.requestId;
			this.chargerDataSourche();
	}

	getIncidentPaiement(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.getIncidentPaiement({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource3 = data["result"];
					if (data["result"].length > 0) {
						this.dateSituation = data["result"][0]["cipSituation"];
					}
				});
		}
	}
	refreshIncidentPaiement(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.refreshIncidentPaiement({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource3 = data["result"];
					if (data["result"].length > 0) {
						this.dateSituation = data["result"][0]["cipSituation"];
					}
				});
		}
	}

	chargerDataSourche(): void {
		this.getIncidentPaiement();
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["requestId"]) {
			this.getIncidentPaiement();
		}
	}

	rafraichir(): void {
		this.refreshIncidentPaiement();
	}
}
