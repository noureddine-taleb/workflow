export class RequestEvent {
	eventId: number;
	requestId: number;
	eventTypeCode: string;
	eventTypeName: string;
}
