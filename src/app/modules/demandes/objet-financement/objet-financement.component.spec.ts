import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ObjetFinancementComponent } from "./objet-financement.component";

describe("ObjetFinancementComponent", () => {
	let component: ObjetFinancementComponent;
	let fixture: ComponentFixture<ObjetFinancementComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ObjetFinancementComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ObjetFinancementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
