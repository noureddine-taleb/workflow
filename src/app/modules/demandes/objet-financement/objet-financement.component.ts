import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { FinancialObject } from "app/modules/demandes/objet-financement/financial-object.types";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { Subscription } from "rxjs";
import { EtudeAddComponent } from "../etude-add/etude-add.component";

@Component({
	selector: "app-objet-financement",
	templateUrl: "./objet-financement.component.html",
	styleUrls: ["./objet-financement.component.css"],
})
export class ObjetFinancementComponent implements OnInit {
	@Input() listBien;

	request: LoanRequest;
	displayedColumns: string[] = [
		"goodTypeName",
		"landTitle",
		"owner",
		"size",
		"adress",
		"value",
		"proofTransaction",
		"action",
	];
	dataSource: FinacialObject[];
	subscription = new Subscription();
	constructor(
		private demandeService: DemandeService,
		private snackBar: SnackBarService,
		private communService: CommonService,
		private dialog: MatDialog,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getListTypeBien();
		this.getList()
	}

	getListTypeBien(): void {
		let sub = this.communService
			.getTypeBien({ parentId: this.request.creditTypeId })
			.subscribe((data) => {
				this.listBien = data["result"];
			});
		this.subscription.add(sub);
	}

	getList(): void {
		this.demandeService
			.getFinancialObject({
				personId: this.request.personId,
				requestId: this.request.requestId,
			})
			.subscribe((data) => {
				this.dataSource = data.result;
			});
	}

	onDelete(element): void {
		this.snackBar
			.openConfirmSnackBar({ message: "Êtes-vous sûr de vouloir supprimer cette ligne" })
			.then((result) => {
				if (result.value) {
					this.demandeService
						.deleteFinacialObject({
							personId: this.request.personId,
							requestId: this.request.requestId,
							identifiant: element.identifiant,
						})
						.subscribe((data) => {
							this.getList();
							this.snackBar.openSuccesSnackBar({ message: data.message });
						});
				}
			});
	}

	// tslint:disable-next-line:use-lifecycle-interface
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.requestId) {
			this.getList();
		}
	}
	editStat = 0;
	fiancialObjectDto: FinancialObject;
	onPrepareAdd(): void {
		this.editStat = 1;
		this.fiancialObjectDto = new FinancialObject();
		this.fiancialObjectDto.requestId = this.request.requestId;
	}

	onAdd(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		this.demandeService.addFinancialObject(this.fiancialObjectDto).subscribe((data) => {
			this.getList();
		});
		this.editStat = 0;
	}
	onAdd2() {
		this.fiancialObjectDto = new FinancialObject();
		this.fiancialObjectDto.requestId = this.request.requestId;
		const dialogRef = this.dialog.open(EtudeAddComponent, {
			data: {
				objet: this.fiancialObjectDto,
				casTraite: 1,
				listType: this.listBien,
			},
			width: "90vw",
		});

		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
	}
	displayedColumnsFooter() {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

    onUpdate(element): void {
        this.fiancialObjectDto = element;
        this.fiancialObjectDto.requestId = this.request.requestId;
        const dialogRef = this.dialog.open(EtudeAddComponent, {
            data: {
                objet: this.fiancialObjectDto,
                casTraite: 1,
                listType: this.listBien,
            },
            width: "90vw",
        });

        let sub = dialogRef.afterClosed().subscribe((data) => {
            this.getList();
        });
        this.subscription.add(sub);
    }
}
// // Type bien , N° Titre, Propiétaire , Superficie (m²), Adresse , Prix Achat ,Justif transaction

export interface FinacialObject {
	adress: string;
	annulationDate: string;
	canceledWith: string;
	createdWith: string;
	creationDate: string;
	entityId: number;
	goodTypeId: number;
	goodTypeName: string;
	identifiant: number;
	landTitle: string;
	observation: string;
	owner: string;
	proofTransaction: string;
	quota: number;
	requestId: number;
	size: number;
	status: string;
	userEntityId: number;
	userId: number;
	value: number;
}
