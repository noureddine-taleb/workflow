import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { Subscription } from "rxjs";
import { Patrimony } from "app/modules/demandes/patrimony/patrimony.types";
import { EtudeAddComponent } from "../etude-add/etude-add.component";

@Component({
	selector: "app-patrimony",
	templateUrl: "./patrimony.component.html",
	styleUrls: ["./patrimony.component.css"],
})
export class PatrimonyComponent implements OnInit {
	@Input() listBien;
	subscription = new Subscription();
	displayedColumns: string[] = [
		"goodTypeName",
		"landTitle",
		"observation",
		"value",
		"quota",
		"action",
	];
	dataSource: Patrimony[];
	request: LoanRequest;

	constructor(
		private demandeService: DemandeService,
		private snackBar: SnackBarService,
		private communService: CommonService,
		private refGtToken: AuthService,
		private tokenServ: AuthService,
		private dialog: MatDialog,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.getListTypeBien();
		this.getList()
	}

	getListTypeBien(): void {
		let sub = this.communService
			.getTypeBien({ parentId: this.request.creditTypeId })
			.subscribe((data) => {
				this.listBien = data["result"];
			});
		this.subscription.add(sub);
	}

	getList(): void {
		if (this.request.requestId) {
			// console.warn(this.personId , this.requestId);
			let sub = this.demandeService
				.getPatrimoine({
					personId: this.request.personId,
					requestId: this.request.requestId,
				})
				.subscribe((data) => {
					this.dataSource = data.result;
				});
			this.subscription.add(sub);
		}
	}

	onDelete(element): void {
		this.snackBar.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let sub = this.demandeService
					.deletePatrimoine({
						personId: this.request.personId,
						requestId: this.request.requestId,
						identifiant: element.identifiant,
					})
					.subscribe((data) => {
						this.getList();
						this.snackBar.openSuccesSnackBar({ message: data.message });
					});
				this.subscription.add(sub);
			}
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.requestId) {
			this.getList();
		}
	}
	editStat = 0;
	fiancialObjectDto: Patrimony;
	onPrepareAdd(): void {
		this.editStat = 1;
		this.fiancialObjectDto = new Patrimony();
		this.fiancialObjectDto.requestId = this.request.requestId;
	}

	onAdd(): void {
		// this.fiancialObjectDto.goodTypeId = 1;
		// this.fiancialObjectDto.quota = 30;
		let sub = this.demandeService.addPatrimoine(this.fiancialObjectDto).subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
		this.editStat = 0;
	}

	onAdd2() {
		this.fiancialObjectDto = new Patrimony();
		this.fiancialObjectDto.requestId = this.request.requestId;
		const dialogRef = this.dialog.open(EtudeAddComponent, {
			data: {
				objet: this.fiancialObjectDto,
				casTraite: 3,
				listType: this.listBien,
			},
			width: "90vw",
		});

		let sub = dialogRef.afterClosed().subscribe((data) => {
			this.getList();
		});
		this.subscription.add(sub);
	}

    onUpdate(element): void {
        this.fiancialObjectDto = element;
        this.fiancialObjectDto.requestId = this.request.requestId;
        const dialogRef = this.dialog.open(EtudeAddComponent, {
            data: {
                objet: this.fiancialObjectDto,
                casTraite: 3,
                listType: this.listBien,
            },
            width: "90vw",
        });

        let sub = dialogRef.afterClosed().subscribe((data) => {
            this.getList();
        });
        this.subscription.add(sub);
    }

	displayedColumnsFooter(): any {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
