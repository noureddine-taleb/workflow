export class Patrimony {
	adress: string;
	entityId: number;
	goodTypeId: number;
	identifiant: number;
	landTitle: string;
	observation: string;
	owner: string;
	proofTransaction: string;
	quota: number;
	requestId: number;
	size: number;
	userEntityId: number;
	userId: number;
	value: number;
}