import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ProductsSimpleComponent } from "./products-simple.component";

describe("ProductsSimpleComponent", () => {
	let component: ProductsSimpleComponent;
	let fixture: ComponentFixture<ProductsSimpleComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ProductsSimpleComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ProductsSimpleComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
