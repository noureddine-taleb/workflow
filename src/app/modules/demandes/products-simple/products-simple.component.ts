import { Component, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestType } from "app/shared/models/RequestType";
import { AttachementComponent } from "../attachement/attachement.component";

@Component({
	selector: "app-products-simple",
	templateUrl: "./products-simple.component.html",
	styleUrls: ["./products-simple.component.css"],
})
export class ProductsSimpleComponent implements OnInit {
	spans = [];
	tempRowId = null;
	tempRowCount = null;
	request: LoanRequest;
	requestTypes = RequestType;

	constructor(
		private dialog: MatDialog,
		private router: Router,
		public utilsService: UtilsService,
		private demandeService: DemandeService,
		private routingService: RoutingService,
	) {}
	dataSource: Products[];
	displayedColumns = [
		"financialProductName",
		"prelevementTypeCode",
		"goodAmount",
		"rate",
		"duration",
		"differe",
		"monthlyPayment",
		"insurance",
		"cntStatusName",
		"startDate"
	];

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.dataSource = this.request.products;
		if (this.request.creditTypeId == RequestType.HABITAT)
			this.displayedColumns = ["financialObjectName", ...this.displayedColumns];

		if (this.request.creditTypeId == RequestType.HORS_AGRI || this.request.creditTypeId == RequestType.AGRI) {
			this.displayedColumns.splice(this.displayedColumns.indexOf("cntStatusName"), 1)
			this.displayedColumns = [...this.displayedColumns, "endDate"];
		}
	}

	// TODO: make this less hack-ish
	// intended behaviour: wait for privileges to load first then check them
	checkColPriv() {
		if (
			this.utilsService.hasPrivilege(this.utilsService.privilegeCodes.attacher_dossier_a) &&
			this.displayedColumns.indexOf("Evolan") === -1 &&
			this.displayedColumns.indexOf("Action") === -1
		)
			this.displayedColumns = [...this.displayedColumns, "Evolan", "Action"];
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["products"]) {
			this.dataSource = this.request.products;
			//alert('change product');
		}
	}

	onAttache(element): void {
		const dialogRef = this.dialog.open(AttachementComponent, {
			data: {
				produit: element,
				requestId: this.request.requestId,
			},
			width: "95vw",
		});

		dialogRef.afterClosed().subscribe((data) => {
			this.routingService.reloadCurrentRoute()
		});
	}

	typeToString(type: string) {
		switch (type) {
			case "S":
				return "Source";
			case "B":
				return "Bancaire";
			default:
				return type;
		}
	}
}

export interface Products {
	productId: number;
	insurance: number;
	monthlyPayment: number;
	bonus: number;
	rate: number;
	financialProductId: number;
	financialProductName: string;
	prelevementTypeCode: string;
	prelevementTypeName: string;
	deferredPeriod: number;
	duration: number;
	contributionAmount: number;
	goodAmount: number;
	financialObjectCode: string;
	financialObjectName: string;
	derogation: number;
	cntStatusCode: string;
	cntStatusName: string;
}
