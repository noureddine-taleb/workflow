import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorisationLivraisonAddComponent } from './autorisation-livraison-add.component';

describe('AutorisationLivraisonAddComponent', () => {
  let component: AutorisationLivraisonAddComponent;
  let fixture: ComponentFixture<AutorisationLivraisonAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutorisationLivraisonAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorisationLivraisonAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
