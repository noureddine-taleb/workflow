import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComponentState } from 'app/core/models/ComponentState';
import { SnackBarService } from 'app/layout/services/snackbar.service';
import { AutorisationLivraisonService } from '../autorisation-livraison/autorisation-livraison.service';
import { AutorisationLivraison, AutorisationLivraisonMaterial } from '../autorisation-livraison/autorisation-livraison.types';

interface InputData {
	state: ComponentState,
	autorisation?: AutorisationLivraison,
};

@Component({
selector: 'app-autorisation-livraison-add',
templateUrl: './autorisation-livraison-add.component.html',
styleUrls: ['./autorisation-livraison-add.component.scss']
})
export class AutorisationLivraisonAddComponent implements OnInit {
	material = new AutorisationLivraisonMaterial();
	autorisation = new AutorisationLivraison();

	displayedColumns = [ 'designation', 'wwNumber', 'brand', 'model', 'productionYear', 'action']
	addState: boolean = false;

	constructor(
		private dialogRef: MatDialogRef<AutorisationLivraisonAddComponent>,
		private autorisationLivraisonService: AutorisationLivraisonService,
		private snackBarService: SnackBarService,
		@Inject(MAT_DIALOG_DATA) public data: InputData,
	) { }

	ngOnInit(): void {
		if (this.data.state == ComponentState.ReadOnly || this.data.state == ComponentState.Update) {
			this.autorisation = this.data.autorisation
		}
	}

	isReadOnly() {
		return this.data.state == ComponentState.ReadOnly;
	}

	onAdd() {
		this.addState = true;
	}

	onDelete(material: AutorisationLivraisonMaterial) {
		let index = this.autorisation.materials.indexOf(material);
		this.autorisation.materials.splice(index, 1);
		this.autorisation.materials = [...this.autorisation.materials];
	}

	onAppend() {
		this.autorisation.materials = [...this.autorisation.materials, this.material];
		this.onQuit();
	}

	onQuit() {
		this.addState = false;
		this.material = new AutorisationLivraisonMaterial();
	}

	getFooterCols() {
		if (this.addState)
			return this.displayedColumns;
		return [];
	}

	save() {
		this.autorisationLivraisonService.autorisationLivraisonCreate(this.autorisation).subscribe(result => {
			this.snackBarService.openSuccesSnackBar({message: result['message']});
			this.dialogRef.close(true);
		})
	}

	close() {
		this.dialogRef.close();
	}
}
