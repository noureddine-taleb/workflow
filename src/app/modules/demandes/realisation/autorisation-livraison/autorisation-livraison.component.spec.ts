import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorisationLivraisonComponent } from './autorisation-livraison.component';

describe('AutorisationLivraisonComponent', () => {
  let component: AutorisationLivraisonComponent;
  let fixture: ComponentFixture<AutorisationLivraisonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutorisationLivraisonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorisationLivraisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
