import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentState } from 'app/core/models/ComponentState';
import { CommonService } from 'app/core/services/common.service';
import { RoutingService } from 'app/core/services/routing.service';
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from 'app/layout/services/snackbar.service';
import { DialogObjectActionsComponent } from 'app/shared/dialog-object-actions/dialog-object-actions.component';
import { Action } from 'app/shared/models/Action';
import { take } from 'rxjs/operators';
import { LoanRequest } from '../../models/LoanRequest';
import { PrivilegeCode } from '../../models/PrivilegeCode';
import { DemandeService } from '../../services/demande.service';
import { AutorisationLivraisonAddComponent } from '../autorisation-livraison-add/autorisation-livraison-add.component';
import { AutorisationLivraisonService } from './autorisation-livraison.service';
import { AutorisationLivraison } from './autorisation-livraison.types';

@Component({
  selector: 'app-autorisation-livraison',
  templateUrl: './autorisation-livraison.component.html',
  styleUrls: ['./autorisation-livraison.component.scss']
})
export class AutorisationLivraisonComponent implements OnInit {
	displayedColumns: string[] = [
		"authorizationNumber",
		"dateCreation",
		"statusName",
		"action"		
	];
	displayedColumnsLielle: string[] = [
		"N° Autorisation",
		"Date creation",
		"Statut",
		"Action"
    ];
	dataSource: AutorisationLivraison[] = [];
	demandeDTO: LoanRequest;

	constructor(
		private dialog: MatDialog,
		private autorisationLivraisonService: AutorisationLivraisonService,
		private snackBarService: SnackBarService,
		private commonService: CommonService,
		public routingService: RoutingService,
		private utilsService: UtilsService,
		private demandeService: DemandeService,
	) {}

	ngOnInit(): void {
		this.demandeDTO = this.demandeService.request;
		this.getDemandePrivileges();
		this.getList();
	}

	onAdd() {
		const dialogRef = this.dialog.open(AutorisationLivraisonAddComponent, {
			width: "80vw",
			data: {
				state: ComponentState.Add,
			}
		});
		dialogRef.afterClosed().subscribe(success => {
			if (success)
				this.getList();
		});
	}

	getDemandePrivileges() {
		this.utilsService.loadRequestPrivs();
	}

	getList() {
		this.autorisationLivraisonService
			.autorisationLivraisonList()
			.pipe(take(1))
			.subscribe((data) => {
				this.dataSource = data["result"];
			});
	}

	loadActions(autorisation: AutorisationLivraison) {
		if (
			this.routingService.isDemandeRO() ||
			(Array.isArray(autorisation.actions))
		)
			return;
		this.commonService
			.getAutorisationLivActions(
				autorisation.statusCode,
			)
			.pipe(take(1))
			.subscribe((data: any) => {
				autorisation.actions = data.result;
			});
	}

	dispatchAction(action: Action, autorisation: AutorisationLivraison) {
		let postAction = () => {
			this.getList();
			this.reloadDemande();
			this.getDemandePrivileges();
			// this will force actions reload
			delete autorisation.actions
		}

		if (action.users.length == 1) {
			this.snackBarService
			.openConfirmSnackBar({ message: `confirmer l'action (${action?.actionName}) ?` })
			.then((result) => {
				if (result.value) {
					this.demandeService
						._dispatchAction(autorisation, action, action.users[0])
						.subscribe((data) => {
							this.snackBarService.openSuccesSnackBar({ message: data["message"] });
							postAction();
						});
				}
			});
		} else {
			const dialogRef = this.dialog.open(DialogObjectActionsComponent, { data: {
					action: action,
					obj: autorisation,
				}
			});
			dialogRef.afterClosed().subscribe(success => {
				if (success)
					postAction()
			});	
		}
	}

	showDetails(autorisation: AutorisationLivraison) {
		let copy = JSON.parse(JSON.stringify(autorisation));
		this.dialog.open(AutorisationLivraisonAddComponent, {
			width: "80vw",
			data: {
				state: this.hasEditPriv(autorisation) ? ComponentState.Update: ComponentState.ReadOnly,
				autorisation: copy
			}
		})
		.afterClosed().subscribe(success => {
			if (success)
				this.getList();
		});
	}

	reloadDemande() {
		this.routingService.reloadCurrentRoute()
	}

	hasAddPriv() {
		return this.routingService.isDemandeRW() && this.utilsService.hasPrivilege(PrivilegeCode.mjal_m)
	}

	hasEditPriv(autorisation: AutorisationLivraison) {
		return this.routingService.isDemandeRW() && this.utilsService.hasPrivilege(PrivilegeCode.mjal_m) && autorisation.statusCode == 'ATLIV_SA';
	}
}
