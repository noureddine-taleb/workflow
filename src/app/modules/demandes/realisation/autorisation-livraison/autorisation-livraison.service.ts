import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "../../services/demande.service";
import { AutorisationLivraison } from "./autorisation-livraison.types";

@Injectable({ providedIn: "root" })
export class AutorisationLivraisonService {
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient, private authService: AuthService, private demandeService: DemandeService) {}

	autorisationLivraisonCreate(autorisationLivraison: AutorisationLivraison) {
		autorisationLivraison.entityId = this.authService.user.entityId;
		autorisationLivraison.userId = this.authService.user.userId;
		autorisationLivraison.userEntityId = this.authService.user.userEntityId;
		autorisationLivraison.creditRequestId = this.demandeService.request.requestId;

		return this.httpClient.post(
			this.wkfPath + "/deliveryAuthorization/create",
			autorisationLivraison,
		);
	}

	autorisationLivraisonList() {
		return this.httpClient.post(this.wkfPath + "/deliveryAuthorization/getDeliveryAuthorization", {
			...this.authService.user,
			requestId: this.demandeService.request.requestId,
		});
	}
}
