import { Action } from "app/shared/models/Action";

export class AutorisationLivraison {
	constructor() {
		this.materials = [];
	}

	authorizatoionId: number;
	entityId: number;
	first: number;
	pas: number;
	requestId: number;
	status: string;
	creationDate: Date;
	userEntityId: number;
	userId: number;
	
	action: string;
	actionUserId: number;
	authorizationId: number;
	creditRequestId: number;
	documents: string;	
	materials: AutorisationLivraisonMaterial[];
	signingDate: Date;
	signingUser: string;
	userActionId: number;
	authorizationNumber:string;
	dateCreation: Date;
	statusCode: string;
	statusName: string;

	actions?: Action[];
}

export class AutorisationLivraisonMaterial {
	designation: string; // check this
	authorizationId: number;
	brand: string;
	entityId: number;
	materialId: number;
	model: string;
	productionYear: number;
	userEntityId: number;
	userId: number;
	wwNumber: string
}