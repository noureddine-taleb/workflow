import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcContainerComponent } from "./cc-container.component";

describe("CcContainerComponent", () => {
	let component: CcContainerComponent;
	let fixture: ComponentFixture<CcContainerComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcContainerComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcContainerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
