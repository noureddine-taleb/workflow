import { Component, Input, OnInit } from "@angular/core";
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";

@Component({
	selector: "app-cc-container",
	templateUrl: "./cc-container.component.html",
	styleUrls: ["./cc-container.component.scss"],
})
export class CcContainerComponent implements OnInit {
	constructor(
		private ccgService: CcgService
	) {}

	@Input() demandeDTO
	@Input() persons
	ngOnInit(): void {
		console.log(this.demandeDTO)
		console.log(this.persons)
	}

	selectedTab = 1;
	onSelectTab(number: number) {
		this.selectedTab = number;
	}

	onReciveEvent($event: number) {
		this.selectedTab = this.selectedTab + $event;
	}
}
