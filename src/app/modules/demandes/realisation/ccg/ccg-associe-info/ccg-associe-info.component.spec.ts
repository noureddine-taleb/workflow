import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgAssocieInfoComponent } from "./ccg-associe-info.component";

describe("CcgAssocieInfoComponent", () => {
	let component: CcgAssocieInfoComponent;
	let fixture: ComponentFixture<CcgAssocieInfoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgAssocieInfoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgAssocieInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
