import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { cloneDeep } from "lodash";
import { ServiceNavigation } from '../navigation-service/service-navigation';

@Component({
    selector: 'app-ccg-associe-info',
    templateUrl: './ccg-associe-info.component.html',
    styleUrls: ['./ccg-associe-info.component.scss']
})
export class CcgAssocieInfoComponent implements OnInit {

    displayedColumns = ['name', 'firstName', 'cin', 'experience', 'projectEngagement', 'familyConnection', 'part', 'Action'];
    displayedColumnsLib = ['Nom', 'Prénom', 'CIN/RC/PP/C.S', 'Experience', 'Eng. Projet', 'L.Fam.', 'Part'];
    displayedColumnsToolTip = ['Nom', 'Prénom', 'CIN/RC/Passeport/Carte Séjour', 'Expérience', 'Engagement Projet', 'Liaison Familiale', 'Part'];
    editMode = 0;

    constructor(
        private ccgService: CcgService,
        private snackBarService: SnackBarService,
        private serviceNavigation: ServiceNavigation,
        public routingService: RoutingService
    ) { }

    @Input() demandeDTO;
    @Input() editedProject;
    @Input() persons
    ngOnInit(): void {
        switch (this.editedProject.product) {
            case 1:
                this.displayedColumns = ['name', 'firstName', 'cin', 'experience', 'projectEngagement', 'familyConnection', 'part', 'Action'];
                break;
            case 2:
                this.displayedColumns = ['name', 'firstName', 'cin', 'experience', 'projectEngagement', 'familyConnection', 'part', 'Action'];
                break;

            default:
                break;
        }
        this.getAssociateProjects()
    }

    deleteActionColumn(displayedColumns: string[]) {
        let displayedeCol = cloneDeep(this.displayedColumns);
        displayedeCol.splice(this.displayedColumns.length - 1, 1)
        return displayedeCol;
    }

    editedItem;
    onPrepareEdit(item) {
        this.editMode = 2;
        this.editedItem = item
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Associés', 'Editer']);
    }

    onPrepareAdd() {
        this.editMode = 1;
        this.editedItem = {}
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Associés', 'Ajouter']);
    }

    onRetourEdit(number: number) {
        this.editMode = number;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Associés']);
        this.getAssociateProjects()
    }

    onPrepareDelete(item) {
        this.snackBarService.openConfirmSnackBar({ message: "all" })
            .then((result) => {
                if (result.value) {
                    item.mode = "S"
                    this.deleteAssociateProject(item);
                }
            })
    }

    @Output() emmteur = new EventEmitter<number>();
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }

    deleteAssociateProject(item) {
        this.ccgService.postMethode('/ccg/saveAssociateProject', item).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getAssociateProjects()
                }
            }, (error) => {
            }, () => {
            }
        )
    }

    listeAssociateProject: any = []
    getAssociateProjects() {
        let params = {
            projectId: this.editedProject.id,
        }
        this.ccgService.postMethode('/ccgQuery/associateProjects', params).subscribe(
            (response) => {
                this.listeAssociateProject = response.result
            }, (error) => {
            }, () => {
            }
        )
    }

}
