import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgChoixCcgProjetDialogComponent } from "./ccg-choix-ccg-projet-dialog.component";

describe("CcgChoixCcgProjetDialogComponent", () => {
	let component: CcgChoixCcgProjetDialogComponent;
	let fixture: ComponentFixture<CcgChoixCcgProjetDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgChoixCcgProjetDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgChoixCcgProjetDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
