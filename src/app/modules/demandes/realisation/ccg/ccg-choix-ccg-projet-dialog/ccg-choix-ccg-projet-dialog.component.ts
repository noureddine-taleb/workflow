import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm } from "@angular/forms";
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { ProjetCcg } from '../dto/projetccg';

@Component({
  selector: 'app-ccg-choix-ccg-projet-dialog',
  templateUrl: './ccg-choix-ccg-projet-dialog.component.html',
  styleUrls: ['./ccg-choix-ccg-projet-dialog.component.scss']
})
export class CcgChoixCcgProjetDialogComponent implements OnInit {

  form: FormGroup;

  product = new FormControl('');
  grouper = new FormControl('');

  matcher = new MyErrorStateMatcher();
  projet: ProjetCcg = new ProjetCcg()

  constructor(public dialogRef: MatDialogRef<CcgChoixCcgProjetDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private snackBarService: SnackBarService,
  ) {

  }

  ngOnInit(): void {
  }

  onValid() {
    let formValid = true;
    if (!this.product.value) {
      this.product.markAsTouched();
      formValid = false;
    }
    if (this.data.itemsCount > 1 && !this.grouper.value) {
      formValid = false;
    }
    if (formValid == true) {
      let data = {
        product: this.product.value,
        grouper: this.grouper.value,
      }
      this.dialogRef.close(data);
    }
  }


}

/** Error when invalid control is dirty or touched*/
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}