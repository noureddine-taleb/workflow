import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgCreditInfoComponent } from "./ccg-credit-info.component";

describe("CcgCreditInfoComponent", () => {
	let component: CcgCreditInfoComponent;
	let fixture: ComponentFixture<CcgCreditInfoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgCreditInfoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgCreditInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
