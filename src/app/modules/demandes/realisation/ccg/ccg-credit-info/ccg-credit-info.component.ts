import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { cloneDeep } from "lodash";
import { ServiceNavigation } from "../navigation-service/service-navigation";

@Component({
    selector: 'app-ccg-credit-info',
    templateUrl: './ccg-credit-info.component.html',
    styleUrls: ['./ccg-credit-info.component.scss']
})
export class CcgCreditInfoComponent implements OnInit {

    displayedColumns = [];
    editMode = 0;
    constructor(
        private serviceNavigation: ServiceNavigation,
        public ccgService: CcgService,
        private snackBarService: SnackBarService,
        public routingService: RoutingService
    ) { }

    @Input() editedProject;
    @Input() demandeDTO;
    ngOnInit(): void {
        this.displayedColumns = ['bankCreditNumber','creditTypeLib','lineType','duration', 'differed', 'amount', 'deadline', 'benefitRate','Action'];
        this.getCreditProjects()
    }

    deleteActionColumn(displayedColumns: string[]) {
        let displayedeCol = cloneDeep(this.displayedColumns);
        displayedeCol.splice(this.displayedColumns.length - 1, 1)
        return displayedeCol;
    }

    editedItem;
    onPrepareEdit(item) {
        this.editMode = 2;
        this.editedItem = item;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Crédits', 'Editer']);
    }

    onPrepareAdd() {
        this.editMode = 1;
        this.editedItem = {};
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Crédits', 'Ajouter']);
    }

    onRetourEdit(number: number) {
        this.editMode = number;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Crédits']);
        this.getCreditProjects()
    }

    onPreparDelete(item) {
        this.snackBarService.openConfirmSnackBar({ message: "all" })
            .then((result) => {
                if (result.value) {
                    item.mode = "S"
                    this.deleteCreditProject(item)
                }
            })
    }

    @Output() emmteur = new EventEmitter<number>();
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }

    deleteCreditProject(item) {
        this.ccgService.postMethode('/ccg/saveCreditProject', item).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getCreditProjects()
                }
            }, (error) => {
                this.getCreditProjects()
            }, () => {
            })
    }

    creditProjectsList = []
    getCreditProjects() {
        let params = {
            projectId: this.editedProject.id,
            requestId: this.demandeDTO.requestId
        }
        this.ccgService.postMethode('/ccgQuery/creditProjects', params).subscribe(
            (response) => {
                this.creditProjectsList = response.result
            }, () => {
            })
    }

}

