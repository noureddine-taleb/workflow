import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgInitComponent } from "./ccg-init.component";

describe("CcgInitComponent", () => {
	let component: CcgInitComponent;
	let fixture: ComponentFixture<CcgInitComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgInitComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgInitComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
