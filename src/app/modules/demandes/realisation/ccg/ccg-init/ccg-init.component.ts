import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Subscription } from "rxjs";
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { CcgChoixCcgProjetDialogComponent } from "../ccg-choix-ccg-projet-dialog/ccg-choix-ccg-projet-dialog.component";
import { Client } from '../dto/client';
import { Dossier, InitCcg } from '../dto/initccg';
import { ProjetCcg } from '../dto/projetccg';

@Component({
    selector: 'app-ccg-init',
    templateUrl: './ccg-init.component.html',
    styleUrls: ['./ccg-init.component.scss']
})
export class CcgInitComponent implements OnInit {

    subscription = new Subscription();

    projet: ProjetCcg = new ProjetCcg()

    constructor(
        private dialog: MatDialog,
        private ccgService: CcgService,
        private snackBarService: SnackBarService,
        public routingService: RoutingService
    ) { }

    @Input() demandeDTO
    @Input() persons
    ngOnInit(): void {
        this.addCheckedField();
        this.getProduit()
        this.getProduitFinancement()
        this.getCustomerCard()
    }

    @Output() emmteur = new EventEmitter<number>();

    produitFinancementList = [];
    selectedProductsList = [];
    displayedColumn = ['productName', 'creditAmount', 'action'];
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }
    addCheckedField() {
        this.produitFinancementList.forEach(x => x['checked'] = false)
    }

    checkedItemsCount(): number {
        let count = 0
        this.selectedProductsList = []
        this.produitFinancementList.forEach(element => {
            if (element.checked) {
                this.selectedProductsList.push(element)
                count++
            }
        });
        return count
    }

    onPrepareCreateProjectCCg() {
        if (this.checkedItemsCount() > 0) {
            const dialogRef = this.dialog.open(CcgChoixCcgProjetDialogComponent, {
                data: {
                    itemsCount: this.checkedItemsCount(),
                    typeProduit: this.typeProduit
                }
            })
            dialogRef.afterClosed().subscribe(data => {
                if (data != null) {
                    let initCcg = new InitCcg()
                    if (this.persons && this.persons.length > 0) {
                        initCcg.adress = this.persons[0].adressName
                        initCcg.birthDate = this.persons[0].birthDate
                        initCcg.cardNumber = this.persons[0].cin
                        initCcg.firstName = this.persons[0].firstName
                        initCcg.gender = this.persons[0].genderCode
                        initCcg.lastName = this.persons[0].lastName
                        initCcg.pprNumber = this.persons[0].registrationNumber
                        initCcg.legalStatus = this.persons[0].legalStatus
                        initCcg.rcNumber = this.persons[0].rcNumber
                        initCcg.tiers = this.persons[0].personId
                        initCcg.city = this.persons[0].cityId
                        initCcg.companyName = this.persons[0].entityName
                    }
                    initCcg.account = this.demandeDTO.accountId
                    initCcg.agency = this.demandeDTO.creationEntityId
                    initCcg.groupe = data.grouper
                    initCcg.product = data.product
                    initCcg.requestId = this.demandeDTO.requestId

                    this.selectedProductsList.forEach(element => {
                        let dossier = new Dossier();
                        dossier.userId = element.userId
                        dossier.userEntityId = element.userEntityId
                        dossier.entityId = element.entityId
                        dossier.reportId = element.reportId
                        initCcg.overallCreditAmount += element.creditAmount

                        initCcg.dossiers.push(dossier)
                    });

                    this.shapeCreditRequest(initCcg)

                }
            })
        } else {
            this.snackBarService.openInfoSnackBar({ message: "Veuillez sélectionner au moins un élément!" })
        }
    }

    shapeCreditRequest(object) {
        this.ccgService.postMethode('/ccg/initCCG', object).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getProduitFinancement()
                }
            }, (error) => {
            }, () => {
            })
    }

    customerCard: Client
    getCustomerCard() {
        let params: any = {
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/getCustomerCard', params).subscribe(
            (response) => {
                if (response.status == 200) {
                    if (response.result.id != null) {
                        this.customerCard = response.result
                    }
                }
            }, (error) => {
            }, () => {
            })
    }

    getProduitFinancement() {
        let params = {
            requestId: this.demandeDTO.requestId
        }
        this.ccgService.postMethode('/ccgQuery/getFinancedProduct', params).subscribe(
            (response) => {
                this.produitFinancementList = response.result;
            }, (error) => {
            }, () => {
            })
    }

    typeProduit = [];
    getProduit() {
        this.ccgService.getList('/ccgQuery/getConventionalProduct').subscribe(
            (response) => {
                this.typeProduit = response.result;
            }, (error) => {
            }, () => {
            })
    }



}
