import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgLandingComponent } from "./ccg-landing.component";

describe("CcgLandingComponent", () => {
	let component: CcgLandingComponent;
	let fixture: ComponentFixture<CcgLandingComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgLandingComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgLandingComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
