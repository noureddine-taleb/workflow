import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgProjectInfoDynamicComponent } from "./ccg-project-info-dynamic.component";

describe("CcgProjectInfoDynamicComponent", () => {
	let component: CcgProjectInfoDynamicComponent;
	let fixture: ComponentFixture<CcgProjectInfoDynamicComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgProjectInfoDynamicComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgProjectInfoDynamicComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
