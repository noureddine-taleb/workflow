import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { ProjetCcg } from '../dto/projetccg';
import { ServiceNavigation } from "../navigation-service/service-navigation";

@Component({
    selector: 'app-ccg-project-info-dynamic',
    templateUrl: './ccg-project-info-dynamic.component.html',
    styleUrls: ['./ccg-project-info-dynamic.component.scss']
})
export class CcgProjectInfoDynamicComponent implements OnInit {

    projet: ProjetCcg = new ProjetCcg();

    typeProduit = [
        { code: 'DA', libelle: 'DA' },
        { code: 'ADL', libelle: 'ADL' },
        { code: 'SA', libelle: 'SA' },
        { code: 'AMAQ', libelle: 'AMAQ' },
        { code: 'DE', libelle: 'DE' },
        { code: 'REL', libelle: 'REL' },
        { code: 'RELH', libelle: 'RELH' },
        { code: 'EP', libelle: 'EP' },
    ];
    selectedProduct: any;

    @Output() newItemEvent = new EventEmitter<any>();
    @Input() editedProject;
    @Input() editedMode;
    @Input() demandeDTO;
    @Input() persons;

    constructor(
        private fb: FormBuilder,
        private serviceNavigation: ServiceNavigation,
        public ccgService: CcgService,
        private datePipe: DatePipe,
        public routingService: RoutingService
    ) {

    }

    ngOnInit(): void {
        if (this.editedMode == 2) {
            this.getProject()
            this.getVille()
            this.getNatureTF()
            this.getVendeurLogement()
            this.getAcquisitionIndiv()
            this.getTypeLogement()
            this.getPaysAcceuil()
            this.getFiliere()
            this.getDiplome()
            this.getTypeLocal()
            this.getAppreciation()
            this.getTypeHebergement()
            this.getContractantDebiteur()
            this.getFinalite()
            this.getTypeDossier()
            this.getExportAfrique()
            this.getCustomerCard()
            this.getProduitMetier()
        }
    }

    onReciveEmittedValue($event: any) {
        this.newItemEvent.emit('0');
    }

    @Output() emmteur = new EventEmitter<number>();
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }

    onValide() {
        this.newItemEvent.emit('0');
    }

    onReturn() {
        this.newItemEvent.emit('0');
        this.serviceNavigation.mettreAJourChemin([]);
    }

    getProject() {
        let params = {
            id: this.editedProject.id,
            projectId: this.editedProject.id,
            requestId: this.demandeDTO.requestId,
        }

        this.ccgService.postMethode('/ccgQuery/project', params).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.projet = response.result
                    this.projet.dateAgreement = this.datePipe.transform(this.projet.dateAgreement, "yyyy-MM-dd")
                    this.projet.avalDate = this.datePipe.transform(this.projet.avalDate, "yyyy-MM-dd")
                    this.projet.drawDate = this.datePipe.transform(this.projet.drawDate, "yyyy-MM-dd")
                }
            }, (error) => {
            }, () => {

            })
    }

    saveProject() {
        this.projet.customerCard = this.customerCard.id;
        this.projet.mode = "";
        this.ccgService.postMethode('/ccg/saveProject', this.projet).subscribe(
            (response) => {
                this.newItemEvent.emit('0');
            }, (error) => {
            }, () => {
            })
    }

    customerCard: any
    getCustomerCard() {
        let params = {
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/getCustomerCard', params).subscribe(
            (response) => {
                this.customerCard = response.result
            }, (error) => {
            }, () => {
            })
    }


    listeVille = []
    getVille() {
        this.ccgService.getList('/ccgQuery/getCity').subscribe(
            (response) => {
                this.listeVille = response.result;
            }, (error) => {
            }, () => {
            })
    }
    listeNatureTF = []
    getNatureTF() {
        this.ccgService.getList('/ccgQuery/getTfNature').subscribe(
            (response) => {
                this.listeNatureTF = response.result;
            }, (error) => {
            }, () => {
            })
    }
    listeVendeurLogement = []
    getVendeurLogement() {
        this.ccgService.getList('/ccgQuery/getHousingSeller').subscribe(
            (response) => {
                this.listeVendeurLogement = response.result;
            }, (error) => {
            }, () => {
            })
    }
    listeAcquisitionIndiv = []
    getAcquisitionIndiv() {
        this.ccgService.getList('/ccgQuery/getJointAcquisition').subscribe(
            (response) => {
                this.listeAcquisitionIndiv = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeTypeLogement = []
    getTypeLogement() {
        this.ccgService.getList('/ccgQuery/getHousingType').subscribe(
            (response) => {
                this.listeTypeLogement = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listePaysAcceuil = []
    getPaysAcceuil() {
        this.ccgService.getList('/ccgQuery/getHostCountry').subscribe(
            (response) => {
                this.listePaysAcceuil = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeFiliere = []
    getFiliere() {
        this.ccgService.getList('/ccgQuery/getSpinneret').subscribe(
            (response) => {
                this.listeFiliere = response.result;
            }, (error) => {
            }, () => {
            })
    }


    listeDiplome = []
    getDiplome() {
        this.ccgService.getList('/ccgQuery/getDiploma').subscribe(
            (response) => {
                this.listeDiplome = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeTypeLocal = []
    getTypeLocal() {
        this.ccgService.getList('/ccgQuery/getLocalType').subscribe(
            (response) => {
                this.listeTypeLocal = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeAppreciation = []
    getAppreciation() {
        this.ccgService.getList('/ccgQuery/getAppreciation').subscribe(
            (response) => {
                this.listeAppreciation = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeTypeHebergement = []
    getTypeHebergement() {
        this.ccgService.getList('/ccgQuery/getAccommodatinType').subscribe(
            (response) => {
                this.listeTypeHebergement = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeContractantDebiteur = []
    getContractantDebiteur() {
        this.ccgService.getList('/ccgQuery/getDeptorContractingParty').subscribe(
            (response) => {
                this.listeContractantDebiteur = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeTypeDossier = []
    getTypeDossier() {
        let params = {
            produit: this.editedProject.product,
        }
        this.ccgService.postMethode('/ccgQuery/getFolderType', params).subscribe(
            (response) => {
                this.listeTypeDossier = response.result;
            }, (error) => {
            }, () => {
            })
    }


    listeFinalite = []
    getFinalite() {
        let params = {
            produit: this.editedProject.product,
        }
        this.ccgService.postMethode('/ccgQuery/getFinality', params).subscribe(
            (response) => {
                this.listeFinalite = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeExportAfrique = []
    getExportAfrique() {
        this.ccgService.getList('/ccgQuery/getAfricExport').subscribe(
            (response) => {
                this.listeExportAfrique = response.result;
            }, (error) => {
            }, () => {
            })
    }

    listeProduitMetier: any = []
    getProduitMetier() {
        let params = {
            produit: this.editedProject.product,
        }
        this.ccgService.postMethode('/ccgQuery/getInternalProduct', params).subscribe(
            (response) => {
                this.listeProduitMetier = response.result;
            }, (error) => {
            }, () => {
            })
    }


    modeAmortissementList: any = []
    getModeAmortissement() {
        let params = {
            produit: this.editedProject.product,
        }
        this.ccgService.postMethode('/ccgQuery/getDampingMode', params).subscribe(
            (response) => {
                this.modeAmortissementList = response.result;
            }, (error) => {
            }, () => {
            })
    }

}
