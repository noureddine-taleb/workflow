import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgProjetInfoComponent } from "./ccg-projet-info.component";

describe("CcgProjetInfoComponent", () => {
	let component: CcgProjetInfoComponent;
	let fixture: ComponentFixture<CcgProjetInfoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgProjetInfoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgProjetInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
