import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";

@Component({
	selector: "app-ccg-projet-info",
	templateUrl: "./ccg-projet-info.component.html",
	styleUrls: ["./ccg-projet-info.component.scss"],
})
export class CcgProjetInfoComponent implements OnInit {
	constructor(private fb: FormBuilder) {}

	ngOnInit(): void {
		this.form = this.fb.group({
			id: this.fb.control(""),
		});
		this.initForm(this.fields);
	}

	/*fields=[{
      name:'ebe',
      libelle:'EBE',
      type:'text',
  },
      {
          name:'dettepassifcirculant',
          libelle:'Dette du Passif Circulant',
          type:'text',
      },
      {
          name:'Radio',
          libelle:'Radion',
          type:'radio',
          options:[{valeur:1,libelle:'option 1'},{valeur:2,libelle:'option 2'}]
      },
      {
          name:'Radio',
          libelle:'Radion',
          type:'list',
          options:[{valeur:1,libelle:'option 1'},{valeur:2,libelle:'option 2'}]
      },
      {
          name:'number',
          libelle:'Number',
          type:'number',
      },
      {
          name:'date',
          libelle:'date',
          type:'date',
      }

      ]
*/
	form;
	initForm(result: any[]): void {
		result.forEach((rub) => {
			this.form.addControl(
				// tslint:disable-next-line:max-line-length
				rub.name,
				new FormControl(""),
			);
		});
	}

	toFormGroup(controlName: string) {
		const group: any = {};

		group[controlName] = new FormControl("");

		return new FormGroup(group);
	}
	public fields: any[] = [
		{
			type: "text",
			name: "firstName",
			label: "First Name",
			value: "",
			required: true,
		},
		{
			type: "text",
			name: "lastName",
			label: "Last Name",
			value: "",
			required: true,
		},
		{
			type: "text",
			name: "email",
			label: "Email",
			value: "",
			required: true,
		},
		{
			type: "dropdown",
			name: "country",
			label: "Country",
			value: "in",
			required: true,
			options: [
				{ key: "in", label: "India" },
				{ key: "us", label: "USA" },
			],
		},
		{
			type: "radio",
			name: "country",
			label: "Country",
			value: "in",
			required: true,
			options: [
				{ key: "m", label: "Male" },
				{ key: "f", label: "Female" },
			],
		},
		{
			type: "checkbox",
			name: "hobby",
			label: "Hobby",
			required: true,
			options: [
				{ key: "f", label: "Fishing" },
				{ key: "c", label: "Cooking" },
			],
		},
	];
	getFields() {
		return this.fields;
	}

	@Output() emmteur = new EventEmitter<number>();
	onPrevious() {
		this.emmteur.emit(-1);
	}

	onNext() {
		this.emmteur.emit(1);
	}
}
