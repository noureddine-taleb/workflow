import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgProjetInfrTableComponent } from "./ccg-projet-infr-table.component";

describe("CcgProjetInfrTableComponent", () => {
	let component: CcgProjetInfrTableComponent;
	let fixture: ComponentFixture<CcgProjetInfrTableComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgProjetInfrTableComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgProjetInfrTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
