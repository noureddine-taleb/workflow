import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from 'app/core/services/routing.service';
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeActionDialogComponent } from 'app/modules/demandes/demande-action-dialog/demande-action-dialog.component';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Action } from 'app/shared/models/Action';
import { cloneDeep } from "lodash";
import { take } from 'rxjs/operators';
import { ServiceNavigation } from "../navigation-service/service-navigation";

@Component({
    selector: 'app-ccg-projet-infr-table',
    templateUrl: './ccg-projet-infr-table.component.html',
    styleUrls: ['./ccg-projet-infr-table.component.scss']
})
export class CcgProjetInfrTableComponent implements OnInit {
    editMode: number = 0;

    displayedColumns;
    displayedColumnsLib;
    displayedColumnsToolTip;

    displayedColumnStructure = [
        { name: 'libProduct', libelle: 'Produit', matTooltip: 'Produit', type: 'text' },
        { name: 'overallCreditAmount', libelle: 'Montant', matTooltip: 'Montant', type: 'currency' },
        { name: 'dateAgreement', libelle: 'Date Accord', matTooltip: 'Date Accord', type: 'date' },
        { name: 'Action', libelle: 'Action', matTooltip: 'Action', type: '' },
    ]

    chemin: string[] = [];

    constructor(
        private serviceNavigation: ServiceNavigation,
        public ccgService: CcgService,
        private snackBarService: SnackBarService,
        public routingService: RoutingService,
        private communService: CommonService,
		private dialog: MatDialog,
		private demandeService: DemandeService,
		private authService: AuthService,
		public utilsService: UtilsService
    ) {
        serviceNavigation.chemin$.subscribe(newChemin => this.chemin = newChemin);
    }

    @Input() demandeDTO;
    @Input() persons
    ngOnInit(): void {
        this.displayedColumns = this.getDisplayedColumn('name');
        this.displayedColumnsLib = this.getDisplayedColumn('libelle');
        this.displayedColumnsToolTip = this.getDisplayedColumn('matTooltip');

        this.getProjects()
    }

    getDisplayedColumn(field: string) {
        let arr = [];
        this.displayedColumnStructure.forEach(x => arr.push(x[field]))
        return arr;
    }

    deleteActionColumn(displayedColumns: string[]) {
        let displayedeCol = cloneDeep(this.displayedColumns);
        displayedeCol.splice(this.displayedColumns.length - 1, 1)
        return displayedeCol;
    }
    
    editedItem;
    onPrepareEdit(element) {
        this.editMode = 2;
        this.editedItem = element;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Editer']);
    }
    onPrepareAdd() {
        this.editMode = 1;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Ajouter']);
    }

    onRetourEdit(number: number) {
        this.editMode = number;
        this.serviceNavigation.mettreAJourChemin([]);
        this.getProjects()
    }

    onPreparDelete(item) {
        this.snackBarService.openConfirmSnackBar({ message: "all" })
            .then((result) => {
                if (result.value) {
                    item.mode = "S"
                    this.deleteProject(item)
                }
            })
    }

    deleteProject(item) {
        this.ccgService.postMethode('/ccg/saveProject', item).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getProjects()
                }
            }, (error) => {
                this.snackBarService.openErrorSnackBar({ message: "Il faut supprimer (crédits, associés, sûretés) liés à ce projet avant de le supprimer" })
            }, () => {
            })
    }

    loadProjectActions(project: any) {
		// project.actions: used also as a flag so if the dispatch was successfull, we reset actions
		if (Array.isArray(project.actions))
			return;

		this.communService.getCcProjectActions(project.statusCode, project.idWorkflow).subscribe(data => {
			project.actions = data.result;
		})
    }

    dispatchAction(project: any, action: Action) {
		this._dispatchAction(project, action);
    }

	// TODO: we need to refactor this
	_dispatchAction(project: {actions: Action[] | undefined, idWorkflow: number, statusCode: string}, action: Action) {
		if (action?.users?.length == 1) {
			this.snackBarService
			.openConfirmSnackBar({ message: "confirmer l'action ?" })
			.then((result) => {
				if (result.value) {
					let user = action.users[0];
					this.demandeService.executeCcgProjectAction(user, action, null, project).subscribe(
						(data) => {
							this.snackBarService.openSuccesSnackBar({ message: data["message"] });
							this.postActionDispatch(project);
						}
					)
				}
			});
			return;
		}

		const dialogRef = this.dialog.open(DemandeActionDialogComponent, {
			width: "30vw",
			data: {
				actions: project?.actions,
				handler: this.demandeService.executeCcgProjectAction.bind(this.demandeService),
				args: [project]
			},
		});

		dialogRef.afterClosed().subscribe((data) => {
			if (data && data["validation"] == true)
                this.postActionDispatch(project);
		});
	}

    postActionDispatch(project) {
        delete project.actions;
        this.getProjects();
    }

    // _dispatchAction(project: any, action: Action) {
	// 	this.communService.dispatchAction(project, action).subscribe((data) => {
	// 		this.snackBarService.openSuccesSnackBar({ message: data["message"] });
	// 		delete project.actions;
	// 	});
    // }

    @Output() emmteur = new EventEmitter<number>();
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }

    onAfficheCredit(element) {
        this.editMode = 3;
        this.editedItem = element;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Crédits'])
    }

    onAfficheAssocie(element) {
        this.editMode = 4;
        this.editedItem = element;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Associés'])
    }
    onAfficheInformationsRetour(element) {
        this.editMode = 5;
        this.editedItem = element;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Retour'])
    }
    onAfficheInformationsSurete(element) {
        this.editMode = 6;
        this.editedItem = element;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Sûreté'])
    }

    listeProject: any = []
    getProjects() {
        let params = {
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/projects', params).subscribe(
            (response) => {
                this.listeProject = response.result
                for (let project of this.listeProject) {
                    this.communService.getCcgProjectPrivelege(project.statusCode).pipe(take(1)).subscribe(data => {
                        project.privs = data.result;
                    })
                }

            }, (error) => {
            }, () => {
            })
    }
}
