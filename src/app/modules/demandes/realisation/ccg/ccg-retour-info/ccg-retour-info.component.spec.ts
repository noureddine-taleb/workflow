import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgRetourInfoComponent } from "./ccg-retour-info.component";

describe("CcgCcgRetourInfoComponent", () => {
	let component: CcgRetourInfoComponent;
	let fixture: ComponentFixture<CcgRetourInfoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgRetourInfoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgRetourInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
