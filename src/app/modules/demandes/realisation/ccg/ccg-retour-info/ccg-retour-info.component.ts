import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { RetourCcg } from '../dto/retourccg';

@Component({
    selector: 'app-ccg-retour-info',
    templateUrl: './ccg-retour-info.component.html',
    styleUrls: ['./ccg-retour-info.component.scss']
})
export class CcgRetourInfoComponent implements OnInit {

    retour: RetourCcg = new RetourCcg();
    @Output() newItemEvent = new EventEmitter<any>();

    constructor(
        public ccgService: CcgService,
        public routingService: RoutingService,
        private datePipe: DatePipe,
        private snackBarService: SnackBarService,

    ) { }

    @Input() editMode;
    @Input() editedItem;
    @Input() editedProject;
    @Input() persons
    @Input() demandeDTO
    ngOnInit(): void {
        this.getStatusProject()
        this.getRejectReason()
    }

    onReturn() {
        this.newItemEvent.emit('0');
    }

    saveStatusProject() {
        if (this.retour.response == 'A') {
            this.retour.reasonReject = null
        }
        this.retour.project = this.editedProject.id
        this.retour.mode = ""
        this.retour.creationDate = this.datePipe.transform(this.retour.creationDate, "yyyy-MM-dd")
        this.ccgService.postMethode('/ccg/saveStatusProject', this.retour).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getStatusProject()
                }
            }, (error) => {
            }, () => {
            }
        )
    }

    statusProjectList = []
    getStatusProject() {
        let params = {
            first: 0,
            id: 0,
            pas: 0,
            requestId: this.demandeDTO.requestId,
            projectId: this.editedProject.id,
        }
        this.ccgService.postMethode('/ccgQuery/statusProject', params).subscribe(
            (response) => {
                if (response.result.id) {
                    this.retour.copyRetourCcgObj(response.result);
                    this.retour.avalDate = this.datePipe.transform(this.retour.avalDate, "yyyy-MM-dd")
                    this.retour.validateAvalDate = this.datePipe.transform(this.retour.validateAvalDate, "yyyy-MM-dd")
                } else {
                    this.retour = new RetourCcg();
                }
            }, (error) => {
            }, () => {
            }
        )
    }


    rejectReasonList = []
    getRejectReason() {
        let params = {
            creditType: 0,
            first: 0,
            pas: 0,
            productType: this.editedProject.productName,
            produit: this.editedProject.id,
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/getRejectReason', params).subscribe(
            (response) => {
                this.rejectReasonList = response.result
            }, (error) => {
            }, () => {
            }
        )
    }

    onPrepareDelete() {
        this.snackBarService.openConfirmSnackBar({ message: "all" })
            .then((result) => {
                if (result.value) {
                    this.retour.mode = "S"
                    this.deleteRetour(this.retour);
                }
            })
    }

    deleteRetour(item) {
        item.creationDate = this.datePipe.transform(item.creationDate, "yyyy-MM-dd")
        this.ccgService.postMethode('/ccg/saveStatusProject', item).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getStatusProject()
                }
            }, (error) => {
            }, () => {
            })
    }

}
