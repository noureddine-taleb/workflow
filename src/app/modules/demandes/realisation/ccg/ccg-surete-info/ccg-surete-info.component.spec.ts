import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CcgSureteInfoComponent } from "./ccg-surete-info.component";

describe("CcgCcgSureteInfoComponent", () => {
	let component: CcgSureteInfoComponent;
	let fixture: ComponentFixture<CcgSureteInfoComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CcgSureteInfoComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CcgSureteInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
