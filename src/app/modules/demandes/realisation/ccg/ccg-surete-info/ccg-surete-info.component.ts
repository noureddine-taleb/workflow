import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { cloneDeep } from "lodash";
import { ServiceNavigation } from '../navigation-service/service-navigation';

@Component({
    selector: 'app-ccg-surete-info',
    templateUrl: './ccg-surete-info.component.html',
    styleUrls: ['./ccg-surete-info.component.scss']
})
export class CcgSureteInfoComponent implements OnInit {

    displayedColumns = [];
    editMode = 0;

    constructor(
        public ccgService: CcgService,
        private snackBarService: SnackBarService,
        private serviceNavigation: ServiceNavigation,
        public routingService: RoutingService
    ) { }

    @Input() demandeDTO;
    @Input() editedProject;
    @Input() persons
    ngOnInit(): void {
        if (this.editedProject.productName == 'DIS' || this.editedProject.productName == 'DAT') {
            this.displayedColumns = ['suretyName', 'Action'];
        } else[
            this.displayedColumns = ['suretyCode', 'suretyId', 'rank', 'amount', 'Action']
        ]

        this.getSuretyProjects()
    }

    deleteActionColumn(displayedColumns: string[]) {
        let displayedeCol = cloneDeep(this.displayedColumns);
        displayedeCol.splice(this.displayedColumns.length - 1, 1)
        return displayedeCol;
    }

    editedItem;
    onPrepareEdit(item) {
        this.editMode = 2;
        this.editedItem = item
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Sûreté', 'Editer']);
    }

    onPrepareAdd() {
        this.editMode = 1;
        this.editedItem = {}
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Sûreté', 'Ajouter']);
    }

    onRetourEdit(number: number) {
        this.editMode = number;
        this.serviceNavigation.mettreAJourChemin(['Projet', 'Sûreté']);
        this.getSuretyProjects()
    }

    onPrepareDelete(item) {
        this.snackBarService.openConfirmSnackBar({ message: "all" })
            .then((result) => {
                if (result.value) {
                    item.mode = "S"
                    this.deleteSurete(item);
                }
            })
    }

    @Output() emmteur = new EventEmitter<number>();
    onPrevious() {
        this.emmteur.emit(-1);
    }

    onNext() {
        this.emmteur.emit(1);
    }

    deleteSurete(item) {
        this.ccgService.postMethode('/ccg/saveSuretyeProject', item).subscribe(
            (response) => {
                if (response.status == 200) {
                    this.snackBarService.openSuccesSnackBar({ message: response.message })
                    this.getSuretyProjects()
                }
            }, (error) => {
            }, () => {
            })
    }

    listeSuretyProject: any = []
    getSuretyProjects() {
        let params = {
            requestId: this.demandeDTO.requestId,
            projectId: this.editedProject.id,
        }
        this.ccgService.postMethode('/ccgQuery/suretyProjects', params).subscribe(
            (response) => {
                this.listeSuretyProject = response.result
            }, (error) => {
            }, () => {
            }
        )
    }



}
