import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatTooltipModule } from "@angular/material/tooltip";
import { CardModule } from "app/shared/card";
import { NgxCurrencyModule } from "ngx-currency";
import { CcContainerComponent } from "./cc-container/cc-container.component";
import { CcgAssocieInfoComponent } from "./ccg-associe-info/ccg-associe-info.component";
import { CcgChoixCcgProjetDialogComponent } from "./ccg-choix-ccg-projet-dialog/ccg-choix-ccg-projet-dialog.component";
import { CcgCreditInfoComponent } from "./ccg-credit-info/ccg-credit-info.component";
import { CcgInitComponent } from "./ccg-init/ccg-init.component";
import { CcgLandingComponent } from "./ccg-landing/ccg-landing.component";
import { CcgProjectInfoDynamicComponent } from "./ccg-project-info-dynamic/ccg-project-info-dynamic.component";
import { CcgProjetInfoComponent } from "./ccg-projet-info/ccg-projet-info.component";
import { CcgProjetInfrTableComponent } from "./ccg-projet-infr-table/ccg-projet-infr-table.component";
import { CcgRetourInfoComponent } from "./ccg-retour-info/ccg-retour-info.component";
import { CcgSureteInfoComponent } from "./ccg-surete-info/ccg-surete-info.component";
import { ClientIdentityComponent } from "./client-identity/client-identity.component";
import { ClientInfosComponent } from "./client-infos/client-infos.component";
import { EditAssocieComponent } from "./edit-associe/edit-associe.component";
import { EditCcgComponent } from "./edit-ccg/edit-ccg.component";
import { EditRetourComponent } from "./edit-retour/edit-retour.component";
import { EditSureteComponent } from "./edit-surete/edit-surete.component";

@NgModule({
	declarations: [
		CcContainerComponent,
		ClientIdentityComponent,
		ClientInfosComponent,
		CcgLandingComponent,
		CcgCreditInfoComponent,
		CcgProjetInfoComponent,
		CcgAssocieInfoComponent,
		EditCcgComponent,
		CcgProjectInfoDynamicComponent,
		EditAssocieComponent,
		CcgProjetInfrTableComponent,
		CcgInitComponent,
		CcgChoixCcgProjetDialogComponent,
		CcgRetourInfoComponent,
		CcgSureteInfoComponent,
		EditSureteComponent,
		EditRetourComponent,
	],
	exports: [CcContainerComponent],
	imports: [
		CommonModule,
		CardModule,
		MatFormFieldModule,
		MatTableModule,
		NgxCurrencyModule,
		MatButtonModule,
		FormsModule,
		ReactiveFormsModule,
		MatSelectModule,
		MatDialogModule,
		MatInputModule,
		MatMenuModule,
		MatIconModule,
		MatTooltipModule,
		MatRadioModule,
		MatSelectModule,
		MatDatepickerModule
	],
})
export class CcgModule {}
