import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";
import { ConfigService } from 'app/core/config/config.service';
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";
import { UtilsService } from "app/core/services/utils.service";

@Injectable({ providedIn: "root" })
export class CcgService {
	
	constructor(
		private httpClient: HttpClient, 
		private authService: AuthService,
		private utilsService: UtilsService,
	) {}

	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;
	getList(url): Observable<any> {
		//url doit commencer par '/'
		return this.httpClient.get(this.wkfPath + url);
	}

	postMethode(url, object): Observable<any> {
		//url doit commencer par '/'
		object["userId"] = this.authService.user.userId;
		object["userEntityId"] = this.authService.user.userEntityId;
		object["entityId"] = this.authService.user.entityId;
		return this.httpClient.post(this.wkfPath + url, object);
	}

	/**
	 * @param project should contain its privileges in privs property
	 * @returns is project editable
	 */
	canEdit(project: {privs: any[]}) {
		return this.utilsService._hasPrivilege(PrivilegeCode.mj_cov_m, project.privs);
	}

	canDelete(project: {privs: any[]}) {
		return this.utilsService._hasPrivilege(PrivilegeCode.mj_cov_s, project.privs);
	}

	canReject(project: {privs: any[]}) {
		return this.utilsService._hasPrivilege(PrivilegeCode.dlavl_m, project.privs);
	}
}
