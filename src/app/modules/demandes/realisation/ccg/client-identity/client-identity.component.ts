import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Client } from '../dto/client';

@Component({
  selector: 'app-client-identity',
  templateUrl: './client-identity.component.html',
  styleUrls: ['./client-identity.component.scss']
})
export class ClientIdentityComponent implements OnInit {
  projet: any;
  formjuridique = 1;

  identityClient: Client = new Client();


  constructor(
    private ccgService: CcgService,
    private snackBarService: SnackBarService,
    public routingService: RoutingService
  ) { }

  @Input() demandeDTO
  @Input() persons
  ngOnInit(): void {
    this.getCustomerCard()
    this.getLegalStatus()
    this.getVille()
  }

  @Output() emmteur = new EventEmitter<number>();
  onPrevious() {
    this.emmteur.emit(-1);
  }

  onNext() {
    this.saveCustomerCard()
  }

  saveCustomerCard() {
    this.ccgService.postMethode('/ccg/saveCustomerCard', this.identityClient).subscribe(
      (response) => {
        if (response.status == 200) {
          this.emmteur.emit(1);
        } else {
          this.snackBarService.openErrorSnackBar({ message: response.message })
        }

      }, (error) => {
      }, () => {
      })
  }

  getCustomerCard() {
    let params: any = {
      requestId: this.demandeDTO.requestId,
    }
    this.ccgService.postMethode('/ccgQuery/getCustomerCard', params).subscribe(
      (response) => {
        if (response.status == 200) {
          if (response.result.id != null) {
            this.identityClient.copyObjectClient(response.result)
            this.identityClient.idDemandeWkf = this.demandeDTO.requestId
          } else {
            if (this.persons && this.persons.length > 0) {
              this.identityClient.account = this.demandeDTO.accountId
              this.identityClient.idDemandeWkf = this.demandeDTO.requestId
              this.identityClient.adress = this.persons[0].adressName
              this.identityClient.agency = this.demandeDTO.creationEntityId
              this.identityClient.birthDate = this.persons[0].birthDate
              this.identityClient.cardNumber = this.persons[0].cin
              this.identityClient.firstName = this.persons[0].firstName
              this.identityClient.gender = this.persons[0].genderCode
              this.identityClient.lastName = this.persons[0].lastName
              this.identityClient.rcNumber = this.persons[0].rcNumber
              this.identityClient.tiers = this.persons[0].personId
            }
          }
        }
      }, (error) => {
      }, () => {
      })
  }

  listeLegalStatus: any = []
  getLegalStatus() {
    let params = {
      formejuridique: this.persons[0].legalStatus,
    }
    this.ccgService.postMethode('/ccgQuery/getLegalStatus', params).subscribe(
      (response) => {
        this.listeLegalStatus = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeVille: any = []
  getVille() {
    this.ccgService.getList('/ccgQuery/getCity').subscribe(
      (response) => {
        this.listeVille = response.result;
      }, (error) => {
      }, () => {
      })
  }

}
