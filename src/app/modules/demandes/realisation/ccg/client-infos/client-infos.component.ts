import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { Client } from '../dto/client';

@Component({
  selector: 'app-client-infos',
  templateUrl: './client-infos.component.html',
  styleUrls: ['./client-infos.component.scss']
})
export class ClientInfosComponent implements OnInit {
  client: Client = new Client();

  constructor(
    private ccgService: CcgService,
    private datePipe: DatePipe,
    private snackBarService: SnackBarService,
    public routingService: RoutingService
  ) { }

  @Input() demandeDTO
  @Input() persons
  ngOnInit(): void {
    this.getProjects()
    this.getProfession()
    this.getVille()
    this.getActivite()
    this.getCustomerCard()
  }
  @Output() emmteur = new EventEmitter<number>();
  onPrevious() {
    this.emmteur.emit(-1);
  }

  onNext() {
    this.emmteur.emit(1);
  }

  saveCustomerCard() {
    this.ccgService.postMethode('/ccg/saveCustomerCard', this.client).subscribe(
      (response) => {
        if (response.status == 200) {
          this.emmteur.emit(1);
        } else {
          this.snackBarService.openErrorSnackBar({ message: "" })
        }

      }, (error) => {
      }, () => {
      })
  }

  listeProfession: any = []
  getProfession() {
    let params = {}

    this.ccgService.postMethode('/ccgQuery/getProfession', params).subscribe(
      (response) => {
        this.listeProfession = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeVille: any = []
  getVille() {
    this.ccgService.getList('/ccgQuery/getCity').subscribe(
      (response) => {
        this.listeVille = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeActivite: any = []
  getActivite() {
    let params = {}
    this.ccgService.postMethode('/ccgQuery/getActivity', params).subscribe(
      (response) => {
        this.listeActivite = response.result;
      }, (error) => {
      }, () => {
      })
  }

  getCustomerCard() {
    let params: any = {
      requestId: this.demandeDTO.requestId,
    }
    this.ccgService.postMethode('/ccgQuery/getCustomerCard', params).subscribe(
      (response) => {
        if (response.status == 200) {
          if (response.result.id != null) {
            this.client.copyObjectClient(response.result)
            this.client.idDemandeWkf = this.demandeDTO.requestId
            this.client.depositDate = this.datePipe.transform(this.client.depositDate, "yyyy-MM-dd")
          } else {
            if (this.persons && this.persons.length > 0) {
              this.client.account = this.demandeDTO.accountId
              this.client.idDemandeWkf = this.demandeDTO.requestId
              this.client.adress = this.persons[0].adressName
              this.client.agency = this.demandeDTO.creationEntityId
              this.client.birthDate = this.persons[0].birthDate
              this.client.cardNumber = this.persons[0].cin
              this.client.firstName = this.persons[0].firstName
              this.client.gender = this.persons[0].genderCode
              this.client.lastName = this.persons[0].lastName
              this.client.rcNumber = this.persons[0].rcNumber
              this.client.tiers = this.persons[0].personId
            }
          }
        }

        this.client.creationDate= this.datePipe.transform(this.client.creationDate, 'yyyy-MM-dd')

      }, (error) => {
      }, () => {
      })
  }


  productList: string[] = []
  getProjects() {
    let params = {
      requestId: this.demandeDTO.requestId,
    }
    this.ccgService.postMethode('/ccgQuery/projects', params).subscribe(
      (response) => {
        response.result.forEach(element => {
          this.productList.push(element.productName)
        });
      }, (error) => {
      }, () => {
      })
  }

}
