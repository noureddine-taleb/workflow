export class AssocieCcg {
    birthDate: any
    cin: string
    entityId: number
    experience: number
    familyConnection: string
    firstName: string
    gender: number
    id: number
    instructionLevel: number
    mode: string
    name: string
    part: number
    project: number
    projectEngagement: string
    tiers: number
    userEntityId: number
    userId: number

    constructor() {
        this.birthDate = null
        this.cin = null
        this.entityId = null
        this.experience = null
        this.familyConnection = null
        this.firstName = null
        this.gender = null
        this.id = null
        this.instructionLevel = null
        this.mode= null
        this.name = null
        this.part = null
        this.project = null
        this.projectEngagement = null
        this.tiers = null
        this.userEntityId = null
        this.userId = null
    }

    copyAssocieCcgObj(obj: AssocieCcg): void {
        this.birthDate = obj.birthDate
        this.cin = obj.cin
        this.entityId = obj.entityId
        this.experience = obj.experience
        this.familyConnection = obj.familyConnection
        this.firstName = obj.firstName
        this.gender = obj.gender
        this.id = obj.id
        this.instructionLevel = obj.instructionLevel
        this.mode= obj.mode
        this.name = obj.name
        this.part = obj.part
        this.project = obj.project
        this.projectEngagement = obj.projectEngagement
        this.tiers = obj.tiers
        this.userEntityId = obj.userEntityId
        this.userId = obj.userId
    }
}