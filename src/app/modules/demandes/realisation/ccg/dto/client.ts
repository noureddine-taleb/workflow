export class Client {
  account: any
  activity: number
  adress: string
  aeRegitryNumber: number
  agency: number
  beneficiaryAdress: string
  beneficiaryCity: number
  birthDate: Date
  cardNumber: string
  city: number
  cmc: number
  companyName: string
  creationDate: string
  depositDate: any
  entityId: number
  exportTurnOver: number
  firstName: string
  gender: string
  id: number
  idDemandeWkf: number
  internalProduct: number
  lastName: string
  legalStatus: number
  overallLoad: number
  pprNumber: number
  profession: number
  rcNumber: string
  tiers: number
  turnOver: number
  userEntityId: number
  userId: number
  workForceNumber: number
  year: number

  constructor() {
    this.account = ""
    this.activity = null
    this.adress = ""
    this.aeRegitryNumber = null
    this.agency = null
    this.beneficiaryAdress = ""
    this.beneficiaryCity = null
    this.birthDate = null
    this.cardNumber = ""
    this.city = null
    this.cmc = null
    this.companyName = ""
    this.creationDate = null
    this.depositDate = null
    this.entityId = null
    this.exportTurnOver = null
    this.firstName = ""
    this.gender = ""
    this.id = null
    this.idDemandeWkf = null
    this.internalProduct = null
    this.lastName = ""
    this.legalStatus = null
    this.overallLoad = null
    this.pprNumber = null
    this.profession = null
    this.rcNumber = ""
    this.tiers = null
    this.turnOver = null
    this.userEntityId = null
    this.userId = null
    this.workForceNumber = null
    this.year = null
  }

  copyObjectClient(obj: Client): void {
    this.account = obj.account
    this.activity = obj.activity
    this.adress = obj.adress
    this.aeRegitryNumber = obj.aeRegitryNumber
    this.agency = obj.agency
    this.beneficiaryAdress = obj.beneficiaryAdress
    this.beneficiaryCity = obj.beneficiaryCity
    this.birthDate = obj.birthDate
    this.cardNumber = obj.cardNumber
    this.city = obj.city
    this.cmc = obj.cmc
    this.companyName = obj.companyName
    this.creationDate = obj.creationDate
    this.depositDate = obj.depositDate
    this.entityId = obj.entityId
    this.exportTurnOver = obj.exportTurnOver
    this.firstName = obj.firstName
    this.gender = obj.gender
    this.id = obj.id
    this.idDemandeWkf = obj.idDemandeWkf
    this.internalProduct = obj.internalProduct
    this.lastName = obj.lastName
    this.legalStatus = obj.legalStatus
    this.overallLoad = obj.overallLoad
    this.pprNumber = obj.pprNumber
    this.profession = obj.profession
    this.rcNumber = obj.rcNumber
    this.tiers = obj.tiers
    this.turnOver = obj.turnOver
    this.userEntityId = obj.userEntityId
    this.userId = obj.userId
    this.workForceNumber = obj.workForceNumber
    this.year = obj.year
  }

}
