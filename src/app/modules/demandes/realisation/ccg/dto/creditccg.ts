export class CreditCcg {
  agency: number
  amount: number
  bankCreditNumber: string
  benefitRate: number
  capitalizedSum: number
  commissionAmount: number
  commissionReglementDate: Date
  cpiAmount: number
  creditDiId: number
  creditLineBankId: string
  creditObject: number
  creditType: number
  cumulFlag: string
  dampingMode: number
  deadline: any
  delayBenefitRate: number
  detailLineType: string
  differed: number
  dueAmount: number
  dueFirstAmount: number
  duration: number
  entityId: number
  financingQuota: number
  flagSurety: string
  flagjumele: string
  id: number
  idCcg: number
  idWorkflow: number
  idopsiablocage: number
  idopsiareglement: number
  lineAmount: number
  lineType: number
  mode: string
  object: string
  otherCamLineAmount: number
  oxygeneCcgId: string
  periodicity: any
  primeType: number
  project: number
  rateName: string
  remainingCapitalAmount: number
  updatedNum: string
  userEntityId: number
  userId: number

  constructor() {
    this.agency = null
    this.amount = 0
    this.bankCreditNumber = ""
    this.benefitRate = null
    this.capitalizedSum = null
    this.commissionAmount = null
    this.commissionReglementDate = null
    this.cpiAmount = null
    this.creditDiId = null
    this.creditLineBankId = "0"
    this.creditObject = null
    this.creditType = null
    this.cumulFlag = null
    this.dampingMode = null
    this.deadline = null
    this.delayBenefitRate = null
    this.detailLineType = null
    this.differed = null
    this.dueAmount = null
    this.dueFirstAmount = null
    this.duration = null
    this.entityId = null
    this.financingQuota = null
    this.flagSurety = null
    this.flagjumele = null
    this.id = null
    this.idCcg = null
    this.idWorkflow = null
    this.idopsiablocage = null
    this.idopsiareglement = null
    this.lineAmount = null
    this.lineType = null
    this.mode = null
    this.object = null
    this.otherCamLineAmount = null
    this.oxygeneCcgId = null
    this.periodicity = null
    this.primeType = null
    this.project = null
    this.rateName = null
    this.remainingCapitalAmount = null
    this.updatedNum = null
    this.userEntityId = null
    this.userId = null
  }

  copyCreditCcgObj(obj: CreditCcg): void {
    this.agency = obj.agency
    this.amount = obj.amount
    this.bankCreditNumber = obj.bankCreditNumber
    this.benefitRate = obj.benefitRate
    this.capitalizedSum = obj.capitalizedSum
    this.commissionAmount = obj.commissionAmount
    this.commissionReglementDate = obj.commissionReglementDate
    this.cpiAmount = obj.cpiAmount
    this.creditDiId = obj.creditDiId
    this.creditLineBankId = obj.creditLineBankId
    this.creditObject = obj.creditObject
    this.creditType = obj.creditType
    this.cumulFlag = obj.cumulFlag
    this.dampingMode = obj.dampingMode
    this.deadline = obj.deadline
    this.delayBenefitRate = obj.delayBenefitRate
    this.detailLineType = obj.detailLineType
    this.differed = obj.differed
    this.dueAmount = obj.dueAmount
    this.dueFirstAmount = obj.dueFirstAmount
    this.duration = obj.duration
    this.entityId = obj.entityId
    this.financingQuota = obj.financingQuota
    this.flagSurety = obj.flagSurety
    this.flagjumele = obj.flagjumele
    this.id = obj.id
    this.idCcg = obj.idCcg
    this.idWorkflow = obj.idWorkflow
    this.idopsiablocage = obj.idopsiablocage
    this.idopsiareglement = obj.idopsiareglement
    this.lineAmount = obj.lineAmount
    this.lineType = obj.lineType
    this.mode = obj.mode
    this.object = obj.object
    this.otherCamLineAmount = obj.otherCamLineAmount
    this.oxygeneCcgId = obj.oxygeneCcgId
    this.periodicity = obj.periodicity
    this.primeType = obj.primeType
    this.project = obj.project
    this.rateName = obj.rateName
    this.remainingCapitalAmount = obj.remainingCapitalAmount
    this.updatedNum = obj.updatedNum
    this.userEntityId = obj.userEntityId
    this.userId = obj.userId
  }


}