
export class InitCcg {
    account: number = null
    adress: string = null
    agency: number = null
    birthDate: Date
    cardNumber: string = null
    city: number = null
    companyName: string = null
    dossiers: Dossier[] = []
    entityId: number = null
    firstName: string = null
    gender: string = null
    groupe: string = null
    lastName: string = null
    legalStatus: number = null
    mode: string = null
    overallCreditAmount: number = 0
    pprNumber: number = null
    product: number = null
    profession: number = null
    projectId: number = null
    rcNumber: string = null
    requestId: number = null
    tiers: number = null
    userEntityId: number = null
    userId: number = null
}

export class Dossier {
    creditId: number = null
    entityId: number = null
    reportId: number = null
    userEntityId: number = null
    userId: number = null

}