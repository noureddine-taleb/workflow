export class ProjetCcg {
    accommodationType: number
    accomodationAdress: string
    accompanimentType: number
    accumulationOtherCosts: number
    activityApreciation: number
    advancementLevel: number
    africExp: number
    amountAllocatedCpi: number
    amountAllocatedFrs: number
    amountOld: number
    annulatedBy: number
    annulationDate: Date
    area: number
    avalDate: string
    averageMonthlyIncome: number
    balanceSheetTotal: number
    bankingSeniority: number
    cashAssets: number
    city: number
    committeeDate: any
    conjointIncome: number
    costsCapitalizes: number
    cotation: string
    createdBy: number
    creationDate: Date
    cumulativeRegistrationFees: number
    cumulativeTuitionFees: number
    currentLoad: number
    currentResult: number
    customerCard: number
    customsDuty: number
    dateAgreement: string
    debtCt: number
    debtMlt: number
    debtorContractingParty: number
    denominationAbregeeSchool: string
    denominationCompleteSchool: string
    dependent: number
    drawDate: string
    ebe: number
    energyCharges: number
    entityId: number
    finality: string
    financialCharge: number
    financialResult: number
    fiscalCharges: number
    flagAfricExp: string
    flagFirstCreation: string
    flagRuralImpl: string
    globalCost: number
    guardianGuarantor: string
    honorary: number
    hostCountry: number
    housingSeller: number
    ice: string
    id: number
    idCcg: string
    idWorkflow: number
    indivisionAcquisition: number
    landPrice: number
    liabilityCash: number
    libProduct: string
    ligneType: number
    localType: number
    lodgingType: number
    lowLevelActivity: number
    maintenance: number
    married: number
    mode: string
    nameFirstNameGuardianSurety: string
    netProfit: number
    numberOfJobToBeCreated: number
    oldProject: number
    others: number
    outstandingPassiveDebt: number
    overallCreditAmount: number
    ownCapital: number
    personnalCharge: number
    price: number
    principalOld: number
    product: number
    productName: string
    projcetNumber: string
    projectAdress: string
    projectCity: number
    projectCost: number
    projectType: number
    quotityOld: number
    reference: string
    relatedCustomerAccount: number
    rent: number
    requestDate: Date
    researchDiploma: number
    schoolCityCode: number
    sectorCode: number
    segment: string
    socialChargeAmount: number
    socialCharges: number
    status: string
    studiyDuration: number
    tfNature: number
    tfNumber: string
    userEntityId: number
    userId: number

    constructor() {
        this.accommodationType = null
        this.accomodationAdress = null
        this.accompanimentType = null
        this.accumulationOtherCosts = null
        this.activityApreciation = null
        this.advancementLevel = null
        this.africExp = null
        this.amountAllocatedCpi = null
        this.amountAllocatedFrs = null
        this.amountOld = null
        this.annulatedBy = null
        this.annulationDate = null
        this.area = null
        this.avalDate = null
        this.averageMonthlyIncome = null
        this.balanceSheetTotal = null
        this.bankingSeniority = null
        this.cashAssets = null
        this.city = null
        this.committeeDate = null
        this.conjointIncome = null
        this.costsCapitalizes = null
        this.cotation = null
        this.createdBy = null
        this.creationDate = null
        this.cumulativeRegistrationFees = null
        this.cumulativeTuitionFees = null
        this.currentLoad = null
        this.currentResult = null
        this.customerCard = null
        this.customsDuty = null
        this.dateAgreement = null
        this.debtCt = null
        this.debtMlt = null
        this.debtorContractingParty = null
        this.denominationAbregeeSchool = null
        this.denominationCompleteSchool = null
        this.dependent = null
        this.drawDate = null
        this.ebe = null
        this.energyCharges = null
        this.entityId = null
        this.finality = null
        this.financialCharge = null
        this.financialResult = null
        this.fiscalCharges = null
        this.flagAfricExp = null
        this.flagFirstCreation = null
        this.flagRuralImpl = null
        this.globalCost = null
        this.guardianGuarantor = null
        this.honorary = null
        this.hostCountry = null
        this.housingSeller = null
        this.ice = null
        this.id = null
        this.idCcg = null
        this.idWorkflow = null
        this.indivisionAcquisition = null
        this.landPrice = null
        this.liabilityCash = null
        this.libProduct = null
        this.ligneType = null
        this.localType = null
        this.lodgingType = null
        this.lowLevelActivity = null
        this.maintenance = null
        this.married = null
        this.mode = null
        this.nameFirstNameGuardianSurety = null
        this.netProfit = null
        this.numberOfJobToBeCreated = null
        this.oldProject = null
        this.others = null
        this.outstandingPassiveDebt = null
        this.overallCreditAmount = null
        this.ownCapital = null
        this.personnalCharge = null
        this.price = null
        this.principalOld = null
        this.product = null
        this.productName = null
        this.projcetNumber = null
        this.projectAdress = null
        this.projectCity = null
        this.projectCost = null
        this.projectType = null
        this.quotityOld = null
        this.reference = null
        this.relatedCustomerAccount = null
        this.rent = null
        this.requestDate = null
        this.researchDiploma = null
        this.schoolCityCode = null
        this.sectorCode = null
        this.segment = null
        this.socialChargeAmount = null
        this.socialCharges = null
        this.status = null
        this.studiyDuration = null
        this.tfNature = null
        this.tfNumber = null
        this.userEntityId = null
        this.userId = null
    }

    copyProjetCcgObj(obj: ProjetCcg): void {

        this.accommodationType = obj.accommodationType
        this.accomodationAdress = obj.accomodationAdress
        this.accompanimentType = obj.accompanimentType
        this.accumulationOtherCosts = obj.accumulationOtherCosts
        this.activityApreciation = obj.activityApreciation
        this.advancementLevel = obj.advancementLevel
        this.africExp = obj.africExp
        this.amountAllocatedCpi = obj.amountAllocatedCpi
        this.amountAllocatedFrs = obj.amountAllocatedFrs
        this.amountOld = obj.amountOld
        this.annulatedBy = obj.annulatedBy
        this.annulationDate = obj.annulationDate
        this.area = obj.area
        this.avalDate = obj.avalDate
        this.averageMonthlyIncome = obj.averageMonthlyIncome
        this.balanceSheetTotal = obj.balanceSheetTotal
        this.bankingSeniority = obj.bankingSeniority
        this.cashAssets = obj.cashAssets
        this.city = obj.city
        this.committeeDate = obj.committeeDate
        this.conjointIncome = obj.conjointIncome
        this.costsCapitalizes = obj.costsCapitalizes
        this.cotation = obj.cotation
        this.createdBy = obj.createdBy
        this.creationDate = obj.creationDate
        this.cumulativeRegistrationFees = obj.cumulativeRegistrationFees
        this.cumulativeTuitionFees = obj.cumulativeTuitionFees
        this.currentLoad = obj.currentLoad
        this.currentResult = obj.currentResult
        this.customerCard = obj.customerCard
        this.customsDuty = obj.customsDuty
        this.dateAgreement = obj.dateAgreement
        this.debtCt = obj.debtCt
        this.debtMlt = obj.debtMlt
        this.debtorContractingParty = obj.debtorContractingParty
        this.denominationAbregeeSchool = obj.denominationAbregeeSchool
        this.denominationCompleteSchool = obj.denominationCompleteSchool
        this.dependent = obj.dependent
        this.drawDate = obj.drawDate
        this.ebe = obj.ebe
        this.energyCharges = obj.energyCharges
        this.entityId = obj.entityId
        this.finality = obj.finality
        this.financialCharge = obj.financialCharge
        this.financialResult = obj.financialResult
        this.fiscalCharges = obj.fiscalCharges
        this.flagAfricExp = obj.flagAfricExp
        this.flagFirstCreation = obj.flagFirstCreation
        this.flagRuralImpl = obj.flagRuralImpl
        this.globalCost = obj.globalCost
        this.guardianGuarantor = obj.guardianGuarantor
        this.honorary = obj.honorary
        this.hostCountry = obj.hostCountry
        this.housingSeller = obj.housingSeller
        this.ice = obj.ice
        this.id = obj.id
        this.idCcg = obj.idCcg
        this.idWorkflow = obj.idWorkflow
        this.indivisionAcquisition = obj.indivisionAcquisition
        this.landPrice = obj.landPrice
        this.liabilityCash = obj.liabilityCash
        this.libProduct = obj.libProduct
        this.ligneType = obj.ligneType
        this.localType = obj.localType
        this.lodgingType = obj.lodgingType
        this.lowLevelActivity = obj.lowLevelActivity
        this.maintenance = obj.maintenance
        this.married = obj.married
        this.mode = obj.mode
        this.nameFirstNameGuardianSurety = obj.nameFirstNameGuardianSurety
        this.netProfit = obj.netProfit
        this.numberOfJobToBeCreated = obj.numberOfJobToBeCreated
        this.oldProject = obj.oldProject
        this.others = obj.others
        this.outstandingPassiveDebt = obj.outstandingPassiveDebt
        this.overallCreditAmount = obj.overallCreditAmount
        this.ownCapital = obj.ownCapital
        this.personnalCharge = obj.personnalCharge
        this.price = obj.price
        this.principalOld = obj.principalOld
        this.product = obj.product
        this.productName = obj.productName
        this.projcetNumber = obj.projcetNumber
        this.projectAdress = obj.projectAdress
        this.projectCity = obj.projectCity
        this.projectCost = obj.projectCost
        this.projectType = obj.projectType
        this.quotityOld = obj.quotityOld
        this.reference = obj.reference
        this.relatedCustomerAccount = obj.relatedCustomerAccount
        this.rent = obj.rent
        this.requestDate = obj.requestDate
        this.researchDiploma = obj.researchDiploma
        this.schoolCityCode = obj.schoolCityCode
        this.sectorCode = obj.sectorCode
        this.segment = obj.segment
        this.socialChargeAmount = obj.socialChargeAmount
        this.socialCharges = obj.socialCharges
        this.status = obj.status
        this.studiyDuration = obj.studiyDuration
        this.tfNature = obj.tfNature
        this.tfNumber = obj.tfNumber
        this.userEntityId = obj.userEntityId
        this.userId = obj.userId

    }


}
