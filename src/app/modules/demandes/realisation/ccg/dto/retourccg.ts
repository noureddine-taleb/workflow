export class RetourCcg {
    avalDate: string
    commissionAmount: number
    createBy: number
    creationDate: string
    entityId: number
    garantedAmount: number
    id: number
    loanAmount: number
    mode: string
    project: number
    quotity: number
    reasonReject: string
    response: string
    userEntityId: number
    userId: number
    validateAvalDate: string

    constructor() {
        this.avalDate = null
        this.commissionAmount = 0
        this.createBy = null
        this.creationDate = null
        this.entityId = null
        this.garantedAmount = 0
        this.id = null
        this.loanAmount = 0
        this.mode = ""
        this.project = null
        this.quotity = 0
        this.reasonReject = ""
        this.response = ""
        this.userEntityId = null
        this.userId = null
        this.validateAvalDate = null
    }
    copyRetourCcgObj(obj: RetourCcg): void {
        this.avalDate = obj.avalDate
        this.commissionAmount = obj.commissionAmount
        this.createBy = obj.createBy
        this.creationDate = obj.creationDate
        this.entityId = obj.entityId
        this.garantedAmount = obj.garantedAmount
        this.id = obj.id
        this.loanAmount = obj.loanAmount
        this.mode = obj.mode
        this.project = obj.project
        this.quotity
        this.reasonReject = obj.reasonReject
        this.response = obj.response
        this.userEntityId = obj.userEntityId
        this.userId = obj.userId
        this.validateAvalDate = obj.validateAvalDate
    }
}