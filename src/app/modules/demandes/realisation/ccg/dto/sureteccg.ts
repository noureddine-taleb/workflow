export class SureteCcg {

    amount: number
    entityId: number
    id: number
    project: number
    rank: number
    suretyCode: number
    suretyId: string
    suretyName: string
    tiers: number
    userEntityId: number
    userId: number
    mode: string

    constructor() {
        this.amount = 0
        this.entityId = null
        this.id = null
        this.project = null
        this.rank = null
        this.suretyCode = null
        this.suretyId = ""
        this.suretyName = ""
        this.tiers = null
        this.userEntityId = null
        this.userId = null
        this.mode = ""
    }

    copySureteCcgObj(obj: SureteCcg): void {
        this.amount = obj.amount
        this.entityId = obj.entityId
        this.id = obj.id
        this.project = obj.project
        this.rank = obj.rank
        this.suretyCode = obj.suretyCode
        this.suretyId = obj.suretyId
        this.suretyName = obj.suretyName
        this.tiers = obj.tiers
        this.userEntityId = obj.userEntityId
        this.userId = obj.userId
        this.mode = obj.mode
    }
}