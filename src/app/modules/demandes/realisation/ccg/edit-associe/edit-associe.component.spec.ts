import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EditAssocieComponent } from "./edit-associe.component";

describe("EditAssocieComponent", () => {
	let component: EditAssocieComponent;
	let fixture: ComponentFixture<EditAssocieComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EditAssocieComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditAssocieComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
