import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { AssocieCcg } from '../dto/associeccg';

@Component({
    selector: 'app-edit-associe',
    templateUrl: './edit-associe.component.html',
    styleUrls: ['./edit-associe.component.scss']
})
export class EditAssocieComponent implements OnInit {

    associe: AssocieCcg = new AssocieCcg();
    @Output() newItemEvent = new EventEmitter<any>();

    constructor(
        public ccgService: CcgService,
        public routingService: RoutingService,
        private datePipe: DatePipe
    ) { }

    @Input() editMode;
    @Input() editedItem;
    @Input() editedProject;
    @Input() persons
    ngOnInit(): void {
        if (this.editMode == 2) {
            this.associe.copyAssocieCcgObj(this.editedItem)
            this.associe.birthDate = this.datePipe.transform(this.associe.birthDate, "yyyy-MM-dd")
            this.associe.gender = +this.associe.gender
        } else {
            this.associe = new AssocieCcg();
        }
        this.getNiveauInstruction()
    }

    onReturn() {
        this.newItemEvent.emit('0');
    }

    saveAssociateProject() {
        this.associe.project = this.editedProject.id
        this.associe.tiers = this.persons[0].personId
        this.associe.mode = ""
        this.ccgService.postMethode('/ccg/saveAssociateProject', this.associe).subscribe(
            (response) => {
                this.newItemEvent.emit('0');
            }, (error) => {
            }, () => {
            }
        )
    }


    listeNiveauInstruction: []
    getNiveauInstruction() {
        this.ccgService.getList('/ccgQuery/getEducationalLevel').subscribe(
            (response) => {
                this.listeNiveauInstruction = response.result;
            }, (error) => {
            }, () => {
            }
        )
    }

}