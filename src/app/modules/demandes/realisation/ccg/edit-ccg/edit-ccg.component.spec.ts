import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EditCcgComponent } from "./edit-ccg.component";

describe("EditCcgComponent", () => {
	let component: EditCcgComponent;
	let fixture: ComponentFixture<EditCcgComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EditCcgComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditCcgComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
