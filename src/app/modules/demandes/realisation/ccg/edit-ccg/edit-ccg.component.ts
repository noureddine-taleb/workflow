import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CreditCcg } from '../dto/creditccg';

@Component({
  selector: 'app-edit-ccg',
  templateUrl: './edit-ccg.component.html',
  styleUrls: ['./edit-ccg.component.scss']
})
export class EditCcgComponent implements OnInit {
  credit: CreditCcg = new CreditCcg()
  matcher = new MyErrorStateMatcher();
  bankCreditNumber = new FormControl('');
  @Output() newItemEvent = new EventEmitter<any>();
  @Input() editedItem;
  @Input() editedProject;
  @Input() editMode;
  @Input() demandeDTO;
  constructor(
    public ccgService: CcgService,
    private datePipe: DatePipe,
    private snackBarService: SnackBarService,
    public routingService: RoutingService
  ) { }


  displayedColumns = ['mecanisme', 'plafond', 'echelle', 'dureeMaxRembourss', 'tauxHT']
  convention = [
    {
      mecanisme: 1,
      plafond: 20000,
      echelle: "1 à 8",
      dureeMaxRembourss: "48",
      tauxHT: 0
    },
    {
      mecanisme: 1,
      plafond: 20000,
      echelle: "9 et plus",
      dureeMaxRembourss: "24",
      tauxHT: 0
    },
    {
      mecanisme: 2,
      plafond: 30000,
      echelle: "",
      dureeMaxRembourss: "Inférieur ou égale à 12",
      tauxHT: 1
    },
    {
      mecanisme: 2,
      plafond: 30000,
      echelle: "",
      dureeMaxRembourss: "Entre 13 et 24",
      tauxHT: 2
    },
    {
      mecanisme: 2,
      plafond: 30000,
      echelle: "",
      dureeMaxRembourss: "Entre 25 et 36",
      tauxHT: 3
    },
  ]

  ngOnInit(): void {
    if (this.editMode == 2) {
      this.credit.copyCreditCcgObj(this.editedItem)
      // Format Dead line
      this.credit.deadline = this.datePipe.transform(this.credit.deadline, "yyyy-MM-dd")
      // Convert periodicity id from string to number
      this.credit.periodicity = +this.credit.periodicity
    } else {
      // Set new credit object
      this.credit = new CreditCcg()
      this.credit.project = this.editedProject.id
      this.credit.agency = this.demandeDTO.creationEntityId
    }

    this.getPeriodicite()
    this.getModeAmortissement()
    this.getTypeLigne()
    this.getTypePrimeActuariel()
    this.getTypeCredit()
    this.getObjetCredit()
    if (this.routingService.isDemandeRO() || (this.editMode == 2 && (!this.ccgService.canEdit(this.editedProject))) )
      this.bankCreditNumber.disable()
  }

  onCreditTypeChange() {
    if (this.editMode == 2) {
      this.credit.copyCreditCcgObj(this.editedItem)
    } else {
      this.credit = new CreditCcg()
    }
  }

  onReturn() {
    this.newItemEvent.emit('0');
  }

  saveCreditProject() {
    this.credit.bankCreditNumber = this.credit.bankCreditNumber.trim()
    if (this.credit.bankCreditNumber == "") {
      this.bankCreditNumber.markAsTouched();
      return
    }
    this.credit.mode = "";
    this.ccgService.postMethode('/ccg/saveCreditProject', this.credit).subscribe(
      (response) => {
        this.newItemEvent.emit('0');
      }, (error) => {
      }, () => {
      })
  }

  listePeriodicite: []
  getPeriodicite() {
    this.ccgService.getList('/ccgQuery/getPeriodicity').subscribe(
      (response) => {
        this.listePeriodicite = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeAmortissement: []
  getModeAmortissement() {
    this.ccgService.getList('/ccgQuery/getDampingMode').subscribe(
      (response) => {
        this.listeAmortissement = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeTypeLigne: []
  getTypeLigne() {
    this.ccgService.getList('/ccgQuery/getLineType').subscribe(
      (response) => {
        this.listeTypeLigne = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeTypeCredit: any = []
  getTypeCredit() {
    let params = {
      produit: this.editedProject.product
    }
    this.ccgService.postMethode('/ccgQuery/getLoanType', params).subscribe(
      (response) => {
        this.listeTypeCredit = response.result;
      }, (error) => {
      }, () => {
      })
  }

  listeObjetCredit: any = []
  getObjetCredit() {
    let params = {
      produit: this.editedProject.product,
    }
    this.ccgService.postMethode('/ccgQuery/getLoanObject', params).subscribe(
      (response) => {
        this.listeObjetCredit = response.result;
      }, (error) => {
      }, () => {
      })
  }


  typePrimeActuarielList = []
  getTypePrimeActuariel() {
    this.ccgService.getList('/ccgQuery/getActuarialPrimeType').subscribe(
      (response) => {
        this.typePrimeActuarielList = response.result;
      }, (error) => {
      }, () => {
      })
  }

}

/** Error when invalid control is dirty or touched*/
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}