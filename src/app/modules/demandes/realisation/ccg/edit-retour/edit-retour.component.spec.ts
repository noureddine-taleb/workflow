import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EditRetourComponent } from "./edit-retour.component";

describe("EditRetourComponent", () => {
	let component: EditRetourComponent;
	let fixture: ComponentFixture<EditRetourComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EditRetourComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditRetourComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
