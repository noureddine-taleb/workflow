import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { RetourCcg } from '../dto/retourccg';

@Component({
    selector: 'app-edit-retour',
    templateUrl: './edit-retour.component.html',
    styleUrls: ['./edit-retour.component.scss']
})
export class EditRetourComponent implements OnInit {

    retour: RetourCcg = new RetourCcg();
    @Output() newItemEvent = new EventEmitter<any>();

    constructor(
        private ccgService: CcgService,
        private datePipe: DatePipe,
        private snackBarService: SnackBarService,

    ) { }

    @Input() editMode;
    @Input() editedItem;
    @Input() editedProject;
    @Input() persons
    @Input() demandeDTO
    ngOnInit(): void {
        
    }

    onReturn() {
        this.newItemEvent.emit('0');
    }

}