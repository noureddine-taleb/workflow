import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EditSureteComponent } from "./edit-surete.component";

describe("EditSureteComponent", () => {
	let component: EditSureteComponent;
	let fixture: ComponentFixture<EditSureteComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EditSureteComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EditSureteComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
