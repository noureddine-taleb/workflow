import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CcgService } from "app/modules/demandes/realisation/ccg/ccg.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SureteCcg } from '../dto/sureteccg';

@Component({
    selector: 'app-edit-surete',
    templateUrl: './edit-surete.component.html',
    styleUrls: ['./edit-surete.component.scss']
})
export class EditSureteComponent implements OnInit {

    surete: SureteCcg = new SureteCcg();
    @Output() newItemEvent = new EventEmitter<any>();

    constructor(
        public ccgService: CcgService,
        public routingService: RoutingService,
    ) { }

    @Input() editMode;
    @Input() editedItem;
    @Input() editedProject;
    @Input() persons
    @Input() demandeDTO
    ngOnInit(): void {
        this.getFamilleSurete()
        
        if (this.editMode == 2) {
            this.surete.copySureteCcgObj(this.editedItem)
            this.getNatureSurete()
            this.getCodeSurete()
        } else {
            this.surete = new SureteCcg();
        }
        
    }

    onReturn() {
        this.newItemEvent.emit('0');
    }

    saveSuretyeProject() {

        this.surete.project = this.editedProject.id
        this.surete.tiers = this.persons[0].personId
        this.surete.mode = "";
        this.ccgService.postMethode('/ccg/saveSuretyeProject', this.surete).subscribe(
            (response) => {
                this.newItemEvent.emit('0');
            }, (error) => {
            }, () => {
            }
        )
    }


    listeFamilleSurete: []
    getFamilleSurete() {
        this.ccgService.getList('/ccgQuery/getSuretyFamily').subscribe(
            (response) => {
                this.listeFamilleSurete = response.result;
            }, (error) => {
            }, () => {
            }
        )
    }

    garantieMereId: null;
    listeNatureSurete: []
    getNatureSurete() {
        let params = {
            garantieMere: this.garantieMereId,
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/getSuretyNature', params).subscribe(
            (response) => {
                this.listeNatureSurete = response.result;
            }, (error) => {
            }, () => {
            }
        )
    }

    natureId = null;
    listeCodeSurete: []
    getCodeSurete() {
        let params = {
            nature: this.natureId,
            requestId: this.demandeDTO.requestId,
        }
        this.ccgService.postMethode('/ccgQuery/getSuretyCode', params).subscribe(
            (response) => {
                this.listeCodeSurete = response.result;
            }, (error) => {
            }, () => {
            })
    }


}