import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({ providedIn: "root" })
export class ServiceNavigation {
	private chemin = new Subject<string[]>();
	chemin$ = this.chemin.asObservable();

	mettreAJourChemin(newChemin: string[]) {
		this.chemin.next(newChemin);
	}
}
