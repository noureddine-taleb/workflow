import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DeblocageDialogComponent } from "./deblocage-dialog.component";

describe("DeblocageDialogComponent", () => {
	let component: DeblocageDialogComponent;
	let fixture: ComponentFixture<DeblocageDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DeblocageDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DeblocageDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
