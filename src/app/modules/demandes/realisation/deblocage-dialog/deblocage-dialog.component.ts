import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-deblocage-dialog",
	templateUrl: "./deblocage-dialog.component.html",
	styleUrls: ["./deblocage-dialog.component.scss"],
})
export class DeblocageDialogComponent implements OnInit {
	constructor(
		public dialogRef: MatDialogRef<DeblocageDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {}

	ngOnInit(): void {}

	onValide() {}

	fermer() {
		this.dialogRef.close(false);
	}
}
