import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DeblocageDialogComponent } from "../deblocage-dialog/deblocage-dialog.component";

@Component({
	selector: "app-deblocage",
	templateUrl: "./deblocage.component.html",
	styleUrls: ["./deblocage.component.scss"],
})
export class DeblocageComponent implements OnInit {
	displayedColumns: string[] = [
		"NDOSSIER",
		"NOMINAL",
		"DATEEVENEMENT",
		"DATETRAITEMENT",
		"MONTANTDEBLOQUE",
		"BLOCAGESIOB",
		"STATUT",
		"ACTION",
	];

	displayedColumnsLielle: string[] = [
		"N°DOSSIER",
		"NOMINAL",
		"DATE EVENEMENT",
		"DATE TRAITEMENT",
		"MONTANT DEBLOQUE",
		"BLOCAGE SIOB",
		"STATUT",
		"ACTION",
	];
	dataSource = [
		{
			NDOSSIER: "CM85421",
			NOMINAL: 1000,
			DATEEVENEMENT: "24/01/2021",
			DATETRAITEMENT: "02/02/2021",
			MONTANTDEBLOQUE: 2000,
			BLOCAGESIOB: "",
			STATUT: "Attente déblocage",
		},
	];

	constructor(private dialog: MatDialog) {}

	ngOnInit(): void {}
	editStat = 0;
	displayedColumnsFooter(): any {
		return this.editStat === 0 ? [] : this.displayedColumns;
	}

	onDebloque(element): void {
		const dialogRef = this.dialog.open(DeblocageDialogComponent, {
			data: {
				produit: element,
			},
			width: "20vw",
		});

		dialogRef.afterClosed().subscribe((data) => {});
	}
}
