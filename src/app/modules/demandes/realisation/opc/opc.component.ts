import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { GedService } from "app/core/services/ged.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeDocsAttchComponent } from "app/shared/demande-docs-attch/demande-docs-attch.component";
import { EventCode } from "app/modules/demandes/models/EventCode";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { PrivilegeService } from "app/modules/demandes/services/privilege.service";
import { RequestPerson } from "app/shared/models/RequestPerson";
import { Subscription } from "rxjs";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { OpcAddProductDialogComponent } from "../../../../shared/opc-add-product-dialog/opc-add-product-dialog.component";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { RequestDoc } from "app/shared/demande-docs/demande-docs.types";
import { DialogOpcActionsComponent } from "app/shared/dialog-opc-actions/dialog-opc-actions.component";
import { ReportGeneratorService } from "app/shared/reportGenerator/report-generator.service";

@Component({
    selector: "app-opc",
    templateUrl: "./opc.component.html",
    styleUrls: ["./opc.component.scss"],
})
export class OpcComponent implements OnInit {
    request: LoanRequest;
    listprod;
    listOpc = [];
    listReport = [];
    typeContracts = [];
    subscription = new Subscription();

    eventCodes = EventCode;
    persons: RequestPerson[];
    person: RequestPerson;
    cautionDisplayedColumns: string[] = [
        "firstName",
        "lastName",
        "reference",
        "birthDate",
        "adress",
        "annule",
        "annulationDate",
    ];
    guarantiesDisplayedColumns: string[] = [
        "reference",
        "natureName",
        "value",
        "rank",
        "annule",
        "annulationDate",
    ];
    guaranties = [];
    deposits = [];
    opc: Opc;

    constructor(
        private dialog: MatDialog,
        public demandeService: DemandeService,
        private snackBarService: SnackBarService,
        private communService: CommonService,
        private globasprivilege: PrivilegeService,
        public utilsService: UtilsService,
        private authService: AuthService,
        public routingService: RoutingService,
        private gedService: GedService,
        private reportGeneratorService: ReportGeneratorService,
    ) {}

    ngOnInit(): void {
        this.request = this.demandeService.request;
        this.persons = this.demandeService.persons;
        this.getListOpc();
        this.getListReport();
        this.getTypeContract();
        this.person = this.persons[0];
    }

    rechargerDemande() {
        this.demandeService.reloadDemande().subscribe(_=>_);
    }

    getListReport() {
        let sub = this.demandeService.listReports(this.request.requestId).subscribe((data) => {
            this.listReport = data["result"];
            this.updateGuarantiesDeposits(this.listReport);
        });
        this.subscription.add(sub);
    }
    getListOpc() {
        let sub = this.demandeService.listOpc(this.request.requestId).subscribe((data) => {
            this.listOpc = data["result"];
            if (this.opc && this.opc.contractId) {
                this.opc = this.listOpc.find((item) => item.contractId == this.opc.contractId);
                this.getAction().then((data) => {
                    this._getAction(data);
                    this.getPrivilegeOpc();
                });
            }
        });
        this.subscription.add(sub);
    }

    updateGuarantiesDeposits(reports: any[]) {
        this.guaranties = (reports?.map((r) => (r.garanties || [])) || ([] as any)).flat();
        this.deposits = (reports?.map((r) => (r.deposits || [])) || ([] as any)).flat();
    }

    saveOpc() {
        this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
            if (result.value) {
                let sub = this.demandeService.saveOpc(this.opc).subscribe((data) => {
                    this.opc.contractId = data["result"];
                    this.getListOpc();
                    this.snackBarService.openSuccesSnackBar({ message: data["message"] });
                });
                this.subscription.add(sub);
            }
        });
    }

    deleteOpc(opc) {
        this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
            if (result.value) {
                let sub = this.demandeService.deleteOpc(opc).subscribe((data) => {
                    this.getListOpc();
                    this.snackBarService.openSuccesSnackBar({ message: data["message"] });
                });
                this.subscription.add(sub);
            } //else this.demandeService.saveOpcJson(this.opc)
        });
    }
    detacherReport(report) {
        // we are in insert mode so we can safely remove the product on the fly
        if (!this.opc.contractId) {
            this.opc.reports = this.opc.reports?.filter((p) => p.productId != report.productId);
            this.updateGuarantiesDeposits(this.opc.reports);
            return;
        }

        this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
            if (result.value) {
                //report['']
                this.demandeService.detacherReport(report).subscribe((data) => {
                    this.snackBarService.openSuccesSnackBar({ message: data["message"] });
                });
            }
        });
    }

    remettreClient() {
        this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
            if (result.value) {
                let sub = this.demandeService.remettreClient(this.opc).subscribe((data) => {
                    this.getListOpc();
                    this.snackBarService.openSuccesSnackBar({ message: data["message"] });
                });
                this.subscription.add(sub);
            } //else this.demandeService.saveOpcJson(this.opc)
        });
    }

    selected = 1;
    compareId(o1, o2) {
        return o1 == o2;
    }
    onSelectOpc(_select) {
        this.opc = _select;
        this.getPrivilegeOpc();
        this.getAction().then(this._getAction.bind(this));
    }

    getPrivilegeOpc() {
        if (this.opc.contractId) {
            this.communService.getOpcPrivelege(this.opc["statusCode"])
                .subscribe((data) => {
                    this.globasprivilege["listPrivilegeOpc"] = data["result"];
                });
        } else {
            this.globasprivilege["listPrivilegeOpc"] = this.globasprivilege["listPrivilege"];
        }
    }

    onAddProduct() {
        this.listReport.forEach((item) => {
            let temp = this.opc.reports?.find((x) => x.productId == item.productId);

            if (temp) {
                item["checked"] = true;
            } else item["checked"] = false;
        });
        const dialogRef = this.dialog.open(OpcAddProductDialogComponent, {
            data: { typeDemande: this.request.creditTypeId, products: this.listReport },
            width: "60vw",
        });
        dialogRef.afterClosed().subscribe((data) => {
            if (data) {
                this.listprod = data;
                this.opc.reports = data;
                this.updateGuarantiesDeposits(this.opc.reports);
            }
        });
    }

    getCheckedProduct() {
        if (this.listprod) return this.listprod.filter((x) => x["checked"]);
        return [];
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    onPrepareAddOpc() {
        this.opc = new Opc();
        this.opc.creditRequest = this.request.requestId;
        this.getPrivilegeOpc();
        this.listAction = [];
    }
    getTypeContract() {
        let sub = this.demandeService
            .getTypeContract({ requestId: this.request.requestId })
            .subscribe((data) => {
                this.typeContracts = data["result"];
            });
        this.subscription.add(sub);
    }

    listAction = [];

    getAction() {
        return this.communService.getOpcActions(this.opc["statusCode"], this.opc["contractId"]).toPromise();
    }

    _getAction(data) {
        this.listAction = data["result"];
    }

    _onExecute(action: any, user: any) {
        let critere = {};
        critere["userId"] = this.authService.user.userId;
        critere["entityId"] = this.authService.user.entityId;
        critere["userEntityId"] = this.authService.user.userEntityId;
        critere["contractId"] = this.opc.contractId;
        critere["creditRequest"] = this.request.requestId;
        critere["url"] = action["urlAction"];
        critere["offerDate"] = this.opc.offerDate;
        critere["acceptanceDate"] = this.opc.acceptanceDate;
        critere["userActionId"] = user.userId;
        critere["action"] = action.actionCode;

        this.communService.executeActionByUrl(critere).subscribe((data) => {
            this.snackBarService.openSuccesSnackBar({ message: data["message"] });
            if(action.actionCode==='IM') {
                const criteria = {};
                criteria['contrat'] = this.opc.contractId;
                criteria['report'] = 'contrat';
                criteria['creditRequest'] = this.request.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    //console.log(data.result);
                    this.reportGeneratorService.convertToPdf(data.result, 'contrat');
                });
            }
            this.postAction();
        });
    }

    onExecuter(itemElement) {
        if (itemElement.users.length == 1) {
            this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
                if (result.value) {
                    this._onExecute(itemElement, itemElement.users[0]);
                }
            });
        } else {
            let critere = {};
            critere["requestId"] = this.request.requestId;
            critere["contractId"] = this.opc.contractId;
            critere["urlAction"] = itemElement["urlAction"];
            critere["action"] = itemElement;
            critere["offerDate"] = this.opc.offerDate;
            critere["acceptanceDate"] = this.opc.acceptanceDate;
            const dialogRef = this.dialog.open(DialogOpcActionsComponent, { data: critere });
            dialogRef.afterClosed().subscribe(success => {
                if (success) {
                    this.postAction();
                }
            });
        }
    }

    postAction() {
        this.getListOpc();
        this.rechargerDemande();
    }

    upload() {
        this.dialog.open(DemandeDocsAttchComponent, {
            data: {
                demandeId: this.request.requestId,
                documentId: 4,
                relationId: this.opc.contractId
            },
            width: "600px",
        })
    }

    uploadWithType(typedoc): void{
        this.dialog.open(DemandeDocsAttchComponent, {
            data: {
                demandeId: this.request.requestId,
                documentId: typedoc,
                relationId: this.opc.contractId
            },
            width: '600px',
        });
    }

    download() {
        this.gedService.getUploadedFiles(this.request.requestId).subscribe((data) => {
            let doc: RequestDoc = data.result.find(file => file.documents.find(d => d.docId == 4));
            if (doc)
                this.gedService.downloadSaveFile(doc);
            else
                this.snackBarService.openErrorSnackBar({ message: "aucun fichier avec docId = 4"});
        });
    }

    onEditContrat(opcInt: Opc): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['contrat'] = opcInt.contractId;
                criteria['report'] = 'contrat';
                criteria['creditRequest'] = this.request.requestId;

                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    //console.log(data.result);
                    this.reportGeneratorService.convertToPdf(data.result,'contrat') ;
                });
            }
        });
    }
}
export class Opc {
    acceptanceDate: Date;
    assurance: string;
    conditionsReserves: string;
    contractType: number;
    contractId: number;
    court: string;
    creditObject: string;
    creditRequest: number;
    description: string;
    entityId: number;
    guarantieeCapital: number;
    identifiant: number;
    insuranceAdress: string;
    insuranceCompagny: string;
    insuranceDuration: number;
    insurancePrime: string;
    insuranceRisk: string;
    landConservation: string;
    offerDate: Date;
    registration: string;
    reports: Report[];
    representedBy: string;
    riskExcluded: string;
    specialClause: string;
    surety: string;
    userEntityId: number;
    userId: number;
    validityDate: Date;
}

export class Report {
    accessoryFres: number;
    amortizedCapital: number;
    certificateId: number;
    contractId: number;
    creditAmount: number;
    deadlineDate: Date;
    deadlineNumber: number;
    description: string;
    differed: number;
    dueAmount: number;
    duration: number;
    entityId: number;
    firstDeadlineDate: Date;
    fres: number;
    guarantyId: number;
    insuranceAmount: number;
    interstAmount: number;
    lastDeadlineDate: Date;
    nodoss: string;
    nominalAmount: number;
    periodicity: string;
    productId: number;
    productName: string;
    rate: number;
    reportId: number;
    statusCode: string;
    statusName: string;
    teg: number;
    userEntityId: number;
    userId: number;
    variablePartRate: number;
    variableRate: number;
    versementInitial: number;
    deposits: any[];
    guaranties: any[];
}
