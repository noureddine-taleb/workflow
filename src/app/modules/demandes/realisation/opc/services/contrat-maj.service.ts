import { Injectable } from '@angular/core';
import {ConfigService} from '../../../../../core/config/config.service';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../../../auth/auth.service';
import {DemandeService} from '../../../services/demande.service';
import {ApiResponse} from "../../../../../core/models/ApiResponse";
import {Action} from "../../../../../shared/models/Action";
import {Opc} from "../opc.component";

@Injectable({
  providedIn: 'root'
})
export class ContratMajService {
    private restWKFUrl = ConfigService.settings.apiServer.restWKFUrl + '/ref/';
    private restREFUrl = ConfigService.settings.apiServer.restREFUrl + '/ref/';

    private restWKFBaseUrl = ConfigService.settings.apiServer.restWKFUrl + '/';
  constructor(private http: HttpClient,
              private authService: AuthService,
              private demandeService: DemandeService) { }

    getActions(opc: Opc, override: any = {}): any {
        const critere: any = {};
        critere['userEntityId'] = this.authService.user.userEntityId;
        critere['userId'] = this.authService.user.userId;
        critere['entityId'] = this.authService.user.entityId;
        critere['requestId'] = this.demandeService.request.requestId;
        critere['lineId'] = opc.identifiant;
        critere['module'] = 'CONTRAT';
        critere['creationEntityId'] = this.demandeService.request.creationEntityId;
        critere['statusCode'] = null;

        return this.http.post<ApiResponse<Action[]>>(this.restWKFBaseUrl + 'action/getActionContrat', {...critere, ...override});
    }
}
