import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import {
	MatDateFormats,
} from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { DialogOpcActionsModule } from "app/shared/dialog-opc-actions/dialog-opc-actions.module";
import { NgxCurrencyModule } from "ngx-currency";
import { AutorisationLivraisonAddComponent } from './autorisation-livraison-add/autorisation-livraison-add.component';
import { AutorisationLivraisonComponent } from './autorisation-livraison/autorisation-livraison.component';
import { CcgModule } from "./ccg/ccg.module";
import { DeblocageDialogComponent } from "./deblocage-dialog/deblocage-dialog.component";
import { DeblocageComponent } from "./deblocage/deblocage.component";
import { GuarantiesComponent } from "./guaranties/guaranties.component";
import { OpcComponent } from "./opc/opc.component";

export const MY_FORMATS: MatDateFormats = {
	parse: {
		dateInput: "DD/MM/YYYY",
	},
	display: {
		dateInput: "DD/MM/YYYY",
		monthYearLabel: "MMM YYYY",
		dateA11yLabel: "LL",
		monthYearA11yLabel: "MMMM YYYY",
	},
};

@NgModule({
	declarations: [
		OpcComponent,
		GuarantiesComponent,
		DeblocageComponent,
		DeblocageDialogComponent,
  		AutorisationLivraisonComponent,
    	AutorisationLivraisonAddComponent,
	],
	exports: [
		OpcComponent, 
		DeblocageComponent, 
		CcgModule, 
		AutorisationLivraisonComponent,
    	AutorisationLivraisonAddComponent,
	],
	imports: [
		CommonModule,
		NgxCurrencyModule, 
		FormsModule,
		MatTableModule,
		MatExpansionModule,
		MatRadioModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatButtonModule,
		CcgModule,
		DialogOpcActionsModule,
		MatIconModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatMenuModule,
	],
})
export class RealisationModule {}
