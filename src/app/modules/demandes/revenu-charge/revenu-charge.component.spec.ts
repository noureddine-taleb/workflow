import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RevenuChargeComponent } from "./revenu-charge.component";

describe("RevenuChargeComponent", () => {
	let component: RevenuChargeComponent;
	let fixture: ComponentFixture<RevenuChargeComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [RevenuChargeComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(RevenuChargeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
