import { Component, OnInit } from "@angular/core";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestPerson } from "app/shared/models/RequestPerson";
import { Subscription } from "rxjs";

@Component({
	selector: "app-revenu-charge",
	templateUrl: "./revenu-charge.component.html",
	styleUrls: ["./revenu-charge.component.css"],
})
export class RevenuChargeComponent implements OnInit {
	request: LoanRequest;
	persons: RequestPerson[];
	hasModifyPrivilege: boolean = true;

	constructor(
		private snackBarService: SnackBarService,
		private demandeService: DemandeService,
		private authService: AuthService,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.persons = this.demandeService.persons;
		this.request = this.demandeService.request;
		this.hasModifyPrivilege = this.utilsService.hasPrivilege(
			this.utilsService.privilegeCodes.maj_revenu_charge_m,
		);
	}
	subscription: Subscription = new Subscription();
	onSave(item): void {
		this.snackBarService
			.openConfirmSnackBar({ message: "Confirmer vôtre choix" })
			.then((result) => {
				if (result.value) {
					item["userId"] = this.authService.user.userId;
					item["userEntityId"] = this.authService.user.userEntityId;
					item["requestId"] = this.request.requestId;

					item["monthlyPaymentImmoCam"] = item["monthlyPaymentImmoCam"]
						? item["monthlyPaymentImmoCam"]
						: 0;
					item["monthlyPaymentImmoConf"] = item["monthlyPaymentImmoConf"]
						? item["monthlyPaymentImmoConf"]
						: 0;

					item["monthlyPaymentConsoCam"] = item["monthlyPaymentConsoCam"]
						? item["monthlyPaymentConsoCam"]
						: 0;
					item["monthlyPaymentConsoConf"] = item["monthlyPaymentConsoConf"]
						? item["monthlyPaymentConsoConf"]
						: 0;

					item["monthlyPaymentImmo"] =
						item["monthlyPaymentImmoCam"] + item["monthlyPaymentImmoConf"];
					item["monthlyPaymentConso"] =
						item["monthlyPaymentConsoCam"] + item["monthlyPaymentConsoConf"];

					item["requestId"] = this.request.requestId;
					let sub = this.demandeService.updateTier(item).subscribe((data) => {
						this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					});
					this.subscription.add(sub);
				}
			});
	}
}
