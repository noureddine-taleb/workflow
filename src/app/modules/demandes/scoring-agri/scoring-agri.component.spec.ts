import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoringAgriComponent } from './scoring-agri.component';

describe('ScoringAgriComponent', () => {
  let component: ScoringAgriComponent;
  let fixture: ComponentFixture<ScoringAgriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScoringAgriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoringAgriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
