import { Component, OnInit } from '@angular/core';
import { ScoringService } from "app/modules/demandes/services/scoring.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestPerson } from 'app/shared/models/RequestPerson';
import { PersonScore } from './scoring.types';

@Component({
  selector: 'app-scoring-agri',
  templateUrl: './scoring-agri.component.html',
  styleUrls: ['./scoring-agri.component.scss']
})
export class ScoringAgriComponent implements OnInit {
	scores: PersonScore[] = [];
	request: LoanRequest;
	persons: RequestPerson[];

	constructor(
	private demandeService: DemandeService,
		private scoringService: ScoringService
	) { }

	ngOnInit(): void {
		this.request = this.demandeService.request;
			this.persons = this.demandeService.persons;
		this.loadScoring();
	}

  
	loadScoring() {
		this.scoringService.getAgriScoring(this.persons[0].personId, this.request.requestId).subscribe(data => {
			this.scores = data.result;
		})
	}
}
