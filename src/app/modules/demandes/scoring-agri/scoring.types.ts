export class PersonScore {
	name: string;
	possibleCredit: string;
	creditDuration: string;
	scoreDate: string;
	insuranceAmount: string;
	fileCost: string;
	askedAmount: string;
	creationAgency: string;
	status: string;
	classe: string;
}
