import { Component, OnInit } from "@angular/core";
import { RoutingService } from "app/core/services/routing.service";
import { ScoringService } from "app/modules/demandes/services/scoring.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestPerson } from "app/shared/models/RequestPerson";

@Component({
	selector: "app-scoring",
	templateUrl: "./scoring.component.html",
	styleUrls: ["./scoring.component.scss"],
})
export class ScoringComponent implements OnInit {
	request: LoanRequest;
	persons: RequestPerson[];
	avis: string;
	critere = {};

	constructor(
		public routingService: RoutingService,
		public utilsService: UtilsService,
		private demandeService: DemandeService,
		private snackBarService: SnackBarService,
		private authService: AuthService,
		private scoringService: ScoringService
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.persons = this.demandeService.persons;
	}


	idScoring() {
		if (this.persons?.[0]["scoringId"])
			return true;
		return false;
	}

	onAvis() {
		this.critere["opinion"] = this.avis;
		this.critere["requestId"] = this.request.requestId;
		this.critere["userEntityId"] = this.authService.user.entityId;
		this.critere["userId"] = this.authService.user.identifiant;

		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeService.scoringAvis(this.critere).subscribe((data) => {
					this.critere["adviceId"] = data["result"];
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
			}
		});
	}

	onScoring() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.scoringService.getScoring(this.request.requestId).subscribe(_ => {
					this.demandeService.getTiers(this.request.requestId).subscribe((tier) => {
						this.persons = tier.result.results;
					});
				});
			}
		});
	}
}
