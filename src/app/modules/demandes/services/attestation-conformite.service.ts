import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { AuthService } from 'app/modules/auth/auth.service';
import {Observable} from "rxjs";

@Injectable({ providedIn: "root" })
export class AttestationConformiteService {
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient, private authService: AuthService) {}

	getDossierCredit(requestId: number) {
		return this.httpClient.post(this.wkfPath + "/creditReport/list", {
			...this.authService.user,
			requestId,
		});
	}

	attestationConformiteCreate(attestationConformite, requestId: number) {
		attestationConformite.entityId = this.authService.user.entityId;
		attestationConformite.userId = this.authService.user.userId;
		attestationConformite.userEntityId = this.authService.user.userEntityId;
		attestationConformite.requestId = requestId;

		return this.httpClient.post(
			this.wkfPath + "/complianceCertificate/create",
			attestationConformite,
		);
	}

    attestationConformiteGetDefaultValue(attestationConformite,requestID): Observable<any> {
        attestationConformite.entityId = this.authService.user.entityId;
        attestationConformite.userId = this.authService.user.userId;
        attestationConformite.userEntityId = this.authService.user.userEntityId;
        attestationConformite.requestId = requestID;

        return this.httpClient.post(
            this.wkfPath + "/complianceCertificate/defaulValue",
            attestationConformite,
        );
    }

	attestationConformiteList(requestId: number) {
		return this.httpClient.post(this.wkfPath + "/complianceCertificate/getCertificates", {
			...this.authService.user,
			requestId,
		});
	}

	attestationConformiteDetacherDossier(element: any) {
		return this.httpClient.post(this.wkfPath + "/complianceCertificate/detach", element);
	}

	onValidate(element) {
		return this.httpClient.post(this.wkfPath + "/complianceCertificate/validate", element);
	}

	onSigning(element) {
		return this.httpClient.post(this.wkfPath + "/complianceCertificate/signing", element);
	}

	onDelete(element) {
		return this.httpClient.post(this.wkfPath + "/complianceCertificate/delete", element);
	}

	////////autorisation de deblocage

	autorisationDeblockage(criter) {
		return this.httpClient.post(this.wkfPath + "/unblokingCertificate/getCertificates", criter);
	}

	autorisationDeblockageAdd(criter) {
		return this.httpClient.post(this.wkfPath + "/unblokingCertificate/create", {
			...criter,
			...this.authService.user,
		});
	}

	autorisationDeblockageDelete(criter) {
		return this.httpClient.post(this.wkfPath + "/unblokingCertificate/delete", criter);
	}
}
