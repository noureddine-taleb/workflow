import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { FileSaverService } from "ngx-filesaver";
import { ConfigService } from 'app/core/config/config.service';
import { Cnt } from "../cnt-tableau/cnt.types";

@Injectable({ providedIn: "root" })
export class CntService {
	private wkfUrl = ConfigService.settings.apiServer.restWKFUrl;
	constructor(
		private httpClient: HttpClient,
		private authService: AuthService,
		private _FileSaverService: FileSaverService,
	) {}

	getReservations(requestId) {
		let critere = {};
		critere["requestId"] = requestId;
		critere["userId"] = this.authService.user.userId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["entityId"] = this.authService.user.entityId;
		return this.httpClient.post(this.wkfUrl + "/cnt/getReservations", critere);
	}

	crateRequest(cntDto: Cnt) {
		cntDto.userEntityId = this.authService.user.userEntityId;
		cntDto.userId = this.authService.user.userId;
		cntDto.entityId = this.authService.user.entityId;
		//this.jsonToFile(cntDto);
		return this.httpClient.post(this.wkfUrl + "/cnt/create", cntDto);
	}

	getHistory(critere) {
		critere.userEntityId = this.authService.user.userEntityId;
		critere.userId = this.authService.user.userId;
		critere.entityId = this.authService.user.entityId;
		return this.httpClient.post(this.wkfUrl + "/cnt/getHistory", critere);
	}

	jsonToFile(jsonObject) {
		const fileType = this._FileSaverService.genType("savedFile.json");
		const txtBlob = new Blob([JSON.stringify(jsonObject)], { type: "json" });
		this._FileSaverService.save(txtBlob, "savedFile.json");
	}

	executeAction(critere) {
		critere.userEntityId = this.authService.user.userEntityId;
		critere.userId = this.authService.user.userId;
		critere.entityId = this.authService.user.entityId;
		return this.httpClient.post(this.wkfUrl + "/cnt/execute", critere);
	}
}
export class CntAction {
	clientType: number;
	creditType: number;
	statusCode: string;
	userEntityId?: number;
	userId?: number;
}
