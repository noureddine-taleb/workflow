import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from 'app/modules/auth/auth.service';
import { forkJoin } from "rxjs";
import { tap } from "rxjs/operators";
import { DemandeService } from "app/modules/demandes/services/demande.service";

@Injectable({ providedIn: "root" })
export class DemandeResolverService {
	constructor(
		private requestService: DemandeService,
		private authService: AuthService,
	) {}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		let params = {
			requestId: route.params["requestId"],
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.identifiant,
			entityId: this.authService.user.entityId,
		};

		return forkJoin({
			demande: this.requestService._getDemande(params).pipe(
				tap((result) => {
					this.requestService.setRequest(result.result);
				}),
			),
			tiers: this.requestService._getTiers(params).pipe(
				tap((result) => {
					this.requestService.setPersons(result.result.results);
				}),
			),
		});
	}
}
