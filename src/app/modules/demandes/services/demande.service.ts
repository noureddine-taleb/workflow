import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeConso } from "app/modules/conso/models/DemandeConso";
import { Guaranty } from "app/modules/demandes/constitution/constitution.types";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { FinancialObject } from "app/modules/demandes/objet-financement/financial-object.types";
import { Patrimony } from "app/modules/demandes/patrimony/patrimony.types";
import { InvestCalculate } from "app/modules/investitssement/investissement.types";
import { OtherRequest } from "app/modules/other-demandes/other-demandes.types";
import { DemandeQuatro } from "app/modules/quatro/dto/demande-quatro";
import { Simulation } from "app/modules/simulation/simulation.types";
import { User } from "app/shared/models/User";
import { FileSaverService } from "ngx-filesaver";
import { forkJoin, Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { RoutingService } from "../../../core/services/routing.service";
import { Action } from "../../../shared/models/Action";
import { RequestPerson } from "../../../shared/models/RequestPerson";
import { RequestType } from "../../../shared/models/RequestType";
import { RequestComment } from "../comments/comment.types";
import { DemandeMazaya } from "app/modules/mazaya/dto/demande-mazaya";
import {DemandeIstidama} from "../../istidama/dto/demande-istidama";
import {DemandeTasbiqFda} from "../../tasbikFda/dto/demande-tasbiqFda";

@Injectable({ providedIn: "root" })
export class DemandeService {
	request: LoanRequest;
	persons: RequestPerson[];
	requestActions: {
		next: Action[],
		prev: Action[]
	};
	requestComments: RequestComment[]

	constructor(
		private authService: AuthService,
		public dialog: MatDialog,
		private httpClient: HttpClient,
		private _FileSaverService: FileSaverService,
		private routingService: RoutingService
	) {}

	private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

	saveDemande(demandeDto: Simulation): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/creditRequest/create", demandeDto);
	}

	createDemandeAgri(projet: any) {
		projet["entityId"] = this.authService.user.entityId;
		projet["userId"] = this.authService.user.userId;
		projet["userEntityId"] = this.authService.user.userEntityId;
		//this.toJsonServ.jsonToFile(projet)
		return this.httpClient.post(
			this.demandesPath + "/creditRequest/createCreditAgri",
			projet,
		);
	}

	createDemandeHorsAgri(projet: any) {
		projet["entityId"] = this.authService.user.entityId;
		projet["userId"] = this.authService.user.userId;
		projet["userEntityId"] = this.authService.user.userEntityId;
		return this.httpClient.post(
			this.demandesPath + "/creditRequest/createCreditHorsAgri",
			projet,
		);
	}

	createDemandeConso(conso: DemandeConso) {
		conso.userId = this.authService.user.userId;
		conso.entityId = this.authService.user.entityId;
		conso.userEntityId = this.authService.user.userEntityId;
		return this.httpClient.post(
			`${this.demandesPath}/creditRequest/createConsumerCredit`,
			conso,
		);
	}

    createDemandeQuatro(quatro: DemandeQuatro) {
        quatro.userId = this.authService.user.userId;
        quatro.entityId = this.authService.user.entityId;
        quatro.userEntityId = this.authService.user.userEntityId;
        return this.httpClient.post(
            `${this.demandesPath}/creditRequest/createQuatroCredit`,
            quatro,
        );
    }
	createDemandeMazaya(mazaya: DemandeMazaya) {
        mazaya.userId = this.authService.user.userId;
        mazaya.entityId = this.authService.user.entityId;
        mazaya.userEntityId = this.authService.user.userEntityId;
        return this.httpClient.post(
            `${this.demandesPath}/creditRequest/createMazayaCredit`,
            mazaya,
        );
    }

    createDemandeIstidama(istidama: DemandeIstidama) {
        istidama.userId = this.authService.user.userId;
        istidama.entityId = this.authService.user.entityId;
        istidama.userEntityId = this.authService.user.userEntityId;
        return this.httpClient.post(
            `${this.demandesPath}/creditRequest/createIstidamaCredit`,
            istidama,
        );
    }

    createDemandeTasbiqFda(istidama: DemandeTasbiqFda) {
        istidama.userId = this.authService.user.userId;
        istidama.entityId = this.authService.user.entityId;
        istidama.userEntityId = this.authService.user.userEntityId;
        return this.httpClient.post(
            `${this.demandesPath}/creditRequestTasbiqFda/createTasbiqFdaCredit`,
            istidama,
        );
    }

	createDemandeOther(other: any) {
		other.userId = this.authService.user.userId;
		other.entityId = this.authService.user.entityId;
		other.userEntityId = this.authService.user.userEntityId;
		return this.httpClient.post<ApiResponse<any>>(
			`${this.demandesPath}/creditRequest/createCreditOthers`,
			other,
		);
	}

	getOldProducts(accountId: number) {
		return this.httpClient.post(`${this.demandesPath}/creditEntreprise/getLineRenewal`, {
			userId: this.authService.user.userId,
			userEntityId: this.authService.user.userEntityId,
			entityId: this.authService.user.entityId,
			accountId,
		});
	}

	saveDemandeHabitat(demandeDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/creditRequest/shape", demandeDto);
	}

	onDemandEdit(demandeDto: {requestId: number, creditTypeId: number}) {
		this.routingService.gotoRequest(demandeDto);
	}

	calculateProduct(critere: InvestCalculate) {
		return this.httpClient.post(this.demandesPath + "/creditRequest/calculate", critere);
	}
	getSejour(critere) {
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		return this.httpClient.post(this.demandesPath + "/creditRequest/getStayRequest", critere);
	}

	setDemandeFromSimulation(simulationDto: Simulation): Simulation {
		// tslint:disable-next-line:prefer-const
		let demandeAddDto: Simulation = new Simulation();

		demandeAddDto.simulationId = simulationDto?.simulationId;
		demandeAddDto.activityStatus = simulationDto.activityStatus;
		demandeAddDto.convention = simulationDto.convention;
		demandeAddDto.goodLocation = simulationDto.goodLocation;
		demandeAddDto.monthlyPay = simulationDto.monthlyPay;
		demandeAddDto.monthlyPaymentConso = simulationDto.monthlyPaymentConso
			? simulationDto.monthlyPaymentConso
			: 0;
		demandeAddDto.monthlyPaymentConsoCam = simulationDto.monthlyPaymentConsoCam
			? simulationDto.monthlyPaymentConsoCam
			: 0;
		demandeAddDto.monthlyPaymentConsoConf = simulationDto.monthlyPaymentConsoConf
			? simulationDto.monthlyPaymentConsoConf
			: 0;
		demandeAddDto.monthlyPaymentImmo = simulationDto.monthlyPaymentImmo
			? simulationDto.monthlyPaymentImmo
			: 0;
		demandeAddDto.monthlyPaymentImmoCam = simulationDto.monthlyPaymentImmoCam
			? simulationDto.monthlyPaymentImmoCam
			: 0;
		demandeAddDto.monthlyPaymentImmoConf = simulationDto.monthlyPaymentImmoConf
			? simulationDto.monthlyPaymentImmoConf
			: 0;
		demandeAddDto.natureFinancial = simulationDto.natureFinancial;
		demandeAddDto.objectCredit = simulationDto.objectCredit;
		demandeAddDto.organism = simulationDto.organism;
		demandeAddDto.othersMonthlyIncome = simulationDto.othersMonthlyIncome
			? simulationDto.othersMonthlyIncome
			: 0;
		demandeAddDto.personId = simulationDto.personId;
		demandeAddDto.segment = simulationDto.segment;
		demandeAddDto.seniority = simulationDto.seniority;
		demandeAddDto.simulationId = simulationDto.simulationId;

		demandeAddDto.userEntityId = this.authService.user?.entityId;
		demandeAddDto.userId = this.authService.user?.identifiant;

		demandeAddDto.creditType = simulationDto.creditType;
		demandeAddDto.entityId = this.authService.user.entityId;
		return demandeAddDto;
	}

	setUpdateObjectFromDemandeDTO(param: LoanRequest) {
		const constructObject = new Simulation();

		constructObject.activityStatus = param.activityStatusCode;
		constructObject.convention = param.conventionId;
		constructObject.creditType = param.creditTypeId;

		constructObject.goodLocation = param.goodLocationCode;
		constructObject.monthlyPay = param.monthlyPay;
		constructObject.monthlyPaymentConso = param.monthlyPaymentConso;
		constructObject.monthlyPaymentConsoCam = param.monthlyPaymentConsoCam;
		constructObject.monthlyPaymentConsoConf = param.monthlyPaymentConsoConf;
		constructObject.monthlyPaymentImmo = param.monthlyPaymentImmo;
		constructObject.monthlyPaymentImmoCam = param.monthlyPaymentImmoCam;
		constructObject.monthlyPaymentImmoConf = param.monthlyPaymentImmoConf;
		constructObject.natureFinancial = param.financialNatureCode;
		constructObject.objectCredit = param.requestObject?.toUpperCase();
		constructObject.organism = param.organismId;
		constructObject.othersMonthlyIncome = param.othersMonthlyIncome;
		constructObject.personId = param.personId;
		constructObject.requestId = param.requestId;
		constructObject.segment = param.segmentId;
		constructObject.seniority = param.seniority;
		constructObject.simulationId = param.simulationId;
		constructObject.details = param.details;
		return constructObject;
	}

	setRequest(request: LoanRequest) {
		this.request = request;
	}

	_getDemande(param: {
		requestId: number;
		userEntityId: number;
		entityId: number;
		userId: number;
	}) {
		return this.httpClient.post<ApiResponse<LoanRequest>>(
			this.demandesPath + "/creditRequest/getRequest",
			param,
		);
	}

	getDemande(requestId: number) {
		let payload = {
			requestId,
			userEntityId: this.authService.user.userEntityId,
			entityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
		}

		return this._getDemande(payload);
	}

	reloadDemande() {
		return this.getDemande(this.request.requestId).pipe(
			tap((result) => {
				this.setRequest(result['result']);
			}),
		);
	}


	updateDemande(param: LoanRequest): Observable<any> {
		let constructObject = new Simulation();
		constructObject = this.setUpdateObjectFromDemandeDTO(param);
		constructObject.userId = this.authService.user.identifiant;
		constructObject.userEntityId = this.authService.user.entityId;
		constructObject.entityId = this.authService.user.entityId;
		return this.httpClient.post(
			this.demandesPath + "/creditRequest/updateRequest",
			constructObject,
		);
		// tslint:disable-next-line:align
	}

	setPersons(persons: RequestPerson[]) {
		this.persons = persons;
	}

	_getTiers(dmdParam: {
		requestId: any;
		userEntityId: number;
		entityId: number | undefined;
		userId: number;
	}) {
		dmdParam["first"] = 0;
		dmdParam["pas"] = 100;

		return this.httpClient.post<ApiResponse<{ rowsNumber: number; results: RequestPerson[] }>>(
			this.demandesPath + "/creditRequest/listPersons",
			dmdParam,
		);
	}

	getTiers(requestId: number) {
		let payload = {
			requestId,
			userEntityId: this.authService.user.userEntityId,
			entityId: this.authService.user.entityId,
			userId: this.authService.user.userId,
		}

		return this._getTiers(payload);
	}

	updateTier(item) {
		return this.httpClient.post(this.demandesPath + "/creditRequest/updatePerson", item);
	}

	getEndettementCam(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/endettement", param);
	}
	refreshEndettementCam(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/refreshEndettement", param);
	}

	getAvis(param) {
		let avisCreaterya = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.entityId,
			userId: this.authService.user.identifiant,
			...param,
		};
		return this.httpClient.post(this.demandesPath + "/advice/getAdvices", avisCreaterya);
	}

	saveAvis(avis) {
		avis.userEntityId = this.authService.user.entityId;
		avis.userId = this.authService.user.identifiant;
		return this.httpClient.post(this.demandesPath + "/advice/create", avis);
	}

	scoringAvis(avis) {
		avis.userEntityId = this.authService.user.entityId;
		avis.userId = this.authService.user.identifiant;
		return this.httpClient.post(this.demandesPath + "/scoring/advice", avis);
	}

	getSituationComptCam(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/accounts", param);
	}

	refreshSituationComptCam(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/refreshAccounts", param);
	}

	getDomiciliation(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/domiciliation", param);
	}
	refreshDomiciliation(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/refreshDomiciliation", param);
	}

	getIncidentPaiement(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/incidents", param);
	}
	refreshIncidentPaiement(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/situations/refreshIncidents", param);
	}

	getFinancialObject(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(
			this.demandesPath + "/financialObject/getFinancialObjcet",
			param,
		);
	}

	addFinancialObject(param: FinancialObject): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/financialObject/create", param);
	}

	deleteFinacialObject(param): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;

		return this.httpClient.post(this.demandesPath + "/financialObject/delete", param);
	}

	// guarantees/getGaranties

	getGaranties(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/guarantees/getGaranties", param);
	}

	addGarantie(param: Guaranty): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/guarantees/create", param);
	}
	deleteGarantie(param): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;

		return this.httpClient.post(this.demandesPath + "/guarantees/delete", param);
	}

	// Patrimoine

	getPatrimoine(param: any): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/patrimony/getPatrimony", param);
	}

	addPatrimoine(param: Patrimony): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;
		return this.httpClient.post(this.demandesPath + "/patrimony/create", param);
	}
	deletePatrimoine(param): Observable<any> {
		param.entityId = this.authService.user.entityId;
		param.userId = this.authService.user.identifiant;
		param.userEntityId = this.authService.user.entityId;

		return this.httpClient.post(this.demandesPath + "/patrimony/delete", param);
	}

	createCreditReport(dossier) {
		dossier.entityId = this.authService.user.entityId;
		dossier.userEntityId = this.authService.user.userEntityId;
		dossier.userId = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/creditReport/create", dossier);
	}

	listReports(requestId: any) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
			requestId: requestId,
		};
		return this.httpClient.post(this.demandesPath + "/creditReport/list", critere);
	}

	listOpc(requestId) {
		let critere = {
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
			requestId: requestId,
		};
		return this.httpClient.post(this.demandesPath + "/contractRequest/getContracts", critere);
	}

	saveOpc(opc: {}) {
		opc["entityId"] = this.authService.user.entityId;
		opc["userEntityId"] = this.authService.user.userEntityId;
		opc["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/contractRequest/create", opc);
	}

	remettreClient(opc: {}) {
		opc["entityId"] = this.authService.user.entityId;
		opc["userEntityId"] = this.authService.user.userEntityId;
		opc["userId"] = this.authService.user.userId;
		//this.jsonToFile(opc)
		return this.httpClient.post(this.demandesPath + "/contractRequest/handOver", opc);
	}

	deleteOpc(critere) {
		//let critere={};

		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		// critere['contractId']=opc['contractId']
		// critere['identifiant']=opc['contractId']

		return this.httpClient.post(this.demandesPath + "/contractRequest/delete", critere);
	}
	saveOpcJson(opc: {}) {
		opc["entityId"] = this.authService.user.entityId;
		opc["userEntityId"] = this.authService.user.userEntityId;
		opc["userId"] = this.authService.user.userId;

		this.jsonToFile(opc);
		//return this.httpClient.post(this.demandesPath +'/contractRequest/create',opc);
	}
	remettreCleint(opc) {
		let critere = {};
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/contractRequest/contractTypes", critere);
	}
	getTypeContract(critere) {
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/contractRequest/contractTypes", critere);
	}

	detacherReport(_critere) {
		let critere = {};
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;

		critere["idContract"] = _critere["contractId"];
		critere["idReport"] = _critere["reportId"];

		critere["contractId"] = _critere["contractId"];
		critere["reportId"] = _critere["reportId"];

		return this.httpClient.post(this.demandesPath + "/contractRequest/detach", critere);
	}

	jsonToFile(jsonObject) {
		const fileType = this._FileSaverService.genType("savedFile.json");
		const txtBlob = new Blob([JSON.stringify(jsonObject)], { type: "json" });
		this._FileSaverService.save(txtBlob, "savedFile.json");
	}

	getReportById(critere) {
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		return this.httpClient.post(this.demandesPath + "/creditReport/creditReport", critere);
	}

	guarantyAction(guaranty: any, action: any) {
		return this._dispatchAction(guaranty, action, action.users[0]);
	}

	_dispatchAction(obj: any, action: { urlAction: string, actionCode: string }, user: {userId: number}) {
		obj["entityId"] = this.authService.user.entityId;
		obj["userEntityId"] = this.authService.user.userEntityId;
		obj["userId"] = this.authService.user.userId;
		obj["userActionId"] = user.userId;
		obj["action"] = action.actionCode;

		return this.httpClient.post(`${this.demandesPath}/${action.urlAction}`, {...obj, actions: undefined, privs: undefined});
	}


	bordereauAction(bordereau: any, action: any) {
		return this._dispatchAction(bordereau, action, action.users[0]);
	}

	saveGuarantees(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;
		//this.jsonToFile(guarantyDto)

		return this.httpClient.post(this.demandesPath + "/guaranty/create", guarantyDto);
		//return of({status:200,result:'',message:''})
	}

	deleteGuarante(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/delete", guarantyDto);
	}

	validerGaranty(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/validate", guarantyDto);
	}
	verifGaranty(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/verify", guarantyDto);
	}
	getConstitutionGarantie(requestId) {
		let critere = {};
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		critere["requestId"] = requestId;
		return this.httpClient.post(this.demandesPath + "/guaranty/getGaranties", critere);
	}
	garantyAttacheReport(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/attach", guarantyDto);
	}
	garantyDetacheReport(guarantyDto) {
		guarantyDto["entityId"] = this.authService.user.entityId;
		guarantyDto["userEntityId"] = this.authService.user.userEntityId;
		guarantyDto["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/attach", guarantyDto);
	}

	createBo(bordereau): Observable<any> {
		bordereau["entityId"] = this.authService.user.entityId;
		bordereau["userEntityId"] = this.authService.user.userEntityId;
		bordereau["userId"] = this.authService.user.userId;

		return this.httpClient.post(this.demandesPath + "/guaranty/createBo", bordereau);
	}
	listBordereau(requestId) {
		let critere = {};
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		critere["requestId"] = requestId;
		return this.httpClient.post(this.demandesPath + "/guaranty/getBos", critere);
	}
	deleteBordereau(bo) {
		///guaranty/deleteBo
		bo["entityId"] = this.authService.user.entityId;
		bo["userEntityId"] = this.authService.user.userEntityId;
		bo["userId"] = this.authService.user.userId;
		return this.httpClient.post(this.demandesPath + "/guaranty/deleteBo", bo);
	}

	signerBordereau(bo: any) {
		bo["entityId"] = this.authService.user.entityId;
		bo["userEntityId"] = this.authService.user.userEntityId;
		bo["userId"] = this.authService.user.userId;
		return this.httpClient.post(this.demandesPath + "/guaranty/signingBo", bo);
	}
	getConstitutionGaranties(safetyId: string, requestId: number) {
		const safety: any = {};
		safety.entityId = this.authService.user.entityId;
		safety.userEntityId = this.authService.user.userEntityId;
		safety.userId = this.authService.user.userId;
		safety.requestId = requestId;
		safety.safetyId = safetyId;
		return this.httpClient.post(this.demandesPath + "/guaranty/getSafety", safety);
	}

	fetchDemandeActions() {
		let payload = {};
		payload["statusCode"] = this.request.statusCode;
		payload["clientType"] = this.request.clientCategoryId;
		payload["creditType"] = this.request.creditTypeId;
		payload["requestId"] = this.request.requestId;
		payload["creationEntityId"] = this.request.creationEntityId;
		payload["lineId"] = this.request.requestId;
		payload["module"] = "CRED";
		payload["userEntityId"] = this.authService.user.userEntityId;
		payload["userId"] = this.authService.user.userId;
		payload["entityId"] = this.authService.user.entityId;

		return forkJoin({
			next: this.httpClient.post<ApiResponse<Action[]>>(this.demandesPath + "/action/get", {...payload, sens: 'S'}).pipe(
				map(data => data['result'])
			),
			prev: this.httpClient.post<ApiResponse<Action[]>>(this.demandesPath + "/action/get", {...payload, sens: 'R'}).pipe(
				map(data => data['result'])
			),
		})
		.pipe(
			tap((data) => {
				this.requestActions = data;
			})
		)
	}

	executeDemandeAction(user: User, action: Action, note: string, override: any = {}) {
		let obs: Observable<any>;
		let payload = {};

		payload["statusCode"] = this.request.statusCode;
		payload["clientType"] = this.request.clientCategoryId;
		payload["creditType"] = this.request.creditTypeId;
		payload["requestId"] = this.request.requestId;
		payload["creationEntityId"] = this.request.creationEntityId;

		payload["action"] = action.actionCode;
		payload["workflowId"] = action.workflowId;
		payload["actionUserId"] = user?.userId;
		payload["note"] = note;
		payload["urlAction"] = action.urlAction;

		payload["userEntityId"] = this.authService.user.userEntityId;
		payload["userId"] = this.authService.user.userId;
		payload["entityId"] = this.authService.user.entityId;

		payload = {
			...payload,
			...override,
		}

		let url = payload["urlAction"] ? `/${payload["urlAction"]}` : "/creditRequest/execute";

		obs = this.httpClient.post(this.demandesPath + url, payload);

		// reload the new actions automatically after executing the old one
		return obs.pipe(
			tap(_ => {
				this.fetchDemandeActions().subscribe()
				this.getRequestComments().subscribe()
			})
		)
	}

	// {
	// 	"ccgId": 157 (workflowId),
	// 	"statusCode": "CONV_AG_IN",
	// 	"action": "VA",
	// 	"userActionId": user destination (getaction),

	// 	"creationEntityId": null,
	// 	"requestId": null,
	// 	"statusColor": null,
	// 	"statusName": null,

	// 	"entityId": entite user connected,
	// 	"userEntityId": entite user connected,
	// 	"userId": user connected
	// }
	executeCcgProjectAction(user: User, action: Action, note: string, project: { idWorkflow: number, statusCode: string }, override: any = {}) {
		let payload = {};

		payload["statusCode"] = project.statusCode;
		payload["requestId"] = this.request.requestId;
		payload["ccgId"] = project.idWorkflow;

		payload["action"] = action.actionCode;
		payload["userActionId"] = user?.userId;
		payload["actionUserId"] = user?.userId;
		payload["note"] = note;
		payload["urlAction"] = action.urlAction;

		payload["userEntityId"] = this.authService.user.userEntityId;
		payload["userId"] = this.authService.user.userId;
		payload["entityId"] = this.authService.user.entityId;

		payload = {
			...payload,
			...override,
		}

		let url = payload["urlAction"] ? `/${payload["urlAction"]}` : "/creditRequest/execute";

		return this.httpClient.post(this.demandesPath + url, payload);
	}

	getRequestComments() {
		let payload = {
			userId: this.authService.user.userId,
			entityId: this.authService.user.entityId,
			userEntityId: this.authService.user.userEntityId,
			requestId: this.request.requestId
		}
		return this.httpClient.post<ApiResponse<RequestComment[]>>(`${this.demandesPath}/creditRequest/getRemarkRequest`, payload).pipe(
			tap(data => {
				this.requestComments = data.result;
			})
		);
	}

	getOpcTitle() {
		return this.request.creditTypeId == RequestType.HABITAT ? "OPC" : "Contrat";
	}

	__isDemandeOther(request: LoanRequest) {
		return request.creditTypeId >= RequestType.MAINLEVEE;
	}

	isDemandeOther() {
		return this.__isDemandeOther(this.request);
	}
}
