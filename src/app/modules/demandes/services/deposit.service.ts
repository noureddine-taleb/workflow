import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { ConfigService } from 'app/core/config/config.service';

@Injectable({
	providedIn: "root",
})
export class DepositService {
	private api = ConfigService.settings.apiServer.restWKFUrl + "/deposit";

	constructor(private httpClient: HttpClient, private authService: AuthService) {}

	getDeposits(requestId: number) {
		const req = {
			entityId: this.authService.user.entityId,
			requestId: requestId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
		};

		return this.httpClient.post(`${this.api}/getDeposits`, req);
	}

	createDeposit(deposit: any, requestId: number) {
		const req = {
			...deposit,
			entityId: this.authService.user.entityId,
			requestId: requestId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
		};

		return this.httpClient.post(`${this.api}/create`, req);
	}

	deleteDeposit(deposit: any, requestId: number) {
		const req = {
			...deposit,
			entityId: this.authService.user.entityId,
			requestId: requestId,
			userEntityId: this.authService.user.userEntityId,
			userId: this.authService.user.userId,
		};

		return this.httpClient.post(`${this.api}/delete`, req);
	}
}
