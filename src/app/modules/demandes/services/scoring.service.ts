import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from 'app/modules/auth/auth.service';
import { PersonScore } from '../scoring-agri/scoring.types';
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';

@Injectable({
  providedIn: 'root'
})
export class ScoringService {

  private wkfPath = ConfigService.settings.apiServer.restWKFUrl ;
  constructor(private httpClient:HttpClient, private authService:AuthService)
  {}

  
	getScoring(requestId) {
		let payload = {};
		payload["requestId"] = requestId;
		payload["userEntityId"] = this.authService.user.userEntityId;
		payload["userId"] = this.authService.user.userId;
		payload["entityId"] = this.authService.user.entityId;
		return this.httpClient.post(`${this.wkfPath}/scoring/score`, payload);
	}

  getAgriScoring(personId: any, requestId: number) {
    return this.httpClient.post<ApiResponse<PersonScore[]>>(`${this.wkfPath}/scoring/agriculturalScoring`, {
        requestId, 
        tiers: personId, 
        userId: this.authService.user.userId, 
        entityId: this.authService.user.entityId, 
        userEntityId: this.authService.user.userEntityId
    });
  }
}
