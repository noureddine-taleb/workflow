import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { ConfigService } from 'app/core/config/config.service';

@Injectable({
	providedIn: "root",
})
export class SectionService {
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient, private tokenService: AuthService) {}

	create(section: any, requestId: number) {
		section.userEntityId = this.tokenService.user.userEntityId;
		section.userId = this.tokenService.user.userId;
		section.entityId = this.tokenService.user.entityId;
		section.requestId = requestId;
		return this.httpClient.post(this.wkfPath + "/section/create", section);
	}

	delete(section: any, requestId: number) {
		section.userEntityId = this.tokenService.user.userEntityId;
		section.userId = this.tokenService.user.userId;
		section.entityId = this.tokenService.user.entityId;
		section.requestId = requestId;
		return this.httpClient.post(this.wkfPath + "/section/delete", section);
	}

	getAll(requestId: number) {
		const req = {
			entityId: this.tokenService.user.entityId,
			userEntityId: this.tokenService.user.entityId,
			userId: this.tokenService.user.userId,
			requestId: requestId,
		};

		return this.httpClient.post(this.wkfPath + "/section/getsection", req);
	}
}
