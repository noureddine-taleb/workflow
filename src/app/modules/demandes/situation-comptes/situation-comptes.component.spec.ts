import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SituationComptesComponent } from "./situation-comptes.component";

describe("SituationComptesComponent", () => {
	let component: SituationComptesComponent;
	let fixture: ComponentFixture<SituationComptesComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [SituationComptesComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SituationComptesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
