import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { UtilsService } from "app/core/services/utils.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { RoutingService } from "app/core/services/routing.service";

@Component({
	selector: "app-situation-comptes",
	templateUrl: "./situation-comptes.component.html",
	styleUrls: ["./situation-comptes.component.scss"],
})
export class SituationComptesComponent implements OnInit {
	personId;
	requestId;
	displayedColumns1 = [
		"agencyName",
		"productName",
		"accountNumber",
		"accountName",
		"balance",
		"action",
	];

	dataSource1;

	constructor(
		private demandeService: DemandeService,
		public routingService: RoutingService,
		public utilsService: UtilsService,
	) {}

	ngOnInit(): void {
		this.requestId = this.demandeService.request.requestId;
		this.personId = this.demandeService.request.personId;

			this.chargerDataSourche();
	}

	onglet = 1;

	getSituationAccount(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.getSituationComptCam({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource1 = data["result"];
					if (data["result"].length > 0) {
						this.onSituationChanged.emit(data["result"][0]["accountSituation"]);
					}
				});
		}
	}

	refreshSituationAccount(): void {
		if (this && this.requestId) {
			//console.warn(this.personId , this.requestId);
			this.demandeService
				.refreshSituationComptCam({ personId: this.personId, requestId: this.requestId })
				.subscribe((data) => {
					this.dataSource1 = data["result"];
					if (data["result"].length > 0) {
						this.onSituationChanged.emit(data["result"][0]["accountSituation"]);
					}
				});
		}
	}

	chargerDataSourche(): void {
		this.getSituationAccount();
	}

	@Output() onSituationChanged: EventEmitter<any> = new EventEmitter<any>();
	rafraichir(): void {
		this.refreshSituationAccount();
	}
}
