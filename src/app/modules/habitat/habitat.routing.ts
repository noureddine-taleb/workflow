import { Route } from "@angular/router";
import { ListDemandeComponent } from "app/shared/list-demande/list-demande.component";
import { RequestType } from "app/shared/models/RequestType";

export const habitatRoutes: Route[] = [
	{
		path: "",
		component: ListDemandeComponent,
		data: { title: "Liste des demandes de crédit habitat", demandeType: RequestType.HABITAT, addAllowed: true },
	}
] 