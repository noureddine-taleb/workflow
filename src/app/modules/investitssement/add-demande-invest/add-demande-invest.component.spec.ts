import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AddDemandeInvestComponent } from "./add-demande-invest.component";

describe("AddDemandeInvestComponent", () => {
	let component: AddDemandeInvestComponent;
	let fixture: ComponentFixture<AddDemandeInvestComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AddDemandeInvestComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddDemandeInvestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
