import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { RequestType } from "app/shared/models/RequestType";
import { Subscription } from "rxjs";
import { Account } from "../../../shared/models/Account";
import { AddProductInvestComponent } from "../add-product-invest/add-product-invest.component";
import { DemandeInvest, InvestProduct } from "../investissement.types";

@Component({
	selector: "app-add-demande-invest",
	templateUrl: "./add-demande-invest.component.html",
	styleUrls: ["./add-demande-invest.component.scss"],
})
export class AddDemandeInvestComponent implements OnInit {
	projet: DemandeInvest = new DemandeInvest();
	subscriptions = new Subscription();
	creditTypeId: RequestType;

	constructor(
		private _activatedRoute: ActivatedRoute,
		private dialog: MatDialog,
		private snackeBarService: SnackBarService,
		private commonService: CommonService,
		private demandeService: DemandeService,
		private routingService: RoutingService
	) {}

	ngOnInit(): void {
		this.creditTypeId = Number(this._activatedRoute.snapshot.params.requestTypeId);
	}

	retour() {
		this.routingService.gotoRequestsList(this.creditTypeId)
	}

	listProducts: InvestProduct[] = [];
	historyListProducts: InvestProduct[] = [];

	getOldProducts() {
		this.demandeService.getOldProducts(this.compte.accountId).subscribe((data) => {
			this.historyListProducts = data["result"];
		});
	}

	onAddProduct() {
		const dialogRef = this.dialog.open(AddProductInvestComponent, { data: { creditTypeId: this.creditTypeId } });
		dialogRef.afterClosed().subscribe((data) => {
			if (data?.product) this.listProducts = this.listProducts.concat(data?.product);
		});
	}

	onDelete(element) {
		this.snackeBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.listProducts = this.listProducts.filter((p) => p != element);
			}
		});
	}

	onUpdate(product) {
		this.dialog.open(AddProductInvestComponent, { data: { product, creditTypeId: this.creditTypeId }, width: "90vw" });
	}

	onAddHistoryProduct(product) {
		const dialogRef = this.dialog.open(AddProductInvestComponent, {
			data: { product: { ...product }, creditTypeId: this.creditTypeId },
			width: "90vw",
		});
		dialogRef.afterClosed().subscribe((data) => {
			if (data?.product) {
				// used to disable further renewal
				product.__used = true;
				this.listProducts = this.listProducts.concat({...data?.product, flgRenewal: "O", renewalCredit: product.renewalCredit, __used: true});
			}
		});
	}

	compte: Account;
	getAccount(account: Account) {
		this.compte = account;
		this.getOldProducts();
		this.getCategories();
	}

	listCateories = [];
	listTypeProduct = [];
	getCategories() {
		this.commonService.getCategories(this.creditTypeId).subscribe((data) => {
			this.listCateories = data["result"];
		});
	}

	isReadyToBeSaved(): boolean {
		return !!(
			this.compte &&
			this.projet.objectCredit &&
			this.projet.newConstitution &&
			this.projet.clientCategory &&
			this.listProducts?.length
		);
	}

	onCreateInvestissementDemande() {
		if (!this.isReadyToBeSaved()) {
			this.snackeBarService.openErrorSnackBar({ message: "certaines informations manquent" });
			return;
		}
		this.projet.accountId = this.compte.accountId;
		this.projet.products = this.listProducts;
		this.projet.creditType = this.creditTypeId;

		if (this.creditTypeId == RequestType.AGRI)
			this.demandeService.createDemandeAgri(this.projet).subscribe((data) => {
				this.routingService.gotoTask(data["result"])
			});
		else if (this.creditTypeId == RequestType.HORS_AGRI)
			this.demandeService.createDemandeHorsAgri(this.projet).subscribe((data) => {
				this.routingService.gotoTask(data["result"])
			});
	}

	getFieldToDisplay(element: any) {
		if (element.fieldToDisplay == "T")
			return element.lastRate
		if (element.fieldToDisplay == "PB")
			return element.lastBasePoint
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}
}
//TODO
//complement d'information
