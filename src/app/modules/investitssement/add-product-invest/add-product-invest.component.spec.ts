import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AddProductInvestComponent } from "./add-product-invest.component";

describe("AddDemandeDialogComponent", () => {
	let component: AddProductInvestComponent;
	let fixture: ComponentFixture<AddProductInvestComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AddProductInvestComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddProductInvestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
