import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { TreeData } from "mat-tree-select-input";
import { InvestCalculate, InvestProduct } from "../investissement.types";

@Component({
	selector: "app-add-product-invest",
	templateUrl: "./add-product-invest.component.html",
	styleUrls: ["./add-product-invest.component.scss"],
})
export class AddProductInvestComponent implements OnInit {	
	product: InvestProduct = new InvestProduct();
	typeTaux = [];
	productForm: FormGroup;
	calculateSuccess = false;
	onUpdate = false;
	fieldToDisplay: string;
	filteredListObjetFinancement = [];
	agreements = []
	providers = []
	filteredAgreements = []
	filteredProviders = []

	constructor(
		public dialogRef: MatDialogRef<AddProductInvestComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private snackeBarService: SnackBarService,
		private commonService: CommonService,
		private demandeService: DemandeService,
		private fb: FormBuilder,
	) {}

	ngOnInit(): void {
		this.productForm = this.fb.group({
			productType: ["", []],

			lastProduct: ["", []],
			lastPrelevementType: ["B", []],

			provider: ["", []],
			agreement: ["", []],

			financielObject: ["", []],

			lastAmountGranted: ["", []],
			lastContributionAmount: ["", []],

			lastDuration: ["", []],
			lastRateType: ["", []],
			lastRate: ["", []],
			lastBasePoint: ["", []],

			startDate: ["", []],
			endDate: ["", []],

			lastDeferredPeriod: ["", []],
			periodicity: ["", []],
		});

		if (this.data?.product)
			this.onUpdate = true;

		this.productForm
			.get("productType")
			.valueChanges.subscribe(this.onChangeSelectedProductType.bind(this));
		this.productForm.get("lastProduct").valueChanges.subscribe(this.onProductChange.bind(this));
		
		this.productForm.get("agreement").valueChanges.subscribe(agreementId => {
			this.filteredProviders = this.providers.filter(p => p.parentId == agreementId)
			this.commonService.getRateType({ productId: this.productForm.get("lastProduct").value, parentId: agreementId }).subscribe((data) => {
				this.typeTaux = data["result"];
				this.triggerValueChange("lastRateType");
			});
		});

		this.productForm
			.get("lastRateType")
			.valueChanges.subscribe(this.onRateTypeChange.bind(this));

		if (this.onUpdate)
			this.setProductFormValues(this.data?.product)

		this.getTypeProductInversissement();
		this.getProductList();
		this.getPeriodicity();
		this.loadAgreements();
		this.loadProviders();
	}

	loadAgreements() {
		this.commonService.getAgreements().subscribe(data => {
			this.agreements = data["result"];
		})
	}

	loadProviders() {
		this.commonService.getProviders().subscribe(data => {
			this.providers = data["result"];
		})
	}

	setProductFormValues(values: any) {
		this.productForm.patchValue(values);
	}

	setFinancialObjectByValue(value: string, data: TreeData[]) {
		for (let fo of data) {
			if (fo.value == value) {
				this.productForm.get("financielObject").setValue(fo);
				return true;
			}
			if (this.setFinancialObjectByValue(value, fo.children)) return true;
		}
		return false;
	}

	filter(array: TreeData[], text: string) {
		const getNodes = (result, object) => {
			if (object.name.toLowerCase().startsWith(text)) {
				result.push(object);
				return result;
			}
			if (Array.isArray(object.children)) {
				const children = object.children.reduce(getNodes, []);
				if (children.length) result.push({ ...object, children });
			}
			return result;
		};

		this.filteredListObjetFinancement = array.reduce(getNodes, []);
	}

	onChangeSelectedProductType($event) {
		if ($event)
			this.filtredProductList = this.listProduitFinancement.filter(
				(x) => x["parentCode"] == $event,
			);
	}

	isContracted(): boolean {
		return this.getProductById(this.productForm.get("lastProduct").value)?.conventionProviderFlag === "O";
	}

	noSimulation() {
		return this.getProductById(this.productForm.get("lastProduct").value)?.simulationFlag === 'N';
	}

	onClose(): void {
		this.dialogRef.close();
	}

	onRateTypeChange(rateTypeId) {
		let rateType = this.typeTaux.find((tt) => tt.code == rateTypeId);
		this.fieldToDisplay = rateType?.fieldToDisplay;
		this.productForm.get("lastRate").setValue(rateType?.defaultRate);
		// this.resetRate();
	}
	
	resetRate() {
		if (this.fieldToDisplay !== "T") this.productForm.get("lastRate").setValue("");
		if (this.fieldToDisplay !== "PB") this.productForm.get("lastBasePoint").setValue("");
	}

	resetRateType() {
		this.productForm.get("lastRateType").setValue("");
	}

	getProductById(id: number) {
		return this.filtredProductList?.find(
			(pf) => pf.id == id,
		)
	}

	onValide() {
		this.snackeBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				let financielObject = this.productForm.value.financielObject;
				if (financielObject) {
					financielObject = financielObject.value;
				}
				this.product = { ...this.product, ...this.productForm.value, financielObject };

				this.product.financialProductName = this.getProductById(this.product.lastProduct)?.designation;
				this.product.periodicityName = this.perdictees?.find(
					(o) => o.code === this.product.periodicity,
				)?.designation;
				(this.product as any).fieldToDisplay = this.fieldToDisplay;

				if (this.onUpdate)
					for (let key of Object.keys(this.product))
						this.data.product[key] = this.product[key];
				this.dialogRef.close({ product: this.product });
			}
		});
	}

	onCalculer() {
		if (this.productForm.invalid) return;
		this.calculateSuccess = false;
		let critere: InvestCalculate = {
			amount: this.productForm.get("lastAmountGranted").value,
			differed: this.productForm.get("lastDeferredPeriod").value,
			duration: this.productForm.get("lastDuration").value,
			rate: this.productForm.get("lastRate").value,
		};
		this.demandeService.calculateProduct(critere).subscribe((data) => {
			this.product.lastInsurance = data["result"]["insuranceAmount"];
			this.product.lastMonthlyPayment = data["result"]["deadLineAmount"];
			this.calculateSuccess = true;
		});
	}

	onReset() {
		this.product = new InvestProduct();
		this.productForm.reset();
		this.calculateSuccess = false;
	}

	listTypeProduct = [];
	listProduitFinancement = [];
	listObjetFinancement = [];

	getTypeProductInversissement() {
		this.commonService.getTypeProductInversissement().subscribe((data) => {
			this.listTypeProduct = data["result"];
		});
	}

	onProductChange(productId: string) {
		let critere = { productId, parentId: this.productForm.get("agreement").value };
		this.commonService.getRateType(critere).subscribe((data) => {
			this.typeTaux = data["result"];
			this.triggerValueChange("lastRateType");
		});
		this.getFinancialObjectEntreprise();
		this.filteredAgreements = this.agreements.filter(a => a.parentId == productId)
	}

	perdictees = [];
	getPeriodicity() {
		let critere = {};
		this.commonService.getMensualites().subscribe((data) => {
			this.perdictees = data["result"];
		});
	}

	filtredListProduitFinancement = [];

	transformFinancialProducts(arr: any[]) {
		return arr.map((o) => {
			if (o.child) o.child = this.transformFinancialProducts(o.child);
			return {
				name: o.financialObjectName,
				value: o.financialObjectCode,
				children: o.child,
			};
		});
	}

	getFinancialObjectEntreprise() {
		this.commonService.getFinancialObjectEntreprise(this.productForm.get("lastProduct").value).subscribe((data) => {
			this.listObjetFinancement = this.transformFinancialProducts(data.result);
			this.filteredListObjetFinancement = this.listObjetFinancement;
			if (this.onUpdate)
				this.setFinancialObjectByValue(this.data?.product?.financielObject, this.listObjetFinancement);
		});
	}

	filtredProductList: Product[];
	productList;

	getProductList() {
		this.commonService.getproductList(this.data.creditTypeId).subscribe((data) => {
			this.listProduitFinancement = data["result"];
			this.filtredProductList = this.listProduitFinancement;
			this.onChangeSelectedProductType(this.productForm.get("productType").value);
		});
	}

	triggerValueChange(formControl: string) {
		this.productForm.get(formControl).setValue(this.productForm.get(formControl).value);
	}
}

class Product
{
	parentCode: string;
	parentId: number | null;
	contributionFlag: "N" | "O" | null;
	conventionProviderFlag: "N" | "O" | null;
	contractedFlag: "N" | "O";
	simulationFlag: "N" | "O";
	id: number;
	code: string | null;
	designation: string;
}