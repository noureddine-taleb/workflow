import { Manager } from "./manager";
import { Partner } from "./partner";
import { Person } from "./person";

export interface FicheSignaletique {
	persons?: Person[];
	partners?: Partner[];
	managers?: Manager[];
}
