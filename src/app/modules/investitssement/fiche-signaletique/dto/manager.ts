export interface Manager {
	managerId: number;
	personComplementId?: number;
	personRequestId?: number;
	beneficalOwnerId?: number;
	activityId?: any;
	diploma?: any;
	experience?: any;
	creationDate?: any;
	managerName?: string;
	managerNumber: string;
	activityName?: any;
	qualityName?: string;
	beneficalOwnerName?: any;
	beneficalOwnerNumber?: any;
	beneficalOwnerPart?: any;
}
