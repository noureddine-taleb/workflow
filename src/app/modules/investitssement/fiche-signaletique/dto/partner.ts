export interface Partner {
	managerId?: number;
	personComplementId?: number;
	personRequestId?: number;
	beneficalOwnerId?: number;
	activityId?: string;
	diploma?: string;
	experience?: string;
	creationDate?: string;
	managerName?: string;
	managerNumber?: string;
	activityName?: string;
	qualityName?: string;
	beneficalOwnerName?: string;
	beneficalOwnerNumber?: string;
	beneficalOwnerPart?: string;
}
