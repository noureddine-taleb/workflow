export interface Person {
	requestPersonId: number;
	personId: number;
	entityName?: any;
	creationDate?: any;
	ice?: any;
	registrationNumber?: any;
	legalStatusId: number;
	legalStatusName: string;
	capital?: any;
	courtId?: any;
	courtName?: any;
	phoneNumber: string;
	email?: any;
	adress: string;
}
