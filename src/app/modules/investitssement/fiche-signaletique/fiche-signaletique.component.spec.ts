import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FicheSignaletiqueComponent } from "./fiche-signaletique.component";

describe("FicheSignaletiqueComponent", () => {
	let component: FicheSignaletiqueComponent;
	let fixture: ComponentFixture<FicheSignaletiqueComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [FicheSignaletiqueComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(FicheSignaletiqueComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
