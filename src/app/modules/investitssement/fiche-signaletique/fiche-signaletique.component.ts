import { Component, OnInit } from "@angular/core";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { FicheSignaletique } from "./dto/fiche-signaletique";
import { Manager } from "./dto/manager";
import { Partner } from "./dto/partner";
import { FicheSignaletiqueService } from "./service/fiche-signaletique.service";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";

@Component({
	selector: "app-fiche-signaletique",
	templateUrl: "./fiche-signaletique.component.html",
	styleUrls: ["./fiche-signaletique.component.scss"],
})
export class FicheSignaletiqueComponent implements OnInit {
	request: LoanRequest;

	userEntityId;
	entityId;
	userId;

	ficheSignaletique: FicheSignaletique;

	//List Data
	listPartner = new Array<Partner>();
	listManagers = new Array<Manager>();

	constructor(
		private snackBar: SnackBarService,
		private ficheSignaletiqueService: FicheSignaletiqueService,
		private demandeService: DemandeService,
		private utilsSerivce: UtilsService
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request
		this.getDataSheet();
	}

	hasWritePriv() {
		return this.utilsSerivce.hasPrivilege(PrivilegeCode.maj_fs_a) && this.utilsSerivce.hasPrivilege(PrivilegeCode.maj_fs_s) && this.utilsSerivce.hasPrivilege(PrivilegeCode.maj_fs_m)
	}

	getDataSheet(): void {
		this.ficheSignaletiqueService.getDataSheet({ requestId: this.request.requestId }).subscribe(
			(data) => {
				this.ficheSignaletique = data.result;

				if (Array.isArray(data.result.persons) && data.result.persons.length > 0) {
					this.ficheSignaletique.persons = data.result.persons;
				}

				if (Array.isArray(data.result.partners) && data.result.partners.length > 0) {
					this.listPartner = this.ficheSignaletique.partners;
				}

				if (Array.isArray(data.result.managers) && data.result.managers.length > 0) {
					this.listManagers = this.ficheSignaletique.managers;
				}
			},
			(error) => {
				this.snackBar.openErrorSnackBar({ message: error.error });
			},
		);
	}

	updateDataSheet(): void {
		this.ficheSignaletiqueService
			.updateDataSheet({
				managers: this.listManagers,
				partners: this.listPartner,
				requestId: this.request.requestId,
			})
			.subscribe((data) => {
				this.snackBar.openSuccesSnackBar({ message: data.message });
				this.getDataSheet();
			});
	}
}
