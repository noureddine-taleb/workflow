import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { ConfigService } from 'app/core/config/config.service';
import { Observable } from "rxjs";
import { Manager } from "../dto/manager";
import { Partner } from "../dto/partner";

@Injectable({
	providedIn: "root",
})
export class FicheSignaletiqueService {
	private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient, private tokenService: AuthService) {}

	getDataSheet(_param: { requestId: number }): Observable<any> {
		let param = {
			userId: this.tokenService.user.userId,
			entityId: this.tokenService.user.entityId,
			userEntityId: this.tokenService.user.userEntityId,
			..._param,
		};
		return this.httpClient.post(this.demandesPath + "/dataSheet/getDataSheet", param);
	}

	updateDataSheet(_param: {
		managers: Array<Manager>;
		partners: Array<Partner>;
		requestId: number;
	}): Observable<any> {
		let param = {
			userId: this.tokenService.user.userId,
			entityId: this.tokenService.user.entityId,
			userEntityId: this.tokenService.user.userEntityId,
			..._param,
		};
		return this.httpClient.post(this.demandesPath + "/dataSheet/update", param);
	}
}
