import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CommonService } from "app/core/services/common.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { GoodType } from '../investissement.types';

@Component({
  selector: 'app-immeuble-form',
  templateUrl: './immeuble-form.component.html',
  styleUrls: ['./immeuble-form.component.scss']
})
export class ImmeubleFormComponent implements OnInit {
	immeubleForm: FormGroup;
	goodTypes: GoodType[] = []

	constructor(
		private fb: FormBuilder,
		private dialogRef: MatDialogRef<ImmeubleFormComponent>,
		private commonService: CommonService,
		private demandeService: DemandeService
	) { }

	ngOnInit(): void {
		this.immeubleForm = this.fb.group({
			landTitle: [''],
			owner: [''],
			size: [''],
			quota: [''],
			adress: [''],
			value: [''],
			goodTypeId: [''],
		})

		this.commonService.getTypeBien({ parentId: this.demandeService.request.creditTypeId }).subscribe(data => {
			this.goodTypes = data.result;
		})
	}

	onSave() {
		this.dialogRef.close({ immeuble: this.immeubleForm.value })
	}
}
