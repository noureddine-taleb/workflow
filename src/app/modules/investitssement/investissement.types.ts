export class DemandeInvest {
	accountId: number;
	clientCategory: number;
	creditType: number;
	entityId: number;
	newConstitution: string;
	objectCredit: string;
	products: InvestProduct[];
	requestId: number;
	userEntityId: number;
	userId: number;
}

export class GoodType {
	parentCode: number;
	parentId: number;
	id: number;
	code: string;
	designation: string;
}

export class InvestProduct {
	amountGranted: number;
	basePoint: number;
	bonus: number;
	contracted: string;
	contributionAmount: number;
	deferredPeriod: number;
	duration: number;
	entityId: number;
	financialObject: number;
	financialProduct: number;
	financielObject: string;
	goodAmount: number;
	insurance: number;
	lastAmountGranted: number;
	lastBasePoint: number;
	lastBonus: number;
	lastContributionAmount: number;
	lastDeferredPeriod: number;
	lastDuration: number;
	lastFinancialProduct: number;
	lastGoodAmount: number;
	lastInsurance: number;
	lastMonthlyPayment: number;
	lastOthersFees: number;
	lastPrelevementType: string;
	lastRate: number;
	lastRateType: string;
	monthlyPayment: number;
	othersFees: number;
	prelevementType: string;
	productId: number;
	productType: string;
	rate: number;
	rateType: string;
	refRequest: string;
	requestId: number;
	status: string;
	userEntityId: number;
	userId: number;
	periodicity: string;
	lastProduct: number;

	periodicityName: string;
	financialProductName: string;
}

export class InvestCalculate {
	amount: number;
	deadLineAmount?: number;
	differed?: number;
	duration: number;
	insuranceAmount?: number;
	interestAmount?: number;
	rate: number;
}


export class Rubrique {
	nom: string;
    libelle: string;
    checked: boolean;
}