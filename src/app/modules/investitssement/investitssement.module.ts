import { CommonModule, registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatTabsModule } from "@angular/material/tabs";
import { RouterModule } from "@angular/router";
import { AccountModule } from "app/shared/account/account.module";
import { CardModule } from "app/shared/card/card.module";
import { DemandeDocsAttchModule } from "app/shared/demande-docs-attch/demande-docs-attch.module";
import { SimulationResultModule } from "app/shared/simulation-result/simulation-result.module";
import { VerticalTableModule } from "app/shared/vertical-table/vertical-table.module";
import { MatTreeSelectInputModule } from "mat-tree-select-input";
import { NgxCurrencyModule } from "ngx-currency";
import { CcgModule } from "../demandes/realisation/ccg/ccg.module";
import { AddDemandeInvestComponent } from "./add-demande-invest/add-demande-invest.component";
import { AddProductInvestComponent } from "./add-product-invest/add-product-invest.component";
import { FicheSignaletiqueComponent } from "./fiche-signaletique/fiche-signaletique.component";
import { ImmeubleFormComponent } from "./immeuble-form/immeuble-form.component";
import { routes } from "./investitssement.routing";
import { PresentationEntrepriseComponent } from "./presentation-entreprise/presentation-entreprise.component";
import { BatimentConstructionComponent } from "./projet-investissement-tables/batiment-construction/batiment-construction.component";
import { ChiffreAffairePrevComponent } from "./projet-investissement-tables/chiffre-affaire-prev/chiffre-affaire-prev.component";
import { CompteExploitationPrevComponent } from "./projet-investissement-tables/compte-exploitation-prev/compte-exploitation-prev.component";
import { MoyenHumainComponent } from "./projet-investissement-tables/moyen-humain/moyen-humain.component";
import { PlanFinancementComponent } from "./projet-investissement-tables/plan-financement/plan-financement.component";
import { ProgrammeInvestissementComponent } from "./projet-investissement-tables/programme-investissement/programme-investissement.component";
import { SupportFoncierComponent } from "./projet-investissement-tables/support-foncier/support-foncier.component";
import { TauxUtilisationPrevComponent } from "./projet-investissement-tables/taux-utilisation-prev/taux-utilisation-prev.component";
import { ProjetInvestissementComponent } from "./projet-investissement/projet-investissement.component";

registerLocaleData(localeFr);
registerLocaleData(localeEs);
@NgModule({
	declarations: [
		AddDemandeInvestComponent,
		AddProductInvestComponent,
		FicheSignaletiqueComponent,
		PresentationEntrepriseComponent,
		ProjetInvestissementComponent,
		BatimentConstructionComponent,
		ChiffreAffairePrevComponent,
		CompteExploitationPrevComponent,
		MoyenHumainComponent,
		PlanFinancementComponent,
		ProgrammeInvestissementComponent,
		SupportFoncierComponent,
		TauxUtilisationPrevComponent,
		ImmeubleFormComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		NgxCurrencyModule,
		CardModule,
		CcgModule,
		MatTreeSelectInputModule,
		MatExpansionModule,
		MatSelectModule,
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		MatDatepickerModule,
		MatTabsModule,
		MatTableModule,
		DemandeDocsAttchModule,
		MatRadioModule,
		MatSelectModule,
		MatIconModule,
		AccountModule,
		MatDialogModule,
		SimulationResultModule,
		VerticalTableModule,
		MatSelectModule
	],
	exports: [
		AddDemandeInvestComponent,
		AddProductInvestComponent,
		FicheSignaletiqueComponent,
		PresentationEntrepriseComponent,
		ProjetInvestissementComponent,
		BatimentConstructionComponent,
		ChiffreAffairePrevComponent,
		CompteExploitationPrevComponent,
		MoyenHumainComponent,
		PlanFinancementComponent,
		ProgrammeInvestissementComponent,
		SupportFoncierComponent,
		TauxUtilisationPrevComponent,
		ImmeubleFormComponent
	]
})
export class InvestitssementModule {}
