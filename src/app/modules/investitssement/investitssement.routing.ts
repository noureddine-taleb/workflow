import { Routes } from "@angular/router";
import { ListDemandeComponent } from "app/shared/list-demande/list-demande.component";
import { RequestType } from "app/shared/models/RequestType";
import { AddDemandeInvestComponent } from "./add-demande-invest/add-demande-invest.component";

export const routes: Routes = [
	{
		path: "agri",
		data: { title: "Liste des demandes de crédit agri", demandeType: RequestType.AGRI, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "hors-agri",
		data: { title: "Liste des demandes de crédit hors-agris", demandeType: RequestType.HORS_AGRI, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "amaq",
		data: { title: "Liste des demandes de crédit AMAQ", demandeType: RequestType.AMAQ, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "add/:requestTypeId",
		component: AddDemandeInvestComponent,
	},
];
