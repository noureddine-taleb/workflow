export interface CompetitionDto {
	competetionId?: number;
	description?: string;
	entityId?: number;
	label?: string;
	mark?: string;
	partMarket?: number;
	presentationId?: number;
	userEntityId?: number;
	userId?: number;
	autre?: any;
}
