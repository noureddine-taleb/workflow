export interface CreditRequestCriteria {
	accountNumber?: string;
	action?: string;
	actionUserId?: number;
	cin?: string;
	creditType?: number;
	endDate?: Date;
	entityId?: number;
	first?: number;
	note?: string;
	pas?: number;
	productType?: string;
	requestId?: number;
	startDate?: Date;
	statusCode?: string;
	userEntityId?: number;
	userId?: number;
}
