export interface EquipmentDto {
	competetionId?: number;
	description?: string;
	entityId?: number;
	label?: string;
	mark?: string;
	partMarket?: number;
	presentationId?: number;
	userEntityId?: number;
	userId?: number;
	category?: any;
	value?: any;
}
