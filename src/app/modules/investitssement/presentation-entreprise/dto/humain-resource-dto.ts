export interface HumainResourceDto {
	administrativeNumber?: number;
	commercialNumber?: number;
	entityId?: number;
	humainResourceId?: number;
	personCategory?: string;
	personNumber?: number;
	presentationId?: number;
	productionNumber?: number;
	userEntityId?: number;
	userId?: number;
}
