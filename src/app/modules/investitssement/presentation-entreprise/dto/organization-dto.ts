export interface OrganizationDto {
	entityId?: number;
	foreignLocal?: string;
	label?: string;
	organizationId?: number;
	payementChoice?: string;
	presentationId?: number;
	type?: string;
	userEntityId?: number;
	userId?: number;
}
