export interface PatrimonyDto {
	entityId?: number;

	identifiant?: number;
	adress?: string;
	goodTypeId?: number;
	landTitle?: string;
	observation?: string;
	owner?: string;
	proofTransaction?: string;
	quota?: number;
	size?: number;
	value?: number;
	statut?: string;
	statusName?: string;

	requestId?: number;
	userEntityId?: number;
	userId?: number;
}
