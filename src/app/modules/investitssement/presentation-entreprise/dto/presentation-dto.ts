export interface PresentationDto {
	businessSectorPresentation?: string;
	companyPresentation?: string;
	entityId?: number;
	presentationId?: number;
	requestId?: number;
	userEntityId?: number;
	userId?: number;
}
