export interface RequestPresentation {
	accountId?: string;
	entityId?: number;
	first?: number;
	pas?: number;
	presentationId?: number;
	projectId?: string;
	requestId?: string;
	requeteId?: string;
	type?: string;
	userEntityId?: number;
	userId?: number;
}
