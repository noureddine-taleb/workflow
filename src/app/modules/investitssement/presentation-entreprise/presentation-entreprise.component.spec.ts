import { ComponentFixture, TestBed } from "@angular/core/testing";
import { PresentationEntrepriseComponent } from "./presentation-entreprise.component";

describe("PresentationEntrepriseComponent", () => {
	let component: PresentationEntrepriseComponent;
	let fixture: ComponentFixture<PresentationEntrepriseComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [PresentationEntrepriseComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(PresentationEntrepriseComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
