import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { CheckRubriquesDialogComponent } from 'app/modules/demandes/check-rubriques-dialog/check-rubriques-dialog.component';
import { DemandeDocsAttchComponent } from 'app/shared/demande-docs-attch/demande-docs-attch.component';
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";
import { ImmeubleFormComponent } from '../immeuble-form/immeuble-form.component';
import { CompetitionDto } from './dto/competition-dto';
import { EquipmentDto } from './dto/equipment-dto';
import { HumainResourceDto } from './dto/humain-resource-dto';
import { OrganizationDto } from './dto/organization-dto';
import { PatrimonyDto } from './dto/patrimony-dto';
import { PresentationDto } from './dto/presentation-dto';
import { PresentationEntrepriseService } from './service/presentation-entreprise.service';
import { FormControl } from '@angular/forms';
import { CacheService } from 'app/core/services/cache.service';
import { DemandeService } from 'app/modules/demandes/services/demande.service';
import { Rubrique } from '../investissement.types';

@Component({
    selector: 'app-presentation-entreprise',
    templateUrl: './presentation-entreprise.component.html',
    styleUrls: ['./presentation-entreprise.component.scss']
})
export class PresentationEntrepriseComponent implements OnInit {
    @Input() requestId;
    @Input() demandeDTO;

    presentationDto: PresentationDto = {};
    organizationDto: OrganizationDto = {};
    humainResourceDto: HumainResourceDto = {};
    projectBuildingDto: PatrimonyDto = {};
    equipmentDto: EquipmentDto = {};
    competitionDto: CompetitionDto = {};

    listOrganization: OrganizationDto[];

    listRubs: Rubrique[];

    ModeRegelement:[{code: 'CH', Lib: 'Chéque'  },{code: 'ES', Lib: 'Espèces'  },{code: 'CA', Lib: 'Cartes bancaires'  },{code: 'AM', Lib: 'Autres moyens de paiement'  } ];

    constructor(private dialog: MatDialog,
                private presentationEntrepriseService: PresentationEntrepriseService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
				private utilsService: UtilsService,
                private cacheService: CacheService,
                private demandeService: DemandeService
    ) {
        this.listRubs = this.getRubs();
    }

    ngOnInit(): void {
        this.getPresentation();
        // this.yearControl.valueChanges.subscribe(this.yearChanged.bind(this))
    }

    getRubs(): Rubrique[] {
        let cachedRubs = this.getCachedRubs();
        if (cachedRubs)
            return cachedRubs;

        return [
            {nom:'volet1',libelle:'Présentation du secteur d`\'activité',checked:true},
            {nom:'volet2',libelle:'Présentation de la sociéte',checked:true},
            {nom:'volet3',libelle:'Organisation Commerciale',checked:true},
            {nom:'volet4',libelle:'Moyens de production',checked:true},
            {nom:'volet6',libelle:'Diagnostic financier',checked:true},
        ]
    }

    getCachedRubs() {
        let key = `${this.demandeService.request.requestId}-presentation-entreprise`;
        return this.cacheService.fetch<Rubrique[]>(key);
    }

    setCachedRubs(rubriques: Rubrique[]) {
        let key = `${this.demandeService.request.requestId}-presentation-entreprise`;
        return this.cacheService.store(key, rubriques);
    }

    clients:OrganizationDto[];// = [{organizationId:null,label:'fournisseur 1',payementChoice:'Chéque',foreignLocal:'Etranger'}];
    fournisseurs:OrganizationDto[];// =[{fournisseur:'fournisseur 1',moderegelement:'Chéque',local_etranger:'Etranger'}];
    concurrents: CompetitionDto[]; //=[{principauxConcurent:'Concurrent 1',partMarche:'50',marque:'Ellitium',autre:''}];
    moyenHumains:HumainResourceDto[];//=[{categorie:'Employés',commercial:2,administratif:1,production:8}];
    moyenEquipements: EquipmentDto[];// = [{nature:'Nature 1',designation:'designation',estimation:30000}];

    /*----------------------------- Presentation --------------------------------------------*/
    getPresentation(): void {
        this.presentationEntrepriseService.getPresentation({
            entityId: this.tokenService.user.entityId,
            first: 0,
            pas: 10,
            requestId: this.requestId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        })
            .subscribe((data) => {
                if (data.status === 200) {
                    this.presentationDto = data.result;
                    if (!this.presentationDto){
                        this.presentationDto = {};
                        return;
                    }

                    //Promise.all([this.getListOrganization(), this.getListHumains(), this.getListEquipment()]);

                    //Client,Fournisseur
                    this.getListOrganization()

                    //Humains
                    this.getListHumains();

                    //Equipment
                    this.getListEquipment();

                    //Competitions
                    this.getListCompetitions();

                    //Synthetic Balance
                    this.getSyntheticBalance();

                    //Structure Analysis
                    this.getStructureAnalysis();

                    //Activity
                    this.getActivity()

                    //Patrimon
                    this.getListProjectBuilding();
                } /*else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            }, (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error.message });
            */});
    }

	hasPriv() {
		return this.utilsService.hasPrivilege(PrivilegeCode.maj_presentation_m)
	}

    onValidatePresentation() {


        this.presentationDto.userId = this.tokenService.user.userId;
        this.presentationDto.entityId = this.tokenService.user.entityId;
        this.presentationDto.userEntityId = this.tokenService.user.userEntityId;
        this.presentationDto.requestId = this.requestId;

            this.presentationEntrepriseService.updatePresentation(this.presentationDto)
                .subscribe((data) => {
                        //if (data.status === 200) {
                            this.snackeBarService.openSuccesSnackBar({ message: data.message });
                            this.getPresentation();
                        /*} else {
                            this.snackeBarService.openErrorSnackBar({ message: data.message });
                        }
                    },
                    (error) => {
                        this.snackeBarService.openErrorSnackBar({ message: error.error.message });
                    */});
    }

    /*----------------------------- Organization <Start>--------------------------------------------*/
    getListOrganization(): void {
        this.presentationEntrepriseService.getOrganization({
            presentationId: this.presentationDto.presentationId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {

                //if (data.status === 200) {
                    this.listOrganization=data.result;
                    this.clientdataSource       = new MatTableDataSource(this.listOrganization.filter(o=>o.type ==='CL'));
                    this.fournisseurdataSource  = new MatTableDataSource(this.listOrganization.filter(o=>o.type ==='FR'));

               /* } else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error.message });
            */});
    }

    onDeleteOrganization(element) {
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId = this.tokenService.user.userId;
                    element.entityId = this.tokenService.user.entityId;
                    element.userEntityId = this.tokenService.user.userEntityId;
                    element.presentationId = this.presentationDto.presentationId;

                    this.presentationEntrepriseService.deleteOrganization(element)
                        .subscribe(
                            (data) => {
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.addClient = false;
                                    this.getListOrganization();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: error.error.message });
                            */});
                }
            })
    }
    /*----------------------------- Organization <End>--------------------------------------------*/

    /*----------------------------- Client <Start>--------------------------------------------*/
    addClient=false;
    client: any;
    clientdataSource ;//= new MatTableDataSource(this.clients);

    onAddClient() {
        this.organizationDto = {};
        this.addClient = true;
    }

    onValidAddClient() {
        this.organizationDto.userId         = this.tokenService.user.userId;
        this.organizationDto.entityId       = this.tokenService.user.entityId;
        this.organizationDto.userEntityId   = this.tokenService.user.userEntityId;
        this.organizationDto.presentationId = this.presentationDto.presentationId;

        this.presentationEntrepriseService.updateClient(this.organizationDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addClient = false;
                        this.getListOrganization();
                   /* } else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }*/
                }/*, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: error.error.message });
                }*/);
    }

    getClientFooterCols() {
        return this.addClient ?  ['1','2','3','action']:[];
    }

    onDeleteClient(element) {
        this.onDeleteOrganization(element);
    }
    /*----------------------------- Client <End>--------------------------------------------*/

    /*----------------------------- Fournisseur <Start>--------------------------------------------*/
    fournisseurdataSource;// = new MatTableDataSource(this.fournisseurs);
    addfournisseur=false;

    onAddFournisseur() {
        this.organizationDto = {};
        this.addfournisseur=true;
    }

    onValidAddFournisseur(){
        this.organizationDto.userId         = this.tokenService.user.userId;
        this.organizationDto.entityId       = this.tokenService.user.entityId;
        this.organizationDto.userEntityId   = this.tokenService.user.userEntityId;
        this.organizationDto.presentationId = this.presentationDto.presentationId;

        this.presentationEntrepriseService.updateProvider(this.organizationDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addfournisseur = false;
                        this.getListOrganization();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: error.error.message });
                */});
    }

    getFournisseurFooterCols() {
        return this.addfournisseur ?  ['1','2','3','action']:[];
    }

    onDeleteFournisseur(element) {
        this.onDeleteOrganization(element);
    }
    /*----------------------------- Fournisseur <End>--------------------------------------------*/

    /*----------------------------- concurrent <Start>--------------------------------------------*/
    concurrentdataSource;// = new MatTableDataSource(this.concurrents);
    addConcurrent=false;
    getListCompetitions(): void {
        this.presentationEntrepriseService.getCompetition({
            presentationId: this.presentationDto.presentationId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
            //if (data.status === 200) {
                this.concurrentdataSource =new MatTableDataSource(data.result);
            /*} else {
                this.snackeBarService.openErrorSnackBar({ message: data.message });
            }
        },(error) => {
            this.snackeBarService.openErrorSnackBar({ message: error.error });*/
        });
    }
    onAddConcurrent() {
        this.competitionDto = {};
        this.addConcurrent=true;
    }

    onValidAddConcurrent(){
        this.competitionDto.userId         = this.tokenService.user.userId;
        this.competitionDto.entityId       = this.tokenService.user.entityId;
        this.competitionDto.userEntityId   = this.tokenService.user.userEntityId;
        this.competitionDto.presentationId = this.presentationDto.presentationId;

        this.presentationEntrepriseService.updateCompetition(this.competitionDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addConcurrent = false;
                        this.getListCompetitions();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                });
    }

    getConcurrentFooterCols() {
        return this.addConcurrent ?  ['1','2','3','4','action']:[];
    }

    onDeleteConcurrent(element) {
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId = this.tokenService.user.userId;
                    element.entityId = this.tokenService.user.entityId;
                    element.userEntityId = this.tokenService.user.userEntityId;
                    element.presentationId = this.presentationDto.presentationId;

                    this.presentationEntrepriseService.deleteCompetition(element)
                        .subscribe(
                            (data) => {
                               // if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.addMyenHumain = false;
                                    this.getListCompetitions();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                            });
                }
            })
    }
    /*----------------------------- concurrent <End>--------------------------------------------*/

    //Humains
    /*----------------------------- Humains <Start>--------------------------------------------*/
    humainsdataSource //= new MatTableDataSource(this.moyenHumains);
    addMyenHumain=false;
    getListHumains(): void {
        this.presentationEntrepriseService.getHumainResource({
            presentationId: this.presentationDto.presentationId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                //if (data.status === 200) {
                    this.humainsdataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }

    onAddMyenHumain() {
        this.humainResourceDto = {};
        this.addMyenHumain=true;
    }

    onValidAddMyenHumain(){
        this.humainResourceDto.userId         = this.tokenService.user.userId;
        this.humainResourceDto.entityId       = this.tokenService.user.entityId;
        this.humainResourceDto.userEntityId   = this.tokenService.user.userEntityId;
        this.humainResourceDto.presentationId = this.presentationDto.presentationId;

        this.presentationEntrepriseService.updateHumainResource(this.humainResourceDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addMyenHumain = false;
                        this.getListHumains();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }*/
                }/*, (error) => {
                    console.log(error)
                    this.snackeBarService.openErrorSnackBar({ message: error.error });
                }*/);
    }

    getHumainFooterCols() {
        return this.addMyenHumain ?  ['1','2','3','4','5','action']:[];
    }

    onDeleteHumain(element) {
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId = this.tokenService.user.userId;
                    element.entityId = this.tokenService.user.entityId;
                    element.userEntityId = this.tokenService.user.userEntityId;
                    element.presentationId = this.presentationDto.presentationId;

                    this.presentationEntrepriseService.deleteHumainResource(element)
                        .subscribe(
                            (data) => {
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.addMyenHumain = false;
                                    this.getListHumains();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                            });
                }
            })
    }
    /*----------------------------- Humains <End>--------------------------------------------*/

    //Immeubles
    immeubledataSource ;//= new MatTableDataSource(this.moyenImmeubles);
    addImmeuble=false;

    getListProjectBuilding(): void {
        this.presentationEntrepriseService.getPatrimony({
            requestId: this.requestId,
            presentationId: this.presentationDto.presentationId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                //if (data.status === 200) {
                    this.immeubledataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }

    onAddMyenImmeuble() {
        this.projectBuildingDto = {};
        // this.addImmeuble=true;
		this.dialog.open(ImmeubleFormComponent, {width: "50vw"}).afterClosed().subscribe(data => {
			if (data?.immeuble) {
				this.projectBuildingDto = data.immeuble;
				this.onValidAddMyenImmeuble()
			}
		})
    }

    onValidAddMyenImmeuble(){
        this.projectBuildingDto.userId         = this.tokenService.user.userId;
        this.projectBuildingDto.entityId       = this.tokenService.user.entityId;
        this.projectBuildingDto.userEntityId   = this.tokenService.user.userEntityId;
        this.projectBuildingDto.requestId      = this.requestId;

        this.presentationEntrepriseService.updatePatrimony(this.projectBuildingDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addImmeuble = false;
                        this.getListProjectBuilding();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                });
    }

    getImmeubleFooterCols() {
        return this.addImmeuble ?  ['1','2','3','4','5','6','7','8','action']:[];
    }

    onDeleteImmeuble(element) {
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId      = this.tokenService.user.userId;
                    element.entityId        = this.tokenService.user.entityId;
                    element.userEntityId    = this.tokenService.user.userEntityId;
                    element.presentationId  = this.presentationDto.presentationId;

                    this.presentationEntrepriseService.deletePatrimony(element)
                        .subscribe(
                            (data) => {
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.addMyenHumain = false;
                                    this.getListProjectBuilding();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                            });
                }
            })
    }

    //Immeubles
    equipementdataSource;// = new MatTableDataSource(this.moyenEquipements);
    addEquipement=false;

    getListEquipment(): void {
        this.presentationEntrepriseService.getEquipment({
            presentationId: this.presentationDto.presentationId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                //if (data.status === 200) {
                    this.equipementdataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }

    onAddMyenEquipement() {
        this.equipmentDto= {};
        this.addEquipement=true;
    }

    onValidAddMyenEquipement(){
        this.equipmentDto.userId         = this.tokenService.user.userId;
        this.equipmentDto.entityId       = this.tokenService.user.entityId;
        this.equipmentDto.userEntityId   = this.tokenService.user.userEntityId;
        this.equipmentDto.presentationId = this.presentationDto.presentationId;

        this.presentationEntrepriseService.updateEquipment(this.equipmentDto)
            .subscribe(
                (data) => {
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.addEquipement = false;
                        this.getListEquipment();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                });
    }

    getEquipementFooterCols() {
        return this.addEquipement ?  ['1','2','3','action']:[];
    }

    onDeleteEquipement(element) {
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId = this.tokenService.user.userId;
                    element.entityId = this.tokenService.user.entityId;
                    element.userEntityId = this.tokenService.user.userEntityId;
                    element.presentationId = this.presentationDto.presentationId;

                    this.presentationEntrepriseService.deleteEquipment(element)
                        .subscribe(
                            (data) => {
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.addMyenHumain = false;
                                    this.getListEquipment()
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: error.error.message });*/
                            });
                }
            })
    }

    //Bilan syntétique
    bilanSyntetique = {};//= [{exercice:'2021',actifImmobilise:'250000',actifcirculant:30000,tresorerieActife:25000,totalActif:0,financementPermanent:500000,passifCirculant:250000,tresoreriePassif:360000,totalPassif:0}];
    bilanSyntetiquedataSource = [];// = new MatTableDataSource(this.bilanSyntetiques);
    addSyntetique=false;

    getSyntheticBalance(): void {
        this.presentationEntrepriseService.getSyntheticBalance({
            requestId: this.requestId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
			this.bilanSyntetiquedataSource = data.result;
			if (this.bilanSyntetiquedataSource.length)
				this.yearChanged(this.bilanSyntetiquedataSource[this.bilanSyntetiquedataSource.length - 1].exercice)
        });
    }

    getYears() {
        return this.bilanSyntetiquedataSource.map(el => el.exercice)
    }

	year: number;
    yearChanged(year: number) {
		this.year = year;
		if (this.bilanSyntetiquedataSource?.length)
			this.bilanSyntetique = this.bilanSyntetiquedataSource.find(el => el.exercice == year);
		if (this.analyseStructuredataSource?.length)
        	this.analyseStructure = this.analyseStructuredataSource.find(el => el.exercice == year);
		if (this.activitedataSource?.length)
        	this.activite = this.activitedataSource.find(el => el.exercice == year);
    }

    onAddBilanSyntetique() {
        this.client=new Object();
        this.addSyntetique=true;
    }


    //analyse de structure
    //analyseStructures ;//= [{financementPermanent:250000,actifImmobilise:250000,fdr:30000,actifCirculant:25000,passifCirculant:500000,bfdr:250000,tresorerieNette:360000}];
    analyseStructuredataSource;// = new MatTableDataSource(this.analyseStructures);
    analyseStructure = {};// = new MatTableDataSource(this.analyseStructures);
    addAnalyseStructure=false;

    getStructureAnalysis(): void {
        this.presentationEntrepriseService.getStructureAnalysis({
            requestId: this.requestId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
			this.analyseStructuredataSource = data.result;
			if (this.analyseStructuredataSource.length)
				this.yearChanged(this.analyseStructuredataSource[this.analyseStructuredataSource.length - 1].exercice)
		});
    }
    activitedataSource;//:MatTableDataSource<Activite> = new MatTableDataSource(this.activites);
    activite = {};//:MatTableDataSource<Activite> = new MatTableDataSource(this.activites);
    activiteItem: any;
    // addActivite=false;


    getActivity(): void {
        this.presentationEntrepriseService.getActivity({
            requestId: this.requestId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
				this.activitedataSource = data.result;
				if (this.activitedataSource.length)
					this.yearChanged(this.activitedataSource[this.activitedataSource.length - 1].exercice)
		});
    }

    onAddRubrique() {
        const dialogRef = this.dialog.open(CheckRubriquesDialogComponent, {
            width: '50vw',data:this.listRubs,disableClose:true
        });

        dialogRef.afterClosed().subscribe(result => {
            this.listRubs = result;
            this.setCachedRubs(result);
        });
    }

    afficherVolet(nomVolet: string) {
        const item=this.listRubs.filter(x=>x.nom==nomVolet);
        if(item.length>0){
            return item[0].checked
        }
        return true;
    }

    onAddDocument() {
        const dialogRef = this.dialog.open(DemandeDocsAttchComponent, {
            width: '50vw',data: {
                lisDocs:[
                    {name: 'CIN', checked: false}]
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: `,result);
        });
    }
}


export class Activite{
    chiffreAffaire:number;
    resultatExploitation:number;
    chargeInteret:number;
    resultatNet:number;
    amortissement:number;
    cashFlow:number;
    resultatNetExploitation:number;
    consommationExercice:number;
    valeurAjoutee:number;
    ebe:number;
    resultatFinancier:number;
    resultatCourant:number;
    impotResultat:number;
    productionExercice:number;
}
