import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { Observable } from "rxjs";
import { CompetitionDto } from "../dto/competition-dto";
import { CreditRequestCriteria } from "../dto/credit-request-criteria-dto";
import { EquipmentDto } from "../dto/equipment-dto";
import { HumainResourceDto } from "../dto/humain-resource-dto";
import { OrganizationDto } from "../dto/organization-dto";
import { PatrimonyDto } from "../dto/patrimony-dto";
import { PresentationDto } from "../dto/presentation-dto";
import { RequestPresentation } from "../dto/request-presentation";

@Injectable({
	providedIn: "root",
})
export class PresentationEntrepriseService {
	baseUrl = "/creditEntreprise/";
	private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(private httpClient: HttpClient) {}

	/*---------------------- Presentation ------------------------------ */
	updatePresentation(param: PresentationDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "presentation", param);
	}

	getPresentation(param: RequestPresentation): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getPresentation", param);
	}
	/*---------------------- Presentation ------------------------------ */

	/*---------------------- Organization ------------------------------ */
	deleteOrganization(param: any): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "deleteOrganization", param);
	}

	getOrganization(param: any): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getOrganisation", param);
	}
	/*---------------------- Organization ------------------------------ */

	/*---------------------- Client ------------------------------ */
	updateClient(param: OrganizationDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "client", param);
	}
	/*---------------------- Client ------------------------------ */

	/*---------------------- Fournisseur ------------------------------ */
	updateProvider(param: OrganizationDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "provider", param);
	}
	/*---------------------- Fournisseur ------------------------------ */

	/*---------------------- humain Resource ------------------------------ */
	getHumainResource(param: RequestPresentation): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getHumainResource", param);
	}

	updateHumainResource(param: HumainResourceDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "humainResource", param);
	}

	deleteHumainResource(param: HumainResourceDto): Observable<any> {
		return this.httpClient.post(
			this.demandesPath + this.baseUrl + "deleteHumainResource",
			param,
		);
	}
	/*---------------------- humain Resource ------------------------------ */

	/*---------------------- Patrimony ------------------------------ */
	getPatrimony(param: RequestPresentation): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/patrimony/getPatrimony", param);
	}

	updatePatrimony(param: PatrimonyDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/patrimony/create", param);
	}

	deletePatrimony(param: PatrimonyDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + "/patrimony/delete", param);
	}
	/*---------------------- Patrimony ------------------------------ */

	/*---------------------- Equipment ------------------------------ */
	getEquipment(param: RequestPresentation): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getEquipment", param);
	}

	updateEquipment(param: EquipmentDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "equipement", param);
	}

	deleteEquipment(param: EquipmentDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "deleteEquipment", param);
	}
	/*---------------------- Equipment ------------------------------ */

	/*---------------------- Competition ------------------------------ */
	getCompetition(param: RequestPresentation): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getCompetition", param);
	}

	updateCompetition(param: CompetitionDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "competition", param);
	}

	deleteCompetition(param: CompetitionDto): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "deleteCompetition", param);
	}
	/*---------------------- Competition ------------------------------ */

	/*---------------------- Bilan synthétique  ------------------------------ */
	getSyntheticBalance(param: CreditRequestCriteria): Observable<any> {
		return this.httpClient.post(
			this.demandesPath + this.baseUrl + "getSyntheticBalance",
			param,
		);
	}
	/*---------------------- Bilan synthétique  ------------------------------ */

	/*---------------------- Structure Analysis  ------------------------------ */
	getStructureAnalysis(param: CreditRequestCriteria): Observable<any> {
		return this.httpClient.post(
			this.demandesPath + this.baseUrl + "getStructureAnalysis",
			param,
		);
	}
	/*---------------------- Structure Analysis  ------------------------------ */

	/*---------------------- Structure Analysis  ------------------------------ */
	getActivity(param: CreditRequestCriteria): Observable<any> {
		return this.httpClient.post(this.demandesPath + this.baseUrl + "getActivity", param);
	}
	/*---------------------- Structure Analysis  ------------------------------ */
}
