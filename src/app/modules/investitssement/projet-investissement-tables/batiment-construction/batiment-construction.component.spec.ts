import { ComponentFixture, TestBed } from "@angular/core/testing";
import { BatimentConstructionComponent } from "./batiment-construction.component";

describe("BatimentConstructionComponent", () => {
	let component: BatimentConstructionComponent;
	let fixture: ComponentFixture<BatimentConstructionComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [BatimentConstructionComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(BatimentConstructionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
