import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { ProjectBuildingDto } from './dto/project-building-dto';
import { ProjectBuildingService } from './service/project-building.service';

@Component({
    selector: 'app-batiment-construction',
    templateUrl: './batiment-construction.component.html',
    styleUrls: ['./batiment-construction.component.scss']
})
export class BatimentConstructionComponent implements OnInit {
    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;
    projectBuildingDto:ProjectBuildingDto={};

    listItems=[];
    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'designation',libelle:'Désignation',required:false,type:'input'},
        {nom:'usage',libelle:'Usage',required:false,type:'input'},
        // {nom:'description',libelle:'Description',required:false,type:'input'},
        {nom:'lieu',libelle:'Lieu',required:false,type:'input'},
        {nom:'superficie',libelle:'Superficie',required:false,type:'input'},
        {nom:'proLoc',libelle:'Pro/Loc',required:false,type:'input'}
    ]
    dataSource = new MatTableDataSource(this.listItems);
    constructor(private projectBuildingService: ProjectBuildingService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getListProjectBuilding();
    }

    getListProjectBuilding(): void {
        this.projectBuildingService.getProjectBuilding({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                //if (data.status === 200) {
                    this.dataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }
    onValideAdd(){
        this.projectBuildingDto.userId         = this.tokenService.user.userId;
        this.projectBuildingDto.entityId       = this.tokenService.user.entityId;
        this.projectBuildingDto.userEntityId   = this.tokenService.user.userEntityId;
        this.projectBuildingDto.projectId      = this.project.projectId;

        this.projectBuildingService.updateProjectBuilding(this.projectBuildingDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.projectBuildingDto={};
                        this.getListProjectBuilding();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.projectBuildingService.deleteProjectBuilding(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getListProjectBuilding();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }
}
