export interface ProjectBuildingDto {
    area?: string;
    buildingId?: number;
    description?: string;
    entityId?: number;
    place?: string;
    projectId?: number;
    proloc?: string;
    sizeBuilding?: string;
    unity?: string;
    use?: string;
    userEntityId?: number;
    userId?: number;
}