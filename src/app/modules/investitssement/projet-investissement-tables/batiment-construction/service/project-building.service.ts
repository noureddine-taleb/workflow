import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { ProjectBuildingDto } from '../dto/project-building-dto';


@Injectable({
  providedIn: 'root'
})
export class ProjectBuildingService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getProjectBuilding(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectBuilding');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectBuilding', param);
  }

  updateProjectBuilding(param: ProjectBuildingDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'/building');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'building', param);
  }  

  deleteProjectBuilding(param: ProjectBuildingDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteBuilding');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteBuilding', param);
  }
}
