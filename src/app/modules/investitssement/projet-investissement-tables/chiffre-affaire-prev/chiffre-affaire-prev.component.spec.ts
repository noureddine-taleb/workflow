import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ChiffreAffairePrevComponent } from "./chiffre-affaire-prev.component";

describe("ChiffreAffairePrevComponent", () => {
	let component: ChiffreAffairePrevComponent;
	let fixture: ComponentFixture<ChiffreAffairePrevComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ChiffreAffairePrevComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ChiffreAffairePrevComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
