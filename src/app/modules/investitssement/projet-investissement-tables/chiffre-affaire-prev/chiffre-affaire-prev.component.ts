import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { ProjectCaDto } from './dto/project-ca-dto';
import { ProjectCaService } from './service/project-ca.service';

@Component({
    selector: 'app-chiffre-affaire-prev',
    templateUrl: './chiffre-affaire-prev.component.html',
    styleUrls: ['./chiffre-affaire-prev.component.scss']
})
export class ChiffreAffairePrevComponent implements OnInit {
    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    projectCaDto:ProjectCaDto={};
    listprojectCaDto:ProjectCaDto []=[{}];
    listItems=[]
    dataSource = new MatTableDataSource(this.listItems);


    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'exercice',libelle:'Exercice',required:false,type:'input'},
        {nom:'caLocal',libelle:'C.A. Local',required:false,type:'currency'},
        {nom:'caExport',libelle:'C.A. Export',required:false,type:'currency'},
        {nom:'caTotal',libelle:'TOTAL',required:false,type:'currency'}
    ]

    constructor(private projectCaService: ProjectCaService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
    ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getList();
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }

    getList(): void {
        console.log(this.tokenService.getToken('gt'));

        this.projectCaService.getList({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                //if (data.status === 200) {
                    this.listprojectCaDto=data.result;
                    this.dataSource =new MatTableDataSource(this.listprojectCaDto);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }


    onValideAdd(){
        this.projectCaDto.userId         = this.tokenService.user.userId;
        this.projectCaDto.entityId       = this.tokenService.user.entityId;
        this.projectCaDto.userEntityId   = this.tokenService.user.userEntityId;
        this.projectCaDto.projectId      = this.project.projectId;

        this.projectCaService.update(this.projectCaDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.projectCaDto={};
                        this.getList();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                });
    }

    onUpdate(element){
        this.projectCaDto=element;
        this.editState=1;
    }
    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.projectCaService.delete(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getList();
                                /*} else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }


}
