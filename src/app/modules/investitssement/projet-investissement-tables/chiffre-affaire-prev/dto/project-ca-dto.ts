export interface ProjectCaDto {
    exercice?: number;
    caLocal?: number;
    caExport?: number;

    amortization?: number;
    amortizationCmt?: number;
    caTotal?: number;
    cashFlow?: number;
    corporationTax?: number;    
    externalCharges?: number;
    financialExpenses?: number;
    grossMargin?: number;
    investissements?: number;
    netProfit?: number;
    othersExpenses?: number;
    personnelExpense?: number;
    pretaxResulat?: number;
    previsionId?: number;
    purchasesAmount?: number;
    rateForecast?: number;
    taxes?: number;
    tri?: number;
    van?: number;

    projectId?: number;
    entityId?: number;
    userEntityId?: number;
    userId?: number;
        
}