import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { ProjectCaDto } from '../dto/project-ca-dto';


@Injectable({
  providedIn: 'root'
})
export class ProjectCaService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getList(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectCA');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectCA', param);
  }

  update(param: ProjectCaDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'ca');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'ca', param);
  }  

  delete(param: ProjectCaDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteCA');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteCA', param);
  }
}
