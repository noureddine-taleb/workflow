export interface ProjectResourceDto {
    effective?: number;
    entityId?: number;
    job?: string;
    mainMission?: string;
    permanent?: string;
    projectId?: number;
    qualification?: string;
    resourceId?: number;
    total?: number;
    userEntityId?: number;
    userId?: number;
}