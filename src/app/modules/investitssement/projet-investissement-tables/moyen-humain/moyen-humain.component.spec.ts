import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MoyenHumainComponent } from "./moyen-humain.component";

describe("MoyenHumainComponent", () => {
	let component: MoyenHumainComponent;
	let fixture: ComponentFixture<MoyenHumainComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [MoyenHumainComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(MoyenHumainComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
