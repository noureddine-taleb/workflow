import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { ProjectResourceDto } from './dto/project.resource.dto';
import { MoyenHumainService } from './service/moyen.humain.service';

@Component({
    selector: 'app-moyen-humain',
    templateUrl: './moyen-humain.component.html',
    styleUrls: ['./moyen-humain.component.scss']
})
export class MoyenHumainComponent implements OnInit {

    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    moyenHumainDto: ProjectResourceDto={};

    listItems=[];
    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'Poste',libelle:'Poste',required:false,type:'input'},
        {nom:'type_emploi',libelle:'Occasionnel/ Permanent',required:false,type:'input'},
        {nom:'Principales_missions',libelle:'Principales missions',required:false,type:'input'},
        {nom:'Qualification',libelle:'Qualification',required:false,type:'input'},
        {nom:'Effectif',libelle:'Effectif',required:false,type:'number'},
        {nom:'Total',libelle:'Total',required:false,type:'number'}
    ]
    dataSource = new MatTableDataSource(this.listItems);
    constructor(private moyenHumainService: MoyenHumainService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getListProjectBuilding();
    }

    getListProjectBuilding(): void {
        console.log(this.tokenService.getToken('gt'));

        this.moyenHumainService.getMoyenHumain({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                //if (data.status === 200) {
                    this.dataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }
    onValideAdd(){
        this.moyenHumainDto.userId         = this.tokenService.user.userId;
        this.moyenHumainDto.entityId       = this.tokenService.user.entityId;
        this.moyenHumainDto.userEntityId   = this.tokenService.user.userEntityId;
        this.moyenHumainDto.projectId      = this.project.projectId;

        this.moyenHumainService.updateMoyenHumain(this.moyenHumainDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.moyenHumainDto={};
                        this.getListProjectBuilding();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.moyenHumainService.deleteMoyenHumain(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getListProjectBuilding();
                               /* } else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getConcurrentFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }
}
