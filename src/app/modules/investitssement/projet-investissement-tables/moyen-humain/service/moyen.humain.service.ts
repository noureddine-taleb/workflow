import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { ProjectResourceDto } from '../dto/project.resource.dto';


@Injectable({
  providedIn: 'root'
})
export class MoyenHumainService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getMoyenHumain(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectResource');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectResource', param);
  }

  updateMoyenHumain(param: ProjectResourceDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'resource');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'resource', param);
  }  

  deleteMoyenHumain(param: ProjectResourceDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteResource');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteResource', param);
  }
}
