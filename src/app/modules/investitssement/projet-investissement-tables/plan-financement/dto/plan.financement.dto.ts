export interface PlanFinancementDto {
    amount?: string;
    description?: string;    
    nature?: string;  
    
    
    projectFinId?: number;
    productId?: string;
    entityId?: number;
    projectId?: number;
    userEntityId?: number;
    userId?: number;
}