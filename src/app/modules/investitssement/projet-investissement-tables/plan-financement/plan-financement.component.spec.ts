import { ComponentFixture, TestBed } from "@angular/core/testing";
import { PlanFinancementComponent } from "./plan-financement.component";

describe("PlanFinancementComponent", () => {
	let component: PlanFinancementComponent;
	let fixture: ComponentFixture<PlanFinancementComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [PlanFinancementComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(PlanFinancementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
