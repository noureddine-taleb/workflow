import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { PlanFinancementDto } from './dto/plan.financement.dto';
import { PlanFinancementService } from './service/plan.financement.service';

@Component({
    selector: 'app-plan-financement',
    templateUrl: './plan-financement.component.html',
    styleUrls: ['./plan-financement.component.scss']
})
export class PlanFinancementComponent implements OnInit {

    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    planFinancementDto: PlanFinancementDto={};


    listItems=[];
    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'Ressources',libelle:'Rubrique',required:false,type:'input'},
        {nom:'Montant',libelle:'Montant HT',required:false,type:'currency'},
        {nom:'Desription',libelle:'Justificatifs(Réf,fournisseur,date)',required:false,type:'input'},
    ]
    dataSource = new MatTableDataSource(this.listItems);
    constructor(private planFinancementService: PlanFinancementService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getList()
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }

    getList(): void {
        this.planFinancementService.getPlanFinancement({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                //if (data.status === 200) {
                    this.dataSource =new MatTableDataSource(data.result);
                /*} else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });*/
            });
    }


    onValideAdd(){
        this.planFinancementDto.userId         = this.tokenService.user.userId;
        this.planFinancementDto.entityId       = this.tokenService.user.entityId;
        this.planFinancementDto.userEntityId   = this.tokenService.user.userEntityId;
        this.planFinancementDto.projectId      = this.project.projectId;

        this.planFinancementService.updatePlanFinancement(this.planFinancementDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    //if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.planFinancementDto={};
                        this.getList();
                    /*} else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.planFinancementService.deletePlanFinancement(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                //if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getList();
                               /* } else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }


}
