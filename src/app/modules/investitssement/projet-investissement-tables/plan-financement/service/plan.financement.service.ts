import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { PlanFinancementDto } from '../dto/plan.financement.dto';


@Injectable({
  providedIn: 'root'
})
export class PlanFinancementService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getPlanFinancement(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectFunding');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectFunding', param);
  }

  updatePlanFinancement(param: PlanFinancementDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'funding');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'funding', param);
  }  

  deletePlanFinancement(param: PlanFinancementDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteFunding');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteFunding', param);
  }
}
