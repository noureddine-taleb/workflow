export interface ProgrammeInvestissementDto {
    advancement?: string;
    amount?: string;
    amountCommitted?: string;
    amountTtc?: string;
    description?: string;
    entityId?: number;
    nature?: string;
    percentage?: string;
    productId?: string;
    
    programId?: number;
    projectId?: number;
    userEntityId?: number;
    userId?: number;
}