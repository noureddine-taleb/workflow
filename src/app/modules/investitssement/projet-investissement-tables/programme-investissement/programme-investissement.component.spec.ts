import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ProgrammeInvestissementComponent } from "./programme-investissement.component";

describe("ProgrammeInvestissementComponent", () => {
	let component: ProgrammeInvestissementComponent;
	let fixture: ComponentFixture<ProgrammeInvestissementComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ProgrammeInvestissementComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ProgrammeInvestissementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
