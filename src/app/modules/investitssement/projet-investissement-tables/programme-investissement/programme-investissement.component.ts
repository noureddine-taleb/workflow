import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { ProgrammeInvestissementDto } from './dto/programme.investissement.dto';
import { ProgrammeInvestissementService } from './service/programme-investissement.service';

@Component({
    selector: 'app-programme-investissement',
    templateUrl: './programme-investissement.component.html',
    styleUrls: ['./programme-investissement.component.scss']
})
export class ProgrammeInvestissementComponent implements OnInit {

    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    programmeInvestissementDto:ProgrammeInvestissementDto={};

    listItems=[];
    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'description',libelle:'Description',required:false,type:'input'},
        {nom:'Montant_HT',libelle:'Montant HT',required:false,type:'currency'},
        {nom:'amountTtc',libelle:'Montant TTC',required:false,type:'currency'},
        {nom:'nature',libelle:'Nature',required:false,type:'input'},
        {nom:'advancement',libelle:'Avancement( dans le cas des travaux engagé par fonds propres)',required:false,type:'input'},
        {nom:'amountCommitted',libelle:'Montant engagé(dans le cas des travaux engagé par fonds propres)',required:false,type:'currency'},
        {nom:'percentage',libelle:'% de réalisation(dans le cas des travaux engagé par fonds propres)',required:false,type:'number'}
    ]
    dataSource = new MatTableDataSource(this.listItems);
    constructor(private programmeInvestissementService: ProgrammeInvestissementService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getListProjectBuilding();
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }
    getListProjectBuilding(): void {
        console.log(this.tokenService.getToken('gt'));

        this.programmeInvestissementService.getProjectBuilding({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                if (data.status === 200) {
                    this.dataSource =new MatTableDataSource(data.result);
                } else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });
            });
    }


    onValideAdd(){
        this.programmeInvestissementDto.userId         = this.tokenService.user.userId;
        this.programmeInvestissementDto.entityId       = this.tokenService.user.entityId;
        this.programmeInvestissementDto.userEntityId   = this.tokenService.user.userEntityId;
        this.programmeInvestissementDto.projectId      = this.project.projectId;

        this.programmeInvestissementService.updateProjectBuilding(this.programmeInvestissementDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.programmeInvestissementDto={};
                        this.getListProjectBuilding();
                    } else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.programmeInvestissementService.deleteProjectBuilding(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getListProjectBuilding();
                                } else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getConcurrentFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }
}
