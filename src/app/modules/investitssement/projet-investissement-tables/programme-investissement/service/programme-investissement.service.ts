import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { ProgrammeInvestissementDto } from '../dto/programme.investissement.dto';


@Injectable({
  providedIn: 'root'
})
export class ProgrammeInvestissementService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getProjectBuilding(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectProgram');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectProgram', param);
  }

  updateProjectBuilding(param: ProgrammeInvestissementDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'program');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'program', param);
  }  

  deleteProjectBuilding(param: ProgrammeInvestissementDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteProgram');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteProgram', param);
  }
}
