export interface SupportDto {
    adress?: string;
    affectedArea?: string;
    entityId?: number;
    exploitationCondition?: string;
    exploitationProof?: string;
    globalArea?: string;
    projectId?: number;
    refProof?: string;
    supportId?: number;
    userEntityId?: number;
    userId?: number;
}