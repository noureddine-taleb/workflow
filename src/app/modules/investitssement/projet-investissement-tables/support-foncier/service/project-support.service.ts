import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { SupportDto } from '../dto/support-dto';


@Injectable({
  providedIn: 'root'
})
export class ProjectSupportService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getSupport(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getSupport');
    console.log(param);
    return this.httpClient.post(this.demandesPath +this.baseUrl+  'getSupport', param);
  }

  updateSupport(param: SupportDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'/support');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'support', param);
  }  

  deleteSupport(param: SupportDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteSupport');
    console.log(param);
    return this.httpClient.post(this.demandesPath +this.baseUrl+'deleteSupport', param);
  }
}
