import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SupportFoncierComponent } from "./support-foncier.component";

describe("SupportFoncierComponent", () => {
	let component: SupportFoncierComponent;
	let fixture: ComponentFixture<SupportFoncierComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [SupportFoncierComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SupportFoncierComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
