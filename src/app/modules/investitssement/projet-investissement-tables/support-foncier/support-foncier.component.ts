import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { SupportDto } from './dto/support-dto';
import { ProjectSupportService } from './service/project-support.service';

@Component({
    selector: 'app-support-foncier',
    templateUrl: './support-foncier.component.html',
    styleUrls: ['./support-foncier.component.scss']
})
export class SupportFoncierComponent implements OnInit {

    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    supportDto:SupportDto={};

    listItems=[];
    item;
    displayedColumnName=[];
    displayedColumn=[
        {nom:'ref_justif',libelle:'Ref.justif',required:false,type:'input'},
        {nom:'superficie_glob',libelle:'Superficie globale(m²)',required:false,type:'number'},
        {nom:'superficie_concerne',libelle:'Superficie concernée(m²)',required:false,type:'number'},
        {nom:'adresse_sit',libelle:'Adresse/situation',required:false,type:'input'},
        {nom:'justif_expl',libelle:'Justificatif d\'exploitation',required:false,type:'input'},
        {nom:'condition_expl',libelle:'Conditions d\'exploitation',required:false,type:'input'}
    ]
    dataSource = new MatTableDataSource(this.listItems);
    constructor(private projectSupportService: ProjectSupportService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) { }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getListSupport();
    }

    getListSupport(): void {
        this.projectSupportService.getSupport({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);

                if (data.status === 200) {
                    this.dataSource =new MatTableDataSource(data.result);
                } else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                this.snackeBarService.openErrorSnackBar({ message: error.error });
            });
    }

    getDisplayedColumn(){
        if(this.displayedColumnName.length==0){
            this.displayedColumn.forEach(x=>this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }
    editState=0;
    onPrepareAdd(){
        this.editState=1;
        this.item = new Object();
    }
    onRetour(){
        this.editState=0;
    }

    onValideAdd(){
        this.supportDto.userId         = this.tokenService.user.userId;
        this.supportDto.entityId       = this.tokenService.user.entityId;
        this.supportDto.userEntityId   = this.tokenService.user.userEntityId;
        this.supportDto.projectId      = this.project.projectId;

        this.projectSupportService.updateSupport(this.supportDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.supportDto={};
                        this.getListSupport();
                    } else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.projectSupportService.deleteSupport(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getListSupport();
                                } else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getFooterCols() {
        if (this.editState==0) return [];
        return this.displayedColumnName;
    }


}
