import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { EntrepriseCriteria } from 'app/modules/investitssement/projet-investissement/dto/entreprise-criteria';
import { Observable } from 'rxjs';
import { ProjectCaDto } from '../../chiffre-affaire-prev/dto/project-ca-dto';


@Injectable({
  providedIn: 'root'
})
export class TauxUtilisationPrevService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(private httpClient: HttpClient,) { }

  getList(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'getProjectTA');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +  'getProjectTA', param);
  }

  update(param: ProjectCaDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'ca');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl + 'rate', param);
  }  

  delete(param: ProjectCaDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'deleteRate');
    console.log(param);
    return this.httpClient.post(this.demandesPath+this.baseUrl +'deleteRate', param);
  }
}
