import { ComponentFixture, TestBed } from "@angular/core/testing";
import { TauxUtilisationPrevComponent } from "./taux-utilisation-prev.component";

describe("TauxUtilisationPrevComponent", () => {
	let component: TauxUtilisationPrevComponent;
	let fixture: ComponentFixture<TauxUtilisationPrevComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [TauxUtilisationPrevComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TauxUtilisationPrevComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
