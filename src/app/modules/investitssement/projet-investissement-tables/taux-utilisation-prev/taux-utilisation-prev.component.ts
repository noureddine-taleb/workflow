import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { UtilsService } from 'app/core/services/utils.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { ProjectRequestDto } from '../../projet-investissement/dto/project-request-dto';
import { ProjectCaDto } from '../chiffre-affaire-prev/dto/project-ca-dto';
import { TauxUtilisationPrevService } from './service/project-ca.service';

@Component({
    selector: 'app-taux-utilisation-prev',
    templateUrl: './taux-utilisation-prev.component.html',
    styleUrls: ['./taux-utilisation-prev.component.scss']
})
export class TauxUtilisationPrevComponent implements OnInit {
    @Input() demandeDTO;
    @Input() project: ProjectRequestDto;

    projectCaDto:ProjectCaDto={};
    listprojectCaDto:ProjectCaDto []=[{}];
    listItems=[]
    dataSource = new MatTableDataSource(this.listItems);

    item;
    displayedColumnName = [];
    displayedColumn = [
        {nom:'exercice',libelle:'Exercice',required:false,type:'input'},
        {nom:'rateForecast',libelle:'Taux',required:false,type:'number'}
    ]

    constructor(private tauxUtilisationPrevService: TauxUtilisationPrevService,
                private tokenService: AuthService,
                private snackeBarService: SnackBarService,
                private utilsService: UtilsService,
                ) {
    }

    ngOnInit(): void {
        this.getDisplayedColumn();
        this.getList();
    }

    getDisplayedColumn() {
        if (this.displayedColumnName.length == 0) {
            this.displayedColumn.forEach(x => this.displayedColumnName.push(x.nom))
        }
        this.displayedColumnName.push('action')
    }

    editState = 0;

    onPrepareAdd() {
        this.editState = 1;
        this.item = new Object();
    }

    onRetour() {
        this.editState = 0;
    }

    getList(): void {
        console.log(this.tokenService.getToken('gt'));

        this.tauxUtilisationPrevService.getList({
            requestId: this.demandeDTO.requestId,
            projectId: this.project.projectId,
            userEntityId: this.tokenService.user.entityId,
            userId: this.tokenService.user.userId
        }).subscribe((data) => {
                console.log(data);
                if (data.status === 200) {
                    this.listprojectCaDto=data.result;
                    this.dataSource =new MatTableDataSource(this.listprojectCaDto);
                } else {
                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                }
            },
            (error) => {
                console.log(error);
                this.snackeBarService.openErrorSnackBar({ message: error.error });
            });
    }

    onUpdate(element){
        this.projectCaDto=element;
        this.editState=1;
    }
    onValideAdd(){
        this.projectCaDto.userId         = this.tokenService.user.userId;
        this.projectCaDto.entityId       = this.tokenService.user.entityId;
        this.projectCaDto.userEntityId   = this.tokenService.user.userEntityId;
        this.projectCaDto.projectId      = this.project.projectId;

        this.tauxUtilisationPrevService.update(this.projectCaDto)
            .subscribe(
                (data) => {
                    console.log(data);
                    if (data.status === 200) {
                        this.snackeBarService.openSuccesSnackBar({ message: data.message });
                        this.editState=0;
                        this.projectCaDto={};
                        this.getList();
                    } else {
                        this.snackeBarService.openErrorSnackBar({ message: data.message });
                    }
                }, (error) => {
                    this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                });
    }

    onDelete(element){
        this.snackeBarService.openConfirmSnackBar({ message: "all" })
            .then(result => {
                if (result.value) {
                    element.userId         = this.tokenService.user.userId;
                    element.entityId       = this.tokenService.user.entityId;
                    element.userEntityId   = this.tokenService.user.userEntityId;
                    element.projectId      = this.project.projectId;

                    this.tauxUtilisationPrevService.delete(element)
                        .subscribe(
                            (data) => {
                                console.log(data);
                                if (data.status === 200) {
                                    this.snackeBarService.openSuccesSnackBar({ message: data.message });
                                    this.getList();
                                } else {
                                    this.snackeBarService.openErrorSnackBar({ message: data.message });
                                }
                            }, (error) => {
                                this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });
                            });
                }
            });
    }

    canAdd() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }

    getFooterCols() {
        if (this.editState == 0) return [];
        return this.displayedColumnName;
    }
}
