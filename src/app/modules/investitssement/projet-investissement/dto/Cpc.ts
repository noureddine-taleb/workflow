export class Cpc {
	amortization: number;
	amortizationcmt: number;
	amountIs: number;
	cashFlow: number;
	cpcId: number;
	entityId: number;
	exercice: number;
	externalCharges: number;
	financialCosts: number;
	grossMargin: number;
	incomeTaxes: number;
	investment: number;
	netResults: number;
	otherCharges: number;
	personalCharges: number;
	personalExpenses: number;
	projectId: number;
	purchases: number;
	rawResults: number;
	requestId: number;
	totalCharge: number;
	turnover: number;
	tri: number;
	van: number;
}