export interface EntrepriseCriteria {
    accountId?: number;
    entityId?: number;
    first?: number;
    pas?: number;
    presentationId?: number;
    projectId?: number;
    requestId?: number;
    requeteId?: number;
    type?: string;
    userEntityId?: number;
    userId?: number;
}