export interface ProjectRequestDto {
    actualDelay?: string;
    adress?: string;
    city?: string;
    comment?: string;
    competitiveness?: string;
    entityId?: number;
    innovantive?: string;
    planning?: string;
    productionCapacity?: string;
    profitabilityIndex?: string;
    program?: string;
    projectId?: number;
    province?: string;
    region?: string;
    regionData?: string;
    requestId?: number;
    sectorArea?: string;
    startDate?: Date;
    strategyOrganization?: string;
    technicalStudy?: string;
    title?: string;
    userEntityId?: number;
    userId?: number;
    projectProgram?: string;
}