import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ProjetInvestissementComponent } from "./projet-investissement.component";

describe("ProjetInvestissementNotDynComponent", () => {
	let component: ProjetInvestissementComponent;
	let fixture: ComponentFixture<ProjetInvestissementComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ProjetInvestissementComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ProjetInvestissementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
