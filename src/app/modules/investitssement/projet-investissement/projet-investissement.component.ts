import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { CheckRubriquesDialogComponent } from 'app/modules/demandes/check-rubriques-dialog/check-rubriques-dialog.component';
import { DemandeDocsAttchComponent } from 'app/shared/demande-docs-attch/demande-docs-attch.component';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { ProjectRequestDto } from './dto/project-request-dto';
import { ProjetInvestissementService } from './services/projet-investissement.service';
import { UtilsService } from 'app/core/services/utils.service';
import { PrivilegeCode } from 'app/modules/demandes/models/PrivilegeCode';
import { Cpc } from './dto/Cpc';
import { Rubrique } from '../investissement.types';
import { CacheService } from 'app/core/services/cache.service';

@Component({
    selector: 'app-projet-investissement',
    templateUrl: './projet-investissement.component.html',
    styleUrls: ['./projet-investissement.component.scss']
})
export class ProjetInvestissementComponent implements OnInit {
    @Input() requestId;
    @Input() demandeDTO;

    projectRequestDto: ProjectRequestDto ={}
    listRubs: Rubrique[];

    constructor(private dialog:MatDialog,
                private authService: AuthService,
                private projetInvestissementService: ProjetInvestissementService,
                private snackeBarService: SnackBarService,
                private demandeService: DemandeService,
                private utilsService: UtilsService,
                private cacheService: CacheService,
            ) { }

    ngOnInit(): void {
        this.requestId  = this.demandeService.request.requestId;
        this.demandeDTO = this.demandeService.request;
        this.getproject();
        this.listRubs = this.getRubs();
    }

    getRubs(): Rubrique[] {
        let cachedRubs = this.getCachedRubs();
        if (cachedRubs)
            return cachedRubs;

        return [
            {nom:'champ1',libelle:'Aperçu sur le secteur d\'activité',checked:true},
            {nom:'champ2',libelle:'Données sur la région/localité d\'insertion du projet',checked:true},
            {nom:'champ3',libelle:'Stratégie et mode d\'organisation',checked:true},
            {nom: 'champ4',libelle: 'Planning de réalisation',checked:true},
            {nom: 'champ5',libelle: 'Etude technique du projet',checked:true},
            {nom: 'champ6',libelle: 'Compétitivité du projet',checked:true},
            {nom: 'champ7',libelle: 'Capacité de production',checked:true},
            {nom:'volet1',libelle:'Bâtiments et constructions',checked:true},
            {nom:'volet2',libelle:'Support foncier / justificatif d\'exploitation',checked:true},
            {nom:'volet3',libelle:'Programme d\'investissement',checked:true},
            {nom:'volet4',libelle:'Plan de financement',checked:true},
            {nom:'volet5',libelle:'Moyens humain prévisionnels(emplois à créer sur 5 ans)',checked:true},
            {nom:'volet6',libelle:'Chiffre d\'affaire prévisionnel',checked:true},
            {nom:'volet7',libelle:'Taux d\'utilisation prévisionnel',checked:true},
            {nom:'volet8',libelle:'Comptes d\'exploitation prévisionnel',checked:true}
        ]
    }

    getCachedRubs() {
        let key = `${this.demandeDTO.requestId}-projet-investissement`;
        return this.cacheService.fetch<Rubrique[]>(key);
    }

    setCachedRubs(rubriques: Rubrique[]) {
        let key = `${this.demandeDTO.requestId}-projet-investissement`;
        return this.cacheService.store(key, rubriques);
    }

    onAddRubrique() {
        const dialogRef = this.dialog.open(CheckRubriquesDialogComponent, {
            width: '50vw',data:this.listRubs,disableClose:true
        });

        dialogRef.afterClosed().subscribe(result => {
            this.listRubs = result;
            this.setCachedRubs(result);
        });
    }

    afficherVolet(nomVolet: string) {
        const item=this.listRubs.filter(x=>x.nom==nomVolet);
        if(item.length>0){
            return item[0].checked
        }
        return true;
    }

    onAddDocument() {
        const dialogRef = this.dialog.open(DemandeDocsAttchComponent, {
            width: '50vw',data: {
                lisDocs:[
                    {name: 'CIN', checked: false}]
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: `,result);
        });
    }

    onValidateProjet(){
        this.projectRequestDto.userId = this.authService.user.userId;
        this.projectRequestDto.entityId = this.authService.user.entityId;
        this.projectRequestDto.userEntityId = this.authService.user.userEntityId;
        this.projectRequestDto.requestId = this.requestId,

            this.projetInvestissementService.updateProject(this.projectRequestDto)
                .subscribe((data) => {
                        console.log(data);
                        //if (data.status === 200) {
                            this.snackeBarService.openSuccesSnackBar({ message: data.message });
                            this.getproject();
                        /*} else {
                            this.snackeBarService.openErrorSnackBar({ message: data.message });
                        }
                    },
                    (error) => {
                        this.snackeBarService.openErrorSnackBar({ message: JSON.stringify(error) });*/
                    });
    }

    cpc: Cpc = {} as any;
    getproject(){
        this.projetInvestissementService.getProject({
            entityId: this.authService.user.entityId,
            first: 0,
            pas: 10,
            requestId: this.requestId,
            userEntityId: this.authService.user.entityId,
            userId: this.authService.user.userId
        })
        .subscribe((data) => {
            this.projectRequestDto = data.result;
            if(!this.projectRequestDto){
                this.projectRequestDto={};
            }
            this.projetInvestissementService.getCpc(this.projectRequestDto.projectId).subscribe(data => {
                this.cpc = data.result || {} as any;
            })
        });
    }

    saveCpc() {
        this.projetInvestissementService.saveCpc({...this.cpc, projectId: this.projectRequestDto.projectId}).subscribe(res => {
            this.snackeBarService.openSuccesSnackBar({
                message: res.message
            })
        })
    }

    deleteCpc() {
        this.projetInvestissementService.deleteCpc(this.cpc).subscribe(res => {
            this.snackeBarService.openSuccesSnackBar({
                message: res.message
            })

            this.cpc = {} as any;
        })
    }

    canEdit() {
        return this.utilsService.hasPrivilege(PrivilegeCode.maj_investissement_m);
    }
}
