import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from 'app/core/config/config.service';
import { ApiResponse } from 'app/core/models/ApiResponse';
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from 'rxjs';
import { Cpc } from '../dto/Cpc';
import { EntrepriseCriteria } from '../dto/entreprise-criteria';
import { ProjectRequestDto } from '../dto/project-request-dto';

@Injectable({
  providedIn: 'root'
})
export class ProjetInvestissementService {
  baseUrl='/creditEntreprise/';
  private demandesPath = ConfigService.settings.apiServer.restWKFUrl ;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
    ) { }

  /*---------------------- Presentation ------------------------------ */
  updateProject(param: ProjectRequestDto): Observable<any>{
    console.log('Service :'+this.baseUrl+'project');
    console.log(param);

    return this.httpClient.post(this.demandesPath + this.baseUrl+'project', param);
  }

  getProject(param: EntrepriseCriteria): Observable<any>{
    console.log('Service :'+this.baseUrl+'/getProjectRequest');
    console.log(param);
    
    return this.httpClient.post(this.demandesPath +  this.baseUrl+'getProjectRequest', param);
  }
  /*---------------------- Presentation ------------------------------ */
  
	getCpc(projectId: number) {
		return this.httpClient.post<ApiResponse<Cpc>>(this.demandesPath + this.baseUrl + "getCpc", {
      userId: this.authService.user.userId,
      userEntityId: this.authService.user.userEntityId,
      entityId: this.authService.user.entityId,
      projectId
    });
	}
  
	saveCpc(cpc: Cpc) {
		return this.httpClient.post<ApiResponse<Cpc>>(this.demandesPath + this.baseUrl + "cpc", {
      userId: this.authService.user.userId,
      userEntityId: this.authService.user.userEntityId,
      entityId: this.authService.user.entityId,
      ...cpc
    });
	}

  deleteCpc(cpc: Cpc) {
		return this.httpClient.post<ApiResponse<Cpc>>(this.demandesPath + this.baseUrl + "deleteCPC", {
      userId: this.authService.user.userId,
      userEntityId: this.authService.user.userEntityId,
      entityId: this.authService.user.entityId,
      ...cpc
    });
	}
}
