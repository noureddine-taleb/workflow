import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../../../layout/services/snackbar.service';
import {AuthService} from '../../../auth/auth.service';
import {CommonService} from '../../../../core/services/common.service';
import {DemandeService} from '../../../demandes/services/demande.service';
import {SimulationGtService} from '../../../simulation/services/simulation.gt.service';
import {RoutingService} from '../../../../core/services/routing.service';
import {Account} from '../../../../shared/models/Account';
import {SelectModel} from '../../../simulation/simulation-form/simulation-form.component';
import {MatSelectChange} from '@angular/material/select';
import {RequestType} from '../../../../shared/models/RequestType';
import {DemandeIstidama} from '../../dto/demande-istidama';
import {PersonIstidama} from '../../dto/person-istidama';
import {ProductIstidama} from '../../dto/product-istidama';
import {AddProductIstidamaComponent} from '../add-product-istidama/add-product-istidama.component';

@Component({
  selector: 'app-add-demande-istidama',
  templateUrl: './add-demande-istidama.component.html',
  styleUrls: ['./add-demande-istidama.component.scss']
})
export class AddDemandeIstidamaComponent implements OnInit,OnDestroy {

    demandeIstidama: DemandeIstidama = {};
    subscriptions = new Subscription();
    tiers: PersonIstidama[] = [];
    familleSegments = [];
    segments = [];
    filteredSegments = [];
    listProducts: ProductIstidama[] = [];
    displayedColumns=['produit','montant','duree','taux','differe','periodicite'/*,'mensualite','assurance'*/,'action'];
    displayedColumnsLib=['Produit','Montant','Durée','Taux','Différé','Périodicité'/*,'Mensualité','Assurance'*/];
    compte: Account;
    listCateories = [];
    listTypeProduct = [];

    constructor(
        private dialog: MatDialog,
        private snackeBarService: SnackBarService,
        private authService: AuthService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private routingService: RoutingService,
    ) {}
    ngOnInit(): void {}

    retour(): void {
        this.routingService.gotoIstidamaRequests();
    }

    setSegmentName(person, $event): void {
        person.segmentName = this.filteredSegments.find(s => s.id == $event.value)?.designation;
    }

    onAddProduct(): void {
        const dialogRef = this.dialog.open(AddProductIstidamaComponent, {
            data: { persons: this.tiers },
            width: '50vw',
        });
        dialogRef.afterClosed().subscribe((data) => {
            if (data?.product) {this.listProducts = this.listProducts.concat(data?.product);}
        });
    }

    onDelete(product): void {
        this.snackeBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                this.listProducts = this.listProducts.filter(p => p != product);
            }
        });
    }

    onUpdate(product): void {
        this.dialog.open(AddProductIstidamaComponent, {
            data: { persons: this.tiers, product },
            width: '50vw',
        });
    }

    getAccount(account: Account): void {
        this.compte = account;
        this.getTiers();
        this.getSegFamilly();
        this.getSegments();
    }

    getSegFamilly(): void {
        this.simulationService.getList('segmentsType').subscribe((data) => {
            this.familleSegments = data['result'];
        });
    }

    getSegments(): void {
        this.simulationService.getListByPost('segements', new SelectModel()).subscribe((data) => {
            this.segments = data['result'];
            this.filteredSegments = this.segments;
        });
    }

    filtrerSegment($event: MatSelectChange, person: any): void {
        person.segment = null;
        this.filteredSegments = this.segments.filter(x => x.parentCode === $event.value);
    }

    initPersons(): void {
        for (const person of this.tiers) {
            person.monthlyPaymentConsoCam || (person.monthlyPaymentConsoCam = 0);
            person.monthlyPaymentConsoConf || (person.monthlyPaymentConsoConf = 0);
            person.monthlyPaymentImmoCam || (person.monthlyPaymentImmoCam = 0);
            person.monthlyPaymentImmoConf || (person.monthlyPaymentImmoConf = 0);
            person.monthlyPay || (person.monthlyPay = 0);
            person.othersMonthlyIncome || (person.othersMonthlyIncome = 0);
        }
    }

    getTiers(): void {
        this.commonService
            .getListPerson({
                clientId: (this.compte as any).clientId,
            })
            .subscribe((data) => {
                (this.tiers = data['result']), this.initPersons();
            });
    }

    onCreateIstidamaDemande(): void {
        if (!this.isReadyToBeSaved()) {
            this.snackeBarService.openErrorSnackBar({ message: 'certaines informations manquent' });
            return;
        }
        this.demandeIstidama.accountId = this.compte.accountId;
        this.demandeIstidama.products = this.listProducts;
        this.demandeIstidama.creditType = RequestType.MAZAYA;
        this.demandeIstidama.persons = this.tiers;
        for (const person of this.tiers) {
            person.monthlyPaymentConso  =   person.monthlyPaymentConsoCam + person.monthlyPaymentConsoConf;
            person.monthlyPaymentImmo   =   person.monthlyPaymentImmoCam + person.monthlyPaymentImmoConf;
        }

        this.demandeService.createDemandeIstidama(this.demandeIstidama).subscribe((data) => {
            this.routingService.gotoTask(data['result']);
        });
    }

    isTierInfoSaved(): boolean {
        let ret = true;
        for (const person of this.tiers) {
            if (
                person.segment &&
                person.segmentType &&
                person.monthlyPay &&
                (person.othersMonthlyIncome || person.othersMonthlyIncome === 0) &&
                (person.monthlyPaymentImmoCam || person.monthlyPaymentImmoCam === 0) &&
                (person.monthlyPaymentImmoConf || person.monthlyPaymentImmoConf === 0) &&
                (person.monthlyPaymentConsoCam || person.monthlyPaymentConsoCam === 0) &&
                (person.monthlyPaymentConsoConf || person.monthlyPaymentConsoConf === 0)
            )
            {continue;}
            ret = false;
            break;
        }
        return ret;
    }

    isReadyToBeSaved(): boolean {
        return !!(
            this.compte &&
            this.isTierInfoSaved() &&
            this.demandeIstidama.objectCredit &&
            this.listProducts?.length
        );
    }

    ngOnDestroy(): void{
        this.subscriptions.unsubscribe();
    }
}
