import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SnackBarService} from '../../../../layout/services/snackbar.service';
import {CommonService} from '../../../../core/services/common.service';
import {DemandeService} from '../../../demandes/services/demande.service';
import {SimulationGtService} from '../../../simulation/services/simulation.gt.service';
import {TreeData} from 'mat-tree-select-input';
import {RequestType} from '../../../../shared/models/RequestType';
import {IstidamaService} from "../../services/istidama.service";
import {CodeTarification} from "../../dto/code-tarification";

@Component({
  selector: 'app-add-product-istidama',
  templateUrl: './add-product-istidama.component.html',
  styleUrls: ['./add-product-istidama.component.scss']
})
export class AddProductIstidamaComponent implements OnInit {

    product: any = {};
    productForm: FormGroup;
    listProduitFinancement = [];
    typePrelevement = [];
    listObjetFinancement = [];
    filteredListObjetFinancement = [];
    perdictees = [];
    listCodeTarification: CodeTarification[] = [];
    filtredProductList: any[];
    productList;
    calculateSuccess = false;
    onUpdate = false;

    constructor(
        public dialogRef: MatDialogRef<AddProductIstidamaComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private snackeBarService: SnackBarService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private fb: FormBuilder,
        private istidamaService: IstidamaService
    ) {}

    ngOnInit(): void {
        this.productForm = this.fb.group({
            personId: ['', [Validators.required]],
            lastProduct: ['', [Validators.required]],
            financielObject: ['', []],
            lastAmountGranted: ['', [Validators.required]],
            lastDuration: ['', [Validators.required, Validators.min(0), Validators.max(300)]],
            lastRate: ['', [Validators.required]],
            lastDeferredPeriod: ['', [Validators.required]],
            periodicity: ['', [Validators.required]],
            startDate: ['', [Validators.required]],
            lastCodeTarification: ['', [Validators.required]],
        });

        this.productForm.get('lastProduct').valueChanges.subscribe((_) => {
            this.getFinancialObjectEntreprise();
        });
        this.getProductList();
        this.getTypePrelevement();
        this.getPeriodicity();
        this.getListCodeTarification();
        if (this.data.persons?.length === 1)
        {this.productForm.get('personId').setValue(this.data.persons[0].personId);}
    }

    setProductFormValues(values: any): void {
        this.productForm.patchValue(values);
        this.setFinancialObjectByValue(values?.financielObject, this.listObjetFinancement);
    }

    setFinancialObjectByValue(value: string, data: TreeData[]): boolean {
        for (const fo of data) {
            if (fo.value == value) {
                this.productForm.get('financielObject').setValue(fo);
                return true;
            }
            if (this.setFinancialObjectByValue(value, fo.children)) {return true;}
        }
        return false;
    }

    onClose(): void {
        this.dialogRef.close({ product: [] });
    }

    onValide(): void {
        this.snackeBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                let financielObject = this.productForm.value.financielObject;
                if (financielObject) {
                    financielObject = financielObject.value;
                }
                this.product = { ...this.product, ...this.productForm.value, financielObject };
                this.product.productName = this.listProduitFinancement?.find(
                    pf => pf.id == this.product.lastProduct,
                )?.designation;

                if (this.onUpdate)
                {for (const key of Object.keys(this.product))
                {this.data.product[key] = this.product[key];}}
                this.dialogRef.close({ product: this.product });
            }
        });
    }

    onCalculer(e): void {
        if (this.productForm.invalid) {return;}
        this.calculateSuccess = false;
        this.product = {};
        const critere = {
            amount: this.productForm.get('lastAmountGranted').value,
            duration: this.productForm.get('lastDuration').value,
            rate: this.productForm.get('lastRate').value,
        };
        this.demandeService.calculateProduct(critere).subscribe((data) => {
            this.product.lastInsurance = data['result']['insuranceAmount'];
            this.product.lastMonthlyPayment = data['result']['deadLineAmount'];
            this.calculateSuccess = true;
        });
    }

    onResit(): void {
        this.product = {};
        this.productForm.reset();
        this.calculateSuccess = false;
    }

    getPeriodicity(): void {
        this.commonService.getMensualites().subscribe((data) => {
            this.perdictees = data['result'];
        });
    }

    getListCodeTarification(): void {
        this.istidamaService.getListCodeTarification().subscribe((data) => {
            console.log(data)
            this.listCodeTarification = data['result'];
        });
    }

    getTypePrelevement(): void {
        this.simulationService.getList('debtTypes').subscribe((data) => {
            this.typePrelevement = data['result'];
        });
    }

    getFinancialObjectEntreprise(): void {
        this.commonService.getFinancialObjectEntreprise(this.productForm.get('lastProduct').value).subscribe((data) => {
            this.listObjetFinancement = this.transformFinancialProducts(data['result']);
            this.filteredListObjetFinancement = this.listObjetFinancement;
            if (this.data?.product)
            {this.setProductFormValues(this.data?.product), (this.onUpdate = true);}
        });
    }

    filter(array: TreeData[], text: string): void {
        const getNodes = (result, object) => {
            if (object.name.toLowerCase().startsWith(text)) {
                result.push(object);
                return result;
            }
            if (Array.isArray(object.children)) {
                const children = object.children.reduce(getNodes, []);
                if (children.length) {result.push({ ...object, children });}
            }
            return result;
        };

        this.filteredListObjetFinancement = array.reduce(getNodes, []);
    }

    transformFinancialProducts(arr: any[]) {
        return arr.map((o) => {
            if (o.child) {o.child = this.transformFinancialProducts(o.child);}
            return {
                name: o.financialObjectName,
                value: o.financialObjectCode,
                children: o.child,
            };
        });
    }

    getProductList(): void {
        this.commonService.getproductList(RequestType.ISTIDAMA).subscribe((data) => {
            this.listProduitFinancement = data['result'];
        });
    }

}
