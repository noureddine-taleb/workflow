import { Component, OnInit } from '@angular/core';
import {ReportGeneratorService} from "../../../../shared/reportGenerator/report-generator.service";
import {SnackBarService} from "../../../../layout/services/snackbar.service";
import {DemandeService} from "../../../demandes/services/demande.service";
import {UtilsService} from "../../../../core/services/utils.service";

@Component({
  selector: 'app-istidama-window-request',
  templateUrl: './istidama-window-request.component.html',
  styleUrls: ['./istidama-window-request.component.scss']
})
export class IstidamaWindowRequestComponent implements OnInit {

    projectType: string;
    remark: string;

    constructor(private reportGeneratorService: ReportGeneratorService,
                private snackBarService: SnackBarService,
                public demandeService: DemandeService,
                public utilsService: UtilsService,) { }

    ngOnInit(): void {
    }

    changeStatus(event): void{
        console.log(this.projectType);
        console.log(this.remark);
    }

    onEditAutorisation(): any {
        this.snackBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                const criteria = {};
                criteria['report'] = 'AUTORISATION_DIVULGATION_INFORMATIONS_PERSONNELLES';
                criteria['REQUESTID'] = this.demandeService.request.requestId;
                this.reportGeneratorService.generateReport(criteria).subscribe((data) => {
                    this.reportGeneratorService.convertToPdf(data.result,'AUTORISATION_DIVULGATION_INFORMATIONS_PERSONNELLES') ;
                });
            }
        });
    }

}
