export interface CodeTarification {
    identifiant?: number;
    code?: string;
    libelle?: string;
}
