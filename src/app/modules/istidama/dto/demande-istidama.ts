import { PersonIstidama } from "./person-istidama";
import { ProductIstidama } from "./product-istidama";

export interface DemandeIstidama {
    accountId?: number;
    clientCategory?: number;
    creditType?: number;
    entityId?: number;
    newConstitution?: string;
    objectCredit?: string;
    products?: ProductIstidama[];
    persons?: PersonIstidama[];
    requestId?: number;
    userEntityId?: number;
    userId?: number;
}
