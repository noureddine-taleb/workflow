import {Route, RouterModule, Routes} from '@angular/router';
import {ListDemandeComponent} from '../../shared/list-demande/list-demande.component';
import {RequestType} from '../../shared/models/RequestType';
import {AddDemandeIstidamaComponent} from './components/add-demande-istidama/add-demande-istidama.component';

export const IstidamaRoutes: Route[] = [
    {
        path: '',
        component: ListDemandeComponent,
        data: { title: 'Liste des demandes Istidama', demandeType: RequestType.ISTIDAMA, addAllowed: true },
    },
    {
        path: 'add',
        component: AddDemandeIstidamaComponent,
        data: { title: 'Ajouter Istidama demande' },
    },
];
