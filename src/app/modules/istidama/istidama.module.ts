import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IstidamaRoutes} from './istidama-routing.module';
import { AddDemandeIstidamaComponent } from './components/add-demande-istidama/add-demande-istidama.component';
import { AddProductIstidamaComponent } from './components/add-product-istidama/add-product-istidama.component';
import { IstidamaWindowRequestComponent } from './components/istidama-window-request/istidama-window-request.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTreeSelectInputModule} from 'mat-tree-select-input';
import {CardModule} from '../../shared/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {NgxCurrencyModule} from 'ngx-currency';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {AccountModule} from '../../shared/account/account.module';
import {MatRadioModule} from "@angular/material/radio";
import {MatDividerModule} from "@angular/material/divider";


@NgModule({
    declarations: [
        AddDemandeIstidamaComponent,
        AddProductIstidamaComponent,
        IstidamaWindowRequestComponent
    ],
    exports: [
        IstidamaWindowRequestComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(IstidamaRoutes),
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatTreeSelectInputModule,
        CardModule,
        MatExpansionModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        NgxCurrencyModule,
        MatIconModule,
        MatTableModule,
        MatDatepickerModule,
        AccountModule,
        MatRadioModule,
        MatDividerModule,
    ]
})
export class IstidamaModule { }
