import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ConfigService} from '../../../core/config/config.service';
import {AuthService} from '../../auth/auth.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IstidamaService {

    private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

    constructor(private authService: AuthService,
                private httpClient: HttpClient) { }

    getListCodeTarification(): Observable<any> {
        return this.httpClient.post(this.demandesPath + '/creditRequestIstidama/listCodeTarification', {
            entityId: this.authService.user.entityId,
            userId: this.authService.user.identifiant,
            userEntityId: this.authService.user.entityId,
        });
    }
}
