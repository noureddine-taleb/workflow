import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import { DemandeMazaya } from '../../dto/demande-mazaya';
import { PersonMazaya } from '../../dto/person-mazaya';
import { ProductMazaya } from '../../dto/product-mazaya';
import { AddProductMazayaComponent } from '../add-product-mazaya/add-product-mazaya.component';
import { MatSelectChange } from '@angular/material/select';
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from 'app/layout/services/snackbar.service';
import { DemandeService } from 'app/modules/demandes/services/demande.service';
import { SimulationGtService } from 'app/modules/simulation/services/simulation.gt.service';
import { RequestType } from 'app/shared/models/RequestType';
import { AuthService } from 'app/modules/auth/auth.service';
import { CommonService } from 'app/core/services/common.service';
import { Account } from 'app/shared/models/Account';
import { SelectModel } from 'app/modules/simulation/simulation-form/simulation-form.component';

@Component({
  selector: 'app-add-demande-mazaya',
  templateUrl: './add-demande-mazaya.component.html',
  styleUrls: ['./add-demande-mazaya.component.scss']
})
export class AddDemandeMazayaComponent implements OnInit {
    demandeMazaya: DemandeMazaya = {};
    subscriptions = new Subscription();
    tiers: PersonMazaya[] = [];
    familleSegments = [];
    segments = [];
    filteredSegments = [];

    constructor(
        private dialog: MatDialog,
        private snackeBarService: SnackBarService,
        private authService: AuthService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private routingService: RoutingService,
    ) {}
    ngOnInit(): void {}

    retour() {
        this.routingService.gotoMazayaRequests();
    }
    listProducts: ProductMazaya[] = [];

    displayedColumns = [
        'produit',
        'montant',
        'duree',
        'taux',
        'differe',
        'periodicite',
        'mensualite',
        'assurance',
        'action',
    ];
    displayedColumnsLib = [
        'Produit',
        'Montant',
        'Durée',
        'Taux',
        'Différé',
        'Périodicité',
        'Mensualité',
        'Assurance',
    ];

    setSegmentName(person, $event) {
        person.segmentName = this.filteredSegments.find(s => s.id == $event.value)?.designation;
    }

    onAddProduct() {
        const dialogRef = this.dialog.open(AddProductMazayaComponent, {
            data: { persons: this.tiers },
            width: '500px',
        });
        dialogRef.afterClosed().subscribe((data) => {
            if (data?.product) {this.listProducts = this.listProducts.concat(data?.product);}
        });
    }

    onDelete(product) {
        this.snackeBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                this.listProducts = this.listProducts.filter(p => p != product);
            }
        });
    }

    onUpdate(product) {
        this.dialog.open(AddProductMazayaComponent, {
            data: { persons: this.tiers, product },
            width: '90vw',
        });
    }

    compte: Account;
    getAccount(account: Account) {
        this.compte = account;
        this.getTiers();
        this.getSegFamilly();
        this.getSegments();
    }

    getSegFamilly() {
        this.simulationService.getList('segmentsType').subscribe((data) => {
            this.familleSegments = data['result'];
        });
    }

    getSegments() {
        this.simulationService.getListByPost('segements', new SelectModel()).subscribe((data) => {
            this.segments = data['result'];
            this.filteredSegments = this.segments;
        });
    }

    filtrerSegment($event: MatSelectChange, person: any): void {
        person.segment = null;
        this.filteredSegments = this.segments.filter(x => x.parentCode === $event.value);
    }

    initPersons() {
        for (const person of this.tiers) {
            person.monthlyPaymentConsoCam || (person.monthlyPaymentConsoCam = 0);
            person.monthlyPaymentConsoConf || (person.monthlyPaymentConsoConf = 0);
            person.monthlyPaymentImmoCam || (person.monthlyPaymentImmoCam = 0);
            person.monthlyPaymentImmoConf || (person.monthlyPaymentImmoConf = 0);
            person.monthlyPay || (person.monthlyPay = 0);
            person.othersMonthlyIncome || (person.othersMonthlyIncome = 0);
        }
    }

    getTiers() {
        this.commonService
            .getListPerson({
                clientId: (this.compte as any).clientId,
            })
            .subscribe((data) => {
                (this.tiers = data['result']), this.initPersons();
            });
    }

    listCateories = [];
    listTypeProduct = [];

    onCreateMazayaDemande() {
        if (!this.isReadyToBeSaved()) {
            this.snackeBarService.openErrorSnackBar({ message: 'certaines informations manquent' });
            return;
        }
        this.demandeMazaya.accountId = this.compte.accountId;
        this.demandeMazaya.products = this.listProducts;
        this.demandeMazaya.creditType = RequestType.MAZAYA;
        this.demandeMazaya.persons = this.tiers;
        for (const person of this.tiers) {
            person.monthlyPaymentConso  =   person.monthlyPaymentConsoCam + person.monthlyPaymentConsoConf;
            person.monthlyPaymentImmo   =   person.monthlyPaymentImmoCam + person.monthlyPaymentImmoConf;
        }

        this.demandeService.createDemandeMazaya(this.demandeMazaya).subscribe((data) => {
            this.routingService.gotoTask(data['result']);
        });
    }

    isTierInfoSaved(): boolean {
        let ret = true;
        for (const person of this.tiers) {
            if (
                person.segment &&
                person.segmentType &&
                person.monthlyPay &&
                (person.othersMonthlyIncome || person.othersMonthlyIncome === 0) &&
                (person.monthlyPaymentImmoCam || person.monthlyPaymentImmoCam === 0) &&
                (person.monthlyPaymentImmoConf || person.monthlyPaymentImmoConf === 0) &&
                (person.monthlyPaymentConsoCam || person.monthlyPaymentConsoCam === 0) &&
                (person.monthlyPaymentConsoConf || person.monthlyPaymentConsoConf === 0)
            )
                {continue;}
            ret = false;
            break;
        }
        return ret;
    }

    isReadyToBeSaved(): boolean {
        return !!(
            this.compte &&
            this.isTierInfoSaved() &&
            this.demandeMazaya.objectCredit &&
            this.listProducts?.length
        );
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }
}
