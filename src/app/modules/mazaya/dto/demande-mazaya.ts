import { PersonMazaya } from "./person-mazaya";
import { ProductMazaya } from "./product-mazaya";

export interface DemandeMazaya {
    accountId?: number;
    clientCategory?: number;
    creditType?: number;
    entityId?: number;
    newConstitution?: string;
    objectCredit?: string;
    products?: ProductMazaya[];
    persons?: PersonMazaya[];
    requestId?: number;
    userEntityId?: number;
    userId?: number;
}
