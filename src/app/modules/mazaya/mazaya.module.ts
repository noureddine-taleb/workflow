import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MazayaRoutes } from './mazaya.routing';
import { AddDemandeMazayaComponent } from './components/add-demande-mazaya/add-demande-mazaya.component';
import { AddProductMazayaComponent } from './components/add-product-mazaya/add-product-mazaya.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTreeSelectInputModule } from 'mat-tree-select-input';
import { CardModule } from 'app/shared/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { NgxCurrencyModule } from 'ngx-currency';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AccountModule } from 'app/shared/account/account.module';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    AddDemandeMazayaComponent,
    AddProductMazayaComponent
  ],
  imports: [
      CommonModule,
      RouterModule.forChild(MazayaRoutes),
      ReactiveFormsModule,
      MatInputModule,
      MatFormFieldModule,
      MatTreeSelectInputModule,
      CardModule,
      MatExpansionModule,
      FormsModule,
      MatButtonModule,
      MatFormFieldModule,
      MatSelectModule,
      NgxCurrencyModule,
      MatIconModule,
      MatTableModule,
      MatDatepickerModule,
      AccountModule,
      MatDialogModule,
  ]
})
export class MazayaModule { }
