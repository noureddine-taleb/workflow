import {Route} from '@angular/router';
import { ListDemandeComponent } from 'app/shared/list-demande/list-demande.component';
import { RequestType } from 'app/shared/models/RequestType';
import { AddDemandeMazayaComponent } from './components/add-demande-mazaya/add-demande-mazaya.component';

export const MazayaRoutes: Route[] = [
    {
        path: '',
        component: ListDemandeComponent,
        data: { title: 'Liste des demandes Mazaya', demandeType: RequestType.MAZAYA, addAllowed: true },
    },
    {
        path: 'add',
        component: AddDemandeMazayaComponent,
        data: { title: 'Ajouter mazaya demande' },
    },
];
