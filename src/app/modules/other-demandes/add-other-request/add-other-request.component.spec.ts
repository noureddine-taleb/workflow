import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOtherRequestComponent } from './add-other-request.component';

describe('AddOtherRequestComponent', () => {
  let component: AddOtherRequestComponent;
  let fixture: ComponentFixture<AddOtherRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOtherRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOtherRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
