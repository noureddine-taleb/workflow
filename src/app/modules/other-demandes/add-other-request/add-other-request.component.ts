import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { AccountFindShowComponent } from 'app/shared/account/account-find-show/account-find-show.component';
import { Account } from 'app/shared/models/Account';
import {OthersRequestParamsService} from "../services/others-request-params.service";
import {MatDialog} from "@angular/material/dialog";
import {AddSubItemInListComponent} from "../add-sub-item-in-list/add-sub-item-in-list.component";

@Component({
	selector: 'app-add-other-request',
	templateUrl: './add-other-request.component.html',
	styleUrls: ['./add-other-request.component.scss']
})
export class AddOtherRequestComponent implements OnInit {
	requestForm: FormGroup;
	account: Account;
	listCateories = [];
	creditTypeId: number;
    dataSourceListeDossierEndettement: any[];
    dataSourceListeDossierEndettementCHOISI: any[]=[];


	@ViewChild(AccountFindShowComponent) accountFindShowCmp: AccountFindShowComponent;

	constructor(
		private commonService: CommonService,
		private route: ActivatedRoute,
		private demandeService: DemandeService,
		private routingService: RoutingService,
		private snackBarService: SnackBarService,
		private fb: FormBuilder,
        private othersRequestParamsService: OthersRequestParamsService,
        public dialog: MatDialog
	) {}

	ngOnInit(): void {
		this.requestForm = this.fb.group({
			accountId: ["", [Validators.required]],
			objectCredit: [""],
			details: [""],
			creditType: ["", [Validators.required]],
			clientCategory: [""],
		})
		this.route.params.subscribe(params => {
			this.reset();
			this.creditTypeId = Number(params.creditTypeId);
			this.requestForm.get("creditType").setValue(this.creditTypeId)
		});
	}

	getAccount(account: Account) {
		this.account = account;
		this.requestForm.get("accountId").setValue(this.account.accountId);
		this.getCategories();

        if(this.creditTypeId === 8  || this.creditTypeId === 10){
            this.othersRequestParamsService.getListDebt(({creditTypeId: this.creditTypeId,...this.account})).subscribe((data) => {
                this.dataSourceListeDossierEndettement= data['result'];
            });
        }
	}

	getCategories() {
		this.commonService.getCategories(this.creditTypeId).subscribe((data) => {
			this.listCateories = data["result"];
		});
	}

	onCreateDemande() {
        console.log('%c ==>> Validation Errors: ', 'color: red; font-weight: bold; font-size:25px;');

        let totalErrors = 0;

        Object.keys(this.requestForm.controls).forEach((key) => {
            const controlErrors: ValidationErrors = this.requestForm.get(key).errors;
            if (controlErrors != null) {
                totalErrors++;
                Object.keys(controlErrors).forEach((keyError) => {
                    console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                });
            }
        });

        console.log('Number of errors: ' ,totalErrors);

		if (this.requestForm.invalid) {
			this.snackBarService.openErrorSnackBar({message: "certaines informations manquent"});
			return;
		}

        if(this.creditTypeId===10){
            this.requestForm.get('clientCategory') .setValue(10);
        }
		this.demandeService.createDemandeOther({...this.requestForm.value,...{products:this.dataSourceListeDossierEndettementCHOISI}}).subscribe(data => {
			this.snackBarService.openSuccesSnackBar({ message: data.message });
			this.routingService.gotoTask(data.result);
		});
	}

	reset() {
		this.requestForm.reset();
		this.account = null;
		this.accountFindShowCmp?.reset();
	}

    onReporterAutorisation(item: any): void {
        if(this.dataSourceListeDossierEndettementCHOISI.find(o=>o.numerodossier === item.numerodossier) === undefined) {
            const dialogRef = this.dialog.open(AddSubItemInListComponent, {
                width: '500px',
                data: item,
            });

            dialogRef.afterClosed().subscribe((result) => {
                if (this.dataSourceListeDossierEndettementCHOISI.find(o => o.numerodossier === item.numerodossier) === undefined) {
                    item.firstDeadlineDate = result.firstDeadlineDate;
                    item.lastDuration = result.lastDuration;
                    this.dataSourceListeDossierEndettementCHOISI.push(item);
                }
            });
        }

    }

    onDemandeBasculement(item: any): void {
        if (this.dataSourceListeDossierEndettementCHOISI.find(o => o.numerodossier === item.numerodossier) === undefined) {
            this.dataSourceListeDossierEndettementCHOISI.push(item);
        }
    }

    checkIfAdded(item): boolean{
        if (this.dataSourceListeDossierEndettementCHOISI.find(o => o.numerodossier === item.numerodossier) === undefined) {
            return true;
        }else{
            return false;
        }
    }

    onDeleteReporterAutorisation(item: any) {
        if(this.dataSourceListeDossierEndettementCHOISI.find(o=>o.numerodossier === item.numerodossier) !== undefined){
            this.dataSourceListeDossierEndettementCHOISI = this.dataSourceListeDossierEndettementCHOISI.filter(o=>o.numerodossier !== item.numerodossier);
        }
    }
}
