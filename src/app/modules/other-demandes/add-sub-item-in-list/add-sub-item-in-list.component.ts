import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-sub-item-in-list',
  templateUrl: './add-sub-item-in-list.component.html',
  styleUrls: ['./add-sub-item-in-list.component.scss']
})
export class AddSubItemInListComponent  {
    selectedRow: any= {};
    constructor(
        public dialogRef: MatDialogRef<AddSubItemInListComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {}

    onNoClick(): void {
        this.dialogRef.close();
    }
}
