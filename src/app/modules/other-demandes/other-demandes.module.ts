import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Routes } from './other-demandes.routing';
import { AddOtherRequestComponent } from './add-other-request/add-other-request.component';
import { CardModule } from 'app/shared/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { AccountModule } from 'app/shared/account/account.module';
import { AddSubItemInListComponent } from './add-sub-item-in-list/add-sub-item-in-list.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";



@NgModule({
  declarations: [
    AddOtherRequestComponent,
    AddSubItemInListComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(Routes),
        CardModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatTableModule,
        MatSelectModule,
        MatRadioModule,
        MatIconModule,
        AccountModule,
        MatDialogModule,
        MatDatepickerModule,
    ]
})
export class OtherDemandesModule { }
