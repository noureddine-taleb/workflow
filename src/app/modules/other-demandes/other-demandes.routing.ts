import { Route } from "@angular/router";
import { ListDemandeComponent } from "app/shared/list-demande/list-demande.component";
import { RequestType } from "app/shared/models/RequestType";
import { AddOtherRequestComponent } from "./add-other-request/add-other-request.component";

export const Routes: Route[] = [
	{
		path: "add/:creditTypeId",
		component: AddOtherRequestComponent,
	},
	{
		path: "mainlevee",
		data: { title: "Liste des demandes de mainlevée", demandeType: RequestType.MAINLEVEE, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "solde_de_tout_compte",
		data: { title: "Liste des demandes de solde de tout compte", demandeType: RequestType.SOLDE_DE_TOUT_COMPTE, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "report",
		data: { title: "Liste des demandes de report", demandeType: RequestType.REPORT, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "consolidation",
		data: { title: "Liste des demandes de consolidation", demandeType: RequestType.CONSOLIDATION, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "basculement_s_b",
		data: { title: "Liste des demandes de basculement S-->B", demandeType: RequestType.BASCULEMENT_S_B, addAllowed: true },
		component: ListDemandeComponent,
	},
	{
		path: "restructuration",
		data: { title: "Liste des demandes de restructuration", demandeType: RequestType.RESTRUCTURATION, addAllowed: true },
		component: ListDemandeComponent,
	},
]