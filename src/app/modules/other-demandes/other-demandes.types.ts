export class OtherRequest {
	accountId: number;
	objectCredit: string;
	details: string;
	creditType: number;
	clientCategory: number;

	entityId: number;
	userEntityId: number;
	userId: number;
}