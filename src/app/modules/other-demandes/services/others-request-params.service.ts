import { Injectable } from '@angular/core';
import {ConfigService} from '../../../core/config/config.service';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../auth/auth.service';
import {RoutingService} from '../../../core/services/routing.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OthersRequestParamsService {

    private restWKFUrl = ConfigService.settings.apiServer.restWKFUrl + '/otherRequestParams';

    constructor(
        private httpClient: HttpClient,
        private authService: AuthService,
        private routingService: RoutingService,
    ) {}

    getListDebt(criteria: any): Observable<any> {
        criteria['userEntityId'] = this.authService.user.entityId;
        criteria['entityId'] = this.authService.user.entityId;
        criteria['userId'] = this.authService.user.userId;
        criteria['userEntityId'] = this.authService.user.entityId;
        return this.httpClient.post(this.restWKFUrl + '/getListDebt', criteria);
    }
}
