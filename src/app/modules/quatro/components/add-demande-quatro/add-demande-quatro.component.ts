import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {PersonConso} from '../../../conso/models/PersonConso';
import {MatDialog} from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { RoutingService } from 'app/core/services/routing.service';
import { SnackBarService } from 'app/layout/services/snackbar.service';
import { DemandeService } from 'app/modules/demandes/services/demande.service';
import { SimulationGtService } from 'app/modules/simulation/services/simulation.gt.service';
import { RequestType } from 'app/shared/models/RequestType';
import { DemandeQuatro } from '../../dto/demande-quatro';
import { ProductQuatro } from '../../dto/product-quatro';
import { AddProductQuatroComponent } from '../add-product-quatro/add-product-quatro.component';
import { AuthService } from 'app/modules/auth/auth.service';
import { CommonService } from 'app/core/services/common.service';
import { SelectModel } from 'app/modules/simulation/simulation-form/simulation-form.component';
import { AddProductConsoComponent } from 'app/modules/conso/add-product-conso/add-product-conso.component';
import { Account } from 'app/shared/models/Account';

@Component({
  selector: 'app-add-demande-quatro',
  templateUrl: './add-demande-quatro.component.html',
  styleUrls: ['./add-demande-quatro.component.scss']
})
export class AddDemandeQuatroComponent implements OnInit {
    demandeQuatro: DemandeQuatro = {};
    subscriptions = new Subscription();
    tiers: PersonConso[] = [];
    familleSegments = [];
    segments = [];
    filteredSegments = [];

    constructor(
        private dialog: MatDialog,
        private snackeBarService: SnackBarService,
        private authService: AuthService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private routingService: RoutingService,
    ) {}
    ngOnInit(): void {}

    retour() {
        this.routingService.gotoConsoRequests();
    }
    listProducts: ProductQuatro[] = [];

    displayedColumns = [
        'produit',
        'montant',
        'duree',
        'taux',
        'differe',
        'periodicite',
        'mensualite',
        'assurance',
        'action',
    ];
    displayedColumnsLib = [
        'Produit',
        'Montant',
        'Durée',
        'Taux',
        'Différé',
        'Périodicité',
        'Mensualité',
        'Assurance',
    ];

    setSegmentName(person, $event) {
        person.segmentName = this.filteredSegments.find(s => s.id == $event.value)?.designation;
    }

    onAddProduct() {
        const dialogRef = this.dialog.open(AddProductQuatroComponent, {
            data: { persons: this.tiers },
            width: '500px',
        });
        dialogRef.afterClosed().subscribe((data) => {
            if (data?.product) {this.listProducts = this.listProducts.concat(data?.product);}
        });
    }

    onDelete(product) {
        this.snackeBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                this.listProducts = this.listProducts.filter(p => p != product);
            }
        });
    }

    onUpdate(product) {
        this.dialog.open(AddProductQuatroComponent, {
            data: { persons: this.tiers, product },
            width: '90vw',
        });
    }

    compte: Account;
    saveAccount(account: Account) {
		this.compte = account
		this.getTiers();
		this.getSegFamilly();
		this.getSegments();
    }

    getSegFamilly() {
        this.simulationService.getList('segmentsType').subscribe((data) => {
            this.familleSegments = data['result'];
        });
    }

    getSegments() {
        this.simulationService.getListByPost('segements', new SelectModel()).subscribe((data) => {
            this.segments = data['result'];
            this.filteredSegments = this.segments;
        });
    }

    filtrerSegment($event: MatSelectChange, person: any): void {
        person.segment = null;
        this.filteredSegments = this.segments.filter(x => x.parentCode === $event.value);
    }

    initPersons() {
        for (const person of this.tiers) {
            person.monthlyPaymentConsoCam || (person.monthlyPaymentConsoCam = 0);
            person.monthlyPaymentConsoConf || (person.monthlyPaymentConsoConf = 0);
            person.monthlyPaymentImmoCam || (person.monthlyPaymentImmoCam = 0);
            person.monthlyPaymentImmoConf || (person.monthlyPaymentImmoConf = 0);
            person.monthlyPay || (person.monthlyPay = 0);
            person.othersMonthlyIncome || (person.othersMonthlyIncome = 0);
        }
    }

    getTiers() {
        this.commonService
            .getListPerson({
                clientId: (this.compte as any).clientId,
            })
            .subscribe((data) => {
                (this.tiers = data['result']), this.initPersons();
            });
    }

    listCateories = [];
    listTypeProduct = [];

    onCreateConsoDemande() {
        if (!this.isReadyToBeSaved()) {
            this.snackeBarService.openErrorSnackBar({ message: 'certaines informations manquent' });
            return;
        }
        this.demandeQuatro.accountId = this.compte.accountId;
        this.demandeQuatro.products = this.listProducts;
        this.demandeQuatro.creditType = RequestType.QUATRO;
        this.demandeQuatro.persons = this.tiers;
        for (const person of this.tiers) {
            person.monthlyPaymentConso  =   person.monthlyPaymentConsoCam + person.monthlyPaymentConsoConf;
            person.monthlyPaymentImmo   =   person.monthlyPaymentImmoCam + person.monthlyPaymentImmoConf;
        }

        this.demandeService.createDemandeQuatro(this.demandeQuatro).subscribe((data) => {
            this.routingService.gotoTask(data['result']);
        });
    }

    isTierInfoSaved(): boolean {
        let ret = true;
        for (const person of this.tiers) {
            if (
                person.segment &&
                person.segmentType &&
                person.monthlyPay &&
                (person.othersMonthlyIncome || person.othersMonthlyIncome === 0) &&
                (person.monthlyPaymentImmoCam || person.monthlyPaymentImmoCam === 0) &&
                (person.monthlyPaymentImmoConf || person.monthlyPaymentImmoConf === 0) &&
                (person.monthlyPaymentConsoCam || person.monthlyPaymentConsoCam === 0) &&
                (person.monthlyPaymentConsoConf || person.monthlyPaymentConsoConf === 0)
            )
                {continue;}
            ret = false;
            break;
        }
        return ret;
    }

    isReadyToBeSaved(): boolean {
        return !!(
            this.compte &&
            this.isTierInfoSaved() &&
            this.demandeQuatro.objectCredit &&
            this.listProducts?.length
        );
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }
}
