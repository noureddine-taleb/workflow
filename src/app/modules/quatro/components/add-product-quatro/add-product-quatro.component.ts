import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { CommonService } from 'app/core/services/common.service';
import { SnackBarService } from 'app/layout/services/snackbar.service';
import { DemandeService } from 'app/modules/demandes/services/demande.service';
import { AddProductInvestComponent } from 'app/modules/investitssement/add-product-invest/add-product-invest.component';
import { SimulationGtService } from 'app/modules/simulation/services/simulation.gt.service';
import { RequestType } from 'app/shared/models/RequestType';
import { TreeData } from 'mat-tree-select-input';

@Component({
  selector: 'app-add-product-quatro',
  templateUrl: './add-product-quatro.component.html',
  styleUrls: ['./add-product-quatro.component.scss']
})
export class AddProductQuatroComponent implements OnInit {
    product: any = {};
    productForm: FormGroup;
    listProduitFinancement = [];
    typePrelevement = [];
    listObjetFinancement = [];
    filteredListObjetFinancement = [];
    perdictees = [];
    filtredProductList: any[];
    productList;
    calculateSuccess = false;
    onUpdate = false;

    constructor(
        public dialogRef: MatDialogRef<AddProductInvestComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private snackeBarService: SnackBarService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.productForm = this.fb.group({
            personId: ['', [Validators.required]],

            lastProduct: ['', [Validators.required]],

            financielObject: ['', [Validators.required]],

            lastAmountGranted: ['', [Validators.required]],

            lastDuration: ['', [Validators.required, Validators.min(0), Validators.max(300)]],
            lastRate: ['', [Validators.required]],
            startDate: ['', [Validators.required]],

        });
        this.productForm.get("lastProduct").valueChanges.subscribe(_ => {
			this.getFinancialObjectEntreprise();
		})
        this.getProductList();
        this.getTypePrelevement();// 
        this.getPeriodicity();
        if (this.data.persons?.length === 1)
            {this.productForm.get('personId').setValue(this.data.persons[0].personId);}
    }

    setProductFormValues(values: any) {
        this.productForm.patchValue(values);
        this.setFinancialObjectByValue(values?.financielObject, this.listObjetFinancement);
    }

    setFinancialObjectByValue(value: string, data: TreeData[]) {
        for (const fo of data) {
            if (fo.value == value) {
                this.productForm.get('financielObject').setValue(fo);
                return true;
            }
            if (this.setFinancialObjectByValue(value, fo.children)) {return true;}
        }
        return false;
    }

    onClose(): void {
        this.dialogRef.close({ product: [] });
    }

    onValide() {
        this.snackeBarService.openConfirmSnackBar({ message: 'all' }).then((result) => {
            if (result.value) {
                let financielObject = this.productForm.value.financielObject;
                if (financielObject) {
                    financielObject = financielObject.value;
                }
                this.product = { ...this.product, ...this.productForm.value, financielObject };
                this.product.lastPrelevementType= 'B';
                this.product.typePrelevement= 'B';

                this.product.productName = this.listProduitFinancement?.find(
                    pf => pf.id == this.product.lastProduct,
                )?.designation;
                if (this.onUpdate)
                    {for (const key of Object.keys(this.product))
                        {this.data.product[key] = this.product[key];}}
                this.dialogRef.close({ product: this.product });
            }
        });
    }

    onCalculer(e) {
        if (this.productForm.invalid) {return;}
        this.calculateSuccess = false;
        this.product = {};
        const critere = {
            amount: this.productForm.get('lastAmountGranted').value,
            differed: this.productForm.get('lastDeferredPeriod').value,
            duration: this.productForm.get('lastDuration').value,
            rate: this.productForm.get('lastRate').value,
        };
        this.demandeService.calculateProduct(critere).subscribe((data) => {
            this.product.lastInsurance = data['result']['insuranceAmount'];
            this.product.lastMonthlyPayment = data['result']['deadLineAmount'];
            this.calculateSuccess = true;
        });
    }

    onResit() {
        this.product = {};
        this.productForm.reset();
        this.calculateSuccess = false;
    }

    getPeriodicity() {
        this.commonService.getMensualites().subscribe((data) => {
            this.perdictees = data['result'];
        });
    }

    getTypePrelevement() {

    }

    getFinancialObjectEntreprise() {
		this.commonService.getFinancialObjectEntreprise(this.productForm.get("lastProduct").value).subscribe((data) => {
            this.listObjetFinancement = this.transformFinancialProducts(data['result']);
            this.filteredListObjetFinancement = this.listObjetFinancement;
            if (this.data?.product)
                {this.setProductFormValues(this.data?.product), (this.onUpdate = true);}
        });
    }

    filter(array: TreeData[], text: string) {
        const getNodes = (result, object) => {
            if (object.name.toLowerCase().startsWith(text)) {
                result.push(object);
                return result;
            }
            if (Array.isArray(object.children)) {
                const children = object.children.reduce(getNodes, []);
                if (children.length) {result.push({ ...object, children });}
            }
            return result;
        };

        this.filteredListObjetFinancement = array.reduce(getNodes, []);
    }

    transformFinancialProducts(arr: any[]) {
        return arr.map((o) => {
            if (o.child) {o.child = this.transformFinancialProducts(o.child);}
            return {
                name: o.financialObjectName,
                value: o.financialObjectCode,
                children: o.child,
            };
        });
    }

    getProductList() {
        this.commonService.getproductList(RequestType.QUATRO).subscribe((data) => {
            this.listProduitFinancement = data['result'];
        });
    }
}
