import {ProductQuatro} from './product-quatro';
import {PersonQuatro} from './person-quatro';

export interface DemandeQuatro {
    accountId?: number;
    clientCategory?: number;
    creditType?: number;
    entityId?: number;
    newConstitution?: string;
    objectCredit?: string;
    products?: ProductQuatro[];
    persons?: PersonQuatro[];
    requestId?: number;
    userEntityId?: number;
    userId?: number;
}
