import {Route} from '@angular/router';
import { ListDemandeComponent } from 'app/shared/list-demande/list-demande.component';
import { RequestType } from 'app/shared/models/RequestType';
import {AddDemandeQuatroComponent} from "./components/add-demande-quatro/add-demande-quatro.component";

export const QuatroRoutes: Route[] = [
    {
        path: '',
        component: ListDemandeComponent,
        data: { title: 'Liste des demandes Quatro', demandeType: RequestType.QUATRO, addAllowed: true },
    },
    {
        path: 'add',
        component: AddDemandeQuatroComponent,
        data: { title: 'Ajouter quatro demande' },
    },
];
