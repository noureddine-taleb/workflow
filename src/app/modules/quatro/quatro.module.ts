import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDemandeQuatroComponent } from './components/add-demande-quatro/add-demande-quatro.component';
import { AddProductQuatroComponent } from './components/add-product-quatro/add-product-quatro.component';
import {RouterModule} from '@angular/router';
import {QuatroRoutes} from './quatro-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTreeSelectInputModule} from 'mat-tree-select-input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxCurrencyModule } from 'ngx-currency';
import { MatButtonModule } from '@angular/material/button';
import { CardModule } from 'app/shared/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { AccountModule } from 'app/shared/account/account.module';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AddDemandeQuatroComponent,
    AddProductQuatroComponent
  ],
  imports: [
    CommonModule, RouterModule.forChild(QuatroRoutes),
      ReactiveFormsModule,
      MatInputModule,
      MatFormFieldModule,
      MatTreeSelectInputModule,
      MatDatepickerModule,
      NgxCurrencyModule,
      MatButtonModule,
      FormsModule,
      CardModule,
      MatExpansionModule,
      MatTableModule,
      MatSelectModule,
      MatRadioModule,
      MatIconModule,
      AccountModule,
      MatDialogModule,
  ]
})
export class QuatroModule { }
