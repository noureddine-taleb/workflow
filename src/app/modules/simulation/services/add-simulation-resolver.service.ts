import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { Client } from "app/shared/models/Client";

@Injectable({ providedIn: "root" })
export class AddSimulationResolverService implements Resolve<any> {
	constructor(
		private commonServ: CommonService,
	) {}

	criterePerson = new Client();

	// tslint:disable-next-line:typedef
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		this.criterePerson.cin = route.queryParams.cin;
		// @ts-ignore

		return this.commonServ.getPerson(this.criterePerson);
	}
}
