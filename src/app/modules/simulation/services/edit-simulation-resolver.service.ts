import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { SimulationGtService } from "app/modules/simulation/services/simulation.gt.service";
import { AuthService } from 'app/modules/auth/auth.service';

@Injectable({ providedIn: "root" })
export class EditSimulationResolverService implements Resolve<any> {
	constructor(
		private simServ: SimulationGtService,
		private authService: AuthService,
	) {}

	// tslint:disable-next-line:typedef
	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		return this.simServ
			.getSimulation({
				simulationId: route.params.id,
				userEntityId: this.authService.user.entityId,
			})
	}
}
