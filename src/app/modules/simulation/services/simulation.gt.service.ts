import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RoutingService } from "app/core/services/routing.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";
import { ConfigService } from 'app/core/config/config.service';
import { SimulationCriteria } from "../simulation.types";
import { RequestQuery } from "app/shared/list-demande/list-demande.types";
import { ApiResponse } from "app/core/models/ApiResponse";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";

@Injectable({
	providedIn: "root",
})
export class SimulationGtService {
	private simulationsGtPath = ConfigService.settings.apiServer.restGTUrl + "/gt";
	private wkfRef = ConfigService.settings.apiServer.restWKFUrl + "/ref/";
	private simulationsPath = ConfigService.settings.apiServer.restWKFUrl + "/simulation";
	private wkfpath = ConfigService.settings.apiServer.restWKFUrl;

	constructor(
		private httpClient: HttpClient,
		private authService: AuthService,
		private routingService: RoutingService,
	) {}

	simulate(simulationCriteria: any): Observable<any> {
		return this.httpClient.post(`${this.simulationsGtPath}` + "/simulate", simulationCriteria);
	}

    getExemptionList(simulationCriteria: any): Observable<any> {
        return this.httpClient.post(this.simulationsPath + '/getExemptionList', simulationCriteria);
    }

	getList(param: string) {
		return this.httpClient.get(this.wkfRef + param);
	}

	getListByPost(arg0: string, arg1: any) {
		return this.httpClient.post(this.wkfRef + arg0, arg1);
	}

	//taches/////////////////////////////////////////////////////////
	getListDemande(critere: RequestQuery) {
		if (this.routingService.isInMyTasks())
			critere["userId"] = this.authService.user.userId;
		critere.userEntityId = this.authService.user.entityId;

		return this.httpClient.post<ApiResponse<{
			rowsNumber: number,
			results: LoanRequest[]
		}>>(this.wkfpath + "/creditRequest/list", critere);
	}
	//list demande pour le comite/////////////////////////////////////////////////////////
	getListDemandeByStatut(statut): Observable<any> {
		let critere = {};
		critere["userEntityId"] = this.authService.user.entityId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userId"] = this.authService.user.userId;
		critere["userEntityId"] = this.authService.user.entityId;
		critere["first"] = 0;
		critere["pas"] = 100;
		critere["statusCode"] = statut;
		return this.httpClient.post(this.wkfpath + "/creditRequest/list", critere);
	}
	/////////////////////////////////

	getListSimulation(critere: SimulationCriteria) {
		const searcheCritere = new SimulationCriteria();
		searcheCritere.cin = critere.cin;
		searcheCritere.entityId = this.authService.user.entityId;
		searcheCritere.userEntityId = this.authService.user.userEntityId;
		//searcheCritere.userId= this.authService.user.userId;

		searcheCritere.milieuBien =
			critere.milieuBien.code !== null ? critere.milieuBien.code : null;
		searcheCritere.typeAchat = critere.typeAchat.code !== null ? critere.typeAchat.code : null;
		searcheCritere.first = critere.first ? critere.first : 0;
		searcheCritere.pas = critere.pas ? critere.pas : 10;
		searcheCritere.startDate = critere.startDate;
		searcheCritere.endDate = critere.endDate;

		return this.httpClient.post<ApiResponse<{
			rowsNumber: number,
			results: any[]
		}>>(this.simulationsPath + "/list", searcheCritere);
	}

	saveListSimulation(critere): Observable<any> {
		return this.httpClient.post(this.simulationsPath + "/save", critere);
	}

	delete(param: {
		userEntityId: number;
		cin: string;
		refPack: any;
		userId: any;
		entityId: any;
		simulationId: any;
	}) {
		return this.httpClient.post(this.simulationsPath + "/delete", param);
	}

	getSimulation(param: { userEntityId: number; simulationId: any }): Observable<any> {
		return this.httpClient.post(this.simulationsPath + "/getSimulation", param);
	}

	getSimParNumCompte(critere: any) {
		critere["userEntityId"] = this.authService.user.userEntityId;
		critere["userId"] = this.authService.user.userId;
		critere["entityId"] = this.authService.user.entityId;

		return this.httpClient.post(this.simulationsPath + "/getProducts", critere);
	}
}
const DATA_SIMULATION22 = {
	status: 200,
	message: "success",
	result: {
		rowsNumber: 39,
		results: [
			{
				statusColor: "#338e30",
				statusName: "Simulation concrétisée",
				simulationId: 187,
				simulationReference: null,
				simulationDate: "2021-07-06T08:51:20.000+0000",
				entityName: "TETOUAN - CR",
				clientType: "C",
				clientName: "L36630",
				accountNumber: null,
				amountRequested: null,
				rate: null,
				bonus: null,
				monthlyPayment: null,
				amountFinanced: null,
				status: "SIM_CON",
				cin: "L36630",
				productsNumber: 1,
				phoneNumber: "0661492406",
				email: null,
				birthDate: "1958-07-01T00:00:00.000+0000",
				goodLocationName: "URBAIN",
				financialNatureName: "AQUISITION",
			},
			{
				statusColor: "#338e30",
				statusName: "Simulation concrétisée",
				simulationId: 188,
				simulationReference: null,
				simulationDate: "2021-07-07T13:17:09.000+0000",
				entityName: "TETOUAN - CR",
				clientType: "C",
				clientName: "L36630",
				accountNumber: null,
				amountRequested: null,
				rate: null,
				bonus: null,
				monthlyPayment: null,
				amountFinanced: null,
				status: "SIM_CON",
				cin: "L36630",
				productsNumber: 2,
				phoneNumber: "0661492406",
				email: null,
				birthDate: "1958-07-01T00:00:00.000+0000",
				goodLocationName: "URBAIN",
				financialNatureName: "AQUISITION",
			},
		],
	},
};

const SIMPRODUCT = [
	{
		financielObject: "ATR",
		goodAmount: 370000,
		contributionAmount: 0,
		duration: 72,
		deferredPeriod: 0,
		prelevementTypeCriteria: "S",
		prelevementTypeResult: "S",
		othersFees: 0,
		financialProduct: 2034,
		rateType: "TF",
		rate: 4.3,
		bonus: null,
		monthlyPayment: 4730.08,
		amountGranted: 296000,
		insurance: null,
		refRequest: "wkf-0e377106-26a2-48ba-8f3d-dc5a44e54008",
		refPack: "PCK-1839",
		financialObjectName: "Acquisition du terrain destiné à la construction",
		pack: "Pack Sakan",
		organism: null,
	},
];
