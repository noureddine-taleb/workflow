import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from 'app/modules/auth/auth.service';
import { BehaviorSubject, Observable } from "rxjs";
import { map, switchMap, take } from "rxjs/operators";
import { Simulation } from "../simulation.types";

@Injectable({
	providedIn: "root",
})
export class SimulationService {
	// Private
	private _criteria: BehaviorSubject<Simulation | null> = new BehaviorSubject(null);
	private _simulations: BehaviorSubject<Simulation[] | null> = new BehaviorSubject(null);
	private _milieuBien: BehaviorSubject<any[] | null> = new BehaviorSubject(null);
	private _financielNature: BehaviorSubject<any[] | null> = new BehaviorSubject(null);

	/**
	 * Constructor
	 */
	constructor(private _httpClient: HttpClient, private tokenService: AuthService) {}

	// -----------------------------------------------------------------------------------------------------
	// @ Accessors
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Getter for contact
	 */
	get criteria$(): Observable<Simulation> {
		return this._criteria.asObservable();
	}

	/**
	 * Getter for contacts
	 */
	get simulations$(): Observable<Simulation[]> {
		return this._simulations.asObservable();
	}

	/**
	 * Getter for countries
	 */
	get milieuBien$(): Observable<any[]> {
		return this._milieuBien.asObservable();
	}

	/**
	 * Getter for tags
	 */
	get financielNature$(): Observable<any[]> {
		return this._financielNature.asObservable();
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Public methods
	// -----------------------------------------------------------------------------------------------------

	/**
	 * List des simulations
	 */
	getListSimulation(param: any): Observable<Simulation[]> {
		param.userId = this.tokenService.user.userId;
		param.userEntityId = this.tokenService.user.userEntityId;
		param.entityId = this.tokenService.user.entityId;

		return this.simulations$.pipe(
			take(1),
			switchMap((contacts) =>
				this._httpClient.post<Simulation[]>("api/backwkf/simulation/list", {}).pipe(
					map((newContact) => {
						// Update the contacts with the new contact
						this._simulations.next(newContact);

						// Return the new contact
						return newContact;
					}),
				),
			),
		);
	}

	/**
	 * Get milieu bien
	 */
	// getGoodLocation(): Observable<any[]> {
	// 	return this._httpClient.get<any[]>("api/backwkf/ref/goodLocation").pipe(
	// 		tap((countries) => {
	// 			this._milieuBien.next(countries);
	// 		}),
	// 	);
	// }

	// /**
	//  * Get financiel nature
	//  */
	// getFinacielNature(): Observable<any[]> {
	// 	return this._httpClient.get<any[]>("api/backwkf/ref/finacielNature").pipe(
	// 		tap((tags) => {
	// 			this._financielNature.next(tags);
	// 		}),
	// 	);
	// }
}
