export interface DerogationParams {
    identifiant?: number;
    code?: string;
    derogation?: string;
    libelle?: string;
    flagsaisi?: string;
    typechamp?: string;
    nomchamp?: string;
    checked?: boolean;
    valeur?: string;
}
