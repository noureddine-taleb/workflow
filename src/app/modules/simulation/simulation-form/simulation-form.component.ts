import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSelectChange } from "@angular/material/select";
import { ActivatedRoute } from "@angular/router";
import { ComponentState } from "app/core/models/ComponentState";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { CurrencyMaskInputMode } from "ngx-currency";
import { v4 as uuidv4 } from "uuid";
import { SimulationGtService } from "../services/simulation.gt.service";
import { BonusPoint, Simulation, SimulationProduct } from "../simulation.types";
import {DerogationParams} from "./dto/DerogationParams";
import {MatCheckboxChange} from "@angular/material/checkbox";

@Component({
	selector: "app-simulation-form",
	templateUrl: "./simulation-form.component.html",
	styleUrls: ["./simulation-form.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class SimulationFormComponent implements OnInit {
	formFieldHelpers: string[] = [""];
	// simulationDto:Simulation=new Simulation();
	//activityStatus=[{code:1,designation:'Actif'},{code:1,designation:'Retraité'}]
	organisms: SelectModel[];
	organismsDB: SelectModel[];
	conventions: SelectModel[];
	conventionsDB: SelectModel[];
	familleSegments: SelectModel[];
	segments: SelectModel[];
	filtredSegment: SelectModel[];
	segmentsDB: SelectModel[];
	activityStatus: SelectModel[];
	prelevemntType: SelectModel[];
	financialNatures: SelectModel[];
	goodLocations: SelectModel[];
	finacialObjects: SelectModel[];
	finacialObjectsDB: SelectModel[];
	disabledFields: boolean;
	simulationForm: FormGroup;
	isSubmit: boolean = false;
	isClient: boolean = false;
	registerDemande = true;
	params: any[] = [];
	localInputMode = CurrencyMaskInputMode;
	simRO = false;
	bonuses: BonusPoint[] = []
	state: ComponentState;
	// query params
	cin: string;
	milieuBien: string;
	typeAchat: string;

    //dérogation
    dataSourceDerogationParams: DerogationParams[];
    displayedColumnsDerogationSelected: string[] = [ 'code','libelle','value'];
    dataSourceDerogationParamsSelected: DerogationParams[] = [];
    displayedColumnsDerogation: string[] = [ 'libelle','code','valeur'];
    tableFormDerogation: FormGroup;
    instTypeDerogation: DerogationParams = {};

	getFormFieldHelpersAsString(): string {
		return this.formFieldHelpers.join(" ");
	}

	constructor(
		private route: ActivatedRoute,
		private simulationService: SimulationGtService,
		private refGtToken: AuthService,
		private tokenServ: AuthService,
		private snackbarService: SnackBarService,
		public demandeService: DemandeService,
		public commonService: CommonService,
		private fb: FormBuilder,
		private routingService: RoutingService,
	) {}

	filter;

	ngOnInit(): void {
		this.simulationForm = this.fb.group({
			cin: [{ value: "", disabled: true }, [Validators.required]],
			clientName: ["", [Validators.required]],
			birthDate: ["", [Validators.required]],
			phoneNumber: ["", []],
			email: ["", []],
			activityStatus: ["A", [Validators.required]],
			seniority: ["", [Validators.required, Validators.min(0)]],
			objectCredit: ["", []],
			segment: ["", [Validators.required]],
			segmentType: ["", [Validators.required]],
			organism: ["", []],
			convention: ["", []],
			monthlyPay: [0, [Validators.required, Validators.min(1)]],
			othersMonthlyIncome: [0, [Validators.required, Validators.min(0)]],
			monthlyPaymentImmoCam: [0, [Validators.required, Validators.min(0)]],
			monthlyPaymentImmoConf: [0, [Validators.required, Validators.min(0)]],
			monthlyPaymentConsoCam: [0, [Validators.required, Validators.min(0)]],
			monthlyPaymentConsoConf: [0, [Validators.required, Validators.min(0)]],

			financielObject: ["ALG", []],
			prelevementType: ["", []],

			goodAmount: [0, [Validators.required, Validators.min(0)]],
			contributionAmount: [0, [Validators.min(0)]],
			othersFees: [0, [Validators.min(0)]],
			duration: [0, [Validators.required, Validators.min(0)]],
			deferredPeriod: [0, [Validators.min(0)]],
			naturePoints: ["", []],
			points: [0, [Validators.min(0)]],
			flagInssurance: ["N", []],

			simulationId: ["", []],
			personId: ["", []],
			refRequest: ["", []],
			rateType: ["", []],
			accountNumber: ["", []],
			monthlyPaymentImmo: [0, []],
			monthlyPaymentConso: [0, []],
			goodLocation: ["", []],
			natureFinancial: ["", []],
			clientType: ["", []],
			pas: ["", []],
			milieuBien: ["", []],
			typeAchat: ["", []],
			first: ["", []],
			userEntityId: ["", []],
			creditType: ["", []],

            flagDerogation: ["N", []],
            derogationValeur: ["N", []],
		});

		this.state = this.route.snapshot.data.state;
		this.initialiserSimulationDTO(this.route.snapshot.data.resolvedParam.result);

		this.reloadListItems();

		this.commonService.getParams().subscribe((data) => (this.params = data["result"]));

        this.getExemptionList();

        this.tableFormDerogation= this.fb.group({
            users: this.fb.array([])
        });
	}

    isDerogationChecked(): boolean{
        return this.simulationForm.get('flagDerogation').value==='O';
    }
	__setClientFieldStatus(status: "enable" | "disable") {
		this.simulationForm.get("clientName")[status]();
		this.simulationForm.get("birthDate")[status]();
		this.simulationForm.get("phoneNumber")[status]();
		this.simulationForm.get("email")[status]();
		this.simulationForm.get("seniority")[status]();
		this.simulationForm.get("activityStatus")[status]();
		this.simulationForm.get("segment")[status]();
		this.simulationForm.get("segmentType")[status]();
		this.simulationForm.get("monthlyPay")[status]();
		this.simulationForm.get("objectCredit")[status]();
		this.simulationForm.get("organism")[status]();
		this.simulationForm.get("convention")[status]();
		this.simulationForm.get("othersMonthlyIncome")[status]();
		this.simulationForm.get("monthlyPaymentImmoCam")[status]();
		this.simulationForm.get("monthlyPaymentImmoConf")[status]();
		this.simulationForm.get("monthlyPaymentConsoCam")[status]();
		this.simulationForm.get("monthlyPaymentConsoConf")[status]();
	}

	__setExtraFieldStatus(status: "enable" | "disable") {
		this.simulationForm.get("seniority")[status]();
		this.simulationForm.get("activityStatus")[status]();
		this.simulationForm.get("segment")[status]();
		this.simulationForm.get("segmentType")[status]();
		this.simulationForm.get("monthlyPay")[status]();
		this.simulationForm.get("objectCredit")[status]();
		this.simulationForm.get("organism")[status]();
		this.simulationForm.get("convention")[status]();
		this.simulationForm.get("othersMonthlyIncome")[status]();
		this.simulationForm.get("monthlyPaymentImmoCam")[status]();
		this.simulationForm.get("monthlyPaymentImmoConf")[status]();
		this.simulationForm.get("monthlyPaymentConsoCam")[status]();
		this.simulationForm.get("monthlyPaymentConsoConf")[status]();
	}

	/**
	 * you should call this function everytime simulationList changes
	 * because other parameters are unlikely to change like isClient and state
	 */
	updateFormFieldVisibility() {
		this.__setExtraFieldStatus("disable");
		this.__setClientFieldStatus("disable");
		switch (this.state) {
			case ComponentState.Add:
			// fallthrough
			case ComponentState.Update:
				if (this.simulationList.length) break;
				this.__setExtraFieldStatus("enable");
				if (!this.isClient) this.__setClientFieldStatus("enable");
				break;
		}
	}

	getValue() {
			return (
				this.getLibelle(this.simulationForm.get("goodLocation").value, "goodLocations") +
				"/" +
				this.getLibelle(
					this.simulationForm.get("natureFinancial").value,
					"financialNatures",
				)
			);
	}

	setIsClient(result: any) {
		if ((result?.clientType as string)?.toUpperCase() === "C") {
			this.isClient = true;
		}
	}

	getGoodAmountMax() {
		let max = this.params.find((p) => p?.code == "MAXNOMI")?.numericValue;
		this.simulationForm.get("goodAmount").setValidators(Validators.max(max))
		this.touch("goodAmount")
		return max;
	}

	getContributionAmountMax() {
		let max = (
			(this.params.find((p) => p?.code == "APPMAX")?.numericValue *
				this.simulationForm.get("goodAmount").value) /
			100
		);
		this.simulationForm.get("contributionAmount").setValidators(Validators.max(max))
		this.touch("contributionAmount")
		return max;
	}

	getDurationMax() {
		let max = this.params.find((p) => p?.code == "DURCRED")?.numericValue;
		this.simulationForm.get("duration").setValidators(Validators.max(max))
		this.touch("duration")
		return max;
	}

	getDeferredPeriodMax() {
		let max = this.params.find((p) => p?.code == "DIFCRED")?.numericValue;
		this.simulationForm.get("deferredPeriod").setValidators(Validators.max(max))
		this.touch("deferredPeriod")
		return max;
	}

	getFeesMax() {
		let max = (
			(this.params.find((p) => p?.code == "FRANEX")?.numericValue *
				this.simulationForm.get("goodAmount").value) /
			100
		);
		this.simulationForm.get("othersFees").setValidators(Validators.max(max))
		this.touch("othersFees")
		return max;
	}

	getPointMax() {
		let max = this.bonuses.find(bonus => bonus.codeNaturePoints == this.simulationForm.get("naturePoints").value)?.points || 0
		this.simulationForm.get("points").setValidators(Validators.max(max))
		this.touch("points")
		return max;
	}

	getNaturePointsName(codeNaturePoints: string) {
		return this.bonuses.find(b => b.codeNaturePoints == codeNaturePoints)?.nameNaturePoints
	}

	// it doesn't work otherwise, maybe we should refactor this
	touch(controlName: string) {
		let control = this.simulationForm.get(controlName);
		control.setValue(control.value)
		control.updateValueAndValidity()
	}

	title = "Nouvelle Simulation";
	initialiserSimulationDTO(resolverServiceResult): void {
		this.setIsClient(resolverServiceResult);

		// this.disableFields();
		// this.enableFields();
		if (this.state ===  ComponentState.Add) {
			// cas de l'ajout
			this.getAddResolvedParams(resolverServiceResult);
			// this.initialiserMonthlyPayement();
		} else if (this.state === ComponentState.Update) {
			// cas de la modification

			this.title = "Modifier Simulation";
			this.getUpdateResolvedParams(resolverServiceResult);
		}

		this.updateFormFieldVisibility();
		this.fetchBonuses();
	}

	fetchBonuses() {
		// TODO: use this one in resolver & remove the condition inside
		this.commonService.getPersonBonus({ cin: this.simulationForm.get("cin").value }).subscribe(data => {
			let person: any;
			person = data?.result;

			this.bonuses = person?.bonus;
			this.simulationForm.get("personId").setValue(person?.personId);
			// this.bonuses = [
			// 	{
			// 	  "codeNaturePoints": "FD",
			// 	  "nameNaturePoints": "Fidélité",
			// 	  "points": 512,
			// 	  "idPerson": 10724302
			// 	},
			// 	{
			// 	  "codeNaturePoints": "PR",
			// 	  "nameNaturePoints": "Parinage",
			// 	  "points": 1024,
			// 	  "idPerson": 10724302
			// 	},
			// 	{
			// 	  "codeNaturePoints": "FU",
			// 	  "nameNaturePoints": "FUCK YOU",
			// 	  "points": 0,
			// 	  "idPerson": 10724302
			// 	}
			//   ]
		})
	}

	getAddResolvedParams(clientresult): void {
		this.cin = this.route.snapshot.queryParams.cin;
		this.milieuBien = this.route.snapshot.queryParams.milieuBien;
		this.typeAchat = this.route.snapshot.queryParams.typeAchat;

		this.simulationForm.get("creditType").setValue(1);
		if (clientresult) {
			this.simulationForm.get("cin").setValue(clientresult?.cin);
			this.simulationForm.get("clientName").setValue(clientresult?.clientName);
			clientresult?.birthDate &&
				this.simulationForm.get("birthDate").setValue(new Date(clientresult.birthDate));
			this.simulationForm.get("phoneNumber").setValue(clientresult?.phoneNumber);
			this.simulationForm.get("email").setValue(clientresult?.email);
			this.simulationForm.get("clientType").setValue(clientresult?.clientType);
			this.simulationForm.get("goodLocation").setValue(this.milieuBien);
			this.simulationForm.get("natureFinancial").setValue(this.typeAchat);
		}
	}

	getUpdateResolvedParams(resolverServiceResult): void {

		this.simulationForm.get("creditType").setValue(1);

        if (resolverServiceResult.derogation?.length>0){
            this.simulationForm.get('flagDerogation').setValue('O');
            this.dataSourceDerogationParamsSelected = resolverServiceResult.derogation;
        }else{
            this.simulationForm.get('flagDerogation').setValue('N');
        }

		this.simRO = resolverServiceResult.statusCode === 'SIM_CON';
		if (resolverServiceResult?.simulationId) {
			this.simulationForm.patchValue(resolverServiceResult);
			this.simulationForm
				.get("birthDate")
				.setValue(this.toDateString(resolverServiceResult.birthDate as Date));
			this.builListDataSource(resolverServiceResult?.products);
		}
	}

	private toDateString(date: Date): string {
		/*return (date.getFullYear().toString() + '-'
            + ("0" + (date.getMonth() + 1)).slice(-2) + '-'
            + ("0" + (date.getDate())).slice(-2))
            + 'T' + date.toTimeString().slice(0,5);/*
         */
		const arr = date.toString().split("T");
		if (arr.length > 0) {
			return arr[0];
		}
		return "";
	}
	builListDataSource(list): void {
		this.simulationResultList = list;
		this.simulationList = (list);
		this.updateFormFieldVisibility();
	}

	// loadStaticSims() {
	// 	this.simulationResultList = sims;
	// 	this.reafficherTableSimulation();
	// }
	autoTicks = false;
	showTicks = false;
	tickInterval = 1;
	montantBien = 0;
	getSliderTickInterval(): number | "auto" {
		if (this.showTicks) {
			return this.autoTicks ? "auto" : this.tickInterval;
		}

		return 0;
	}

	filterdOrg;
	reloadListItems() {
		/*
    this.simulationService.getListByPost('financielObjects',new selectModel())
        .subscribe(data=>this.finacialObjects=data['result']);
    */
		this.simulationService.getList("goodLocations").subscribe((data) => {
			this.goodLocations = data["result"];
		});
		this.simulationService.getList("statusActivity").subscribe((data) => {
			this.activityStatus = data["result"];
		});
		this.simulationService.getListByPost("conventions", new SelectModel()).subscribe((data) => {
			this.conventionsDB = data["result"];
			this.conventions = data["result"];
		});
		this.simulationService.getList("financielNatures").subscribe((data) => {
			this.financialNatures = data["result"];
		});

		this.simulationService.getListByPost("organisms", new SelectModel()).subscribe((data) => {
			this.organismsDB = data["result"];
			this.organisms = data["result"];
			this.filterdOrg = data["result"];
		});
		this.simulationService.getList("debtTypes").subscribe((data) => {
			this.prelevemntType = data["result"];
		});
		this.simulationService.getListByPost("segements", new SelectModel()).subscribe((data) => {
			this.segmentsDB = data["result"];
			this.segments = data["result"];
			this.filtredSegment = data["result"];
		});

		this.simulationService.getList("segmentsType").subscribe((data) => {
			this.familleSegments = data["result"];
		});

		this.simulationService
			.getListByPost("financielObjects", new SelectModel())
			.subscribe((data) => {
				this.finacialObjects = data["result"];
				this.finacialObjectsDB = data["result"];
			});
	}
	getFinacialObjects() {
		this.simulationService
			.getListByPost("financielObjects", new SelectModel())
			.subscribe((data) => (this.finacialObjects = data["result"]));
		// this.finacialObjects=[{code:'ATR',designation:'Achat terrain',parentCode:null,id:null,parentId:null},{code:'LOG',designation:'Achat terrain',parentCode:null,id:null,parentId:null}]
	}
	disabled = false;
	OnSimulate() {
		this.isSubmit = true;
		if (!this.simulationForm.valid) return;
		this.disabled = true;
		this.simulationForm
			.get("monthlyPaymentConso")
			.setValue(
				Number(this.simulationForm.get("monthlyPaymentConsoCam").value) +
					Number(this.simulationForm.get("monthlyPaymentConsoConf").value),
			);
		this.simulationForm
			.get("monthlyPaymentImmo")
			.setValue(
				Number(this.simulationForm.get("monthlyPaymentImmoCam").value) +
					Number(this.simulationForm.get("monthlyPaymentImmoConf").value),
			);
		this.simulationForm.get("rateType").setValue("TF");
		this.simulationForm.get("refRequest").setValue("wkf-" + uuidv4());

		this.searchSimulationByCriteria({
            derogations: (this.simulationForm.get('flagDerogation').value==='O')? this.dataSourceDerogationParamsSelected : null
            ,...this.simulationForm.getRawValue()});
	}

	simulationResultList: SimulationProduct[] = [];
	listSumulationResultat: SimulationProduct[] = [];

	simulationList = [];
	searchSimulationByCriteria(simulationDto: any) {
		this.simulationService.simulate(simulationDto).subscribe((data) => {
			if (data.message == null) {
				const refSimulation = data.refSimulation;
				const requestRef = data.refRequete;
				this.registerDemande = false;
				// console.warn(' simulation data ',data);
				data?.packList?.forEach((pack) => {
					const simulationResult: SimulationProduct = new SimulationProduct();
					simulationResult.goodNature = simulationDto.financielObject;
					simulationResult.pack = pack.libelle;
					simulationResult.deferredPeriod = simulationDto.deferredPeriod;
					// tslint:disable-next-line:max-line-length
					simulationResult.prelevementType = pack.typePrelevement;
                    simulationResult.tauxApplique = pack.tauxApplique;
					simulationResult.prelevementTypeResult = pack.typePrelevement; // ***
					simulationResult.prelevementTypeCriteria = pack.typePrelevement; // ****
					simulationResult.duration = simulationDto.duration;
					simulationResult.deferred = simulationDto.deferredPeriod;
					simulationResult.rate = pack.taux;
					simulationResult.tauxApplique= pack.tauxApplique;
					simulationResult.rateType = "TF";
					simulationResult.bonus = pack.tauxBonus;
					simulationResult.monthlyPayment = pack.echeance;
					simulationResult.monthlyPaymentInitial = pack.echeanceInitial;
					simulationResult.points = pack.points;
					simulationResult.codeNaturePoints = pack.codeNaturePoints;
					simulationResult.goodAmount = simulationDto.goodAmount;
					simulationResult.amountGranted = pack.montantFinanceReel;
					simulationResult.otherFees = simulationDto.othersFees;
					simulationResult.othersFees = simulationDto.othersFees;
					simulationResult.refSimulation = refSimulation;
					simulationResult.refPack = pack.refPackSimulation;
					simulationResult.refRequest = requestRef;
					simulationResult.financielObject = simulationDto.financielObject;
					simulationResult.contributionAmount = simulationDto.contributionAmount;
					simulationResult.insurance = pack.montantAssurance;
					simulationResult.flagPrincipal = pack.flagPrincipal;
					simulationResult.refPackPrincipal = pack.refPackPrincipal;
					// simulationResult.flagInssurance = pack.flagInssurance;
					this.simulationResultList.unshift(simulationResult);
				});

				this.reafficherTableSimulation();
			} else {
				// this.enableFields();
				this.snackbarService.openErrorSnackBar({ message: data.message });
			}
			//this.onReset();
		});
	}
	displayedSimulationColumns = [
		"goodNature",
		"pack",
		"prelevementType",
		"amountGranted",
		"contributionAmount",
		"goodAmount",
		"duration",
		"rate",
		"tauxApplique",
		//"bonus",
		"deferred",
		"monthlyPayment",
		"insurance",
		"otherFees",
		"action",
	];

	reafficherTableSimulation() {
		this.simulationList = (this.simulationResultList);
		this.updateFormFieldVisibility();
	}

    getExemptionList(): any {
        this.simulationService.getExemptionList({
            userEntityId: this.tokenServ.user.entityId,
            userId: this.tokenServ.user.identifiant,
            entityId: this.tokenServ.user.entityId,
        }).subscribe((data) => {
            //console.log(data.result)
            this.dataSourceDerogationParams = data.result;
            this.dataSourceDerogationParams.forEach((item)=>{
                item.checked=false;
            });
        });
    }

	onDeleteSimRow(element) {
		this.snackbarService
			.openConfirmSnackBar({ message: "Êtes-vous sûr de vouloir supprimer cette ligne" })
			.then((result) => {
				if (result.value) {
					this.simulationService
						.delete(
							{
								cin: this.simulationForm.get("cin").value,
								refPack: element.refPack,
								userEntityId: this.tokenServ.user.entityId,
								userId: this.tokenServ.user.identifiant,
								entityId: this.tokenServ.user.entityId,
								simulationId: element.simulationId,
							},
							/*cin: "L36630", refPack: "PCK-1646", userId: 21555, entityId: 435*/
						)
						.subscribe((data) => {
							// if the element is a parent make sure to remove the children too
							if (this.isParent(element)) {
								this.getChildren(element).forEach(child => {
									this.simulationResultList.splice(
										this.simulationResultList.indexOf(child),
										1,
									);
								})
							}
							this.simulationResultList.splice(
								this.simulationResultList.indexOf(element),
								1,
							);
							this.reafficherTableSimulation();
						});
				}
			});
	}
	getNatureBienLibelle(goodNature: any, financialObjectName: any): string {
		if (financialObjectName) {
			return financialObjectName;
		}
		//console.warn('goodNature : ' + goodNature);
		if (
			this.finacialObjectsDB &&
			this.finacialObjectsDB.filter((x) => x.code === goodNature).length > 0
		) {
			return this.finacialObjectsDB.filter((x) => x.code === goodNature)[0].designation;
		}
		return goodNature;
	}
	getNatureBienCode(goodNature: any, financialObjectName: any, financialObject: any): string {
		/*if (financialObject) return financialObject;
		return goodNature;*/

        if (financialObjectName)
            return financialObjectName;
        else
            return goodNature;
	}

	getLibelle(code: string, listname: string): string {
		if (this[`${listname}`]) {
			const filtredList = this[`${listname}`].filter((x) => x.code === code);
			return filtredList.length > 0 ? filtredList[0].designation : "";
		} else {
			return "";
		}
		// tslint:disable-next-line:max-line-length
		// return this[`${listname}`].filter(x => x.code === code).length > 0 ? this[`${listname}`].filter(x => x.code === code)[0].designation : '';
	}

	private typeToString(type: string) {
		switch (type) {
			case "S":
				return "Source";
			case "B":
				return "Bancaire";
			default:
				return type;
		}
	}

	getTypePrelev(prelevementTypeResult: any): string {
		return this.typeToString(
			this.prelevemntType?.filter(
				(x) => x.code.toUpperCase() === prelevementTypeResult.toUpperCase(),
			).length > 0
				? this.prelevemntType?.filter((x) => x.code === prelevementTypeResult)[0].code
				: "",
		);
	}
	getTypePrelevLibelle(prelevementTypeResult: any): string {
		return this.prelevemntType?.filter(
			(x) => x.code.toUpperCase() === prelevementTypeResult.toUpperCase(),
		).length > 0
			? this.prelevemntType?.filter((x) => x.code === prelevementTypeResult)[0].designation
			: "";
	}
	filtrerSegment($event: MatSelectChange): void {
		this.segmentsDB = this.segments.filter((x) => x.parentCode === $event.value);
		this.simulationForm.get("segment").setValue(null);
		this.simulationForm.get("organism").setValue(null);
		this.simulationForm.get("convention").setValue(null);
	}
	onChangeSegment($event: MatSelectChange) {
		//console.warn($event.value, this.organisms);
		this.organismsDB = this.organisms.filter((x) => x.parentId === $event.value);
		this.simulationForm.get("organism").setValue(null);
	}
	onChangeOrganisme($event: MatSelectChange) {
		//console.warn($event.value, this.organisms);
		this.conventionsDB = this.conventions.filter((x) => x.parentId === $event.value);
		this.simulationForm.get("convention").setValue(null);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	onSubmitSimulations(): void {
		const addSimulationModel = new Simulation();
		addSimulationModel.products = this.simulationResultList;
		/*******************************************************/
		addSimulationModel.cin = this.simulationForm.get("cin").value;
		addSimulationModel.accountNumber = this.simulationForm.get("accountNumber").value;
		addSimulationModel.activityStatus = this.simulationForm.get("activityStatus").value;
		addSimulationModel.birthDate = this.simulationForm.get("birthDate").value;
		addSimulationModel.clientName = this.simulationForm.get("clientName").value;
		addSimulationModel.clientType = this.simulationForm.get("clientType").value;
		addSimulationModel.convention = this.simulationForm.get("convention").value;
		addSimulationModel.creditType = 1; /*******/
		addSimulationModel.email = this.simulationForm.get("email").value;
		addSimulationModel.entityId = this.simulationForm.get("userEntityId").value;
		addSimulationModel.first = 0;
		addSimulationModel.goodLocation = this.simulationForm.get("goodLocation").value;
		addSimulationModel.monthlyPay = this.simulationForm.get("monthlyPay").value;
		addSimulationModel.organism = this.simulationForm.get("organism").value;
		///////////////////////////////
		// tslint:disable-next-line:max-line-length
		addSimulationModel.monthlyPaymentConsoConf =
			this.simulationForm.get("monthlyPaymentConsoConf").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentConsoConf").value;
		addSimulationModel.monthlyPaymentImmoCam =
			this.simulationForm.get("monthlyPaymentImmoCam").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentImmoCam").value;
		addSimulationModel.monthlyPaymentImmoConf =
			this.simulationForm.get("monthlyPaymentImmoConf").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentImmoConf").value;

		addSimulationModel.monthlyPaymentConso =
			this.simulationForm.get("monthlyPaymentConso").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentConso").value;
		addSimulationModel.monthlyPaymentImmo =
			this.simulationForm.get("monthlyPaymentImmo").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentImmo").value;

		addSimulationModel.monthlyPaymentConsoCam =
			this.simulationForm.get("monthlyPaymentConsoCam").value === null
				? 0
				: this.simulationForm.get("monthlyPaymentConsoCam").value;

		addSimulationModel.natureFinancial = this.simulationForm.get("natureFinancial").value;
		addSimulationModel.objectCredit = this.simulationForm.get("objectCredit").value;
		addSimulationModel.othersMonthlyIncome =
			this.simulationForm.get("othersMonthlyIncome").value;
		addSimulationModel.phoneNumber = this.simulationForm.get("phoneNumber").value;
		addSimulationModel.segment = this.simulationForm.get("segment").value;
		addSimulationModel.pas = this.simulationForm.get("pas").value;
		addSimulationModel.seniority = this.simulationForm.get("seniority").value;
		addSimulationModel.userEntityId = this.tokenServ.user.entityId;
		addSimulationModel.userId = this.tokenServ.user.identifiant;
		addSimulationModel.simulationId = this.simulationForm.get("simulationId").value;

        addSimulationModel.derogation = (this.simulationForm.get('flagDerogation').value==='O')? this.dataSourceDerogationParamsSelected : null;

		this.snackbarService.openConfirmSnackBar({ message: "Enregistrer" }).then((result) => {
			if (result.value) {
				this.simulationService.saveListSimulation(addSimulationModel).subscribe((data) => {
					this.simulationForm.get("simulationId").setValue(data.result);
					this.snackbarService.openSuccesSnackBar({ message: data.message });
					this.registerDemande = true;
				});
			}
		});
	}

	// onDemandCredit() {
	// 	this.demandeService.onDemandCredit(this.simulationForm.value);
	// }


	initialiserMonthlyPayement() {
		this.simulationForm.get("monthlyPaymentConso").setValue(0);
		this.simulationForm.get("monthlyPaymentConsoCam").setValue(0);
		this.simulationForm.get("monthlyPaymentConsoConf").setValue(0);

		this.simulationForm.get("monthlyPay").setValue(0);
		this.simulationForm.get("monthlyPaymentImmoCam").setValue(0);
		this.simulationForm.get("monthlyPaymentImmoConf").setValue(0);
		this.simulationForm.get("monthlyPaymentImmo").setValue(0);
		this.simulationForm.get("deferredPeriod").setValue(0);
		this.simulationForm.get("contributionAmount").setValue(0);
		this.simulationForm.get("othersMonthlyIncome").setValue(0);
		this.simulationForm.get("othersFees").setValue(0);
	}

	setSeniorityValidators() {
		const seniority = this.simulationForm.get("seniority");
		const status = this.simulationForm.get("activityStatus");
		if (status.value === "A") {
			seniority.setValidators([Validators.required, Validators.min(0), Validators.max(60)]);
		} else {
			seniority.setValidators([Validators.min(0), Validators.max(60)]);
		}
		seniority.updateValueAndValidity();
	}

	isParent(product: {flagPrincipal?: 'O' | 'N'}) {
		return product.flagPrincipal !== 'N';
	}

	getChildren(product: { flagPrincipal?: 'O' | 'N', refPackPrincipal?: string, refPack?: string }) {
		return this.simulationList.filter(sim => !this.isParent(sim) && sim.refPackPrincipal === product.refPack);
	}

	hasError(control: string) {
		return !!this.simulationForm.get(control).errors
	}

	goBack() {
		this.routingService.gotoSimulations()
	}

	showInsuranceInclusion: boolean = true;
    selDerogationValeur: any;

	conventionChanged(value: string) {
		this.showInsuranceInclusion = !Boolean(value);
		if (!this.showInsuranceInclusion)
			this.simulationForm.get("flagInssurance").setValue('N')
	}

    changeStatutcheckParam($event: MatCheckboxChange, element: DerogationParams) {
        //alert($event.checked + ':' + element.code);
        element.checked = $event.checked;

    }

    filterTypeDerogation($event: MatSelectChange): void {
        this.instTypeDerogation = this.dataSourceDerogationParams.find(o=>o.code === $event.value);
        console.log(this.instTypeDerogation)
    }

    AddTypederogation() {

        if(this.instTypeDerogation.code == undefined){
            return;
        }

        if (this.instTypeDerogation.flagsaisi==='O' && this.simulationForm.get('derogationValeur').value === null){
            this.snackbarService.openErrorSnackBar({ message: 'Valeur de "'+ this.instTypeDerogation.libelle + '" est obligatoire !' });
            return;
        }

        const currentItem = this.dataSourceDerogationParamsSelected.find(o=>o.derogation === this.instTypeDerogation.code);

        if (currentItem !== undefined){
            this.dataSourceDerogationParamsSelected.find(o=>o.derogation === this.instTypeDerogation.code).valeur =
                ((this.instTypeDerogation.flagsaisi==='O')?this.simulationForm.get('derogationValeur').value : null);
        }else{
            this.dataSourceDerogationParamsSelected.push(
                {valeur:((this.instTypeDerogation.flagsaisi==='O')?this.simulationForm.get('derogationValeur').value : null),
                    derogation:this.instTypeDerogation.code,
                    libelle: this.instTypeDerogation.libelle});
        }

        this.simulationForm.get('derogationValeur').setValue(null) ;
    }

    onDeleteTypeDerigation(item: DerogationParams) {
        this.dataSourceDerogationParamsSelected = this.dataSourceDerogationParamsSelected.filter(o=>o.derogation !== item.derogation);
    }
}
export class SelectModel {
	id: number;
	code: string;
	designation: string;
	parentId: number;
	parentCode: string;
}
