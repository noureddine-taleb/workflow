export class SimulationCriteria {
	cin?: string;
	startDate?: Date;
	endDate?: Date;
	entityId?: number;
	milieuBien: any;
	typeAchat: any;
	first: number;
	pas: number;
	userEntityId: number;
}

export class BonusPoint {
	codeNaturePoints: string;
	nameNaturePoints: string;
	points: number;
	idPerson: number;
};

export class Simulation {
	simulationId?: number;
	personId?: number;
	refRequest?: string;
	rateType?: string;
	cin?: string;
	accountNumber?: number;
	clientName?: string;
	phoneNumber?: string;
	email?: string;
	birthDate?: any;
	activityStatus: string;
	segment?: number;
	organism?: number;
	convention?: number;
	seniority?: number;
	monthlyPay?: number;
	monthlyPaymentImmoCam?: number;
	monthlyPaymentImmoConf?: number;
	monthlyPaymentImmo?: number;
	othersMonthlyIncome?: number;
	monthlyPaymentConsoCam?: number;
	monthlyPaymentConsoConf?: number;
	monthlyPaymentConso?: number;
	goodLocation?: string;
	financielObject?: string;
	goodAmount?: number;
	contributionAmount?: number;
	duration?: number;
	deferredPeriod?: number;
	prelevementType?: string;
	natureFinancial?: string;
	othersFees?: number;
	clientType?: string;
	segmentType?: string;
	pas?: number;
	milieuBien?: any;
	typeAchat?: any;
	first?: number;
	userEntityId?: number;
	objectCredit?: string;
	creditType?: number;

	accountId?: number;
	entityId?: number;
	requestId?: number;
	userId?: number;
	products: SimulationProduct[];

	bonus?: number;
	amountFinanced?: number;
	amountRequested?: number;
	entityName?: string;
	financialNatureName?: string;
	goodLocationName?: string;
	monthlyPayment?: number;
	productsNumber?: number;
	rate?: number;
	simulationDate?: string;
	simulationReference?: string;
	status?: string;
	statusColor?: string;
	statusName?: string;
	
	endDate?: string;
	refPack?: string;
	startDate?: string;

	details: string;
}

export class SimulationProduct {
	goodNature: string;
	pack: string;
	prelevementType: string;
	prelevementTypeCriteria: string;
	prelevementTypeResult: string;
	duration: number;
	deferred: number;
	rate: number;
	bonus: string;
	derogation: string;
	monthlyPayment: number;
	monthlyPaymentInitial :  number;
	amountOfGood: number;
	amountGranted: number;
	otherFees: number;
	othersFees: number;
	insurance: number;
	refSimulation: string;
	refPack: string;
	refRequest: string;
	rateType: string;
	financielObject: string;
	goodAmount: number;
	deferredPeriod: number;
	contributionAmount: number;
	financialObjectName: string;
	flagPrincipal: string;
	refPackPrincipal: string;
	points: number;
	codeNaturePoints: number;
	// flagInssurance: 'O' | 'N';
}
