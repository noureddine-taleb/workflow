import { Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { RoutingService } from "app/core/services/routing.service";
import { LayoutService } from "app/layout/services/layout.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { combineLatest } from "rxjs";
import { SimulationGtService } from "../services/simulation.gt.service";
import { SimulationService } from "../services/simulation.service";
import { SimulationCriteria } from "../simulation.types";

@Component({
	selector: "app-simulation-list",
	templateUrl: "./simulation-list.component.html",
	styleUrls: ["./simulation-list.component.scss"],
})
export class SimulationListComponent implements OnInit {
	constructor(
		private _simulationService: SimulationService,
		private routingService: RoutingService,
		private simulationService: SimulationGtService,
		private refGtToken: AuthService,
		private tokenServ: AuthService,
		private layoutService: LayoutService,
	) {}

	// eslint-disable-next-line @typescript-eslint/member-ordering
	clientForm = new FormGroup({
		cin: new FormControl(""),
		startDate: new FormControl(""),
		endDate: new FormControl(""),
		milieuBien: new FormControl(null),
		typeAchat: new FormControl(null),
		first: new FormControl(0),
		pas: new FormControl(10),
		userEntityId: new FormControl(1),
	});
	fnacialNature;
	milieuBien;
	filter: FilterSimulation = {
		cin: "",
		startDate: "",
		endDate: "",
		finacialNature: null,
		goodLocation: null,
	};
	ngOnInit(): void {
		this.layoutService.setTitle("Liste des simulations")
		this.reloadListItems();
		this.critere.pas = 10;
		this.critere.first = 0;
		this.initSimPage();
	}
	initSimPage() {
		const goodLocations$ = this.simulationService.getList("goodLocations");
		const financielNatures$ = this.simulationService.getList("financielNatures");

		combineLatest(goodLocations$, financielNatures$, (goodLocations, financielNatures) => ({
			goodLocations,
			financielNatures,
		})).subscribe((pair) => {
			this.milieuBien = pair.goodLocations["result"];
			this.critere.milieuBien = this.milieuBien[0].code;

			this.fnacialNature = pair.financielNatures["result"];
			this.critere.typeAchat = this.fnacialNature[0].code;

			this.loadSimulations();
		});
	}

	reloadListItems() {
		this.simulationService.getList("goodLocations").subscribe((data) => {
			this.milieuBien = data["result"];
			this.critere.milieuBien = this.milieuBien[0].code;
		});
		this.simulationService.getList("financielNatures").subscribe((data) => {
			this.fnacialNature = data["result"];
			this.critere.typeAchat = this.fnacialNature[0].code;
		});
	}

	// showDate = true;

	// adjustForSearch(): void {
	// 	this.showDate = true;
	// }

	// adjustForAdd(): void {
	// 	this.showDate = false;
	// }

	onAddSim(): void {
		this.routingService.gotoSimulationAdd(this.critere.cin, this.critere.milieuBien, this.critere.typeAchat);
	}

	onChangeCin($event: any): void {}

	// eslint-disable-next-line @typescript-eslint/member-ordering
	formFieldHelpers: string[] = [""];
	getFormFieldHelpersAsString(): string {
		return this.formFieldHelpers.join(" ");
	}

	////////////////////////////////////////////////////////////////////////////
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	dataSource: MatTableDataSource<any>;

	listSimulation;
	displayedColumns = [
		"simulationDate",
		"clientName",
		"cin",
		"birthDate",
		"phoneNumber",
		"clientType",
		"productsNumber",
		"goodLocationName",
		"financialNatureName",
		"status",
		"Actions",
	];

	getSimulations(): void {
		// Get the contact object
		const contact = {
			cin: "",
			userEntityId: 435,
			first: 0,
			pas: 30,
		};
		//console.warn(this.filter);
		//this.contactForm.getRawValue();

		// Update the contact on the server
		this._simulationService.getListSimulation(contact).subscribe((data) => {
			//console.warn('data ',data);
			this.listSimulation = data.filter((x) => !this.filter.cin || x.cin === this.filter.cin);
			this.dataSource = new MatTableDataSource(this.listSimulation);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
		});
	}

	ngAfterViewInit() {
		this.paginator.page.subscribe((event) => {
			this.critere.pas = event.pageSize;
			this.critere.first = event.pageIndex * event.pageSize;
			this.loadSimulations();
		});
	}

	//////web service call
	simulations: any[];
	critere: SimulationCriteria = new SimulationCriteria();
	loadSimulations() {
		this.simulationService
			.getListSimulation(this.critere)
			.subscribe((data) => {
				this.simulations = data.result.results;
				this.paginator.length = data.result.rowsNumber;
		});
	}

	onEdit(element) {
		this.routingService.gotoSimulationEdit(element.simulationId);
	}

	onDelete(element) {}
}
export interface FilterSimulation {
	cin?: string;
	startDate?: string;
	endDate?: string;
	finacialNature?: any;
	goodLocation?: any;
}
