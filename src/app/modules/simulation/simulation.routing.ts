import { Route } from "@angular/router";
import { ComponentState } from "app/core/models/ComponentState";
import { AddSimulationResolverService } from "./services/add-simulation-resolver.service";
import { EditSimulationResolverService } from "./services/edit-simulation-resolver.service";
import { SimulationFormComponent } from "./simulation-form/simulation-form.component";
import { SimulationListComponent } from "./simulation-list/simulation-list.component";

export const simulationRoutes: Route[] = [
	{
		path: "",
		component: SimulationListComponent,
	},
	{
		path: "edit/:id",
		component: SimulationFormComponent,
		data: {
			state: ComponentState.Update
		},
		resolve: { resolvedParam: EditSimulationResolverService },
	},
	{
		path: "add",
		component: SimulationFormComponent,
		data: {
			state: ComponentState.Add
		},
		resolve: { resolvedParam: AddSimulationResolverService },
	},
];
