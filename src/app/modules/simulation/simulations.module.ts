import { CommonModule, registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatRadioModule } from "@angular/material/radio";
import { MatSelectModule } from "@angular/material/select";
import { MatSliderModule } from "@angular/material/slider";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { ThousandSuffixModule } from "app/shared/pipes/thousand-suffix/thousand-suffix.module";
import { NgxCurrencyModule } from "ngx-currency";
import { SimulationFormComponent } from "./simulation-form/simulation-form.component";
import { SimulationListComponent } from "./simulation-list/simulation-list.component";
import { simulationRoutes } from "./simulation.routing";

registerLocaleData(localeFr);
registerLocaleData(localeEs);
@NgModule({
	declarations: [
		SimulationListComponent,
		SimulationFormComponent,
	],
	imports: [
		CommonModule, 
		RouterModule.forChild(simulationRoutes),
		MatPaginatorModule,
		MatRadioModule,
		MatIconModule,
		MatFormFieldModule,
		FormsModule,
		MatTableModule,
		MatSliderModule,
		MatExpansionModule,
		MatSelectModule,
		MatDatepickerModule,
		ReactiveFormsModule,
		ThousandSuffixModule,
		MatInputModule,
		MatMenuModule,
		MatButtonModule,
		NgxCurrencyModule,
	],
})
export class SimulationsModule {}
