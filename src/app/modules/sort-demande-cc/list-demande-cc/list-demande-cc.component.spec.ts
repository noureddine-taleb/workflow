import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ListDemandeCCComponent } from "./list-demande-cc.component";

describe("ListComponent", () => {
	let component: ListDemandeCCComponent;
	let fixture: ComponentFixture<ListDemandeCCComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ListDemandeCCComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ListDemandeCCComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
