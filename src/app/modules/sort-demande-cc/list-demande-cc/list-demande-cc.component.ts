import { LiveAnnouncer } from "@angular/cdk/a11y";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort, Sort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { LayoutService } from "app/layout/services/layout.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { SortDemandeCCService } from "../sort-demande-cc.service";

@Component({
	selector: "app-list-demande-cc",
	templateUrl: "./list-demande-cc.component.html",
	styleUrls: ["./list-demande-cc.component.scss"],
})
export class ListDemandeCCComponent implements OnInit {

	critere = new Critere();

	displayedColumns = ["numerodecompte", "tiers", "agence", "nom", "prenom", "referencepiece", 'reponse', "motifrejet", "Actions"];

	sortDemandeCCDataSource = new MatTableDataSource<any>([]);
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	constructor(
		private sortDemandeService: SortDemandeCCService,
		private snackBarService: SnackBarService,
		private _liveAnnouncer: LiveAnnouncer,
		private layoutService: LayoutService
	) { }

	ngOnInit(): void {
		this.layoutService.setTitle("Sort des avals")
		this.getAvalResponse()
	}

	ngAfterViewInit() {
		this.sortDemandeCCDataSource.paginator = this.paginator;
		this.sortDemandeCCDataSource.sort = this.sort;
	}

	announceSortChange(sortState: Sort) {
		if (sortState.direction) {
			this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
		} else {
			this._liveAnnouncer.announce('Sorting cleared');
		}
	}

	displayPagination() {
		if (this.displayedColumns && this.displayedColumns.length) {
			return false;
		} else {
			return true;
		}
	}

	onPreparePrint(item) {
		this.snackBarService.openConfirmSnackBar({ message: "all" })
			.then((result) => {
				if (result.value) {
					console.log(item)
				}
			})
	}

	onPrepareArchive(item) {
		this.snackBarService.openConfirmSnackBar({ message: "all" })
			.then((result) => {
				if (result.value) {
					console.log(item)
				}
			})
	}

	onRechercher() {
		console.log(this.critere)
		this.getAvalResponse()
	}

	onReset() {
		this.critere = new Critere()
	}

	getAvalResponse() {

		this.sortDemandeService.postMethode('/ccgQuery/getAvalResponse', this.critere).subscribe(
			(response) => {
				console.log(response)
				this.sortDemandeCCDataSource = new MatTableDataSource(response.result)
				response.result.forEach(item => {
					console.log(item.statut)
				});
				this.ngAfterViewInit()
				//this.critere = new Critere()
			}, (error) => {
			}, () => {
			}
		)
	}

	typeProduit = [];
	getConventionalProduct() {
		this.sortDemandeService.getMethode('/ccgQuery/getConventionalProduct').subscribe(
			(response) => {
				console.log(response)
				this.typeProduit = response.result;
			}, (error) => {
			}, () => {
			})
	}

}

export class Critere {
	nom: string = null
	cin: string = null
	requestId: number = null
	userEntityId: number = null
	userId: number = null
	entityId: number = null
	numerodecompte: string = null
	first: number = null
	pas: number = null
}
