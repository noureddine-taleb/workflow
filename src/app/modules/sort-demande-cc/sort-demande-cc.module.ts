import { CommonModule, registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { ListDemandeCCComponent } from "./list-demande-cc/list-demande-cc.component";
import { sortDemandeCCRoutes } from "./sort-demande-cc.routing";

registerLocaleData(localeFr);
registerLocaleData(localeEs);
@NgModule({
	declarations: [
		ListDemandeCCComponent
	],
	imports: 
	[
		CommonModule, 
		RouterModule.forChild(sortDemandeCCRoutes),
		MatMenuModule,
		MatButtonModule,
		MatIconModule,
		MatFormFieldModule,
		FormsModule,
		MatTableModule,
		MatPaginatorModule,
		MatInputModule,
	],
})
export class SortDemandeCCModule {}
