import { Route } from "@angular/router";
import { ListDemandeCCComponent } from "./list-demande-cc/list-demande-cc.component";

export const sortDemandeCCRoutes: Route[] = [
    {
        path: "",
        component: ListDemandeCCComponent,
    },
];
