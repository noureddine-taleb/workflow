import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from 'app/core/config/config.service';
import { AuthService } from 'app/modules/auth/auth.service';
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class SortDemandeCCService {
	constructor(private httpClient: HttpClient, private authService: AuthService) {}
	private wkfPath = ConfigService.settings.apiServer.restWKFUrl;
	getMethode(url): Observable<any> {
		//url doit commencer par '/'
		return this.httpClient.get(this.wkfPath + url);
	}

	postMethode(url, object): Observable<any> {
		//url doit commencer par '/'
		object["userId"] = this.authService.user.userId;
		object["userEntityId"] = this.authService.user.userEntityId;
		object["entityId"] = this.authService.user.entityId;
		return this.httpClient.post(this.wkfPath + url, object);
	}
}