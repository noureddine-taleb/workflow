import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../../../layout/services/snackbar.service';
import {AuthService} from '../../../auth/auth.service';
import {CommonService} from '../../../../core/services/common.service';
import {DemandeService} from '../../../demandes/services/demande.service';
import {SimulationGtService} from '../../../simulation/services/simulation.gt.service';
import {RoutingService} from '../../../../core/services/routing.service';
import {RequestType} from '../../../../shared/models/RequestType';
import {DemandeTasbiqFda} from '../../dto/demande-tasbiqFda';
import {PersonTasbiqFda} from '../../dto/person-tasbiqFda';
import {TasbiqFdaSiobLin} from '../../dto/tasbiq-fda-siob-lin';
import {TasbiqfdaService} from '../../services/tasbiqfda.service';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {TasbiqFdaAccount} from '../../dto/tasbiq-fda-account';


@Component({
  selector: 'app-add-demande-tasbiq-fda',
  templateUrl: './add-demande-tasbiq-fda.component.html',
  styleUrls: ['./add-demande-tasbiq-fda.component.scss']
})
export class AddDemandeTasbiqFdaComponent implements OnInit {
    demandeTasbiqFda: DemandeTasbiqFda = {};
    subscriptions = new Subscription();
    cin: string;
    tiers: PersonTasbiqFda[] = [];
    familleSegments = [];
    segments = [];
    filteredSegments = [];
    listProducts: TasbiqFdaSiobLin[] = [];
    listCompte: TasbiqFdaAccount[] = [];
    selectedListProducts: TasbiqFdaSiobLin[] = [];
    listLigneTasbiqFdaSIOB: TasbiqFdaSiobLin[] = [];

    displayedColumnsTasbiqFda = ['referencedecision','nomprenombeneficiaire', 'cin', 'montantdecision', 'ribagriculture', 'montantdelegation','ribdelegation', 'categorie', 'composant',  'selected'];
    displayedColumnsTasbiqFdaLib = ['Référence décision','Bénéficiaire','CIN ','Montant décision','RIB agriculture','Montant délégation','RIB Délégataire','Catégorie','Composant', ''];

    displayedColumnsAccount = ['accountname', 'accountnumber', 'agency', 'madedate','product', 'refnumber', 'tiers',  'selected'];
    displayedColumnsAccountLib = ['Intitulé compte','Numéro de copte','Agence ','Date naissance','produit','Ref.Pièce','Tier', ''];

    displayedColumns = ['produit', 'montant', 'duree', 'taux', 'differe', 'periodicite'/*,'mensualite','assurance'*/, 'action'];
    displayedColumnsLib = ['Produit', 'Montant', 'Durée', 'Taux', 'Différé', 'Périodicité'/*,'Mensualité','Assurance'*/];

    constructor(
        private dialog: MatDialog,
        private snackeBarService: SnackBarService,
        private authService: AuthService,
        private commonService: CommonService,
        private demandeService: DemandeService,
        private simulationService: SimulationGtService,
        private routingService: RoutingService,
        private tasbiqfdaService: TasbiqfdaService
    ) {
    }

    ngOnInit(): void {
    }

    retour(): void {
        this.routingService.gotoTasbiqFdaRequests();
    }

    getAccountByCin(): void {
        this.tasbiqfdaService.getAccountsByCin({cin : this.cin}).subscribe((data) => {
            this.listCompte = data.result;
            this.getAccount();
        });
    }

    getAccount(): void {
        this.getlistDecision(this.cin);
    }

    getlistDecision(cin: string): void {
        this.tasbiqfdaService.getlistDecisionTasbiqFdaSiob(cin).subscribe((data) => {
            console.log(data.result)
            this.listLigneTasbiqFdaSIOB = data.result;
        });
    }

    initPersons(): void {
        for (const person of this.tiers) {
            person.monthlyPaymentConsoCam || (person.monthlyPaymentConsoCam = 0);
            person.monthlyPaymentConsoConf || (person.monthlyPaymentConsoConf = 0);
            person.monthlyPaymentImmoCam || (person.monthlyPaymentImmoCam = 0);
            person.monthlyPaymentImmoConf || (person.monthlyPaymentImmoConf = 0);
            person.monthlyPay || (person.monthlyPay = 0);
            person.othersMonthlyIncome || (person.othersMonthlyIncome = 0);
        }
    }

    onCreateTasbiqFdaDemande(): void {
        this.selectedListProducts = this.listLigneTasbiqFdaSIOB.filter(o=> o.selected===true);

        if (this.listCompte.find(o=>o.selected===true) === undefined){
            this.snackeBarService.openErrorSnackBar({message: 'Pas de compte sélectionné!'});
            return;
        }

        if (!this.selectedListProducts?.length){
            this.snackeBarService.openErrorSnackBar({message: 'Pas décision Tasbiq FDA sélectionnée!'});
            return;
        }

        this.demandeTasbiqFda.accountId = this.listCompte.find(o=>o.selected===true).accountid;
        this.demandeTasbiqFda.products = this.selectedListProducts;
        this.demandeTasbiqFda.creditType = RequestType.TASBIQFDA;
        this.demandeTasbiqFda.persons = [
            {
                personId:this.listCompte.find(o=>o.selected===true).tiers,
                monthlyPaymentConso:0,
                monthlyPaymentImmo:0,
                monthlyPaymentConsoConf:0,
                monthlyPaymentImmoCam:0,
                monthlyPaymentImmoConf:0,
                monthlyPaymentConsoCam:0,
            }
            ];
        for (const person of this.tiers) {
            person.monthlyPaymentConso = person.monthlyPaymentConsoCam + person.monthlyPaymentConsoConf;
            person.monthlyPaymentImmo = person.monthlyPaymentImmoCam + person.monthlyPaymentImmoConf;
        }

        this.demandeService.createDemandeTasbiqFda(this.demandeTasbiqFda).subscribe((data) => {
            this.routingService.gotoTask(data['result']);
        });
    }

    onChange(ob: MatCheckboxChange, elem: TasbiqFdaSiobLin): void {
        elem.selected = ob.checked;
    }

    onChangeListAccountCheck(ob: MatCheckboxChange, elem: TasbiqFdaAccount): void {
        this.listCompte.forEach((accountSel)=>{
            //if(accountSel.accountid != elem.accountid){
            accountSel.selected = false;
            //}
        });
        elem.selected = ob.checked;
    }

}
