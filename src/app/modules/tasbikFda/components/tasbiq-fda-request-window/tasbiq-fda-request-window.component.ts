import { Component, OnInit } from '@angular/core';
import {TasbiqFdaSiobLin} from "../../dto/tasbiq-fda-siob-lin";
import {TasbiqfdaService} from "../../services/tasbiqfda.service";
import {DemandeService} from "../../../demandes/services/demande.service";

@Component({
  selector: 'app-tasbiq-fda-request-window',
  templateUrl: './tasbiq-fda-request-window.component.html',
  styleUrls: ['./tasbiq-fda-request-window.component.scss']
})
export class TasbiqFdaRequestWindowComponent implements OnInit {

    listLigneTasbiqFdaSIOB: TasbiqFdaSiobLin[] = [];
    displayedColumnsTasbiqFda = ['referencedecision','nomprenombeneficiaire', 'cin', 'montantdecision', 'ribagriculture',
                                 'montantdelegation','ribdelegation', 'categorie', 'composant',  'selected'];
    displayedColumnsTasbiqFdaLib = ['Référence décision','Bénéficiaire','CIN ','Montant décision','RIB agriculture','Montant délégation','RIB Délégataire','Catégorie','Composant', ''];

    affaireregulier: string='N';
    impaye3annees: string='N';
    beneficierprefifda: string='N';

    constructor(private tasbiqfdaService: TasbiqfdaService,
                private demandeService: DemandeService) { }

    ngOnInit(): void {
        this.getlistDecision(this.demandeService.request.requestId);
    }

    onChangeCondition(event,condition): void{
      this.tasbiqfdaService.updateConditions(
            {
                affaireregulier:this.affaireregulier,
                impaye3annees: this.impaye3annees,
                beneficierprefifda: this.beneficierprefifda,
                requestid: this.demandeService.request.requestId
            }).subscribe((data) => {
          this.getlistDecision(this.demandeService.request.requestId);
        });

    }

    getlistDecision(requestId: number): void {
        this.tasbiqfdaService.getlistDecisionTasbiqFdaByRequest(requestId).subscribe((data) => {
            this.listLigneTasbiqFdaSIOB = data.result;
            data.result.forEach((line: TasbiqFdaSiobLin)=>{
                if(line.affaireregulier==='O') {
                    this.affaireregulier = 'O';
                }

                if(line.impaye3annees==='O') {
                    this.impaye3annees = 'O';
                }

                if(line.beneficierprefifda==='O') {
                    this.beneficierprefifda = 'O';
                }
            })
        });
    }
}
