import {PersonTasbiqFda} from './person-tasbiqFda';
import {TasbiqFdaSiobLin} from './tasbiq-fda-siob-lin';

export interface DemandeTasbiqFda {
    accountId?: number;
    clientCategory?: number;
    creditType?: number;
    entityId?: number;
    newConstitution?: string;
    objectCredit?: string;
    products?: TasbiqFdaSiobLin[];
    persons?: PersonTasbiqFda[];
    requestId?: number;
    userEntityId?: number;
    userId?: number;
}
