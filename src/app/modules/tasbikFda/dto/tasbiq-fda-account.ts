export interface TasbiqFdaAccount {
    accountid: number;
    accountname: string;
    accountnumber: string;
    adress: string;
    agency: string;
    madedate: string;
    product: string;
    refnumber: string;
    tiers: number;
    selected: boolean;
}
