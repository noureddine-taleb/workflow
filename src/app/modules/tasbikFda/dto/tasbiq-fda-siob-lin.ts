export interface TasbiqFdaSiobLin {
    referencedecision?: string;
    nomprenombeneficiaire?: string;
    cin?: string;
    montantdecision?: string;
    montantbeneficiaire?: string;
    ribagriculture?: string;
    montantdelegation?: string;
    categorie?: string;
    composant?: string;
    selected?: boolean;
    duree?: string;
    ribdelegation?: string;

    affaireregulier?: string;
    impaye3annees?: string;
    beneficierprefifda?: string;
}
