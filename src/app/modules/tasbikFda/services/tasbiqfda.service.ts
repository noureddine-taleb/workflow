import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ConfigService} from '../../../core/config/config.service';
import {AuthService} from '../../auth/auth.service';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../../../core/models/ApiResponse';
import {TasbiqFdaAccount} from '../dto/tasbiq-fda-account';

@Injectable({
  providedIn: 'root'
})
export class TasbiqfdaService {

    private demandesPath = ConfigService.settings.apiServer.restWKFUrl;

    constructor(private authService: AuthService,
                private httpClient: HttpClient) { }

    getlistDecisionTasbiqFdaSiob(cinId: string): Observable<any> {
        return this.httpClient.post(this.demandesPath + '/creditRequestTasbiqFda/listDecision', {
            entityId: this.authService.user.entityId,
            userId: this.authService.user.identifiant,
            userEntityId: this.authService.user.entityId,
            cin: cinId
        });
    }

    getAccountsByCin(compteCriteria): Observable<any> {
        compteCriteria["userEntityId"] = this.authService.user.userEntityId;
        compteCriteria["userId"] = this.authService.user.userId;
        compteCriteria["entityId"] = this.authService.user.entityId;
        return this.httpClient.post<ApiResponse<TasbiqFdaAccount>>(`${this.demandesPath}` + "/creditRequestTasbiqFda/account", compteCriteria); //--top lent
    }

    getlistDecisionTasbiqFdaByRequest(requestid: number): Observable<any> {
        return this.httpClient.post(this.demandesPath + '/creditRequestTasbiqFda/listDecisionByRequest', {
            entityId: this.authService.user.entityId,
            userId: this.authService.user.identifiant,
            userEntityId: this.authService.user.entityId,
            requestid: requestid
        });
    }

    updateConditions(compteCriteria): Observable<any> {
        compteCriteria["userEntityId"] = this.authService.user.userEntityId;
        compteCriteria["userId"] = this.authService.user.userId;
        compteCriteria["entityId"] = this.authService.user.entityId;
        return this.httpClient.post<ApiResponse<TasbiqFdaAccount>>(`${this.demandesPath}` + "/creditRequestTasbiqFda/updateConditions", compteCriteria); //--top lent
    }
}
