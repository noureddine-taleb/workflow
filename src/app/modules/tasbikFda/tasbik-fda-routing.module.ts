import {Route} from '@angular/router';
import {ListDemandeComponent} from '../../shared/list-demande/list-demande.component';
import {RequestType} from '../../shared/models/RequestType';
import {AddDemandeTasbiqFdaComponent} from './components/add-demande-tasbiq-fda/add-demande-tasbiq-fda.component';

export const TasbiqFdaRoutes: Route[] = [
    {
        path: '',
        component: ListDemandeComponent,
        data: { title: 'Liste des demandes Tasbiq FDA', demandeType: RequestType.TASBIQFDA, addAllowed: true },
    },
    {
        path: 'add',
        component: AddDemandeTasbiqFdaComponent,
        data: { title: 'Ajouter Tasbiq FDA demande' },
    },
];
