import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TasbiqFdaRoutes} from './tasbik-fda-routing.module';
import {AddDemandeTasbiqFdaComponent} from './components/add-demande-tasbiq-fda/add-demande-tasbiq-fda.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTreeSelectInputModule} from 'mat-tree-select-input';
import {CardModule} from '../../shared/card';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {NgxCurrencyModule} from 'ngx-currency';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {AccountModule} from '../../shared/account/account.module';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { TasbiqFdaRequestWindowComponent } from './components/tasbiq-fda-request-window/tasbiq-fda-request-window.component';


@NgModule({
  declarations: [
      AddDemandeTasbiqFdaComponent,
      TasbiqFdaRequestWindowComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(TasbiqFdaRoutes),
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatTreeSelectInputModule,
        CardModule,
        MatExpansionModule,
        FormsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatSelectModule,
        NgxCurrencyModule,
        MatIconModule,
        MatTableModule,
        MatDatepickerModule,
        AccountModule,
        MatRadioModule,
        MatCheckboxModule,
    ],
    exports:[TasbiqFdaRequestWindowComponent]
})
export class TasbikFdaModule { }
