import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { tasksRoutes } from './tasks.routing';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTooltipModule,
    RouterModule.forChild(tasksRoutes)
  ]
})
export class TasksModule { }
