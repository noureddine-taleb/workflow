import { Route } from "@angular/router";
import { ListDemandeComponent } from "app/shared/list-demande/list-demande.component";

export const tasksRoutes: Route[] = [
	{
		path: "", 
		component: ListDemandeComponent,
		data: { title: "Lists des tâches", addAllowed: false, myTasks: true }
	},
]