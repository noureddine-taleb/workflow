import { Routes } from "@angular/router";
import { WelcomeComponent } from "./welcome.component";

export const welcomeRoutes: Routes = [{ path: "", component: WelcomeComponent }];
