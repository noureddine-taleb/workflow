import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { welcomeRoutes } from "./welcome-routing.module";
import { WelcomeComponent } from "./welcome.component";

@NgModule({
	declarations: [WelcomeComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(welcomeRoutes)
	],
})
export class WelcomeModule {}
