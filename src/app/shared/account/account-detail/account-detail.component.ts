import { Component, Input, OnInit } from '@angular/core';
import { CommonService } from 'app/core/services/common.service';
import { Account } from '../../models/Account';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {

  @Input() account: Account;

  constructor(
  ) { }

  ngOnInit(): void {
  }

}
