import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountFindShowComponent } from './account-find-show.component';

describe('AccountFindShowComponent', () => {
  let component: AccountFindShowComponent;
  let fixture: ComponentFixture<AccountFindShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountFindShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFindShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
