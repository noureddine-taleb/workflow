import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Account } from 'app/shared/models/Account';
import { AccountSearchComponent } from '../account-search/account-search.component';

@Component({
  selector: 'app-account-find-show',
  templateUrl: './account-find-show.component.html',
  styleUrls: ['./account-find-show.component.scss']
})
export class AccountFindShowComponent implements OnInit {

	@Output() onAccountFetch = new EventEmitter<Account>();
	account: Account;
	@ViewChild(AccountSearchComponent) accountSearch: AccountSearchComponent;

	constructor() { }

	ngOnInit(): void {
	}

	_onAccountFetch(account: Account) {
		this.account = account;
		this.onAccountFetch.emit(account);
	}

	reset() {
		this.account = null;
		this.accountSearch.reset();
	}
}
