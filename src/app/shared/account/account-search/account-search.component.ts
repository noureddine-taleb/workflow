import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'app/core/services/common.service';
import { Account } from 'app/shared/models/Account';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-account-search',
  templateUrl: './account-search.component.html',
  styleUrls: ['./account-search.component.scss']
})
export class AccountSearchComponent implements OnInit {
	@Output() onAccountFetch = new EventEmitter<Account>();
	accountForm: FormGroup;

	constructor(
		private commonService: CommonService,
		private fb: FormBuilder,
	) { }

	ngOnInit(): void {
		this.accountForm = this.fb.group({
			accountNumber: ["", [Validators.required]]
		})
	}

	getAccount() {
		if (!this.accountForm.valid)
			return;
		this.commonService.getAccountByNumCompte(this.accountForm.value).pipe(take(1)).subscribe((data) => {
			let account = data.result;
			this.dispatchAccount(account);
		}, _ => {
			this.dispatchAccount(null);
		});
	}

	dispatchAccount(account: Account| null) {
		if (!account)
			this.accountForm.get("accountNumber").setErrors({ notFound: true })
		this.onAccountFetch.emit(account)
	}

	reset() {
		this.accountForm.reset();
	}
}
