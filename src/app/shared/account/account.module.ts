import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { AccountSearchComponent } from './account-search/account-search.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { AccountFindShowComponent } from './account-find-show/account-find-show.component';
import { MatInputModule } from '@angular/material/input';



@NgModule({
  declarations: [
    AccountDetailComponent,
    AccountSearchComponent,
    AccountFindShowComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
  ],
  exports: [
    AccountDetailComponent,
    AccountSearchComponent,
    AccountFindShowComponent,
  ]
})
export class AccountModule { }
