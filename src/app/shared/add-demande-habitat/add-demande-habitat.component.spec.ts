import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AddDemandeHabitatComponent } from "./add-demande-habitat.component";

describe("AddDemandeComponent", () => {
	let component: AddDemandeHabitatComponent;
	let fixture: ComponentFixture<AddDemandeHabitatComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [AddDemandeHabitatComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddDemandeHabitatComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
