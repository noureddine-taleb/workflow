import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { Account } from "app/shared/models/Account";
import { Subscription } from "rxjs";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { SimulationGtService } from "../../modules/simulation/services/simulation.gt.service";

@Component({
	selector: "app-add-demande-habitat",
	templateUrl: "./add-demande-habitat.component.html",
	styleUrls: ["./add-demande-habitat.component.scss"],
})
export class AddDemandeHabitatComponent implements OnInit {
	demandeToCreate: CreditRequestDto = new CreditRequestDto();
	simulationList: any;
	subscriptions: Subscription = new Subscription();
	constructor(
		public dialogRef: MatDialogRef<AddDemandeHabitatComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private simService: SimulationGtService,
		private snackbarService: SnackBarService,
		private tokenService: AuthService,
		private demandeService: DemandeService,
	) {}

	ngOnInit(): void {}

	onValide() {
		this.snackbarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeToCreate.products = this.simulationList.filter((x) => x.checked);
				for(let product of this.demandeToCreate.products) {
					product.startDate = (this.demandeToCreate as any).startDate;
				}
				this.demandeToCreate.accountId = this.compte.accountId;
				this.demandeToCreate.userEntityId = this.tokenService.user.userEntityId;
				this.demandeToCreate.userId = this.tokenService.user.userId;
				this.demandeToCreate.natureFinancial = "AQU"; //this.simulationList.filter(x=>x.checked)[0]['financialObject']
				this.demandeToCreate.goodLocation = this.simulationList.filter((x) => x.checked)[0][
					"goodLocation"
				];
				let sub = this.demandeService.saveDemandeHabitat(this.demandeToCreate).subscribe(
					(data) => {
						this.snackbarService.openSuccesSnackBar({ message: data["message"] });
						this.dialogRef.close({ success: true, requestId: data["result"] });
					},
					(error) => {
						this.snackbarService.openErrorSnackBar({ message: error.error.message });
						this.dialogRef.close({ sucsess: false, requestId: -1 });
					},
				);
				this.subscriptions.add(sub);
			}
		});
	}

	compte: Account = new Account();

	onFetchAccount(account: Account) {
		this.compte = account;
		this.simService.getSimParNumCompte({accountNumber: account.accountNumber}).subscribe((data) => {
			this.simulationList = data["result"]["results"];
			this.simulationList.forEach((element) => {
				element["checked"] = false;
			});
		});
	}

	// eslint-disable-next-line max-len
	displayedSimulationColumns = [
		"cin",
		"nomPrenom",
		"goodNature",
		"pack",
		"prelevementType",
		"duration",
		"deferred",
		"rate",
		"monthlyPayment",
		"goodAmount",
		"contributionAmount",
		"otherFees",
		"amountGranted",
		"action",
	];
	getDisplayedColumn(): string[] {
		// eslint-disable-next-line max-len
		return [
			"cin",
			"nomPrenom",
			"goodNature",
			"pack",
			"prelevementType",
			"duration",
			"deferred",
			"rate",
			"monthlyPayment",
			"goodAmount",
			"contributionAmount",
			"otherFees",
			"amountGranted",
			,
			"action",
		];
	}

	onDeleteSimRow(element) {
		this.snackbarService.openConfirmSnackBar({ message: "supprimer" }).then((result) => {
			if (result.value) {
				this.simulationList.splice(this.simulationList.indexOf(element), 1);
			}
		});
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}
}

export class CreditRequestDto {
	accountId: number;
	creditType: number;
	goodLocation: string;
	monthlyPay: number;
	monthlyPaymentConso: number;
	monthlyPaymentConsoCam: number;
	monthlyPaymentConsoConf: number;
	monthlyPaymentImmo: number;
	monthlyPaymentImmoCam: number;
	monthlyPaymentImmoConf: number;
	natureFinancial: string;
	objectCredit: string;
	othersMonthlyIncome: number;
	products: Product[];
	requestId: number;
	userEntityId: number;
	userId: number;
}
export class Product {
	amountGranted: number;
	bonus: number;
	contributionAmount: number;
	deferredPeriod: number;
	duration: number;
	financialObjectName: string;
	financialProduct: number;
	financielObject: string;
	goodAmount: number;
	insurance: number;
	monthlyPayment: number;
	organism: number;
	othersFees: number;
	pack: string;
	prelevementTypeCriteria: string;
	prelevementTypeResult: string;
	rate: number;
	rateType: string;
	refPack: string;
	refRequest: string;
	startDate: Date;
}
