import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDemandeHabitatComponent } from './add-demande-habitat.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { AccountModule } from '../account/account.module';

@NgModule({
  declarations: [AddDemandeHabitatComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDatepickerModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    AccountModule,
  ],
  exports: [AddDemandeHabitatComponent]
})
export class AddDemandeHabitatModule { }
