import { expandCollapse } from "app/shared/animations/expand-collapse";
import {
	fadeIn,
	fadeInBottom,
	fadeInLeft,
	fadeInRight,
	fadeInTop,
	fadeOut,
	fadeOutBottom,
	fadeOutLeft,
	fadeOutRight,
	fadeOutTop
} from "app/shared/animations/fade";
import { shake } from "app/shared/animations/shake";
import {
	slideInBottom,
	slideInLeft,
	slideInRight,
	slideInTop,
	slideOutBottom,
	slideOutLeft,
	slideOutRight,
	slideOutTop
} from "app/shared/animations/slide";
import { zoomIn, zoomOut } from "app/shared/animations/zoom";

export const animations = [
	expandCollapse,
	fadeIn,
	fadeInTop,
	fadeInBottom,
	fadeInLeft,
	fadeInRight,
	fadeOut,
	fadeOutTop,
	fadeOutBottom,
	fadeOutLeft,
	fadeOutRight,
	shake,
	slideInTop,
	slideInBottom,
	slideInLeft,
	slideInRight,
	slideOutTop,
	slideOutBottom,
	slideOutLeft,
	slideOutRight,
	zoomIn,
	zoomOut,
];
