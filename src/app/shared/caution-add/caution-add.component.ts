import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DepositService } from "app/modules/demandes/services/deposit.service";
import { take } from "rxjs/operators";

@Component({
	selector: "app-caution-add",
	templateUrl: "./caution-add.component.html",
	styleUrls: ["./caution-add.component.scss"],
})
export class CautionAddComponent implements OnInit {
	cautionForm: FormGroup;
	requestId: number;

	constructor(
		private fb: FormBuilder,
		private depositService: DepositService,
		private snackBarService: SnackBarService,
		@Inject(MAT_DIALOG_DATA) private data: any,
		private dialogRef: MatDialogRef<CautionAddComponent>,
	) {}

	ngOnInit(): void {
		this.requestId = this.data["requestId"];
		this.cautionForm = this.fb.group({
            depositId: ["" ],
			firstName: ["", [Validators.required]],
			lastName: ["", [Validators.required]],
			adress: ["", [Validators.required]],
			reference: ["", [Validators.required]],
			birthDate: ["", [Validators.required]],
		});
        this.cautionForm.setValue({
            depositId : this.data['objet'].depositId,
            firstName 	: this.data['objet'].firstName,
            lastName	: this.data['objet'].lastName,
            adress      : this.data['objet'].adress,
            reference   : this.data['objet'].reference,
            birthDate   : this.data['objet'].birthDate,
        }) ;
	}

	addCaution() {
		if (!this.cautionForm.valid) return;

		if (!this.shouldSave()) {
			this.dialogRef.close(this.cautionForm.value);
			return;
		}

		this.depositService
			.createDeposit(this.cautionForm.value, this.requestId)
			.pipe(take(1))
			.subscribe((data) => {
				this.snackBarService.openSuccesSnackBar({ message: data["message"] });
                this.dialogRef.close(this.cautionForm.value);
			});
	}

	shouldSave() {
		if (this.data.save == true || this.data.save == undefined) return true;
		else return false;
	}
}
