import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CautionsComponent } from "./cautions.component";

describe("CautionsComponent", () => {
	let component: CautionsComponent;
	let fixture: ComponentFixture<CautionsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CautionsComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CautionsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
