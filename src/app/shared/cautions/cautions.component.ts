import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { DepositService } from "app/modules/demandes/services/deposit.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { CautionAddComponent } from "../caution-add/caution-add.component";

@Component({
	selector: "app-cautions",
	templateUrl: "./cautions.component.html",
	styleUrls: ["./cautions.component.scss"],
})
export class CautionsComponent implements OnInit {
	displayedColumns: string[] = [
		"firstName",
		"lastName",
		"reference",
		"birthDate",
		"adress",
		"action",
	];
	dataSource: any[] = [];
	request: LoanRequest;

	constructor(
		private depositService: DepositService,
		private snackBarService: SnackBarService,
		private dialog: MatDialog,
		private utilsService: UtilsService,
		public routingService: RoutingService,
		private demandeService: DemandeService
	) {}

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.fetchDeposits();
	}

	// TODO: you need to update the object in the backend
	setProductId(caution) {}

	fetchDeposits() {
		this.depositService.getDeposits(this.request.requestId).subscribe((data) => {
			this.dataSource = data["result"];
		});
	}

	deleteCaution(caution) {
		this.snackBarService
			.openConfirmSnackBar({ message: "Êtes-vous sûr de vouloir supprimer cette ligne" })
			.then((v) => {
				if (v.value) {
					this.depositService
						.deleteDeposit(caution, this.request.requestId)
						.subscribe((data) => {
                            this.fetchDeposits();
							this.snackBarService.openSuccesSnackBar({ message: data["message"] });
						});
				}
			});
	}

	addCaution() {
		const dialogRef = this.dialog.open(CautionAddComponent, {
			data: {
				requestId: this.request.requestId,
                objet: null,
			},
			width: "90vw",
		});

		dialogRef.afterClosed().subscribe((data) => {
			this.fetchDeposits();
		});
	}

    onUpdateCaution(element): void {
        const dialogRef = this.dialog.open(CautionAddComponent, {
            data: {
                requestId: this.request.requestId,
                objet: element,
            },
            width: "90vw",
        });

        dialogRef.afterClosed().subscribe((data) => {
            this.fetchDeposits();
        });
    }

	hasAddPriv() {
		return this.utilsService.hasPrivilege(this.utilsService.privilegeCodes.maj_caution_m);
	}
}
