import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { CautionAddModule } from '../caution-add/caution-add.module';
import { CautionsComponent } from './cautions.component';



@NgModule({
  declarations: [CautionsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    CautionAddModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [CautionsComponent]
})
export class CautionsModule { }
