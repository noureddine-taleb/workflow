import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CntHistoriqueComponent } from "./cnt-historique.component";

describe("CntHistoriqueComponent", () => {
	let component: CntHistoriqueComponent;
	let fixture: ComponentFixture<CntHistoriqueComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CntHistoriqueComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CntHistoriqueComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
