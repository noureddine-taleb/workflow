import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-cnt-historique",
	templateUrl: "./cnt-historique.component.html",
	styleUrls: ["./cnt-historique.component.scss"],
})
export class CntHistoriqueComponent implements OnInit {
	displayedColumns = ["userName", "eventName", "eventDate", "statusName"];
	displayedColumnsLielle = ["Utilisateur", "Evènement", "Date", "Statut"];
	/*
	 * [ { "reservationId": 6, "statusName": "Réservation initiée en front office", "eventName": "Nouvelle réservation (Front Office)", "eventDate": "2021-10-07T16:10:45.000+0000", "userName": "HADRAOUI Ilham" } ]
	 * */
	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private dialogRef: MatDialogRef<CntHistoriqueComponent>,
	) {}

	ngOnInit(): void {}
}
