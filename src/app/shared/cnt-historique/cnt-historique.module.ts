import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { CntHistoriqueComponent } from './cnt-historique.component';

@NgModule({
  declarations: [CntHistoriqueComponent],
  imports: [
    CommonModule,
    MatTableModule,
  ],
  exports: [CntHistoriqueComponent]
})
export class CntHistoriqueModule { }
