import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-demande-detail",
	templateUrl: "./demande-detail.component.html",
	styleUrls: ["./demande-detail.component.scss"],
})
export class DemandeDetailComponent implements OnInit {
	constructor(
		private dialogRef: MatDialogRef<DemandeDetailComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {}

	ngOnInit(): void {}
}
