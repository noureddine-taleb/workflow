import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DemandeInfoModule } from '../demande-info/demande-info.module';
import { DemandeProduitsModule } from '../demande-produits/demande-produits.module';
import { DemandeQuiModule } from '../demande-qui/demande-qui.module';
import { DemandeDetailComponent } from './demande-detail.component';



@NgModule({
  declarations: [DemandeDetailComponent],
  imports: [
    CommonModule,
    DemandeProduitsModule,
    DemandeQuiModule,
    DemandeInfoModule
  ],
  exports: [DemandeDetailComponent]
})
export class DemandeDetailModule { }
