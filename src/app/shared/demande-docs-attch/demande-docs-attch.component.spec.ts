import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DemandeDocsAttchComponent } from "./demande-docs-attch.component";

describe("DemandeDocsAttchComponent", () => {
	let component: DemandeDocsAttchComponent;
	let fixture: ComponentFixture<DemandeDocsAttchComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DemandeDocsAttchComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DemandeDocsAttchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
