import { Component, Inject, OnInit } from "@angular/core";
import { ThemePalette } from "@angular/material/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { GedService } from "../../core/services/ged.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { RequestDocument, RequestDocumentType } from "./demande-docs-attch.types";
import { ThousandSuffixPipe } from "../pipes/thousand-suffix/thousand-suffix.pipe";

@Component({
	selector: "app-demande-docs-attch",
	templateUrl: "./demande-docs-attch.component.html",
	styleUrls: ["./demande-docs-attch.component.scss"],
	providers: [
		ThousandSuffixPipe
	]
})
export class DemandeDocsAttchComponent implements OnInit {
	requestId;
	docId: number;

	constructor(
		public dialogRef: MatDialogRef<DemandeDocsAttchComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private gedService: GedService,
		private snackBarService: SnackBarService,
		private thousandSuffix: ThousandSuffixPipe
	) {
		this.requestId = this.data.demandeId;
	}

	ngOnInit(): void {
		this.docId = this.data.documentId;
		if (!this.docId)
			this.getListDocument();
	}
	getListDocument() {
		this.gedService.getDocType(this.requestId).subscribe((data) => {
			this.docs = data.result;
			// this.docsTree = result.map(doc => {
			// 	return {
			// 		name: doc.docTypeName,
			// 		value: doc.docTypeId,
			// 		children: doc.documents.map(doc => {
			// 			return {
			// 				name: doc.docName,
			// 				value: doc.docId,
			// 			}
			// 		}),
			// 	}
			// })
		});
	}

	docs: RequestDocumentType[] = [];
	task: Task = {
		name: "Identité",
		completed: false,
		color: "primary",
		subtasks: [{ name: "CIN", completed: false, color: "primary" }],
	};

	allComplete: boolean = false;

	updateAllComplete() {
		this.allComplete =
			this.task.subtasks != null && this.task.subtasks.every((t) => t.completed);
	}

	someComplete(): boolean {
		if (this.task.subtasks == null) {
			return false;
		}
		return this.task.subtasks.filter((t) => t.completed).length > 0 && !this.allComplete;
	}

	setAll(completed: boolean) {
		this.allComplete = completed;
		if (this.task.subtasks == null) {
			return;
		}
		this.task.subtasks.forEach((t) => (t.completed = completed));
	}

	imageToShow: any;
	selectedFile: File = null;
	docPath: string = "";
	onFileSelected(event) {
		this.selectedFile = <File>event.target.files[0];
		this.docPath = this.selectedFile.name;
		let reader = new FileReader();
		reader.addEventListener(
			"load",
			() => {
				this.imageToShow = reader.result;
			},
			false,
		);
		if (this.selectedFile) {
			reader.readAsDataURL(this.selectedFile);
		}
	}

	/******************************************************/
	private imageSrc: string = "";

	handleInputChange(e) {
		let file: File = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
		this.docPath = file.name;
		this.typedoc = `${file.name}(${this.thousandSuffix.transform(file.size, 2, true)})`
		var pattern = /image-*/;
		var reader = new FileReader();
		if (!file.type.match(pattern)) {
			console.warn("invalid format");
			//return;
		}

		reader.onload = this._handleReaderLoaded.bind(this);
		reader.readAsDataURL(file);
	}

	_handleReaderLoaded(e) {
		let reader = e.target;
		this.imageSrc = reader.result;
	}

	typedoc = "";
	extension = "";
	file_1: any;
	uploadFile() {
		let gedDto = new GedDto();
		gedDto.data = this.imageSrc.split("base64,")[1];

		gedDto.documents = [{ docId: this.docId }];

		gedDto.requestId = this.requestId;
		gedDto.type = this.typedoc;
		gedDto.fileName = this.docPath;
		gedDto.relationId = this.data.relationId

		let ext = this.docPath.split(".");

		this.extension = ext[ext.length - 1];
		gedDto.extension = this.extension;
		console.warn(gedDto);
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.gedService.uploadFile(gedDto).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
			}
		});

		//requestId,type,fileName,extc,data,[{docId}]
	}
}
export interface Task {
	name: string;
	completed: boolean;
	color: ThemePalette;
	subtasks?: Task[];
}

export class GedDto {
	data: string;
	documents: RequestDocument[];
	entityId: number;
	extension: string;
	fileName: string;
	gedCAMId: number;
	gedId: number;
	profil: number;
	requestId: number;
	type: string;
	userEntityId: number;
	userId: number;
	relationId: number;
}

// { "status": 200, "message": null, "result": 
// [{ "docTypeId": 12, "docTypeName": "Documents spécifiques", 
// 	"documents": [{ "docId": 135, "docName": "Attestation d'architecte (constructions ou aménagement)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 130, "docName": "Compromis de vente (acquisition d'un logement ou local commercial)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 132, "docName": "Devis et factures", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 137, "docName": "Autres", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 131, "docName": "Autorisation de construire, Plans approuvés et Plans BET", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 136, "docName": "Etat d'avancement des travaux avec la valeur des travaux engagés estimés par l'architecte (constructions ou aménagement)", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 133, "docName": "Bulletin de paie des 3 derniers mois", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 134, "docName": "Attestation de travail", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }, { "docId": 176, "docName": "Attestation de salaire", "docTypeId": 12, "docTypeName": "Documents spécifiques", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 20, "docTypeName": "Pièces d'identité", "documents": [{ "docId": 79, "docName": "carte professionnelle", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 69, "docName": "CIN", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 85, "docName": "Autres", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }, { "docId": 5, "docName": "Titre de séjour Valide ou pièce d'identité étrangère", "docTypeId": 20, "docTypeName": "Pièces d'identité", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "documents": [{ "docId": 8, "docName": "Attestation de travail", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 16, "docName": "Copie de la Taxe Professionnelle", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 10, "docName": "Attestation de salaire", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 13, "docName": "Relevés des 6 derniers mois de l'ancienne banque pour les nouveaux clients", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 18, "docName": "Contrat de bail en cas de revenus locatifs", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 11, "docName": "Attestation de pension", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 15, "docName": "Justificatifs de l'exercice pour son propore compte", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 17, "docName": "Avis d'imposition ou quittance de réglement ou récepissé d'imposition IR", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 9, "docName": "Etat d'engagement recent", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 113, "docName": "Tout autre document justifiant les autres revenus (contrat de location de terrains agricoles/biens immobiliers¿)", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }, { "docId": 12, "docName": "Attestation CNSS précisnant l'ancienté de la déclaration", "docTypeId": 2, "docTypeName": "Justificatifs du revenu", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 3, "docTypeName": "Dérogations", "documents": [{ "docId": 61, "docName": "Dérogation sur le taux", "docTypeId": 3, "docTypeName": "Dérogations", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 65, "docTypeName": "Documents OPC", "documents": [{ "docId": 156, "docName": "Autorisation de consultation CNT", "docTypeId": 65, "docTypeName": "Documents OPC", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 37, "docTypeName": "Garanties", "documents": [{ "docId": 127, "docName": "Certificats de propriété récents (Maximum 3 mois)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 129, "docName": "Situation patrimoniale détaillée dûment signée et actualisée par le client (Biens mobiliers et immobiliers)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 89, "docName": "Engagement Notaire", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }, { "docId": 128, "docName": "Evaluation récente des sûretés proposées (source de l'évaluation, date)", "docTypeId": 37, "docTypeName": "Garanties", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 49, "docTypeName": "Réalisation des garanties", "documents": [{ "docId": 142, "docName": "Autres", "docTypeId": 49, "docTypeName": "Réalisation des garanties", "gedDocumentId": null, "gedId": null }] }, { "docTypeId": 13, "docTypeName": "Demande client", "documents": [{ "docId": 39, "docName": "Demande de basculement signé par le client", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 31, "docName": "EVCC Basculement", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 155, "docName": "Autorisation de consultation CNT", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }, { "docId": 80, "docName": "Demande du client signée", "docTypeId": 13, "docTypeName": "Demande client", "gedDocumentId": null, "gedId": null }] }] }