import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatAccordion, MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatTreeSelectInputModule } from 'mat-tree-select-input';
import { DemandeDocsAttchComponent } from './demande-docs-attch.component';

@NgModule({
  declarations: [
    DemandeDocsAttchComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    FormsModule,
    MatExpansionModule,
  ],
  exports: [
    DemandeDocsAttchComponent
  ]
})
export class DemandeDocsAttchModule { }
