export class RequestDocumentType {
	docTypeId: number;
	docTypeName: string; 
	documents: RequestDocument[]
}

export class RequestDocument {
	docId: number;
	docName?: string;
	docTypeName?: string;
	gedDocumentId?: number;
	gedId?: number;
}
