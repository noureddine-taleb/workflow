import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DemandeDocsComponent } from "./demande-docs.component";

describe("DemandeDocsComponent", () => {
	let component: DemandeDocsComponent;
	let fixture: ComponentFixture<DemandeDocsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DemandeDocsComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DemandeDocsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
