import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { FileSaverService } from "ngx-filesaver";
import { FileService } from "../../core/services/file.service";
import { GedService } from "../../core/services/ged.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeDocsAttchComponent } from "../demande-docs-attch/demande-docs-attch.component";
@Component({
	selector: "app-demande-docs",
	templateUrl: "./demande-docs.component.html",
	styleUrls: ["./demande-docs.component.scss"],
})
export class DemandeDocsComponent implements OnInit {
	request: LoanRequest;
	constructor(
		public dialog: MatDialog,
		private gedService: GedService,
		private snackBarService: SnackBarService,
		private _FileSaverService: FileSaverService,
		private fileService: FileService,
		private demandeService: DemandeService
	) {}
	docs = [];

	ngOnInit(): void {
		this.request = this.demandeService.request;
		this.lisUploadedFiles();
	}

	lisUploadedFiles() {
		this.gedService.getUploadedFiles(this.request.requestId).subscribe((data) => {
			this.docs = data["result"];
		});
	}

	onUpload() {
		const dialogRef = this.dialog.open(DemandeDocsAttchComponent, {
			data: {
				demandeId: this.request.requestId,
			},
		});

		dialogRef.afterClosed().subscribe((data) => {
			this.lisUploadedFiles();
		});
	}
	//data:image/png;base64,
	onDowloadFile(folder: any) {
		this.gedService.dowloadFile(folder).subscribe((data) => {
			//  console.warn(data)
			const blob = this.fileService.convertBase64ToBlobData(data["result"]["data"]);
			this._FileSaverService.save(blob, data["result"]["fileName"]);
		});
	}

	onDeleteDoc(folder: any) {
		folder["requestId"] = this.request.requestId;
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.gedService.delete(folder).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
					this.lisUploadedFiles();
				});
			}
		});
	}
}
