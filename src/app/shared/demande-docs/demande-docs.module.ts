import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { DemandeDocsAttchModule } from '../demande-docs-attch/demande-docs-attch.module';
import { DemandeDocsComponent } from './demande-docs.component';



@NgModule({
  declarations: [
    DemandeDocsComponent
  ],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    DemandeDocsAttchModule,
    DemandeDocsAttchModule,
  ],
  exports: [
    DemandeDocsComponent
  ]
})
export class DemandeDocsModule { }
