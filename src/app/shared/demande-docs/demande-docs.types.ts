export class RequestDoc {
	gedId: number;
	gedCAMId: number;
	fileName: string;
	gedRef: string;
	extension: string
	data: string;
	creationDate: Date;
	documents: any[];
}
