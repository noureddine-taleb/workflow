import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestType } from "app/shared/models/RequestType";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { SejourHistoriqueDialogComponent } from "../sejour-historique-dialog/sejour-historique-dialog.component";

@Component({
	selector: "app-demande-info",
	templateUrl: "./demande-info.component.html",
	styleUrls: ["./demande-info.component.scss"],
})
export class DemandeInfoComponent implements OnInit {
	@Input() demandeDTO: LoanRequest;
	@Input() mode = "E";
	requestTypes = RequestType;

	constructor(
		private snackBarService: SnackBarService,
		public demandeService: DemandeService,
		private dialog: MatDialog,
		public utilsService: UtilsService,
		public routingService: RoutingService
	) {}

	ngOnInit(): void {}

	editObject = false;
	onEditObjet(): void {
		this.editObject = true;
	}

	onValidEditObjet(): void {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this.demandeService.updateDemande(this.demandeDTO).subscribe((data) => {
					this.snackBarService.openSuccesSnackBar({ message: data["message"] });
				});
			}
		});

		this.editObject = false;
	}

	onclickSejour() {
		const dialogRef = this.dialog.open(SejourHistoriqueDialogComponent, {
			data: { demande: this.demandeDTO.requestId },
			width: "90vw",
		});
		dialogRef.afterClosed().subscribe((data) => {});
	}
}
