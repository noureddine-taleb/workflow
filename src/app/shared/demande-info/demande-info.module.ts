import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { SejourHistoriqueDialogModule } from '../sejour-historique-dialog/sejour-historique-dialog.module';
import { DemandeInfoComponent } from './demande-info.component';



@NgModule({
  declarations: [DemandeInfoComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    SejourHistoriqueDialogModule,
  ],
  exports: [DemandeInfoComponent]
})
export class DemandeInfoModule { }
