import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DemandeProduitsComponent } from "./demande-produits.component";

describe("DemandeProduitsComponent", () => {
	let component: DemandeProduitsComponent;
	let fixture: ComponentFixture<DemandeProduitsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DemandeProduitsComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DemandeProduitsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
