import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UtilsService } from "app/core/services/utils.service";
import { Products } from "app/modules/demandes/products-simple/products-simple.component";

@Component({
	selector: "app-demande-produits",
	templateUrl: "./demande-produits.component.html",
	styleUrls: ["./demande-produits.component.scss"],
})
export class DemandeProduitsComponent implements OnInit {
	spans = [];

	@Input() products: Products[];
	@Input() requestId;

	tempRowId = null;
	tempRowCount = null;

	constructor(private dialog: MatDialog, public utilsService: UtilsService) {}
	dataSource: Products[];
	ngOnInit(): void {
		this.dataSource = this.products;
	}

	displayedColumns = [
		"financialObjectName",
		"financialProductName",
		"prelevementTypeCode",
		"duration",
		"differe",
		"rate",
		"bonus",
		"derogation",
		"monthlyPayment",
		"goodAmount",
		"insurance",
		"cntStatusName",
	];

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["products"]) {
			this.dataSource = this.products;
			//alert('change product');
		}
	}
}
