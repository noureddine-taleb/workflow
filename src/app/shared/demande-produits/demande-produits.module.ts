import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { DemandeProduitsComponent } from './demande-produits.component';



@NgModule({
  declarations: [DemandeProduitsComponent],
  imports: [
    CommonModule,
    MatTableModule
  ],
  exports: [DemandeProduitsComponent]
})
export class DemandeProduitsModule { }
