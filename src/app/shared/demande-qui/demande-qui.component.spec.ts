import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DemandeQuiComponent } from "./demande-qui.component";

describe("DemandeQuiComponent", () => {
	let component: DemandeQuiComponent;
	let fixture: ComponentFixture<DemandeQuiComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DemandeQuiComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DemandeQuiComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
