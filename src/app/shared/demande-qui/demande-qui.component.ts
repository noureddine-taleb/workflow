import { DatePipe } from "@angular/common";
import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RequestPerson } from "app/shared/models/RequestPerson";
import { RequestType } from "app/shared/models/RequestType";

@Component({
	selector: "app-demande-qui",
	templateUrl: "./demande-qui.component.html",
	styleUrls: ["./demande-qui.component.scss"],
})
export class DemandeQuiComponent implements OnInit, OnChanges {
	@Input() persons: RequestPerson[];
	@Input() demandeDTO: LoanRequest;
	requestTypes = RequestType;

	constructor(private datepipe: DatePipe) {}

	ngOnInit(): void {
		this.constructTiers();
	}
	tier: Tiers;
	constructTiers() {
		this.tier = new Tiers();
		this.persons?.forEach((item) => {
			this.tier.nomComplet += item.cin + " " + item.clientName + " , ";
			this.tier.adresse += item.adressName + "\n";
			this.tier.birthDays += this.datepipe.transform(item.birthDate, "dd/MM/yyyy") + " , ";
			this.tier.phone += (item.phoneNumber || "") + " , ";
			this.tier.categorie += " ";
			this.tier.secteurActivite += item.activityName + " , ";
			this.tier.rcNumber += (item.rcNumber || "") + ", ";
			this.tier.entityName += item.entityName + ", ";
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if ("persons" in changes) {
			this.persons = changes.persons.currentValue;

			this.constructTiers();
		}
	}
}

export class Tiers {
	constructor() {
		this.nomComplet = "";
		this.categorie = "";
		this.adresse = "";
		this.phone = "";
		this.counNumber = "";
		this.cin = "";
		this.secteurActivite = "";
		this.countName = "";
		this.birthDays = "";
		this.entityName = "";
		this.rcNumber = "";
	}
	counNumber: string;
	countName: string;
	nomComplet;
	birthDays;
	phone;
	categorie;
	secteurActivite;
	adresse;
	cin;
	entityName: string;
	rcNumber: string;
}

export class DemandeTier {
	requestId: number;
	identifiant: number;
	monthlyPay: number;
	monthlyPaymentImmo: number;
	monthlyPaymentImmoCam: number;
	monthlyPaymentImmoConf: number;
	othersMonthlyIncome: number;
	monthlyPaymentConso: number;
	monthlyPaymentConsoCam: number;
	monthlyPaymentConsoConf: number;
	personId: number;
	clientName: string;
	clientType: string;
	birthDate: Date;
	cin: string;
	phoneNumber: string;
	email: string;
	adressName: string;
	activityName: string;
	cbRequestId: number;
	cbRequestStatus: string;
	cbRequestStatusName: string;
	cbRequestStatusColor: string;
	cbRequestDate: string;
	cbRequestValidityDate: string;
	cinNature: string;
	lastName: string;
	firstName: string;
	entityName: string;
	rcNumber: string;
	genderCode: string;
	patente: string;
	taxIdentification: string;
	legalStatus: number;
	courtCode: string;
}
