import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { DemandeQuiComponent } from './demande-qui.component';



@NgModule({
  declarations: [DemandeQuiComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatIconModule
  ],
  exports: [DemandeQuiComponent]
})
export class DemandeQuiModule { }
