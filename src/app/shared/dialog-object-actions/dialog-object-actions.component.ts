import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { User } from "../models/User";

interface InputData {
	obj: any,
	action: {users: User[]},
}

@Component({
	selector: "app-dialog-object-actions",
	templateUrl: "./dialog-object-actions.component.html",
	styleUrls: ["./dialog-object-actions.component.scss"],
})
export class DialogObjectActionsComponent implements OnInit {
	constructor(
		private snackBarService: SnackBarService,
		private dialogRef: MatDialogRef<DialogObjectActionsComponent>,
		private demandeService: DemandeService,
		@Inject(MAT_DIALOG_DATA) public data: InputData,
	) {
		if (data?.action?.users.length == 1) {
			data["userActionId"] = data?.action?.users[0].userId;
		}
	}

	ngOnInit(): void {
	}

	onExecuter() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this._onExecute();
			}
		});
	}

	_onExecute() {
		this.demandeService._dispatchAction(this.data.obj, this.data.action as any, {userId: this.data["userActionId"]}).subscribe((data) => {
			this.snackBarService.openSuccesSnackBar({ message: data["message"] });
			this.dialogRef.close(true);
		});
	}
}
