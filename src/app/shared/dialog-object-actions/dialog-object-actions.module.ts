import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { DialogObjectActionsComponent } from "./dialog-object-actions.component";

@NgModule({
	declarations: [DialogObjectActionsComponent],
	imports: [
		CommonModule,
		FormsModule,
		MatFormFieldModule,
		MatSelectModule,
		MatDialogModule,
		MatButtonModule,
		MatInputModule,
	],
	exports: [DialogObjectActionsComponent]
})
export class DialogObjectActionsModule {}