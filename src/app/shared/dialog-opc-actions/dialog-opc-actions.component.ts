import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommonService } from "app/core/services/common.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { AuthService } from "app/modules/auth/auth.service";

@Component({
	selector: "app-dialog-opc-actions",
	templateUrl: "./dialog-opc-actions.component.html",
	styleUrls: ["./dialog-opc-actions.component.scss"],
})
export class DialogOpcActionsComponent implements OnInit {
	constructor(
		private snackBarService: SnackBarService,
		private dialogRef: MatDialogRef<DialogOpcActionsComponent>,
		private authService: AuthService,
		private commonService: CommonService,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		if (data?.action?.users.length == 1) {
			data["userId"] = data?.action?.users[0].userId;
		}
	}

	ngOnInit(): void {
	}

	onExecuter() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this._onExecute();
			}
		});
	}

	_onExecute() {
		let critere = {};
		critere["userId"] = this.authService.user.userId;
		critere["entityId"] = this.authService.user.entityId;
		critere["userEntityId"] = this.authService.user.userEntityId;

		critere["contractId"] = this.data.contractId;
		critere["creditRequest"] = this.data.requestId;
		critere["url"] = this.data.urlAction;
		critere["offerDate"] = this.data.offerDate;
		critere["acceptanceDate"] = this.data.acceptanceDate;
		critere["userActionId"] = this.data.userActionId;
		critere["note"] = this.data.note;
		critere["action"] = this.data.action?.actionCode;

		this.commonService.executeActionByUrl(critere).subscribe((data) => {
			this.snackBarService.openSuccesSnackBar({ message: data["message"] });
			this.dialogRef.close(true);
		});
	}
}
