import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSelectModule } from "@angular/material/select";
import { DialogOpcActionsComponent } from "./dialog-opc-actions.component";

@NgModule({
	declarations: [DialogOpcActionsComponent],
	imports: [
		CommonModule,
		FormsModule,
		MatFormFieldModule,
		MatSelectModule
	],
	exports: [DialogOpcActionsComponent]
})
export class DialogOpcActionsModule {}