import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DialogTableDetailComponent } from "./dialog-table-detail.component";

describe("DialogTableDetailComponent", () => {
	let component: DialogTableDetailComponent;
	let fixture: ComponentFixture<DialogTableDetailComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DialogTableDetailComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DialogTableDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
