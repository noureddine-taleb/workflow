import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-dialog-table-detail",
	templateUrl: "./dialog-table-detail.component.html",
	styleUrls: ["./dialog-table-detail.component.scss"],
})
export class DialogTableDetailComponent implements OnInit {
	dataSource;
	displayedColumns;
	displayedColumnsLielle;

	constructor(@Inject(MAT_DIALOG_DATA) public data) {
		this.dataSource = data.dataSource;
		this.displayedColumns = data.displayedColumns;
		this.displayedColumnsLielle = data.displayedColumnsLielle;
	}

	ngOnInit(): void {}
}
