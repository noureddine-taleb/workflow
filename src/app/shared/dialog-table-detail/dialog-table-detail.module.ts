import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { DialogTableDetailComponent } from './dialog-table-detail.component';



@NgModule({
  declarations: [DialogTableDetailComponent],
  imports: [
    CommonModule,
    MatTableModule,
  ],
  exports: [DialogTableDetailComponent]
})
export class DialogTableDetailModule { }
