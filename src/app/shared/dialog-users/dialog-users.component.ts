import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { CntService } from "app/modules/demandes/services/cnt.service";

@Component({
	selector: "app-dialog-users",
	templateUrl: "./dialog-users.component.html",
	styleUrls: ["./dialog-users.component.scss"],
})
export class DialogUsersComponent implements OnInit {
	constructor(
		private snackBarService: SnackBarService,
		private cntService: CntService,
		private dialogRef: MatDialogRef<DialogUsersComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		if (data?.action?.users.length == 1) {
			data["userId"] = data?.action?.users[0].userId;
		}
	}

	ngOnInit(): void {
	}

	onExecuter() {
		this.snackBarService.openConfirmSnackBar({ message: "all" }).then((result) => {
			if (result.value) {
				this._onExecute();
			}
		});
	}

	_onExecute() {
		let critere = {};
		critere["action"] = this.data.action.actionCode;
		critere["userActionId"] = this.data.userId;
		critere["note"] = this.data.note;
		critere["cntId"] = this.data.cntId;

		this.cntService.executeAction(critere).subscribe((data) => {
			this.snackBarService.openSuccesSnackBar({ message: data["message"] });
			this.dialogRef.close(true);
		});
	}
}
