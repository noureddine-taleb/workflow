import { NgModule } from "@angular/core";
import { ScrollbarDirective } from "app/shared/directives/scrollbar/scrollbar.directive";

@NgModule({
	declarations: [ScrollbarDirective],
	exports: [ScrollbarDirective],
})
export class ScrollbarModule {}
