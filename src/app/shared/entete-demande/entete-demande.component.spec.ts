import { ComponentFixture, TestBed } from "@angular/core/testing";
import { EnteteDemandeComponent } from "./entete-demande.component";

describe("EnteteDemandeComponent", () => {
	let component: EnteteDemandeComponent;
	let fixture: ComponentFixture<EnteteDemandeComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [EnteteDemandeComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(EnteteDemandeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
