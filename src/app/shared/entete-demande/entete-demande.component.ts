import { Component, Input, OnInit } from "@angular/core";
import { CommonService } from "app/core/services/common.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { Tiers } from "../demande-qui/demande-qui.component";
import { RequestPerson } from "../models/RequestPerson";

@Component({
	selector: "app-entete-demande",
	templateUrl: "./entete-demande.component.html",
	styleUrls: ["./entete-demande.component.scss"],
})
export class EnteteDemandeComponent implements OnInit {
	@Input() request: LoanRequest;
	@Input() persons: RequestPerson[];

	constructor(
		private commonService: CommonService,
	) {}

	ngOnInit(): void {}

	getPersonsTitle() {
		return this.commonService.getRequestTitle();
		// return "sdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf fsdf sdf sdf sdf sdf sdf s fsdf sdf sdfsdf f"
		// if (personsTitle)
		// 	personsTitle = personsTitle.slice(0, -2);
		// return personsTitle;
	}

    getPersonsTitleSimple(): string {
        return this.commonService.getRequestTitleSimple();
    }
}
