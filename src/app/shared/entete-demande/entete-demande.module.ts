import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EnteteDemandeComponent } from './entete-demande.component';



@NgModule({
  declarations: [
    EnteteDemandeComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    EnteteDemandeComponent
  ]
})
export class EnteteDemandeModule { }
