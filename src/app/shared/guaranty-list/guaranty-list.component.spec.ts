import { ComponentFixture, TestBed } from "@angular/core/testing";
import { GuarantyListComponent } from "./guaranty-list.component";

describe("GuarantyListComponent", () => {
	let component: GuarantyListComponent;
	let fixture: ComponentFixture<GuarantyListComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [GuarantyListComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(GuarantyListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
