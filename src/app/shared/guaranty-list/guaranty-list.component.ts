import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-guaranty-list",
	templateUrl: "./guaranty-list.component.html",
	styleUrls: ["./guaranty-list.component.scss"],
})
export class GuarantyListComponent implements OnInit {
	displayedColumns: string[] = [
		"couverage",
		"guarantyId",
		"amount",
		"guarantyName",
		"reference",
		"guarantor",
		"consistency",
		"valinitGood",
		"amountLastEvaluation",
		"description",
		"status",
	];

	displayedColumnsLielle: string[] = [
		"Couverture",
		"ID Sûreté",
		"Montant Sûreté",
		"Libellé Sûreté",
		"Référence Sûrete",
		"Garant",
		"Consistance",
		"Valeur Initiale",
		"Montant Derniere Evolution",
		"Description",
		"Statut",
	];

	constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

	ngOnInit(): void {}
}
