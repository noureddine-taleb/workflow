import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { GuarantyListComponent } from './guaranty-list.component';



@NgModule({
  declarations: [GuarantyListComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatTableModule
  ],
  exports: [GuarantyListComponent]
})
export class GuarantyListModule { }
