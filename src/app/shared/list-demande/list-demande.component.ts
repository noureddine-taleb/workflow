import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort, Sort } from "@angular/material/sort";
import { ActivatedRoute } from "@angular/router";
import { CommonService } from "app/core/services/common.service";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { LayoutService } from "app/layout/services/layout.service";
import { AuthService } from 'app/modules/auth/auth.service';
import { CommiteServices } from "app/modules/comite/services/commite.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { RequestType } from "app/shared/models/RequestType";
import { SimulationGtService } from "../../modules/simulation/services/simulation.gt.service";
import { AddDemandeHabitatComponent } from "../add-demande-habitat/add-demande-habitat.component";
import { SejourHistoriqueDialogComponent } from "../sejour-historique-dialog/sejour-historique-dialog.component";
import { RequestQuery } from "./list-demande.types";

@Component({
	selector: "app-list-demande",
	templateUrl: "./list-demande.component.html",
	styleUrls: ["./list-demande.component.scss"],
	encapsulation: ViewEncapsulation.None
})
export class ListDemandeComponent implements OnInit {
	demandeType: RequestType = null;
	addAllowed = true;
	title: string;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	entities = [];
	users = [];
	displayedColumns: string[] = [];
	defaultDisplayedColumns = [
		"entityName",
		"accountNumber",
		"accountName",
		"creditTypeName",
		"requestNumber",
		"requestAmount",
		"lastUserNamelastEntityName",
		"stayDuration",
		// "productsNumber",
		"statusColor",
		"Actions",
	];
	requestList: LoanRequest[] = []
	requestType = RequestType;
	mainSearchField = {
		field: 'cin',
		label: 'CIN',
	};

	constructor(
		private simulationService: SimulationGtService,
		private demandeService: DemandeService,
		private dialog: MatDialog,
		public utilsService: UtilsService,
		public communService: CommonService,
		private authService: AuthService,
		private commiteService: CommiteServices,
		private routingService: RoutingService,
		private layoutService: LayoutService,
		private route: ActivatedRoute,
	) {}

	ngOnInit(): void {
		this.getAgencies();
		this.getUsers();
		let data = this.route.snapshot.data;
		this.title = data.title;
		this.demandeType = data.demandeType || null;
		this.addAllowed = Boolean(data.addAllowed);
		this.layoutService.setTitle(this.title);
		// remove creditTypeName-lastUserNamelastEntityName if we are not in my tasks
		this.displayedColumns = this.defaultDisplayedColumns;
		if (!this.routingService.isInMyTasks()) {
			this.displayedColumns.splice(this.displayedColumns.indexOf('creditTypeName'), 1);
			this.displayedColumns.splice(this.displayedColumns.indexOf('lastUserNamelastEntityName'), 1);
		}

		if (this.routingService.isInMyTasks() || this.demandeType == RequestType.AGRI || this.demandeType == RequestType.HORS_AGRI) {
			this.mainSearchField.field = 'accountNumber';
			this.mainSearchField.label = 'Compte';
		}

		this.loadRequests();
	}

	sortRequests(sort: Sort) {
		if (!sort.direction || !sort.active) {
			return;
		}

		let asc = sort.direction === 'asc';
		switch(sort.active) {
			case 'lastUserNamelastEntityName':
				this.requestList = this.requestList.sort((a, b) => compare(a.lastEntityName + "/" + a.lastUserName, b.lastEntityName + "/" + b.lastUserName, asc))
				break;
			default:
				this.requestList = this.requestList.sort((a, b) => compare(a[sort.active], b[sort.active], asc))
		}
	}

	critere = new RequestQuery();
	loadTaches() {}

	ngAfterViewInit() {
		this.paginator.page.subscribe((event) => {
			this.critere.pas = event.pageSize;
			this.critere.first = event.pageIndex * event.pageSize;
			this.loadRequests();
		});
	}

	//////web service call
	loadRequests() {
		this.critere["creditType"] = this.demandeType;
		this.simulationService
		.getListDemande(this.critere)
		.subscribe((data) => {
			this.requestList = data.result.results;
			this.paginator.length = data.result.rowsNumber;
		});
	}

	getAgencies() {
		this.communService.getAgences().subscribe((data) => {
			this.entities = data["result"];
		});
	}

	getUsers() {
		this.commiteService.getUsers().subscribe((data) => {
			this.users = data["result"];
		});
	}

	onEdit(element) {
		this.demandeService.onDemandEdit(element);
	}

	onreset() {
		this.critere = new RequestQuery();
	}

	displayPagination() {
		if (this.displayedColumns && this.displayedColumns.length) {
			return false;
		} else {
			return true;
		}
	}

	onHistoriqueSejour(demande) {
		const dialogRef = this.dialog.open(SejourHistoriqueDialogComponent, {
			data: { demande: demande.requestId },
			width: "90vw",
		});
		dialogRef.afterClosed().subscribe((data) => {});
	}

	handleRequestAdd(): void {
		switch(this.demandeType) {
			case RequestType.HABITAT:
				this.onAddDemandeHabitat()
				break;
			default:
				this.routingService.gotoAdd(this.demandeType)
				break;
		}
	}

	isAddSupported(): boolean {
		return this.addAllowed && this.routingService.isAddSupported(this.demandeType);
	}

	onAddDemandeHabitat(): void {
		const dialogRef = this.dialog.open(AddDemandeHabitatComponent, {
			data: {
				demandeId: null,
			},
			width: "100vw",
			disableClose: true,
		});

		dialogRef.afterClosed().subscribe((data) => {
			if (data["success"]) {
				this.routingService.gotoTask(data["requestId"]);
			}
		});
	}
}

function compare(a: string | number, b: string | number, asc: boolean) {
	return (a < b ? -1: 1) * (asc ? 1: -1);
}