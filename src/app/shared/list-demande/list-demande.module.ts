import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SejourHistoriqueDialogModule } from '../sejour-historique-dialog/sejour-historique-dialog.module';
import { ListDemandeComponent } from './list-demande.component';



@NgModule({
  declarations: [ListDemandeComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    SejourHistoriqueDialogModule,
    MatTooltipModule,
  ],
  exports: [ListDemandeComponent]
})
export class ListDemandeModule { }
