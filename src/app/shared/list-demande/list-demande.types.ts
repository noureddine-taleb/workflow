export class RequestQuery {
	cin?: string;
	accountNumber?: string;
	phase?: any;
	compte?: any;
	produit?: any;
	pas?: number = 10;
	first?: number = 0;
	userEntityId: number;
	entityId: number;
	userId?: number;
}