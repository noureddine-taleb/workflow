export class Account {
	cin?: string;
	clientId?: number;
	entityId?: number;
	userEntityId?: number;
	personId?: number;
	accountId?: number;
	accountNumber?: string;
	
	accountName?: string;
	agencyId?: number;
	agencyName?: string;
	productId?: number;
	productName?: string;
	accountBalance?: number;
	sens?: string;
	openningDate?: Date;
}