import { User } from "./User";

export class Action {
	actionCode: string;
	actionName: string;
	urlAction: string;
	workflowId: number;
	users: User[]
}

export enum ActionModule {
	gauranty = "GAR",
	bordereau = "BOGR",
	attestation_conf = "CONF",
	attestation_dbl = "ATDEB",
	autorisation_liv = "ATLIV",
	opc = "CON",
	request = "CRED",
	cnt = "CNT",
	ccg = "CONV",
}