export class CreationType {
	parentCode: string;
	parentId: number;
	id: number;
	code: string;
	designation: string;
}