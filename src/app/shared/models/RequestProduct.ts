export class RequestProduct {
	bonus: number;
	cntStatusCode: string;
	cntStatusName: string;
	contributionAmount: number;
	deferredPeriod: number;
	derogation: number;
	duration: number;
	financialObjectCode: string;
	financialObjectName: string;
	financialProductId: number;
	financialProductName: string;
	goodAmount: number;
	insurance: number;
	monthlyPayment: number;
	prelevementTypeCode: string;
	prelevementTypeName: string;
	productId: number;
	rate: number;
	requestAdviceId: number;
	requestId: number;
	simulationFlag: string;
}
