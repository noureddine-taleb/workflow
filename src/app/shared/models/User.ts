export interface User {
	identifiant: number;
	userId: number;
	userName: string;
	userFunction: string;
	login: string;
	entityId?: number; /* == */ userEntityId?: number;
	entityName: string;
	fullName: any;
	profilName?: string;

	// used in action user
	profilId?: string;
	profilCode?: string;
}
