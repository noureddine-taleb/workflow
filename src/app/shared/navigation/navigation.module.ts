import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatTooltipModule } from "@angular/material/tooltip";
import { RouterModule } from "@angular/router";
import { NavigationComponent } from ".";
import { ScrollbarModule } from "../directives/scrollbar";
import { NavigationBasicItemComponent } from "./menu/components/basic/basic.component";
import { NavigationCollapsableItemComponent } from "./menu/components/collapsable/collapsable.component";

@NgModule({
	declarations: [
		NavigationBasicItemComponent,
		NavigationCollapsableItemComponent,
		NavigationComponent,
	],
	imports: [
		CommonModule,
		RouterModule,
		MatButtonModule,
		MatDividerModule,
		MatIconModule,
		MatMenuModule,
		MatTooltipModule,
		ScrollbarModule,
		FormsModule,
		ReactiveFormsModule
	],
	exports: [NavigationComponent],
})
export class NavigationModule {}
