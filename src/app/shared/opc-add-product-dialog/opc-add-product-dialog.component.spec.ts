import { ComponentFixture, TestBed } from "@angular/core/testing";
import { OpcAddProductDialogComponent } from "./opc-add-product-dialog.component";

describe("OpcAddProductDialogComponent", () => {
	let component: OpcAddProductDialogComponent;
	let fixture: ComponentFixture<OpcAddProductDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [OpcAddProductDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(OpcAddProductDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
