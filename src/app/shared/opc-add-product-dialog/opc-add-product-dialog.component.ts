import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-opc-add-product-dialog",
	templateUrl: "./opc-add-product-dialog.component.html",
	styleUrls: ["./opc-add-product-dialog.component.scss"],
})
export class OpcAddProductDialogComponent implements OnInit {
	listProduct;
	productArray;
	typeDm;
	constructor(
		public dialogRef: MatDialogRef<OpcAddProductDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		this.listProduct = data.products;
		this.typeDm = data.typeDemande;
	}

	ngOnInit(): void {}

	onValide() {
		this.dialogRef.close(this.listProduct.filter((x) => x["checked"]));
	}

	isProductTaken(product: any) {
		return product?.statusCode == 'SA';
	}
}
