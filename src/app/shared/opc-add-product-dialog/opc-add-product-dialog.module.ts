import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { OpcAddProductDialogComponent } from './opc-add-product-dialog.component';



@NgModule({
  declarations: [OpcAddProductDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCheckboxModule,
    FormsModule,
    MatDialogModule,
  ],
  exports: [OpcAddProductDialogComponent]
})
export class OpcAddProductDialogModule { }
