import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
	name: "thousandSuffix",
})
export class ThousandSuffixPipe implements PipeTransform {
	suffixes = ["", "K", "M", "G", "T", "P", "E"];

	getSuffixIndex(number: number) {
		let i = Math.log10(number) / 3;
		i = Math.floor(i)
		return [i, number / Math.pow(10, i * 3)];
	}

	getComputerSuffixIndex(number: number) {
		let i = Math.log2(number) / 10;
		if (i < 1)
			i = 1;
		i = Math.floor(i)
		return [i, number / Math.pow(2, i * 10)];
	}

	transform(value: number, decimalPoints?: number, computer: boolean = false, ...args: unknown[]): any {
		let index: number;
		let retval: number | string;

		if (!computer && value < 1000)
			return value;

		if (!computer) {
			[index, value] = this.getSuffixIndex(value);
		} else {
			[index, value] = this.getComputerSuffixIndex(value);
		}

		retval = value.toFixed(decimalPoints)
		return retval + this.suffixes[index];
	}
}
