import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ConfigService } from 'app/core/config/config.service';
import { AuthService } from 'app/modules/auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class ReportGeneratorService {
    private restApiReportGeneratorUrl = ConfigService.settings.apiServer.restReportUrl  + '/reportGenerator/';

    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) {}

    generateReport(dataReport: any): any {
        const payload = {
            ...dataReport,
            userId: this.authService.user.userId,
            user: this.authService.user.userId,
            userEntityId: this.authService.user.userEntityId,
            entityId: this.authService.user.entityId,
        };

        return this.http.post(`${this.restApiReportGeneratorUrl}` + 'generateReport', payload);
    }

    convertToPdf(base64String, fileName): any {
        //const source = `data:application/octet-stream;base64,${base64String}`;
        this.openBase64NewTab(base64String);
        //window.open('data:application/pdf;base64, ' + base64String);

        /*const link = document.createElement('a');
        link.href = source;
        link.download = `${fileName}.pdf`;
        link.click();*/
    }

    openBase64NewTab(base64Pdf: string): void {
        const blobT = this.base64toBlob(base64Pdf);
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blobT, 'pdfBase64.pdf');
        } else {
            const blobUrl = URL.createObjectURL(blobT);
            window.open(blobUrl);
        }
    }

    base64toBlob(base64Data: string): any {
        const sliceSize = 1024;
        const byteCharacters = atob(base64Data);
        const bytesLength = byteCharacters.length;
        const slicesCount = Math.ceil(bytesLength / sliceSize);
        const byteArrays = new Array(slicesCount);

        for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            const begin = sliceIndex * sliceSize;
            const end = Math.min(begin + sliceSize, bytesLength);

            const bytes = new Array(end - begin);
            for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: "application/pdf" });
    }
}
