import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RubriqueReportsComponent } from "./rubrique-reports.component";

describe("RubriqueReportsComponent", () => {
	let component: RubriqueReportsComponent;
	let fixture: ComponentFixture<RubriqueReportsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [RubriqueReportsComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(RubriqueReportsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
