import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: "app-rubrique-reports",
	templateUrl: "./rubrique-reports.component.html",
	styleUrls: ["./rubrique-reports.component.scss"],
})
export class RubriqueReportsComponent implements OnInit {
	constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

	ngOnInit(): void {}
}
