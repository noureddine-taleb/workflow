import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { RubriqueReportsComponent } from './rubrique-reports.component';



@NgModule({
  declarations: [RubriqueReportsComponent],
  imports: [
    CommonModule,
    MatDialogModule
  ],
  exports: [RubriqueReportsComponent]
})
export class RubriqueReportsModule { }
