import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { RoutingService } from "app/core/services/routing.service";
import { UtilsService } from "app/core/services/utils.service";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";
import { SectionService } from "app/modules/demandes/services/section.service";
import { LoanRequest } from "app/modules/demandes/models/LoanRequest";
import { RubriqueReportsComponent } from "../rubrique-reports/rubrique-reports.component";
import { PrivilegeCode } from "app/modules/demandes/models/PrivilegeCode";

@Component({
	selector: "app-rubrique",
	templateUrl: "./rubrique.component.html",
	styleUrls: ["./rubrique.component.scss"],
})
export class RubriqueComponent implements OnInit {
	rubriqueForm: FormGroup;
	dataSource = new MatTableDataSource([]);
	@ViewChild(MatPaginator)
	paginator: MatPaginator;
	displayedColumns: string[] = ["section", "amount", "amountConsumed", "ratio", "action"];
	displayedColumnsLielle: string[] = [
		"Rubrique",
		"Montant Total",
		"Montant Consomé",
		"%",
		"Action",
	];
	demande: LoanRequest;

	constructor(
		private fb: FormBuilder,
		private sectionService: SectionService,
		private snackBarService: SnackBarService,
		private dialod: MatDialog,
		private route: ActivatedRoute,
		private router: Router,
		public utilsService: UtilsService,
		public routingService: RoutingService,
		private demandeService: DemandeService,
	) {}

	ngOnInit(): void {
		this.rubriqueForm = this.fb.group({
			section: ["", [Validators.required]],
			amount: [0, [Validators.required]],
		});

		this.fetchDemande();
		this.fetchSections();
	}

	fetchSections() {
		this.sectionService.getAll(this.demande.requestId).subscribe((data: any) => {
			this.dataSource = new MatTableDataSource(data.result);
			this.dataSource.paginator = this.paginator;
		});
	}

	fetchDemande() {
		this.demande = this.demandeService.request;
	}

	submit() {
		if (!this.rubriqueForm.valid) {
			this.snackBarService.openErrorSnackBar({
				message: "il y a un probleme dans le formulaire",
			});
			return;
		}

		this.sectionService
			.create(this.rubriqueForm.value, this.demande.requestId)
			.subscribe((data: any) => {
				this.snackBarService.openSuccesSnackBar({ message: data.message });
				this.rubriqueForm.reset();
				this.reloadDemande();
				this.utilsService.loadRequestPrivs()
				this.fetchSections();
			});
	}

	reloadDemande() {
		this.routingService.reloadCurrentRoute();
	}

	showRepports(section: any) {
		this.dialod.open(RubriqueReportsComponent, { width: "80vw", data: section.reports });
	}

	deleteSection(section: any) {
		this.snackBarService
			.openConfirmSnackBar({ message: "Êtes-vous sûr de vouloir supprimer cette ligne" })
			.then(({ value }) => {
				if (value) {
					this.sectionService
						.delete(section, this.demande.requestId)
						.subscribe((data: any) => {
							this.snackBarService.openSuccesSnackBar({ message: data.message });
							this.fetchSections();
						});
				}
			});
	}

	hasPrivilege() {
		return this.utilsService.hasPrivilege(PrivilegeCode.maj_rubrique_a);
	}
}
