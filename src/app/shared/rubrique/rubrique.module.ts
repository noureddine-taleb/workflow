import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { NgxCurrencyModule } from 'ngx-currency';
import { RubriqueReportsModule } from '../rubrique-reports/rubrique-reports.module';
import { RubriqueComponent } from './rubrique.component';



@NgModule({
  declarations: [RubriqueComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    MatTableModule,
    MatPaginatorModule,
    NgxCurrencyModule,
    RubriqueReportsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
  exports: [RubriqueComponent]
})
export class RubriqueModule { }
