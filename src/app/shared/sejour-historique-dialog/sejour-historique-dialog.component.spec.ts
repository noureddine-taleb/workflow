import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SejourHistoriqueDialogComponent } from "./sejour-historique-dialog.component";

describe("SejourHistoriqueDialogComponent", () => {
	let component: SejourHistoriqueDialogComponent;
	let fixture: ComponentFixture<SejourHistoriqueDialogComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [SejourHistoriqueDialogComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(SejourHistoriqueDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
