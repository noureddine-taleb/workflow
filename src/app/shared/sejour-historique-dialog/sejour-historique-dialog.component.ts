import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SnackBarService } from "app/layout/services/snackbar.service";
import { DemandeService } from "app/modules/demandes/services/demande.service";

@Component({
	selector: "app-sejour-historique-dialog",
	templateUrl: "./sejour-historique-dialog.component.html",
	styleUrls: ["./sejour-historique-dialog.component.scss"],
})
export class SejourHistoriqueDialogComponent implements OnInit {
	displayedColumns = [
		"userName",
		"profilName",
		"entityName",
		"stayDuration",
		"startDate",
		"endDate",
	];
	displayedColumnsLib = ["Utilisateur", "Profil", "Entité", "Durée", "Date début", "Date fin"];
	listHystorique = [];
	constructor(
		public dialogRef: MatDialogRef<SejourHistoriqueDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private demandeService: DemandeService,
		private snackBarService: SnackBarService,
	) {}

	ngOnInit(): void {
		this.sejour();
	}

	sejour() {
		let critere = {};
		critere["requestId"] = this.data["demande"];
		this.demandeService.getSejour(critere).subscribe((data) => {
			this.listHystorique = data["result"];
		});
	}
}

export class SejourDto {
	stayId: number;
	userName: string;
	profilName: string;
	entityName: string;
	startDate: Date;
	endDate: Date;
	stayDuration: string;
}
