import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { SejourHistoriqueDialogComponent } from './sejour-historique-dialog.component';

@NgModule({
  declarations: [SejourHistoriqueDialogComponent],
  imports: [
    CommonModule,
    MatTableModule
  ],
  exports: [SejourHistoriqueDialogComponent]
})
export class SejourHistoriqueDialogModule { }
