import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimulationResultComponent } from './simulation-result/simulation-result.component';



@NgModule({
  declarations: [
    SimulationResultComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SimulationResultComponent
  ]
})
export class SimulationResultModule { }
