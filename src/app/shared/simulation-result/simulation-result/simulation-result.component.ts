import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-simulation-result',
  templateUrl: './simulation-result.component.html',
  styleUrls: ['./simulation-result.component.scss']
})
export class SimulationResultComponent implements OnInit {
  @Input() insurance: number;
  @Input() monthlyPayment: number;

  constructor() { }

  ngOnInit(): void {
  }

}
