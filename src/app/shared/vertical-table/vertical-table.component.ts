import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-vertical-table',
  templateUrl: './vertical-table.component.html',
  styleUrls: ['./vertical-table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VerticalTableComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {
	}
}
