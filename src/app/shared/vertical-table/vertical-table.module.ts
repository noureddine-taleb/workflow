import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerticalTableComponent } from './vertical-table.component';



@NgModule({
  declarations: [VerticalTableComponent],
  imports: [
    CommonModule
  ],
  exports: [
    VerticalTableComponent
  ]
})
export class VerticalTableModule { }
