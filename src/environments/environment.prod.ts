export const environment = {
	production: true,
	name: "prod",

	apiServer: {
		restWKFUrl: "http://10.99.99.220:9080/backwkf",
		restGTUrl: "http://10.99.99.220:9080/gt-evolution",
		restREFUrl: "http://10.99.99.220:9080/referentiel",
	},
};
